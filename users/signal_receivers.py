# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.dispatch import receiver
from django.contrib.auth import get_user_model
from django.core.exceptions import MultipleObjectsReturned

from djrill.signals import webhook_event
from post_office.models import Email, Log, STATUS

from users.signals import mailjet_webhook_event

User = get_user_model()


@receiver(webhook_event)
def handle_bounce_mandrill(sender, event_type, data, **kwargs):
    if event_type in ('hard_bounce', 'soft_bounce', 'reject', 'unsub', 'spam',):
        user_email = data['msg']['email']
        User.objects.filter(email=user_email).update(receive_notifications=False)
        uid = data['msg']['metadata'].get('uid', None)
        if uid:
            try:
                email = Email.objects.get(headers__contains=uid)
                if event_type in ('hard_bounce', 'soft_bounce', 'reject',):
                    email.status = STATUS.failed
                    email.save()
                Log.objects.create(email=email, status=STATUS.failed, message=u'Mandrill event with data: %s' % data)
            except (Email.DoesNotExist, MultipleObjectsReturned):
                pass


unsub_events = frozenset(('bounce', 'spam', 'blocked', 'unsub'))


@receiver(mailjet_webhook_event)
def handle_mailjet_events(sender, event_type, data, **kwargs):
    if event_type in unsub_events:
        user_email = data['email']
        User.objects.filter(email=user_email).update(receive_notifications=False)

        message_id = data['CustomID'] or data['MessageID']
        if message_id:
            try:
                email = Email.objects.get(headers__contains=message_id)
                email.status = STATUS.failed
                email.save()
                Log.objects.create(email=email, status=STATUS.failed, message=u'mailjet event with data: %s' % data)
            except (Email.DoesNotExist, MultipleObjectsReturned):
                pass
