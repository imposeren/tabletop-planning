# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from decimal import Decimal

from django.contrib import admin
from django.contrib import messages
from django.contrib.admin import helpers
from django.contrib.auth.admin import UserAdmin as _UserAdmin
from django.contrib.auth.forms import AdminPasswordChangeForm
from django.db.models import Sum, Value, F, When, Case, DecimalField
from django.db.models.functions import Coalesce
from django.template.response import TemplateResponse
from django.utils.translation import ugettext_lazy as _

from .forms import UserCreationForm, UserChangeForm
from .models import User


def reset_user_balance(modeladmin, request, queryset):
    if request.POST.get('post'):
        n = 0
        for user in queryset:
            user.reset_balance()
            n += 1
        modeladmin.message_user(
            request,
            _("Баланс %(count)d пользователей успешно сброшен.") % {"count": n, },
            messages.SUCCESS)
    else:
        context = {
            'queryset': queryset,
            'opts': modeladmin.model._meta,
            'action_checkbox_name': helpers.ACTION_CHECKBOX_NAME,
            'media': modeladmin.media,
        }
        context.update(modeladmin.admin_site.each_context(request))
        request.current_app = modeladmin.admin_site.name
        return TemplateResponse(
            request,
            'admin/users/confirm_balance_reset.html',
            context,
        )


reset_user_balance.short_description = _('Сбросить "проверенный баланс"')


class UserAdmin(_UserAdmin):
    # The forms to add and change user instances
    form = UserChangeForm
    add_form = UserCreationForm
    actions = [reset_user_balance]

    fieldsets = (
        (None, {'fields': ('email', 'password', 'receive_notifications')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'image', 'facebook_link')}),
        (
            _(u'Опыт и финансы'),
            {
                'fields': (
                    'player_level',
                    'cached_total_player_experience',
                    'cached_total_master_experience',
                    'show_balance',
                    'show_balance_verified',
                ),
            },
        ),
        (
            _('Permissions'),
            {
                'fields': (
                    'is_active',
                    'is_confirmed_master',
                    'has_key',
                    'is_staff',
                    'is_superuser',
                    'requested_to_be_master_datetime',
                    'groups',
                    'user_permissions'
                ),
            }
        ),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'first_name', 'password1', 'password2'),
        }),
    )
    readonly_fields = (
        'cached_total_player_experience', 'cached_total_master_experience',
        'player_level', 'get_unused_tickets_count', 'show_balance', 'show_balance_verified',
    )
    change_password_form = AdminPasswordChangeForm
    list_display = (
        'get_full_name', 'pk', 'email', 'player_level', 'first_name', 'last_name',
        'is_confirmed_master', 'requested_to_be_master_datetime', 'is_staff',
        'get_unused_tickets_count', 'show_balance', 'show_balance_verified',
    )
    list_filter = ('is_confirmed_master', 'requested_to_be_master_datetime', 'is_staff', 'is_superuser', 'is_active', 'groups')
    search_fields = ('email', 'first_name', 'last_name')
    filter_horizontal = ('groups', 'user_permissions',)

    ordering = ('balance_verified_agg', 'balance_agg', 'email', )

    def get_ordering(self, request):
        if '/users/user/' in request.path_info:
            return super(UserAdmin, self).get_ordering(request)
        return ()

    def get_queryset(self, request):
        return (
            super(UserAdmin, self)
            .get_queryset(request)
            .annotate(
                balance_agg=Coalesce(
                    Sum('balance_changes__amount'), Value(Decimal('0.000'))
                ),
                balance_verified_agg=Coalesce(
                    Sum(
                        Case(
                            When(
                                balance_changes__verified=True,
                                then=F('balance_changes__amount')
                            ),
                            default=Decimal('0.000'),
                            output_field=DecimalField(max_digits=7, decimal_places=2,)
                        )
                    ),
                    Value(Decimal('0.000'))
                )
            )
            .order_by()
            .distinct()
        )

    def show_balance(self, instance):
        return instance.balance_agg or 0
    show_balance.admin_order_field = 'balance_agg'
    show_balance.short_description = _("Весь баланс")

    def show_balance_verified(self, instance):
        return instance.balance_verified_agg or 0
    show_balance_verified.admin_order_field = 'balance_verified_agg'
    show_balance_verified.short_description = _("Проверенный баланс")

    def get_actions(self, request):
        actions = super(UserAdmin, self).get_actions(request)
        if not request.user.has_perm('accounting.add_balancechange'):
            if 'reset_user_balance' in actions:
                del actions['reset_user_balance']
        return actions


# Now register the new UserAdmin...
admin.site.register(User, UserAdmin)
