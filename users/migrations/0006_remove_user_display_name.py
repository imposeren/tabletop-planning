# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations  # noqa


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0005_auto_20140912_1742'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='user',
            name='display_name',
        ),
    ]
