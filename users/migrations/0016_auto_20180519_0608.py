# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0015_user_facebook_link'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='facebook_link',
            field=models.CharField(default='', max_length=300, verbose_name='Facebook profile link', blank=True),
        ),
    ]
