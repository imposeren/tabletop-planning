# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0010_auto_20150720_2230'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='user',
            options={'verbose_name': 'user', 'verbose_name_plural': 'users', 'permissions': (('manage_keys', 'Can manage availability of keys for user'),)},
        ),
        migrations.AddField(
            model_name='user',
            name='has_key',
            field=models.BooleanField(default=False, verbose_name='\u0423 \u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u044f \u0435\u0441\u0442\u044c \u043a\u043b\u044e\u0447 \u043e\u0442 \u043a\u043b\u0443\u0431\u0430'),
        ),
    ]
