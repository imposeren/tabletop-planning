# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import sorl.thumbnail.fields


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0003_auto_20140903_2157'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='image',
            field=sorl.thumbnail.fields.ImageField(default='', height_field='image_height', width_field='image_width', upload_to='user_images', blank=True, verbose_name='\u0444\u043e\u0442\u043e'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='user',
            name='image_height',
            field=models.PositiveIntegerField(default=0, null=True, editable=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='user',
            name='image_width',
            field=models.PositiveIntegerField(default=0, null=True, editable=False),
            preserve_default=True,
        ),
    ]
