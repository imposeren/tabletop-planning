# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0002_auto_20140901_0233'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='cached_finished_balance',
            field=models.DecimalField(default=0, verbose_name='\u041f\u043e\u0441\u043b\u0435\u0434\u043d\u0438\u0439 \u0440\u0430\u0441\u0441\u0447\u0438\u0442\u0430\u043d\u043d\u044b\u0439 "\u0437\u0430\u0432\u0435\u0440\u0448\u0435\u043d\u043d\u044b\u0439" \u0431\u0430\u043b\u0430\u043d\u0441', editable=False, max_digits=12, decimal_places=3),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='user',
            name='cached_unfinished_balance',
            field=models.DecimalField(default=0, verbose_name='\u041f\u043e\u0441\u043b\u0435\u0434\u043d\u0438\u0439 \u0440\u0430\u0441\u0441\u0447\u0438\u0442\u0430\u043d\u043d\u044b\u0439 "\u043d\u0435\u0437\u0430\u0432\u0435\u0440\u0448\u0435\u043d\u043d\u044b\u0439" \u0431\u0430\u043b\u0430\u043d\u0441', editable=False, max_digits=12, decimal_places=3),
            preserve_default=True,
        ),
    ]
