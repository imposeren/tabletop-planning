# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf import settings
from django.db import models, migrations
import django.utils.timezone


def create_groups(apps, schema_editor):
    Group = apps.get_model("auth", "Group")
    for group_name in settings.DEFAULT_GROUP_NAMES:
        if not Group.objects.filter(name=group_name).exists():
            Group.objects.create(name=group_name)


def create_service_users(apps, schema_editor):
    User = apps.get_model("users", "User")
    User.objects.create(
        email='playhard@localhost', first_name='PlayHard Club',
        is_service=True,
        is_active=False,
    )
    User.objects.create(
        email='withdraw@localhost', first_name='вывод денег',
        is_service=True,
        is_active=False,
    )
    User.objects.create(
        email='refill@localhost', first_name='ввод денег',
        is_service=True,
        is_active=False,
    )
    User.objects.create(
        email='expenses@localhost', first_name='Траты для клуба',
        is_service=True,
        is_active=False,
    )


UNSUB_TEXT_CONTENT = '\n\nЕсли вы больше не хотите получать такие уведомления, то откройте в браузере следующую ссылку:\n{% get_unsubscribe_link recipient %}'


def create_master_confirmation_template(apps, schema_editor):
    EmailTemplate = apps.get_model("post_office", "EmailTemplate")
    name = 'master-moderation-request'
    if not EmailTemplate.objects.filter(name=name).exists():
        EmailTemplate.objects.create(
            name=name,
            subject='Неоходимо подтвердить ведущего',
            content='{% load common_tags %}\nПожалуйста, подтвердите ведущего по ссылке: {{ user.get_admin_change_url|with_domain }}' + UNSUB_TEXT_CONTENT,
        )

    name = 'new-players-notification'
    if not EmailTemplate.objects.filter(name=name).exists():
        EmailTemplate.objects.create(
            name=name,
            subject='К вам присоединились новые игроки',
            html_content='''{% load common_tags %}
<p>Здравствуйте {{ user }}!</p>

<p>К вашим играм присоединились новые игроки:</p>
<ul>
{% for game in target_games %}
    <li><a href="{{ game.get_absolute_url|with_domain }}">{{ game }}</a>: {{ game.new_players.count }} новых участник(а).</li>
{% endfor %}
</ul>

<p>Если вы больше не хотите получать такие уведомления, то можете <a href="{% get_unsubscribe_link recipient %}">отписаться от уведомлений</a></p>
''',
            content='''{% load common_tags %}Здравствуйте {{ user }}!

К вашим играм присоединились новые игроки:
{% for game in target_games %}
    {{ game }} ({{ game.get_absolute_url|with_domain }}): {{ game.new_players.count }} новых участник(а).
{% endfor %}''' + UNSUB_TEXT_CONTENT,
        )


class Migration(migrations.Migration):

    replaces = []

    dependencies = [
        ('auth', '0001_initial'),
        ('post_office', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(default=django.utils.timezone.now, verbose_name='last login')),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('first_name', models.CharField(max_length=30, verbose_name='first name', blank=True)),
                ('last_name', models.CharField(max_length=30, verbose_name='last name', blank=True)),
                ('email', models.EmailField(unique=True, max_length=75, verbose_name='email address')),
                ('is_staff', models.BooleanField(default=False, help_text='Designates whether the user can log into this admin site.', verbose_name='staff status')),
                ('is_active', models.BooleanField(default=True, help_text='Designates whether this user should be treated as active. Unselect this instead of deleting accounts.', verbose_name='active')),
                ('date_joined', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date joined')),
                ('groups', models.ManyToManyField(to='auth.Group', verbose_name='groups', blank=True)),
                ('user_permissions', models.ManyToManyField(to='auth.Permission', verbose_name='user permissions', blank=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.RunPython(
            code=create_groups,
            reverse_code=None,
            atomic=True,
        ),
        migrations.AlterModelOptions(
            name='user',
            options={'verbose_name': 'user', 'verbose_name_plural': 'users'},
        ),
        migrations.AlterField(
            model_name='user',
            name='first_name',
            field=models.CharField(max_length=30, verbose_name='first name'),
        ),
        migrations.AddField(
            model_name='user',
            name='display_name',
            field=models.CharField(default='the old one', max_length=61, verbose_name='display name', blank=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='user',
            name='is_confirmed_master',
            field=models.BooleanField(default=False, verbose_name='\u041f\u043e\u0434\u0442\u0432\u0435\u0440\u0436\u0434\u0435\u043d \u043a\u0430\u043a \u0432\u0435\u0434\u0443\u0449\u0438\u0439'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='user',
            name='requested_to_be_master_datetime',
            field=models.DateTimeField(default=None, null=True, verbose_name='\u0434\u0430\u0442\u0430 \u0437\u0430\u043f\u0440\u043e\u0441\u0430 \u0441\u0442\u0430\u0442\u044c \u043c\u0430\u0441\u0442\u0435\u0440\u043e\u043c', blank=True),
            preserve_default=True,
        ),
        migrations.RunPython(
            code=create_master_confirmation_template,
            reverse_code=None,
            atomic=True,
        ),
        migrations.AddField(
            model_name='user',
            name='cached_balance',
            field=models.DecimalField(default=0, verbose_name='\u041f\u043e\u0441\u043b\u0435\u0434\u043d\u0438\u0439 \u0440\u0430\u0441\u0441\u0447\u0438\u0442\u0430\u043d\u043d\u044b\u0439 \u0431\u0430\u043b\u0430\u043d\u0441', editable=False, max_digits=12, decimal_places=3),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='user',
            name='cached_total_experience',
            field=models.PositiveIntegerField(default=0, verbose_name='\u041f\u043e\u0441\u043b\u0435\u0434\u043d\u0438\u0439 \u0440\u0430\u0441\u0441\u0447\u0438\u0442\u0430\u043d\u043d\u044b\u0439 \u043e\u043f\u044b\u0442', editable=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='user',
            name='is_service',
            field=models.BooleanField(default=False, verbose_name='\u0441\u043b\u0443\u0436\u0435\u0431\u043d\u044b\u0439'),
            preserve_default=True,
        ),
        migrations.RunPython(
            code=create_service_users,
            reverse_code=None,
            atomic=True,
        ),
    ]
