# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0014_auto_20160104_2136'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='facebook_link',
            field=models.CharField(default='', max_length=300, verbose_name='Facebook profile link'),
            preserve_default=False,
        ),
    ]
