# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations  # noqa


def create_email_change_confirmation_template(apps, schema_editor):
    EmailTemplate = apps.get_model("post_office", "EmailTemplate")
    name = 'email-change-confirmation'
    if not EmailTemplate.objects.filter(name=name).exists():
        EmailTemplate.objects.create(
            name=name,
            subject='Подтверждение смены имейла',
            content=(
                'Для подтверждения смены имейла с адреса "{{ user.email }}"" на адрес "{{ new_email }}" '
                'перейдите по ссылке:\n{{ email_confirmation_url }}'
            ),
        )


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0011_auto_20150811_2016'),
    ]

    operations = [
        migrations.RunPython(
            code=create_email_change_confirmation_template,
            reverse_code=None,
            atomic=True,
        ),
    ]
