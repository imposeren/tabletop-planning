# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0012_email_confirmation_template'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='user',
            name='cached_balance',
        ),
        migrations.RemoveField(
            model_name='user',
            name='cached_finished_balance',
        ),
        migrations.RemoveField(
            model_name='user',
            name='cached_unfinished_balance',
        ),
    ]
