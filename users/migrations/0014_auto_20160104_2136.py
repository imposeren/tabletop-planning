# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0013_auto_20151226_1256'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='user',
            options={'ordering': ('first_name', 'last_name', 'email'), 'verbose_name': 'user', 'verbose_name_plural': 'users', 'permissions': (('manage_keys', 'Can manage availability of keys for user'),)},
        ),
    ]
