# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='cached_total_master_experience',
            field=models.PositiveIntegerField(default=0, verbose_name='\u041f\u043e\u0441\u043b\u0435\u0434\u043d\u0438\u0439 \u0440\u0430\u0441\u0441\u0447\u0438\u0442\u0430\u043d\u043d\u044b\u0439 \u043e\u043f\u044b\u0442 \u0432\u0435\u0434\u0443\u0449\u0435\u0433\u043e', editable=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='user',
            name='cached_total_player_experience',
            field=models.PositiveIntegerField(default=0, verbose_name='\u041f\u043e\u0441\u043b\u0435\u0434\u043d\u0438\u0439 \u0440\u0430\u0441\u0441\u0447\u0438\u0442\u0430\u043d\u043d\u044b\u0439 \u043e\u043f\u044b\u0442 \u0438\u0433\u0440\u043e\u043a\u0430', editable=False),
            preserve_default=True,
        ),
        migrations.RemoveField(
            model_name='user',
            name='cached_total_experience',
        ),
    ]
