# -*- coding: utf-8 -*-
from __future__ import unicode_literals

# django:
from django.contrib.auth import login, REDIRECT_FIELD_NAME
from django.shortcuts import redirect

# third-parties:
from sitegate.signin_flows.modern import ModernSignin


class TunedModernSignin(ModernSignin):

    def handle_form_valid(self, request, form):
        login(request, form.get_user())
        redirect_to = request.GET.get(REDIRECT_FIELD_NAME, False) or self.flow_args.get('redirect_to', self.default_redirect_to)
        if redirect_to:  # TODO Handle lambda variant with user as arg.
            return redirect(redirect_to)
