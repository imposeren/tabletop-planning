# -*- coding: utf-8 -*-
from django.apps import AppConfig


class UsersAppConfig(AppConfig):
    name = 'users'
    verbose_name = "Tabletop Users"

    # def ready(self):
    #     from users.signal_receivers import *
    #     return
