from django.dispatch import Signal

mailjet_webhook_event = Signal(
    providing_args=[
        'event_type', 'time', 'event_data'
    ]
)
