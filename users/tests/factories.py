# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import factory
from factory.django import DjangoModelFactory
from django.contrib.auth import get_user_model
from django.utils import timezone


class UserFactory(DjangoModelFactory):
    class Meta:  # noqa: D101
        model = get_user_model()

    is_active = True
    email = factory.Sequence(lambda n: 'user_%d@example.com' % n)
    first_name = factory.Sequence(lambda n: 'user-firstname-%d' % n)
    last_login = factory.Sequence(lambda n: timezone.now())

    @factory.post_generation
    def set_password(self, create, extracted, **kwargs):
        self.set_password(self.email)
        self.save()
