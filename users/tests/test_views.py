# -*- coding: utf-8 -*-

try:
    from django.core.urlresolvers import reverse
except ImportError:
    from django.urls import reverse


from users.tests.factories import UserFactory
from tabletop_planning.tests import BaseTestCase


class UserViewsTestCase(BaseTestCase):
    def test_my_profile_redirect(self):
        """Test 'my profile' (without user id) redirects to current user."""
        profile_url = reverse('users:view_my_profile')
        new_user = UserFactory.create()

        self.assertTrue(self.client.login(email=new_user.email, password=new_user.email))

        response = self.client.get(profile_url)
        self.assertRedirects(response, reverse('users:view_profile', args=(new_user.pk,)))

    def test_no_anonymous_my_profile(self):
        """Test 'my profile' returns 404 for anonymous users."""
        profile_url = reverse('users:view_my_profile')

        response = self.client.get(profile_url)
        self.assertEqual(
            response.status_code, 404,
            "Unauthorized user should get 404 for on 'My profile' page",
        )
