# -*- coding: utf-8 -*-
from django.conf.urls import url

from users import views

urlpatterns = [
    url(r'^details/$', views.view_profile, name='view_my_profile'),
    url(r'^details/(?P<pk>\d+)/$', views.view_profile, name='view_profile'),
    url(r'^edit/$', views.edit_profile, name='edit_profile'),
    url(r'^has-key-toggle/$', views.key_toggle, name='user_has_key_toggle'),
    url(r'^email-unsubscribe/$', views.email_unsubscribe, name='email_unsubscribe'),

    url(r'^mailjet/$', views.mailjet_hook, name='mailjet_hook'),
    url(
        r'^user-autocomplete/$',
        views.UserAutocomplete.as_view(),
        name='user-autocomplete',
    ),
    url(r'^google-drive-sync/enable/$', views.enable_google_drive, name='enable_google_drive'),
    url(r'^google-drive-sync/disable/$', views.disable_google_drive, name='disable_google_drive'),
]

app_name = 'users'
