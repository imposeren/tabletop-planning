# -*- coding: utf-8 -*-

# python builtins:
import datetime
import re

# django:
from django import forms
from django.conf import settings
from django.contrib import messages
from django.contrib.postgres.fields import ArrayField
from django.core.exceptions import ValidationError
from django.core.mail import mail_admins
from django.core.validators import MinLengthValidator, MaxLengthValidator, RegexValidator
from django.db import models
from django.db.models import Sum, Q, Count, Value
from django.db.models.functions import Coalesce
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
try:
    from django.core.urlresolvers import reverse
except ImportError:
    from django.urls import reverse
from django.utils.translation import ugettext_lazy as _, ugettext
from django.utils import timezone
from django.utils.functional import cached_property
from django.utils.text import slugify
from django.utils.encoding import python_2_unicode_compatible

# thirdparties:
from model_utils import FieldTracker, Choices
from unidecode import unidecode
from sorl.thumbnail import ImageField

# project:
from tabletop_planning.utils import mail_send, config_value


class MinLengthOrNullValidator(MinLengthValidator):
    def compare(self, a, b):
        return a is None or a < b

    def clean(self, x):
        return x is None and None or len(x)


class MaxLengthOrNullValidator(MaxLengthValidator):
    def compare(self, a, b):
        return a is None or a > b

    def clean(self, x):
        return x is None and None or len(x)


class RegexOrNullValidator(RegexValidator):
    def __call__(self, value):
        if value is None:
            return
        else:
            return super(RegexOrNullValidator, self).__call__(value)


class ArraySelectMultiple(forms.SelectMultiple):

    def value_omitted_from_data(self, data, files, name):
        return False


class ChoiceArrayField(ArrayField):

    def formfield(self, **kwargs):
        defaults = {
            'form_class': forms.MultipleChoiceField,
            'choices': self.base_field.choices,
            'widget': ArraySelectMultiple,
        }
        defaults.update(kwargs)
        # Skip our parent's formfield implementation completely as we don't care for it.
        # pylint:disable=bad-super-call
        return super(ArrayField, self).formfield(**defaults)


class UsersQS(models.QuerySet):

    def annotate_session_count(self, start_date=None, end_date=None, role=None):
        qs = self
        query = Q(session_visits__have_visited=True, session_visits__verified=True)
        if role:
            query &= Q(session_visits__role=role)
        if start_date:
            query &= Q(session_visits__cached_start_date__gte=start_date)
        if end_date:
            query &= Q(session_visits__cached_start_date__lte=end_date)
        qs = (
            qs
            .filter(query)
            .annotate(
                sessions_count=Coalesce(
                    Count('session_visits'),
                    Value(0)
                )
            )
        )
        return qs

    def annotate_mastered_player_visits(self, start_date=None, end_date=None):
        from games.models import SessionVisit
        qs = self
        query = Q(
            mastering_games__sessions__visits__role=SessionVisit.ROLES.player,
            mastering_games__sessions__visits__have_visited=True,
            mastering_games__sessions__visits__verified=True,
        )
        if start_date:
            query &= Q(mastering_games__sessions__cached_start_date__gte=start_date)
        if end_date:
            query &= Q(mastering_games__sessions__cached_start_date__lte=end_date)

        qs = qs.filter(query).annotate(
            mastered_player_visits=Coalesce(
                Count('mastering_games__sessions__visits'),
                Value(0)
            )
        )
        return qs

    def get_active(self):
        return self.filter(is_active=True)


class UserManager(BaseUserManager):

    def _create_user(self, email, password,
                     is_staff, is_superuser, **extra_fields):
        """Create and save a User with the given username, email and password."""
        now = timezone.now()
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        if 'username' in extra_fields:
            extra_fields.pop('username')
        user = self.model(
            email=email,
            is_staff=is_staff, is_active=True,
            is_superuser=is_superuser, last_login=now,
            date_joined=now, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        return self._create_user(email, password, False, False,
                                 **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        return self._create_user(email, password, True, True,
                                 **extra_fields)

    # def get_queryset(self):
    #     return super(UserManager, self).get_queryset().filter(is_active=True)


@python_2_unicode_compatible
class User(AbstractBaseUser, PermissionsMixin):
    # not fields:
    _request = None
    _participation_cache = None
    objects = UserManager.from_queryset(UsersQS)()

    tracker = FieldTracker()

    USERNAME_FIELD = 'email'

    CONTACT_FIELDS = Choices(
        ('email', _(u'Электронная почта')),
        ('facebook_link', _(u'Facebook')),
        ('telegram_username', _(u'Telegram')),
        ('vk_link', _(u'VK.com')),
    )

    CONTACT_LINK_PROPERTIES = {
        'email': 'email_link',
        'telegram_username': 'telegram_link',
    }

    CONTACT_DISPLAY_PROPERTIES = {
        'email': 'email_display',
        'telegram_username': 'telegram_display',
        'facebook_link': 'facebook_display',
        'vk_link': 'vk_display',
    }

    #
    # Fields:

    first_name = models.CharField(_('first name'), max_length=30)
    last_name = models.CharField(_('last name'), max_length=30, blank=True)

    email = models.EmailField(_('email address'), unique=True)
    is_staff = models.BooleanField(
        _('staff status'), default=False,
        help_text=_('Designates whether the user can log into this admin '
                    'site.')
    )
    is_active = models.BooleanField(
        _('active'), default=True,
        help_text=_('Designates whether this user should be treated as '
                    'active. Unselect this instead of deleting accounts.')
    )
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)

    is_confirmed_master = models.BooleanField(
        _(u"Подтвержден как ведущий"),
        default=False,
    )

    is_service = models.BooleanField(
        _(u'служебный'), default=False,
    )

    requested_to_be_master_datetime = models.DateTimeField(
        _(u'дата запроса стать мастером'), default=None, null=True, blank=True
    )

    cached_total_player_experience = models.PositiveIntegerField(
        _(u'Опыт игрока'),
        editable=False, default=0,
    )

    cached_total_master_experience = models.PositiveIntegerField(
        _(u'Опыт ведущего'),
        editable=False, default=0,
    )

    image = ImageField(
        _(u"фото"),
        upload_to='user_images',
        height_field='image_height',
        width_field='image_width',
        default='',
        blank=True,
    )
    image_height = models.PositiveIntegerField(editable=False, default=0, null=True)
    image_width = models.PositiveIntegerField(editable=False, default=0, null=True)

    receive_notifications = models.BooleanField(
        _(u"Получать уведомления"), default=True,
        help_text=_(u'Получать уведомления на почту')
    )

    has_key = models.BooleanField(
        _(u"У пользователя есть ключ от клуба"), default=False,
    )

    facebook_link = models.CharField(
        _('Facebook profile link'), max_length=300, default='', blank=True,
    )

    telegram_username = models.CharField(
        _('Telegram username'),
        max_length=32,
        null=True,
        default=None,
        validators=[
            # https://core.telegram.org/method/account.checkUsername
            RegexOrNullValidator(re.compile(r'^[-a-zA-Z0-9_]+\Z')),
            MinLengthOrNullValidator(5),
            MaxLengthOrNullValidator(32),

        ],
    )

    public_contacts = ChoiceArrayField(
        models.CharField(max_length=48, choices=CONTACT_FIELDS),
        verbose_name=_(u"Публичные контакты"),
        blank=False,
        size=len(CONTACT_FIELDS),
        default=lambda: [v for v in User.CONTACT_FIELDS._db_values],
        help_text=_(
            u'Какие контакты показывать другим пользователям. '
            u'Ведущие ваших игр могут видеть ВСЕ ваши контакты. '
            u'Подтвержденные в ваших играх игроки тоже могут видеть все ваши контакты.'
        )
    )

    save_tables_to_drive = models.BooleanField(default=False)

    # start models API

    def __str__(self):
        return self.get_full_name()

    class Meta:  # noqa: D101
        verbose_name = _(u'user')
        verbose_name_plural = _(u'users')
        ordering = ('first_name', 'last_name', 'email')
        base_manager_name = 'objects'

        permissions = (
            ("manage_keys", _("Can manage availability of keys for user")),
        )

    def save(self, *args, **kwargs):
        self._request = kwargs.pop('request', None)
        self.send_notifies()
        super(User, self).save(*args, **kwargs)

    def clean(self):
        suffix = self.get_service_suffix().lower()
        alt_suffix = slugify(unidecode(suffix))
        for field_name in ('first_name', 'last_name',):
            value = getattr(self, field_name).lower()
            variant = slugify(unidecode(value))
            if value.count(suffix) >= 1 or variant.count(alt_suffix) >= 1:
                raise ValidationError(_(u'В имени пользователя нельзя использовать слово "{0}"').format(suffix))

    # end models API

    # start urls
    def get_absolute_url(self):
        return reverse('users:view_profile', args=(self.pk,))

    def get_edit_url(self):
        return reverse('users:edit_profile', args=())

    def get_admin_change_url(self):
        url = reverse('admin:users_user_change', args=[self.pk])
        if self._request:
            url = self._request.build_absolute_uri(url)
        return url
    # end urls

    # start properties and info

    def get_contacts_info(self, public_only=True):
        results = []
        # email_description = None
        public_contacts_set = frozenset(self.public_contacts)
        for contact_field, description in self.CONTACT_FIELDS:
            # if contact_field == 'email':
            #     email_description = description
            field_value = getattr(self, contact_field, None)

            if not (field_value and (contact_field in public_contacts_set or not public_only)):
                continue

            results.append(
                {
                    'contact_field': contact_field,
                    'description': description,
                    'field_value': field_value,
                }
            )

        # if not results:
        #     results.append(
        #         {
        #             'contact_field': 'email',
        #             'description': email_description,
        #         }
        #     )

        for item in results:
            contact_field = item['contact_field']

            link_value = None
            display_value = _(u'Открыть')

            link_property = self.CONTACT_LINK_PROPERTIES.get(contact_field)
            if link_property:
                link_value = getattr(self, link_property, None)

            link_value = link_value or item['field_value']

            link_display_property = self.CONTACT_DISPLAY_PROPERTIES.get(contact_field)
            if link_display_property:
                display_value = getattr(self, link_display_property, None) or display_value

            item['link'] = link_value
            item['display'] = display_value

        return results

    @cached_property
    def contacts_info(self):
        return self.get_contacts_info(public_only=True)

    @cached_property
    def all_contacts_info(self):
        return self.get_contacts_info(public_only=False)

    @property
    def email_link(self):
        return "mailto:{0}".format(self.email)

    @property
    def email_display(self):
        return self.email

    @property
    def telegram_link(self):
        return "https://t.me/{0}".format(self.telegram_username)

    @property
    def telegram_display(self):
        return "@{0}".format(self.telegram_username)

    @cached_property
    def vk_link(self):
        for soc_auth in self.social_auth.all():
            if soc_auth.provider == 'vk-oauth2':
                return "https://vk.com/id" + soc_auth.uid
        return None

    @property
    def vk_display(self):
        return _(u'Профиль')

    @property
    def facebook_display(self):
        return _(u'Профиль')

    @cached_property
    def balance(self):
        return self.balance_changes.all().aggregate(Sum('amount')).get('amount__sum', 0) or 0

    @cached_property
    def balance_verified(self):
        return (
            self.balance_changes.filter(verified=True).aggregate(Sum('amount')).get('amount__sum', 0)
            or
            0
        )

    @property
    def balance_unverified(self):
        return self.balance - self.balance_verified

    @property
    def debt_verified(self):
        if self.balance_verified < 0:
            return -self.balance_verified
        else:
            return 0

    @property
    def debt_unverified(self):
        return -self.balance_unverified

    @property
    def username(self):
        return self.email

    @username.setter
    def username(self, value):
        self.email = value

    def get_full_name(self):
        """Return the first_name plus the last_name, with a space in between."""
        full_name = '%s %s' % (self.first_name, self.last_name)
        result = full_name.strip()
        result = result or self.email
        if self.is_service:
            result += u' (%s)' % self.get_service_suffix()
        return result

    def get_short_name(self):
        """Return the short name for the user."""
        return self.first_name or self.email

    @staticmethod
    def get_service_suffix():
        return settings.SERVICE_USER_SUFFIX

    def get_mastered_player_visits(self, start_date=None, end_date=None):
        return (
            User
            .objects
            .filter(pk=self.pk)
            .annotate_mastered_player_visits(start_date, end_date)
            .values_list('mastered_player_visits', flat=True)
            .first()
        )

    # end properties and info

    # start related objects processing

    @cached_property
    def get_playing_games(self):
        return self.playing_games.filter(is_active=True, is_finished=False).optimize_for_output()

    @cached_property
    def get_played_games(self):
        return self.participated_games.exclude(pk__in=[g.pk for g in self.get_playing_games])

    @cached_property
    def get_mastering_games(self):
        return self.mastering_games.all().optimize_for_output()

    @cached_property
    def get_unfinished_mastering_games(self):
        return [g for g in self.get_mastering_games if not g.is_finished]

    @cached_property
    def get_unfinished_active_mastering_games(self):
        return [g for g in self.get_mastering_games if g.is_active and not g.is_finished]

    @cached_property
    def get_unfinished_inactive_mastering_games(self):
        return [g for g in self.get_mastering_games if (not g.is_active) and (not g.is_finished)]

    @cached_property
    def get_active_mastering_games(self):
        return [g for g in self.get_mastering_games if g.is_active]

    @cached_property
    def get_inactive_mastering_games(self):
        return [g for g in self.get_mastering_games if not g.is_active]

    @cached_property
    def get_finished_active_mastering_games(self):
        return [g for g in self.get_mastering_games if g.is_active and g.is_finished]

    @cached_property
    def get_finished_mastering_games(self):
        return [g for g in self.get_mastering_games if g.is_finished]

    @cached_property
    def default_player_payment(self):
        not_free = (
            not config_value('accounting', 'FREE_FIRST_VISIT', False)
            or self.cached_total_player_experience > 0
            or self.session_visits.filter(verified=True).exists()
        )
        if not_free:
            return self.player_level.one_ticket_cost
        else:
            return 0

    @cached_property
    def default_master_payment(self):
        return self.master_level.one_game_salary

    @property
    def default_safe_hours(self):
        return self.master_level.safe_hours

    @cached_property
    def has_tickets(self, exclude_ticket=None):
        return self.tickets.filter(visit_used__isnull=True).exists()

    @cached_property
    def unused_tickets(self):
        return self.tickets.filter(visit_used__isnull=True)

    def get_unused_tickets_count(self):
        return self.unused_tickets.count()

    get_unused_tickets_count.short_description = _(u'Осталось игр по ваучеру')

    @cached_property
    def player_level(self):
        from accounting.models import PlayerLevelInfo
        level_info = PlayerLevelInfo.objects.filter(total_xp__lte=self.cached_total_player_experience).order_by('-level').first()
        if level_info:
            return level_info
        return PlayerLevelInfo.objects.get(level=1)

    player_level.short_description = _(u'Уровень')

    @cached_property
    def master_level(self):
        from accounting.models import MasterLevelInfo
        level_info = MasterLevelInfo.objects.filter(total_xp__lte=self.cached_total_master_experience).order_by('-level').first()
        if level_info:
            return level_info
        return MasterLevelInfo.objects.get(level=1)

    master_level.short_description = _(u'Уровень ведущего')

    @cached_property
    def associatable_auths(self):
        variants = {
            'vk-oauth2': 'vk.com',
            'facebook': 'facebook.com',
            'google-oauth2': 'google.com',
            'disqus': 'disqus.com',
        }
        existing = self.social_auth.all().values_list('provider', flat=True)
        pre_results = set(variants.keys()) - set(existing)
        return [(provider, variants[provider]) for provider in pre_results]

    def participates_in_game(self, game_or_session):
        if self._participation_cache is None:
            self._participation_cache = {}

        game_pk = getattr(
            game_or_session,
            'game_id',
            getattr(game_or_session, 'pk', None)
        )

        if game_pk not in self._participation_cache:
            game = getattr(game_or_session, 'game', game_or_session)
            res = (
                game
                and
                self.is_authenticated
                and
                (
                    game in self.get_mastering_games
                    or
                    game in self.get_playing_games
                )
            )
            self._participation_cache[game_pk] = res
        else:
            res = self._participation_cache[game_pk]
        return res

    def plays_with_me(self, user):
        """User is confirmed as player of some active game mastered by current user or vice versa."""
        if not (self.pk and user.pk and self.pk != user.pk):
            return False
        return self.mastering_games.filter(
            is_active=True,
            is_finished=False,
            game_players__player=user,
        ).exists() or self.playing_games.filter(
            is_active=True,
            is_finished=False,
            masters=user,
            game_players__player=self,
            game_players__confirmed=True,
        ).exists()

    # end related objects processing

    # start actions

    def send_notifies(self):
        """Send notifications about User creation or modification."""
        master_moderation_needed = (
            self.tracker.has_changed('requested_to_be_master_datetime')
            and not self.is_confirmed_master
        )
        if master_moderation_needed:
            previous = self.tracker.previous('requested_to_be_master_datetime')
            if previous:
                delta = self.requested_to_be_master_datetime - previous
                should_renotify = (
                    delta > datetime.timedelta(
                        days=config_value('tabletop_planning', 'NEW_GM_RENOTIFICATION_DELAY'))
                )
                if not should_renotify:
                    master_moderation_needed = False

            if master_moderation_needed:
                moderators_group = config_value('tabletop_planning', 'GM_MODERATORS_GROUPNAME')
                moderators = list(
                    User.objects.filter(
                        is_active=True,
                        groups__name=moderators_group, email__contains='@',
                    )
                )
                recipients = [m.email for m in moderators]
                if recipients:
                    mail_send(
                        recipients,
                        template='master-moderation-request',
                        context={'user': self},
                        tags=['notification', 'moderation'],
                    )
                else:
                    mail_admins(u'No GM moderators available', u'No activa GM moderators found. Please add some to "%s" group' % moderators_group)

    send_notifies.alters_data = True

    def email_unsubscribe(self, request=None):
        """Set field receive_notifications to False, save, possible send message."""
        if self.receive_notifications:
            self.receive_notifications = False
            self.save(update_fields=['receive_notifications'])
            if request:
                messages.info(
                    request,
                    ugettext(u'Почтовый адрес %(email)s был удалён из списка рассылки.') % {'email': self.email}
                )
        elif request:
            messages.info(
                request,
                ugettext(u'Пользователь уже был отписан от уведомлений.')
            )

    email_unsubscribe.alters_data = True

    def recalc_experience(self, save_on_change=True, master_type=False):
        """Recalculate user experience.

        Save is called if value changed and save_on_change is set to true

        """
        new_experience = (
            self.experience_rewards.filter(verified=True, master_type=master_type)
            .aggregate(Sum('amount')).get('amount__sum', 0) or 0
        )
        if master_type:
            field_name = 'cached_total_master_experience'
        else:
            field_name = 'cached_total_player_experience'
        if new_experience != getattr(self, field_name):
            setattr(self, field_name, new_experience)
            if save_on_change:
                self.save(update_fields=[field_name])
        return new_experience

    recalc_experience.alters_data = True

    def reset_balance(self, reset_by='balance_verified', verified=True, comment='reset user balance', **kwargs):
        from accounting.models import BalanceChange

        if reset_by not in ('balance_verified', 'balance'):
            raise ValueError('Reset by should be one of "balance" or "balance_verified"')
        amount = -getattr(self, reset_by)
        if amount:
            return BalanceChange.objects.create(
                user=self,
                amount=amount,
                verified=verified,
                comment=comment,
                to_budget=True,
                **kwargs
            )
        else:
            return None

    reset_balance.alters_data = True

    # end actions
