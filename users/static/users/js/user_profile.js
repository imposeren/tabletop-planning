$(document).ready(function(){
  'use strict';

  var keyChanger = $('.js-key-change');
  var keyIndicator = $('.js-has-key');
  keyChanger.click(function(){
    $.get(
      keyChanger.data('url'),
      {'current_state': keyChanger.data('state'), 'user_pk': keyChanger.data('user-pk')},
      function(data, textStatus, jqXHR){
        if (data.new_state) {
          keyChanger.children('.text').text(keyChanger.data('state-on-text')+' ');
          keyIndicator.show();
        } else {
          keyChanger.children('.text').text(keyChanger.data('state-off-text')+' ');
          keyIndicator.hide();
        }
      }
    );
  });
});
