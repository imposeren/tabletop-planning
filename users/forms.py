# -*- coding: utf-8 -*-

from __future__ import unicode_literals

# django:
from django import forms
# from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserChangeForm as _UserChangeForm
from django.contrib.auth.forms import UserCreationForm as _UserCreationForm
try:
    from django.core.urlresolvers import reverse
except ImportError:
    from django.urls import reverse
from django.utils.translation import ugettext_lazy as _

# thirdparties:
from crispy_forms.bootstrap import FormActions
from crispy_forms.layout import Submit, Layout, Field
from dal import autocomplete
from parsley.decorators import parsleyfy
# from sitegate.models import EmailConfirmation
from sitegate.signup_flows.modern import ModernSignup, ModernSignupForm

# project
from tabletop_planning.forms import BootstrapModelForm, BootstrapForm
# from tabletop_planning.utils import mail_send
from tabletop_planning.widgets import ImageWidget


User = get_user_model()


class TelegramCleanMixin(object):
    def clean_telegram_username(self):
        ignored_prefixes = ('@', 'https://t.me/')
        telegram_username = self.cleaned_data['telegram_username']
        for prefix in ignored_prefixes:
            if telegram_username.startswith(prefix):
                telegram_username = telegram_username.replace(prefix, '', 1)
                break
        return telegram_username or None


class UserAutocompleteForm(forms.Form):
    user = forms.ModelChoiceField(
        queryset=User.objects.all(),
        widget=autocomplete.ModelSelect2(
            url='users:user-autocomplete',
            attrs={
                # Set some placeholder
                'data-placeholder': _(u'Имя или почта игрока'),
                # Only trigger autocompletion after 3 characters have been typed
                'data-minimum-input-length': 3,
            },
        )
    )


# override contrib's form to not require username (used in admin):
class UserCreationForm(TelegramCleanMixin, _UserCreationForm):

    class Meta:  # noqa: D101
        model = User
        fields = ("email", "first_name", )

    def __init__(self, *args, **kwargs):
        super(UserCreationForm, self).__init__(*args, **kwargs)
        self.fields.pop('username', '')

    def clean_email(self):
        # Since User.username is unique, this check is redundant,
        # but it sets a nicer error message than the ORM. See #13147.
        email = self.cleaned_data["email"]
        try:
            User._default_manager.get(email=email)
        except User.DoesNotExist:
            return email
        raise forms.ValidationError(
            self.error_messages['duplicate_username'],
            code='duplicate_username',
        )


# override contrib's form to not require username (used in admin):
class UserChangeForm(TelegramCleanMixin, _UserChangeForm):

    class Meta:  # noqa: D101
        model = User
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(UserChangeForm, self).__init__(*args, **kwargs)
        self.fields.pop('username', '')


class CustomizedSignupForm(ModernSignupForm):

    class Meta:  # noqa: D101
        model = User
        fields = ('email', 'first_name', 'password1',)


class CustomizedSignup(ModernSignup):

    form = CustomizedSignupForm


signin_layout = Layout(
    Field('username', autofocus='autofocus'),
    'password',
    'signin_flow',
    FormActions(
        Submit(
            'submit', _(u'Вход'), css_class='btn btn-primary js-signin'
        )
    )
)


signup_layout = Layout(
    Field('first_name', autofocus='autofocus'),
    'email',
    'password1',
    'signup_flow',
    FormActions(
        Submit(
            'submit', _(u'Регистрация'), css_class='btn btn-primary js-signin'
        )
    )
)


@parsleyfy
class UserEditForm(TelegramCleanMixin, BootstrapModelForm):

    class Meta:  # noqa: D101
        model = User
        fields = (
            'first_name', 'last_name', 'image', 'receive_notifications',
            'public_contacts', 'telegram_username'
            # 'email',
        )

        widgets = {
            'image': ImageWidget(),
        }

        help_texts = {
            'email': _('Имейл будет изменен не сразу: на новый адрес к вам придёт письмо содержащее ссылку для подтверждения смены имейла.'),
        }

    def __init__(self, *args, **kwargs):
        super(UserEditForm, self).__init__(*args, **kwargs)
        self.helper.layout = Layout(
            # 'email', 'receive_notifications', 'first_name', 'last_name', 'image',
            'receive_notifications', 'public_contacts', 'telegram_username', 'first_name', 'last_name', 'image',
            self._default_actions
        )

    # def clean_email(self):
    #     value = self.cleaned_data.get('email')
    #     if User.objects.filter(email__iexact=value).exclude(pk=self.instance.pk).exists():
    #         raise forms.ValidationError(_('Уже существует другой пользователь с таким email адресом'))
    #     return value

    # def clean(self):
    #     super(UserEditForm, self).clean()
    #     cd = self.cleaned_data
    #     if not self.errors:
    #         if cd['email'] != self.instance.email:
    #             new_email = cd['email']
    #             # do not change email. Send confirmation email instead
    #             cd['email'] = self.instance.email

    #             url = EmailConfirmation.start_email_change(self.instance, new_email=new_email, send_email=False, next_step='finish_email_change')
    #             email_confirmation_url = self.request.build_absolute_uri(url)

    #             recipients = [new_email]

    #             mail_send(
    #                 recipients, template='email-change-confirmation',
    #                 context={'user': self.instance, 'email_confirmation_url': email_confirmation_url, 'new_email': new_email},
    #                 tags=['notification', ],
    #             )
    #             messages.info(self.request, _('Что бы подтвердить смену имейла перейдите по ссылке которая была выслана на адрес %s') % new_email)

    #     return cd


@parsleyfy
class EmailRequiredForm(BootstrapForm):

    email = forms.EmailField()

    def __init__(self, *args, **kwargs):
        super(UserEditForm, self).__init__(*args, **kwargs)
        self.helper.layout = Layout(
            'email',
            self._default_actions
        )
        self.helper.action = reverse(
            'social:complete',
            kwargs={'backend': self.request.session.get('partial_pipeline').get('backend')}
        )
        self.helper.method = 'POST'

    def clean_email(self):
        value = self.cleaned_data.get('email')
        if User.objects.filter(email__iexact=value).exclude(pk=self.instance.pk).exists():
            raise forms.ValidationError(_('Уже существует другой пользователь с таким email адресом'))
        return value
