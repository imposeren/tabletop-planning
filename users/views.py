# -*- coding: utf-8 -*-
from __future__ import unicode_literals

# python builtins:
import json

# django:
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import LogoutView
from django.db.models import Q
from django.http import HttpResponse
from django.http import HttpResponseForbidden
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _


# third-parties:
from annoying.decorators import render_to, ajax_request
from dal import autocomplete
from sitegate.decorators import signup_view, signin_view

# project:
from tabletop_planning.forms import BootstrapFormMixin
from users.signals import mailjet_webhook_event

# app:
from .models import User
from .forms import signin_layout, signup_layout, CustomizedSignup, UserEditForm, EmailRequiredForm
from .signin_flow import TunedModernSignin


@render_to('users/signin.html')
@signin_view(flow=TunedModernSignin, template='users/signin/crispy_signin_form.html')
def signin(request):
    helper = BootstrapFormMixin().helper
    helper.layout = signin_layout
    redirect_url = request.POST.get('next') or request.GET.get('next')

    return {'title': _('Вход'), 'helper': helper, 'next': redirect_url}


@render_to('users/signup.html')
@signup_view(flow=CustomizedSignup, verify_email=True, template='users/signup/crispy_signup_form.html')
def signup(request):
    helper = BootstrapFormMixin().helper
    helper.layout = signup_layout
    redirect_url = request.POST.get('next') or request.GET.get('next')
    return {'title': _('Регистрация'), 'helper': helper, 'next': redirect_url}


def logout(request):
    messages.info(request, _('Вы успешно вышли из своего аккаунта'))
    logout_view = LogoutView.as_view()
    logout_view.next_page = request.POST.get('next') or request.GET.get('next') or '/'
    return logout_view(request)


@render_to('users/user_details.html')
def view_profile(request, pk=None):
    if not pk and not request.user.is_anonymous:
        viewed_user = request.user
        return redirect(viewed_user.get_absolute_url())
    else:
        viewed_user = get_object_or_404(User, pk=pk)

    return {
        'viewed_user': viewed_user,
        'has_key_manage_permission': request.user.has_perm('users.manage_keys'),
    }


@ajax_request
def key_toggle(request):
    if not request.user.has_perm('users.manage_keys'):
        return HttpResponseForbidden()
    pk = int(request.GET.get('user_pk'))
    user = get_object_or_404(User, pk=pk)
    current_state = int(request.GET.get('current_state'))
    correct = True

    if current_state == 1:
        user.has_key = False
    elif current_state == 0:
        user.has_key = True
    else:
        correct = False
    if correct:
        user.save()
    return {'new_state': user.has_key}


@login_required
@render_to('users/user_edit.html')
def edit_profile(request):
    user = request.user
    form = UserEditForm(request.POST or None, request.FILES or None, instance=user, request=request)
    if form.is_valid():
        form.save()
        return redirect('users:view_my_profile')
    return {'form': form}


@login_required
def email_unsubscribe(request):
    request.user.email_unsubscribe(request=request)
    return redirect('users:edit_profile')


def password_reset_done(request):
    messages.info(request, _('Пароль успешно восстановлен'))
    return redirect('/')


@render_to('users/require-email.html')
def require_email(request):
    form = EmailRequiredForm(request.POST or None, request.FILES or None)

    return {'form': form}


def mailjet_hook(request):
    json_data = json.loads(request.body)
    if not isinstance(json_data, list):
        json_data = [json_data]

    for event_datum in json_data:
        mailjet_webhook_event.send(
            sender=mailjet_hook,
            event_type=event_datum['event'].lower(),
            data=event_datum,
        )
    return HttpResponse('')


class UserAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        if not (self.request.user.is_authenticated and self.request.user.is_staff):
            return User.objects.none()

        qs = User.objects.all()

        if self.q:
            qs = qs.filter(Q(email__istartswith=self.q) | Q(first_name__istartswith=self.q) | Q(last_name__istartswith=self.q))

        return qs


@login_required
def enable_google_drive(request):
    if not request.user.save_tables_to_drive:
        request.user.save_tables_to_drive = True
        request.user.save()
    return redirect(
        '%s?drive_access=1' % reverse('social:begin', args=['google-oauth2', ])
    )


@login_required
def disable_google_drive(request):
    if request.user.save_tables_to_drive:
        request.user.save_tables_to_drive = False
        request.user.save()
    return redirect('/')
