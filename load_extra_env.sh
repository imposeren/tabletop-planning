#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

export $(cat .extra_env.conf | xargs)

. $VIRTENV_DIR/bin/activate