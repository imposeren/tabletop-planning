#!/bin/bash
rm -rf /tmp/pip-*-build || true

export OPENSHIFT_DATA_DIR=${WORKSPACE}
GIT_PREVIOUS_SUCCESSFUL_COMMIT=${GIT_PREVIOUS_SUCCESSFUL_COMMIT:-'prev'}
export PIP_ACCEL_TRUST_MOD_TIMES=no

cd ${WORKSPACE}

PREV_DEPLOY_TARGET="${DEPLOY_TARGETS_ROOT}/${GIT_PREVIOUS_SUCCESSFUL_COMMIT}"
cp -a ${PREV_DEPLOY_TARGET}/virtenv ./ || true
rm -rf ./virtenv/local/lib/python2.7/site-packages/pip/ || true

/usr/bin/virtualenv --always-copy virtenv
. virtenv/bin/activate

PYTHON=virtenv/bin/python
$PYTHON virtenv/bin/pip install -U pip==7.1.2
$PYTHON virtenv/bin/pip install -U setuptools==12.2
$PYTHON virtenv/bin/pip install wheel==0.26.0
$PYTHON virtenv/bin/pip install pip-accel==0.37

/usr/bin/virtualenv --relocatable virtenv

PIP_INSTALL="$PYTHON virtenv/bin/pip-accel install --exists-action=w"
$PIP_INSTALL -Ur requirements_dev.pip
/usr/bin/virtualenv --relocatable virtenv
# TODO: reuser previous envs

echo "Running tests..."
export PYTHONPATH="${WORKSPACE}"
$PYTHON ./manage.py jenkins --noinput --enable-coverage --jshint-exclude='*.min.js,Gruntfile.js,bootstrap.js,ie*-*.js,filesaver.js,blob.js,holder.js,customizer.js,tooltip*.js,offcanvas.js,transition.js,modal.js,button.js,dropdown.js,carousel.js,popover.js,scrollspy.js,tab.js,affix.js,alert.js,collapse.js,qunit.js,phantom.js,bs-*.js,application.js' -v1

deactivate


#####################################

echo "moving environ to deployment target"
DEPLOY_TARGET="${DEPLOY_TARGETS_ROOT}/${GIT_COMMIT}"
mkdir "${DEPLOY_TARGET}" || true
cd "${DEPLOY_TARGET}"
export OPENSHIFT_DATA_DIR=${DEPLOY_TARGET}

echo "copying project to deployment target"
cp -a "${WORKSPACE}"/* ./
rm -rf ./virtenv/local/lib/python2.7/site-packages/pip/ || true

/usr/bin/virtualenv --always-copy virtenv
. virtenv/bin/activate

export PYTHONPATH="${DEPLOY_TARGET}"

PYTHON=virtenv/bin/python
$PYTHON virtenv/bin/pip install -U pip==7.1.2
$PYTHON virtenv/bin/pip install -U pip-accel==0.37
PIP_INSTALL="$PYTHON virtenv/bin/pip-accel install --exists-action=w"
echo reinstalling some packages after env movement
$PIP_INSTALL -r requirements_dev.pip --no-index

/usr/bin/virtualenv --relocatable virtenv

echo "saving deployment env..."
export VIRTENV_DIR="${DEPLOY_TARGET}/virtenv"
env | grep OPENSHIFT > /home/jenkins/uwsgi/last_env.conf || true
env | grep DJANGO >> /home/jenkins/uwsgi/last_env.conf || true
env | grep EMAIL >> /home/jenkins/uwsgi/last_env.conf || true
env | grep PGDATABASE >> /home/jenkins/uwsgi/last_env.conf || true
env | grep DEPLOY_TARGET >> /home/jenkins/uwsgi/last_env.conf || true
env | grep VIRTENV_DIR >> /home/jenkins/uwsgi/last_env.conf || true
env | grep SECRET_KEY >> /home/jenkins/uwsgi/last_env.conf || true
env | grep SENTRY_DB_PASSWORD >> /home/jenkins/uwsgi/last_env.conf || true
env | grep RAVEN_CONFIG_DSN >> /home/jenkins/uwsgi/last_env.conf || true
env | grep LC_ >> /home/jenkins/uwsgi/last_env.conf || true

cp /home/jenkins/uwsgi/last_env.conf ${DEPLOY_TARGET}/.extra_env.conf

echo "Updating DB..."
$PYTHON manage.py migrate --fake-initial --noinput

echo "Collecting static..."
mkdir varying 2>>/dev/null || true
mkdir varying/static 2>>/dev/null || true

$PYTHON ./manage.py collectstatic --noinput --clear
$PYTHON ./manage.py compress
mkdir locale || true
$PYTHON ./manage.py compilemessages
$PYTHON ./manage.py createinitialrevisions

cd "${DEPLOY_TARGETS_ROOT}"

# symlink to current revision
rm "${DEPLOY_TARGETS_ROOT}/current" || true
ln -s "${DEPLOY_TARGET}" "${DEPLOY_TARGETS_ROOT}/current"
cd "${DEPLOY_TARGETS_ROOT}/current"
$PYTHON ./manage.py thumbnail clear_delete_all

# manage uwsgi vassals config files:
cd /home/jenkins/uwsgi/vassals-enabled/
rm "${GIT_COMMIT}.ini" || true
ln -s "${DEPLOY_TARGETS_ROOT}/${GIT_COMMIT}/deployment/confs/uwsgi/vassal.template" "${GIT_COMMIT}.ini"
rm current.ini || true
rm sentry.ini || true
ln -s "${GIT_COMMIT}.ini" current.ini
ln -s "${DEPLOY_TARGETS_ROOT}/${GIT_COMMIT}/deployment/confs/sentry/uwsgi.ini" "sentry.ini"


cleanup_previous () {
	# cleanup previous files except sentry, current and previous revision
	find ./ -maxdepth 1 -name "*"  -not -path "./" -not -name "${GIT_COMMIT}*" -not -name "${GIT_PREVIOUS_SUCCESSFUL_COMMIT}*" -not -name "current*" -not -name "sentry*" -exec rm -rf {} \; || true
}

# cleanup old configs
cd /home/jenkins/uwsgi/vassals-enabled/
cleanup_previous

# cleanup previous logs
cd /home/jenkins/uwsgi/logs
cleanup_previous

# cleanup previous pids
cd /home/jenkins/uwsgi/pids
cleanup_previous

# cleanup revisions
cd ${DEPLOY_TARGETS_ROOT}
cleanup_previous
