@Library('python_utils') _


pipeline {

    agent any

    options {
        timestamps()
    }

    environment {
        PYTHON_VERSION = '3.6'
        PYTHONIOENCODING = 'utf-8'
        PYTHON_PIP_CMD = 'pip3'
        LC_ALL = 'en_US.UTF-8'
        LC_LANG = 'en_US.UTF-8'

        VIRTENV_NAME = 'virtenv'

        POSTGRESQL_DB_ACCESS = credentials('PLAYHARD_POSTGRESQL_DB_ACCESS')

        CUSTOM_PROPS_FILE = credentials('TABLETOP_PROPS_FILE')
    }

    stages {

        stage("Update Job") {
            steps {
                script {
                    env.GIT_REMOTELESS_BRANCH = "${env.GIT_BRANCH}".split('/')[1]
                    echo "Deploying branch ${GIT_REMOTELESS_BRANCH}"

                    def deploy_props = readProperties([
                        defaults: readProperties([file: 'deployment/default_deploy.properties']),
                        file: "${env.CUSTOM_PROPS_FILE}"
                    ])

                    for (entry in deploy_props) {
                        env.setProperty(entry.key,  entry.value)
                    }
                    env.DEPLOYED_ROOT = "${env.REVISIONS_ROOT}/${env.BUILD_NUMBER}"

                    if (env.GIT_REMOTELESS_BRANCH == 'master') {
                        properties([
                            disableConcurrentBuilds(),
                            disableResume(),
                            buildDiscarder(logRotator(
                                artifactDaysToKeepStr: '',
                                artifactNumToKeepStr: '',
                                daysToKeepStr: '',
                                numToKeepStr: '50'
                            )),
                            pipelineTriggers([[$class: 'BitBucketTrigger']]),
                        ])
                    }

                    sh "mkdir -p '${env.REVISIONS_ROOT}' || true"
                    sh "mkdir -p '${env.VIRTUALENV_REVISIONS_ROOT}' || true"
                    sh "mkdir -p '${env.CONFIGS_ROOT}/nginx' || true"
                    sh "mkdir -p '${env.CONFIGS_ROOT}/uwsgi/env-vars' || true"
                    sh "mkdir -p '${env.CONFIGS_ROOT}/uwsgi/vassals-enabled' || true"
                    sh "mkdir -p '${env.UWSGI_RUNTIME_ROOT}/pids' || true"
                    sh "mkdir -p '${env.UWSGI_RUNTIME_ROOT}/logs' || true"
                    sh "mkdir -p '${env.UWSGI_RUNTIME_ROOT}/sockets' || true"
                }
            }
        }

        stage("Build: create virtualenv") {
            steps {
                echo "Preparing virtualenv"
                script {
                    def hash_file = 'pip-requirements.md5'
                    def hashable_data_command = "{ cat setup.py ; echo ${env.PIP_EXTRA_REQS}; }"
                    def pip_install_extra_arg = ''
                    cache(caches: [[$class: 'ArbitraryFileCache', excludes: '', includes: hash_file, path: './']], maxCacheSize: 5) {
                        withNewVenv {
//                            venvRunCmd("${env.PYTHON_PIP_CMD} install -U pip==21.1.1")
                            def md5_exit_code = sh([
                                returnStatus: true,
                                script: "${hashable_data_command} | md5sum -c ${hash_file}"
                            ])

                            // reuse previous virtualenv packages
                            def prev_env_path = "${env.VIRTUALENV_REVISIONS_ROOT}/${env.VIRTENV_NAME}_${env.GIT_REMOTELESS_BRANCH}"

                            if (md5_exit_code != 0) {
                                echo "Hash of setup.py and extra installs changed -> rebuild wheels"
                                pip_install_extra_arg = ' -U '
                                venvRunCmd("${env.PYTHON_PIP_CMD} wheel --trusted-host github.com --trusted-host bitbucket.org -f ${env.JENKINS_HOME}/wheelhouse --wheel-dir ${env.JENKINS_HOME}/wheelhouse .[test,memcached] ${env.PIP_EXTRA_REQS}")
                            } else if (fileExists(prev_env_path)) {
                                sh "cp -na ${prev_env_path}/lib/python${env.PYTHON_VERSION}/site-packages/* ${env.VIRTENV_DIR}/lib/python${env.PYTHON_VERSION}/site-packages/"
                                sh "cp -na ${prev_env_path}/bin/* ${env.VIRTENV_DIR}/bin/"
//                                sh '/usr/bin/virtualenv -p "python${PYTHON_VERSION}" --relocatable "${VIRTENV_DIR}"'
                            }

                            echo "Installing packages into virtualenv using wheels only"
                            venvRunCmd("${env.PYTHON_PIP_CMD} install ${pip_install_extra_arg} --no-index -f ${env.JENKINS_HOME}/wheelhouse .[test,memcached] ${env.PIP_EXTRA_REQS}")
                            // sh '~/.local/bin/virtualenv-tools --update-path "${VIRTENV_DIR}" "${VIRTENV_DIR}" || true'
                        }
                        sh "${hashable_data_command} | md5sum > ./${hash_file}"
                    }
                }
            }

        }

        stage("Test") {
            steps {
                script {
                    withEnv(["PYTHONPATH=${env.WORKSPACE}"]){
                        withExistingVenv {
                            dir(env.WORKSPACE){
                                sh "mkdir ./reports || true"
                                echo "Running tests"
                                venvRunCmd(
                                    "coverage run --source='.' manage.py test --noinput -v1"
                                )
                                venvRunCmd(
                                    "coverage xml -o reports/coverage.xml"
                                )
                                venvRunCmd("flake8 --config tox.ini --exit-zero > reports/flake8.report")
                                sh "jshint --reporter=jslint ./ > reports/jshint.xml || true"
                            }
                        }
                    }
                }
            }

            post {
                always {
                    step([
                        $class: 'CoberturaPublisher',
                        autoUpdateHealth: true,
                        autoUpdateStability: true,
                        coberturaReportFile: 'reports/coverage.xml',
                        failNoReports: true,
                        failUnhealthy: false,
                        failUnstable: true,
                        maxNumberOfBuilds: 15,
                        zoomCoverageChart: true,
                        onlyStable: true,
                        healthyTarget: [
                            targets: []
                        ],
                        targets: [
                            [
                                metric: 'LINE',
                                healthy: 66,
                                unhealthy: 0,
                                unstable: 65
                            ],
                            [
                                metric: 'PACKAGES',
                                healthy: 94,
                                unhealthy: 0,
                                unstable: 87
                            ],
                            [
                                metric: 'CLASSES',
                                healthy: 91,
                                unhealthy: 0,
                                unstable: 81
                            ]
                        ]
                    ])
                    warnings([
                        canComputeNew: false,
                        canResolveRelativePaths: false,
                        defaultEncoding: '',
                        excludePattern: '.*/libs/.*,.*/static/scribbler/.*,.*/static/qtip2/.*,.*/static/tooltipster/.*,.*/migrations/.*auto.*,.*\\.example\\.py',
                        healthy: '',
                        includePattern: '',
                        messagesPattern: '',
                        parserConfigurations: [
                            [
                                parserName: 'PyLint',
                                pattern: 'reports/flake8.report'
                            ],
                            [
                                parserName: 'JSLint',
                                pattern: 'reports/jshint.xml'
                            ]
                        ],
                        unHealthy: ''
                    ])
                    step([
                        $class: 'AnalysisPublisher',
                        defaultEncoding: '',
                        failedNewHigh: '0',
                        failedNewLow: '6',
                        failedNewNormal: '0',
                        failedTotalHigh: '0',
                        failedTotalLow: '50',
                        failedTotalNormal: '3',
                        healthy: '0',
                        unHealthy: '43',
                        unstableTotalLow: '39',
                        unstableTotalNormal: '6',
                        useStableBuildAsReference: true
                    ])
                }
            }
        }


        stage('Deploy: migrate revision folder') {
            when {
                expression {
                    return  (currentBuild.result == null || currentBuild.result == 'SUCCESS') && (env.GIT_REMOTELESS_BRANCH in ['production', 'master', 'staging', 'dev'])
                }
            }
            steps {
                echo "Deploy: Migrating revision folder to deploy target"
                script {
                    sh 'mkdir "${DEPLOYED_ROOT}" || true'
                    dir("${env.DEPLOYED_ROOT}"){
                        sh '(cd "${WORKSPACE}"; tar -c --exclude=.git -f - ./) | (cd "${DEPLOYED_ROOT}/" && tar xBf -)'
                    }
                }

            }
        }

        stage('Deploy: copy database') {
            when {
                expression {
                    return  (currentBuild.result == null || currentBuild.result == 'SUCCESS') && (env.GIT_REMOTELESS_BRANCH in ['staging', 'dev'] && 1 > 2)
                }
            }
            steps {
                echo "Deploy: creating copy of production database"
                script {
                    def original_db_name = "${env.PGDATABASE}"
                    def copy_db_name = "${env.PGDATABASE}-${env.GIT_REMOTELESS_BRANCH}"
                    def pgpass_file = '/var/lib/jenkins/.pgpass'

                    sh "echo > ${pgpass_file}"
                    sh "chmod go-rwx ~/.pgpass"
                    for (db_name in [original_db_name, copy_db_name]) {
                        sh "set + x ; echo '${env.POSTGRESQL_DB_HOST}:${env.POSTGRESQL_DB_PORT}:${db_name}:${env.POSTGRESQL_DB_ACCESS_USR}:${env.POSTGRESQL_DB_ACCESS_PSW}' >> ${pgpass_file}"
                    }
                    def pg_options = "-h ${env.POSTGRESQL_DB_HOST} -p ${env.POSTGRESQL_DB_PORT} -U ${env.POSTGRESQL_DB_ACCESS_USR} -w"
                    withEnv(["PGPASSFILE=${pgpass_file}"]) {
                        // sh "dropdb --maintenance-db ${pg_options} ${copy_db_name} || true"
                        // sh "createdb ${pg_options} -T template0 -E utf-8 -l ru_RU.UTF-8 -O playhard ${copy_db_name} || true"
                        sh "pg_dump ${pg_options} -F c ${original_db_name} | pg_restore ${pg_options} -c -d ${copy_db_name}"
                    }

                    if (env.DJANGO_MEDIA_ROOT) {
                        def copy_media_root = "${env.DJANGO_MEDIA_ROOT}-${env.GIT_REMOTELESS_BRANCH}"
                        sh "mkdir -p ${copy_media_root} || true"
                        sh "cp -Rl ${env.DJANGO_MEDIA_ROOT}/* ${copy_media_root}/"
                    }

                }

            }
        }


        stage('Deploy: manage and configure') {
            when {
                expression {
                    return (currentBuild.result == null || currentBuild.result == 'SUCCESS') && env.GIT_REMOTELESS_BRANCH in ['production', 'master', 'staging', 'dev']
                }
            }

            environment {
                LC_ALL = 'ru_RU.UTF-8'
                LC_LANG = 'ru_RU.UTF-8'
                LANG = 'ru_RU.UTF-8'
        	}
            steps {
                echo "Deploy: Preparing project"
                script {
                    def relative_static_root = "${env.DJANGO_RELATIVE_STATIC_ROOT}".trim()
                    def absolute_static_root = "${env.DJANGO_STATIC_ROOT}".trim()
                    dir("${env.DEPLOYED_ROOT}") {
                        sh "mkdir locale || true"
                        withExistingVenv {
                            venvRunCmd('./manage.py migrate --noinput')
                            venvRunCmd('./manage.py collectstatic --noinput --clear')
                            venvRunCmd('./manage.py compress')
                            venvRunCmd('./manage.py compilemessages')
                            venvRunCmd('./manage.py createinitialrevisions')
                            // venvRunCmd('./manage.py thumbnail clear_delete_all || true')
                        }

                        if ("${env.SENTRY_AUTH_TOKEN}".trim() && "${env.SENTRY_ORG}".trim() && "${env.SENTRY_PROJECT}".trim()) {
                            echo "Sentry API auth token, project and org are available -> Try to publish release info..."
                            sh "sentry-cli releases new ${env.BUILD_NUMBER}"
                            def source_maps_dir = "${env.DJANGO_COMPRESS_SOURCE_MAPS_DIR}".trim()
                            def static_url = "${env.DJANGO_STATIC_URL}".trim()
                            def static_root = ''
                            if (relative_static_root) {
                                static_root = "${env.DEPLOYED_ROOT}/${relative_static_root}"
                            } else if (absolute_static_root) {
                                static_root = absolute_static_root
                            }
                            if (
                                source_maps_dir && static_url && fileExists("${static_root}/${source_maps_dir}/")
                            ) {
                                echo "Uploading source-maps..."
                                sh "sentry-cli releases files ${env.BUILD_NUMBER} upload-sourcemaps --url-prefix '~${static_url}${source_maps_dir}/' '${static_root}/${source_maps_dir}/' || true"
                            }
                            sh "sentry-cli releases deploys ${env.BUILD_NUMBER} new -e ${env.GIT_REMOTELESS_BRANCH}"
                        }
                    }

                    withExistingVenv {

                        echo "Saving env on deployment target"

                        def deploy_props = readProperties([
                            defaults: readProperties([file: 'deployment/default_deploy.properties']),
                            file: "${env.CUSTOM_PROPS_FILE}"
                        ])
                        def props_for_env = []
                        for (entry in deploy_props) {
                            if (entry.key != 'PIP_EXTRA_REQS') {
                                props_for_env.add("${entry.key}=${entry.value}");
                            }
                        }

                        def env_pattern = '^(POSTGRESQL_.*|LC_.*|VIRTENV_DIR|DEPLOYED_ROOT|BUILD_NUMBER|PYTHONIOENCODING|GIT_BRANCH|GIT_REMOTELESS_BRANCH)\\=.*'
                        def all_env_data = sh([
                            returnStdout: true,
                            script: "set +x ; printenv | grep -E '${env_pattern}'"
                        ]) + '\n' + props_for_env.join('\n')

                        if (env.GIT_REMOTELESS_BRANCH == 'master' || env.GIT_REMOTELESS_BRANCH == 'production') {
                            echo "Not using suffix"
                            env.DEPLOYMENT_SUFFIX = ''
                        } else {
                            echo "Using suffix"
                            env.DEPLOYMENT_SUFFIX = "-${env.GIT_REMOTELESS_BRANCH}"
                        }

                        writeFile file: "${env.CONFIGS_ROOT}/uwsgi/env-vars/${env.BUILD_NUMBER}.conf", text: all_env_data

                        echo "Environment and properties saved"

                        echo "Configure uwsgi vassal"

                        dir("${env.CONFIGS_ROOT}/uwsgi/") {

                            sh "rm './vassals-enabled/${env.BUILD_NUMBER}.ini' || true"
                            sh "erb build_number=${env.BUILD_NUMBER} wsgi_module_import_path=${env.WSGI_MODULE_IMPORT_PATH} virtenv_dir=${env.VIRTENV_DIR} revisions_root=${env.REVISIONS_ROOT} uwsgi_configs_root=${env.CONFIGS_ROOT}/uwsgi/ configs_suffix=${env.DEPLOYMENT_SUFFIX} '${env.DEPLOYED_ROOT}/deployment/confs/uwsgi/vassal.ini.erb' > './vassals-enabled/${env.BUILD_NUMBER}.ini'"
                        }
                    }
                }
            }
        }

        stage('cleanup') {
            when {
                expression {
                    return (currentBuild.result == null || currentBuild.result == 'SUCCESS') && "${env.GIT_REMOTELESS_BRANCH}" in ['production', 'master', 'staging', 'dev']
                }
            }
            steps {
                echo "cleanup previous deploys"
                script {
                    def num_other_revs_to_keep = 2
                    def items_to_keep = ['current', 'sentry', 'production', 'master', 'staging', "${env.GIT_REMOTELESS_BRANCH}", "${env.BUILD_NUMBER}", 'dev']
                    def num_revs_to_keep = num_other_revs_to_keep + items_to_keep.size()

                    dir ("${env.REVISIONS_ROOT}") {
                        items_to_keep.addAll(
                            sh([
                                returnStdout: true,
                                script: "ls -d ${items_to_keep.join(' ')} | xargs readlink || true"
                            ]).tokenize('\n')
                        )
                    }

                    items_to_keep.addAll(
                        sh([
                            returnStdout: true,
                            script: "ls ${env.REVISIONS_ROOT} | grep -E \"[0-9]+\$\" | sort -rn | head -n ${num_revs_to_keep}"
                        ]).tokenize('\n')
                    )
                    echo "Items to keep: ${items_to_keep}"

                    def items_to_clean = []
                    items_to_clean.addAll(
                        sh([
                            returnStdout: true,
                            script: "ls ${env.REVISIONS_ROOT} | grep -E \"[0-9]+\" | grep -E -v '(${items_to_keep.join('|')})' || true"
                        ]).tokenize('\n')
                    )
                    echo "Items to clean: ${items_to_clean}"

                    if (items_to_clean) {
                        echo "Cleaning old revisions except ${items_to_keep}"

                        def clean_args = items_to_clean.inject([]) {
                            result, item ->
                            result.addAll(["${item}", "${item}.conf", "${item}.log", "${item}.pid", "${item}.ini", "${env.VIRTENV_NAME}_${item}"])
                            result
                        }

                        def dirs_to_clean = [
                            "${env.REVISIONS_ROOT}",
                            "${env.VIRTUALENV_REVISIONS_ROOT}",
                            "${env.CONFIGS_ROOT}/uwsgi/vassals-enabled",
                            "${env.CONFIGS_ROOT}/uwsgi/env-vars",
                            "${env.UWSGI_RUNTIME_ROOT}/logs",
                            "${env.UWSGI_RUNTIME_ROOT}/pids",
                            "${env.UWSGI_RUNTIME_ROOT}/sockets"
                        ]
                        for (target_dir in dirs_to_clean) {
                            dir(target_dir) {
                                sh "rm -rf ${clean_args.join(' ')} || true"
                            }
                        }
                    }
                }
            }
        }
    }

    post {
        success {
            script {
                dir("${env.REVISIONS_ROOT}") {
                    sh "rm './${env.GIT_REMOTELESS_BRANCH}' || true"
                    sh "ln -s '${env.BUILD_NUMBER}' '${env.GIT_REMOTELESS_BRANCH}'"
                }

                dir("${env.VIRTUALENV_REVISIONS_ROOT}") {
                    sh "rm '${env.VIRTUALENV_REVISIONS_ROOT}/${env.VIRTENV_NAME}_${env.GIT_REMOTELESS_BRANCH}' || true"
                    sh "ln -s '${env.VIRTENV_DIR}' '${env.VIRTUALENV_REVISIONS_ROOT}/${env.VIRTENV_NAME}_${env.GIT_REMOTELESS_BRANCH}'"
                }

                dir("${env.DEPLOYED_ROOT}") {
                    sh "ln -s '${env.CONFIGS_ROOT}/uwsgi/env-vars/${env.BUILD_NUMBER}.conf' .extra_env.conf"
                }

                dir("${env.CONFIGS_ROOT}/uwsgi/") {

                    sh "rm './env-vars/${env.GIT_REMOTELESS_BRANCH}.conf' || true"
                    sh "ln -s './${env.BUILD_NUMBER}.conf' './env-vars/${env.GIT_REMOTELESS_BRANCH}.conf'"

                    sh "rm ./vassals-enabled/${env.GIT_REMOTELESS_BRANCH}.ini || true"
                    sh "ln -s './${env.BUILD_NUMBER}.ini' './vassals-enabled/${env.GIT_REMOTELESS_BRANCH}.ini'"

                    for (config_name in ['emperor', 'vassals-default']) {
                        sh "rm './vassals-enabled/${config_name}${env.DEPLOYMENT_SUFFIX}.ini' || true"
                        sh "erb uwsgi_configs_root=${env.CONFIGS_ROOT}/uwsgi uwsgi_runtime_root=${env.UWSGI_RUNTIME_ROOT} configs_suffix=${env.DEPLOYMENT_SUFFIX} '${env.REVISIONS_ROOT}/${env.BUILD_NUMBER}/deployment/confs/uwsgi/${config_name}.ini.erb' > './${config_name}${env.DEPLOYMENT_SUFFIX}.ini'"
                    }
                }
                dir("${env.CONFIGS_ROOT}/nginx/") {
                    sh "cp \"${env.REVISIONS_ROOT}/${env.BUILD_NUMBER}/deployment/confs/nginx/\"*.conf ./"
                }
                cleanWs notFailBuild: true
            }
        }
        failure {
            script {
                sh "rm -rf '${env.REVISIONS_ROOT}/${env.BUILD_NUMBER}' || true"
                sh "rm -rf '${env.CONFIGS_ROOT}/uwsgi/env-vars/${env.BUILD_NUMBER}.conf' || true"
                sh "rm -rf '${env.VIRTUALENV_REVISIONS_ROOT}/${env.VIRTENV_NAME}_${env.BUILD_NUMBER}' || true"
            }
        }
        always {
            sh 'rm -rf /tmp/pip-*-build || true'
            sh "echo > /var/lib/jenkins/.pgpass"
            script {
                sh "rm -rf '${env.REVISIONS_ROOT}/${env.BUILD_NUMBER}@tmp' || true"
                sh "rm -rf '${env.CONFIGS_ROOT}/*@tmp'"
                sh "rm -rf '${env.CONFIGS_ROOT}/nginx/*@tmp'"
                sh "rm -rf '${env.CONFIGS_ROOT}/uwsgi/*@tmp'"
                sh "rm -rf '${env.CONFIGS_ROOT}/uwsgi/env-vars/*@tmp'"
                sh "rm -rf '${env.CONFIGS_ROOT}/uwsgi/vassals-enabled/*@tmp'"
                sh "rm -rf '${env.REVISIONS_ROOT}*@tmp'"
                sh "rm -rf '${env.VIRTUALENV_REVISIONS_ROOT}*@tmp'"
                sh "rm -rf '${env.UWSGI_RUNTIME_ROOT}/*@tmp'"
                sh "rm -rf '${env.UWSGI_RUNTIME_ROOT}/logs/*@tmp'"
                sh "rm -rf '${env.UWSGI_RUNTIME_ROOT}/pids/*@tmp'"
                sh "rm -rf '${env.UWSGI_RUNTIME_ROOT}/sockets/*@tmp'"
            }
        }
    }
}
