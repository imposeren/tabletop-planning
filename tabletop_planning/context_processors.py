# -*- coding: utf-8 -*-
from tabletop_planning.utils import config_value


def default_context(request):
    from django.conf import settings
    context = {
        'PROJECT_NAME': config_value('tabletop_planning', 'PROJECT_NAME'),
        'CURRENCY': config_value('tabletop_planning', 'CURRENCY'),
        'SHOW_DISQUS': config_value('tabletop_planning', 'SHOW_DISQUS'),
        'SHOW_VK_COMMENTS': config_value('tabletop_planning', 'SHOW_VK_COMMENTS'),
        'SMALL_GAME_IMAGE_GEOMETRY': '60x60',
        'BIGGER_GAME_IMAGE_GEOMETRY': '155x165',
        'BIGGEST_GAME_IMAGE_GEOMETRY': '155x175',
        'MASTER_XP_URL': None,
        'PLAYER_XP_URL': None,
        'RAVEN_ENVIRONMENT': getattr(settings, 'RAVEN_CONFIG', {}).get('environment'),
        'RAVEN_RELEASE': getattr(settings, 'RAVEN_CONFIG', {}).get('release'),
        'GOOGLE_ANALYTICS_PROPERTY_ID': getattr(settings, 'GOOGLE_ANALYTICS_PROPERTY_ID', ''),
    }

    return context
