# -*- coding: utf-8 -*-
from django.apps import AppConfig


class MainAppConfig(AppConfig):
    name = 'tabletop_planning'
    verbose_name = "Tabletop Main"
