# -*- coding: utf-8 -*-

# python builtin:
import hashlib
import json
import os
import sys
import uuid
from copy import copy

# django:
from django.conf import settings
from django.contrib.admin.filters import SimpleListFilter
from django.core.management.base import BaseCommand
from django.db.models import F
from django.utils.translation import ugettext_lazy as _

# third-parties
from post_office import mail


def get_cache_key(*args, **kwargs):
    """Get hash data for args and kwargs.

    All args should have string representation available. Suitable for cahce keys
    or for detecting changes to data.

    """
    hasher = hashlib.sha224()
    for arg in args:
        string = (u'%s\n' % arg).encode('utf-8')
        hasher.update(string)
    for key, val in kwargs.items():
        string = (u'%s:%s\n' % (key, val))
        hasher.update(string)
    return hasher.hexdigest()


class multi_method(object):  # noqa: N801

    """Return different objects for class attribute access and instance attribute access.

    Example:
        .. sourcecode:: python

          class Foo(object):
              def inst_bar(self):
                  print('instance method')

              def cls_bar(cls):
                  print('class method')

              bar = multi_method(cls_bar, inst_bar)

          Foo.bar()  # prints 'class method'
          foo = Foo()
          foo.bar()  # prints 'instance method'

    """

    def __init__(self, class_method, instance_method):
        self.class_method = class_method
        self.instance_method = instance_method

    def __get__(self, obj, parent):
        if parent is None:
            objtype = type(obj)
        else:
            objtype = parent
        if obj is None:
            actual_func = self.class_method
        else:
            actual_func = self.instance_method

        def newfunc(*args, **kwargs):
            return actual_func(objtype, *args, **kwargs)
        newfunc.__name__ = actual_func.__name__
        newfunc.__doc__ = actual_func.__doc__
        newfunc.__dict__.update(actual_func.__dict__)
        return newfunc


def make_formset_data(settings, forms, prefix='form'):
    data = {}
    for k in settings:
        data[prefix+'-'+k.upper()] = settings[k]
    for (n, form) in enumerate(forms):
        for k in form:
            val = form[k]
            if hasattr(val, 'pk'):
                val = val.pk
            data[prefix+'-'+str(n)+'-'+k] = val
    return data


class NullFilterSpec(SimpleListFilter):
    title = u''

    parameter_name = u''
    query_parameter = u''

    against_value = None

    set_text = _(u'Yes')
    set_val = '1'

    unset_text = _(u'No')
    unset_val = '0'

    def lookups(self, request, model_admin):
        return (
            (self.set_val, self.set_text, ),
            (self.unset_val, self.unset_text, ),
        )

    def queryset(self, request, queryset):
        kwargs = {
            ('%s' % (self.query_parameter or self.parameter_name)): self.against_value,
        }
        if self.value() == '0':
            queryset = queryset.filter(**kwargs).distinct()
        if self.value() == '1':
            queryset = queryset.exclude(**kwargs).distinct()
        return queryset


class DisqusIDMixin(object):
    @classmethod
    def disqus_id_prefix(cls):
        meta = cls._meta
        return '%s.%s-' % (meta.app_label, meta.model_name)

    @property
    def disqus_id(self):
        return '%s%s' % (self.disqus_id_prefix(), self.id)

    @property
    def dusqus_title(self):
        return str(self)


def mail_send(*args, **kwargs):
    tags = kwargs.pop('tags', '')
    if not isinstance(tags, str):
        tags = u','.join(tags)

    recipients_in_args = False
    if args:
        recipients_in_args = True
        recipients = args[0]
    else:
        recipients = kwargs.get('recipients')

    headers = kwargs.pop('headers', {})
    headers['X-MC-Tags'] = tags

    add_lone_recipient_to_context = 'context' in kwargs and 'recipient' not in kwargs['context']

    for recipient in recipients:
        iter_args = list(args)
        iter_kwargs = copy(kwargs)
        if 'context' in kwargs:
            iter_kwargs['context'] = dict(kwargs['context'])

        uid = uuid.uuid1().hex
        tmp_headers = dict(headers)
        tmp_headers['X-MC-Metadata'] = json.dumps({'uid': uid})
        tmp_headers['X-MJ-CUSTOMID'] = uid
        if recipients_in_args:
            iter_args[0] = [recipient]
        else:
            iter_kwargs['recipients'] = [recipient]

        if add_lone_recipient_to_context:
            iter_kwargs['context']['recipient'] = recipient

        mail.send(
            headers=tmp_headers,
            *iter_args, **iter_kwargs
        )


def get_gspread_client():
    import gspread
    from oauth2client.client import SignedJwtAssertionCredentials

    json_key = json.load(open(os.path.join(settings.MEDIA_ROOT, 'playhard_oauth.json')))
    scope = ['https://spreadsheets.google.com/feeds', 'https://www.googleapis.com/auth/drive.file']
    credentials = SignedJwtAssertionCredentials(json_key['client_email'], json_key['private_key'], scope)

    return gspread.authorize(credentials)


def get_statistics_for_interval(start_date, end_date, tag_stats_field):
    from games.models import SessionVisit
    from users.models import User
    from tags.models import SystemTag, SettingTag, OtherTag

    week_days = {
        'sunday': 1,
        'monday': 2,
        'tuesday': 3,
        'wednesday': 4,
        'thursday': 5,
        'friday': 6,
        'saturday': 7,
    }

    users = User.objects.all()
    approved_visits = SessionVisit.objects.filter(have_visited=True, verified=True)

    if start_date and end_date:
        approved_visits = (
            approved_visits
            .filter(cached_start_date__gte=start_date, cached_start_date__lte=end_date)
        )
    real_start_date = approved_visits.order_by('cached_start_date').values_list('cached_start_date', flat=True).first()
    real_end_date = approved_visits.order_by('-cached_start_date').values_list('cached_start_date', flat=True).first()

    player_visits = (
        approved_visits
        .filter(role=SessionVisit.ROLES.player)
        .order_by('pk').only('pk').distinct()
    )

    period_data = {}

    period_data['top_masters_by_sessions'] = list(
        users
        .annotate_session_count(start_date, end_date, SessionVisit.ROLES.master)
        .order_by('-sessions_count', 'pk').distinct()[:10]
    )

    for user in period_data['top_masters_by_sessions']:
        user.mastered_player_visits = user.get_mastered_player_visits(start_date, end_date) or 0

    period_data['top_players_by_sessions'] = (
        users
        .annotate_session_count(start_date, end_date, SessionVisit.ROLES.player)
        .order_by('-sessions_count', 'pk').distinct()[:10]
    )

    period_data['total_sessions'] = approved_visits.values_list('game_session').distinct().count()

    period_data['masters_active_count'] = (
        approved_visits
        .filter(role=SessionVisit.ROLES.master)
        .values_list('visitor').distinct().count()
    )

    if period_data['total_sessions'] and period_data['masters_active_count']:
        period_data['sessions_per_master'] = 1.0 * period_data['total_sessions'] / period_data['masters_active_count']
    else:
        period_data['sessions_per_master'] = 0

    period_data['total_player_visits'] = player_visits.count()
    if period_data['total_player_visits'] and period_data['total_sessions']:
        period_data['avg_players'] = 1.0 * period_data['total_player_visits'] / period_data['total_sessions']
    else:
        period_data['avg_players'] = 0.0

    if real_start_date and real_end_date:
        period_delta = real_end_date - real_start_date
        week_multiplier = 7.0 / (period_delta.days + 1)
        for week_day_name, week_day_number in week_days.items():
            data_key = '%s_sessions_count' % week_day_name
            period_data[data_key] = (
                approved_visits
                .filter(cached_start_date__week_day=week_day_number)
                .values_list('game_session')
                .distinct()
                .count()
            ) * week_multiplier

    if tag_stats_field:
        models_conf = (
            (SystemTag, 'top_system_tags_by_visits'),
            (SettingTag, 'top_setting_tags_by_visits'),
            (OtherTag, 'top_other_tags_by_visits'),
        )
        order_by = [f'-{tag_stats_field}']
        if tag_stats_field != 'visits_count_all':
            order_by.append('-visits_count_all')
        order_by.append('name')
        for tag_model, key in models_conf:
            period_data[key] = list(
                tag_model.objects
                .filter(**{f'{tag_stats_field}__gte': 1})
                .annotate(target_count=F(tag_stats_field))
                .order_by(*order_by)[:10]
            )

    return period_data


class LoggedCommand(BaseCommand):

    """Ensure raven logging in manage commands.

    Raven should autopatch management commands to use logging but sometimes it does not.
    Explicit use of this class as base command solves this problem.
    """

    def execute(self, *args, **kwargs):
        try:
            return super(LoggedCommand, self).execute(*args, **kwargs)
        except Exception:
            from raven.contrib.django.models import client
            client.captureException(extra={
                'argv': sys.argv
            })
            raise

    # ensure that raven automatic patching does not patch this command
    execute.__raven_patched = True


def config_value(group, option_name, default=None):
    """Return config value (call args similar to livesettings)."""
    from dynamic_preferences.registries import global_preferences_registry
    global_preferences = global_preferences_registry.manager()
    return global_preferences[group + '__' + option_name.lower()]


def get_signed_data_processor_salt():
    return getattr(settings, 'TABLETOP_SIGNED_DATA_PROCESSOR_SALT', 'signed-data-processor')
