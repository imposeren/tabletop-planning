try:
    from django.forms import ClearableFileInput  # noqa
except ImportError:
    from sorl.thumbnail.admin.compat import AdminClearableImageWidget as ImageWidget
else:
    from sorl.thumbnail.admin.current import AdminImageWidget as ImageWidget


__all__ = ('ImageWidget',)
