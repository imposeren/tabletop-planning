# -*- coding: utf-8 -*-
import sys

from settings_default import *


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'playhard',
        'USER': 'playhard',
        'PASSWORD': 'playhard',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    }
}

SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = ''
SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = ''

SOCIAL_AUTH_VK_OAUTH2_KEY = ''
SOCIAL_AUTH_VK_OAUTH2_SECRET = ''

SOCIAL_AUTH_FACEBOOK_KEY = ''
SOCIAL_AUTH_FACEBOOK_SECRET = ''

DEBUG = True
EMAIL_PORT = 1025

# INSTALLED_APPS += ('debug_toolbar', )

DEBUG_TOOLBAR_PANELS = (
    'debug_toolbar.panels.versions.VersionsPanel',
    'debug_toolbar.panels.timer.TimerPanel',
    'debug_toolbar.panels.sql.SQLPanel',
    'debug_toolbar.panels.templates.TemplatesPanel',
    'debug_toolbar.panels.cache.CachePanel',
    'debug_toolbar.panels.signals.SignalsPanel',
    'debug_toolbar.panels.logging.LoggingPanel',
    'template_timings_panel.panels.TemplateTimings.TemplateTimings',
    #'debug_toolbar.panels.profiling.ProfilingPanel',
    #'debug_toolbar_line_profiler.panel.ProfilingPanel',
)

THUMBNAIL_DEBUG = True
THUMBNAIL_ENGINE = 'sorl.thumbnail.engines.wand_engine.Engine'

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
        'KEY_PREFIX': 'playhard',
        'VERSION': '2014-09-28',
        'MAX_ENTRIES': 1500,
        'CULL_FREQUENCY': 5,
    }
}

GOOGLE_ANALYTICS_PROPERTY_ID = 'UA-12345-6'


COMPRESS_ENABLED = True
COMPRESS_OFFLINE = True
COMPRESS_OFFLINE_CONTEXT = {
    'template': 'base.html',
    'GOOGLE_ANALYTICS_PROPERTY_ID': GOOGLE_ANALYTICS_PROPERTY_ID,
}

COMPRESS_JS_FILTERS = [
    # 'compressor.filters.jsmin.JSMinFilter',
    'tabletop_tools.compressor_filters.SourceMappingClosureCompilerFilter'
]


COMPRESS_CLOSURE_COMPILER_BINARY = '/usr/bin/closure-compiler'

if 'test' in sys.argv:
    DEBUG = False
    TESTING = True
    # DATABASES = {
    #     'default': {
    #         'ENGINE': 'django.db.backends.sqlite3',
    #         'NAME': ':memory:',
    #     }
    # }
    CELERY_ALWAYS_EAGER = True
    CELERY_EAGER_PROPAGATES_EXCEPTIONS = True

    CACHES = {
        'default': {
            'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
            'LOCATION': '127.0.0.1:11211',
            'KEY_PREFIX': 'playhard-test',
            'VERSION': '2014-09-28',
            'MAX_ENTRIES': 1500,
            'CULL_FREQUENCY': 5,
        }
    }
else:
    MIDDLEWARE = ('silk.middleware.SilkyMiddleware', ) + MIDDLEWARE

    INSTALLED_APPS = ('silk', ) + INSTALLED_APPS
