# -*- coding: utf-8 -*-
from django.http import HttpResponse
from django.conf import settings


class IgnoreOpenshiftExceptions(object):
    def process_exception(self, request, exception):
        client_ip = request.META.get('HTTP_X_FORWARDED_FOR')
        if not settings.DEBUG and (not client_ip or client_ip.startswith('10.') or client_ip.startswith('127.')):
            return HttpResponse("Service temporary down", content_type="text/plain")
