# -*- coding: utf-8 -*-
import datetime

from django import template
from django.core import signing

from tabletop_planning.utils import config_value, get_signed_data_processor_salt

try:
    from django.core.urlresolvers import reverse
except ImportError:
    from django.urls import reverse

register = template.Library()


@register.inclusion_tag('blocks/image_div_tag.html')
def image_block(image, geometry, crop='center', no_image_text='no image'):
    """Render image padded in inline-block.

    Example:

    {% image_block game.image "300x500" %}

    Note: holder.js is used when image is not available
    """
    geometry = str(geometry)
    x, y = '', ''
    parts = geometry.split(u'x')
    try:
        x = parts[0]
    except IndexError:
        raise template.TemplateSyntaxError("wrong geometry specification")

    if len(parts) > 1:
        y = parts[1]
    else:
        y = ''

    if x and y:
        geometry = u"{0}x{1}".format(x, y)
        empty_geometry = geometry
    elif x:
        geometry = x
        empty_geometry = u"{0}x{0}".format(x)
    else:
        geometry = u'x{0}'.format(y)
        empty_geometry = u"{0}x{0}".format(y)

    should_upscale = bool(
        image and (image.width and image.height) and (image.width >= int(x) or (y and image.height >= int(y)))
    )

    if should_upscale:
        upscale = True
    else:
        upscale = False

    big_geometry = None

    if x and y:
        x, y = int(x), int(y)
        if x*y <= 1000*1000:
            big_geometry = u"{0}x{1}".format(int(x * 2), int(y * 2))

    return {
        'geometry': geometry, 'image': image, 'crop': crop, 'width': x, 'height': y,
        'no_image_text': no_image_text, 'upscale': upscale, 'empty_geometry': empty_geometry,
        'big_geometry': big_geometry,
    }


@register.filter
def with_domain(url, request=None):
    """Rturn full url with protocol and domain (e.g. http://ph.kiev.ua/{{ url }}).

    when called from templates that have access to url it's better to call it with request as argument::

        {% load common_tags %}
        {{ item.get_absolute_url|with_domain:request }}

    if you are using this filter from template that have no access to request (e.g. from mail templates) than it will
    use ``tabletop_planning.PROJECT_URL`` setting from livesettings:

        {% load common_tags %}
        {{ item.get_absolute_url|with_domain }}

    """
    if url.lower().find('http://') == 0 or url.lower().find('https://') == 0:
        result = url
    elif request:
        result = request.build_absolute_uri(url)
    else:
        result = config_value('tabletop_planning', 'PROJECT_URL') + url
        result = result.replace(u'//', u'/')
        result = result.replace(u'http:/', u'http://')
        result = result.replace(u'https:/', u'https://')
    return result


@register.filter
def get_field_name(instance, field_name):
    """Return verbose_name for a field of some model instance."""
    return instance._meta.get_field(field_name).verbose_name.title()


@register.filter
def timeperiod(event_datetime):
    hour = event_datetime.hour + event_datetime.minute * 0.0166
    if hour < 9:
        period = 0
    if 9 <= hour < 14:
        period = 1
    elif 14 <= hour < 18:
        period = 2
    elif 18 <= hour < 22:
        period = 3
    else:
        period = 4
    return str(period)


@register.simple_tag(takes_context=True)
def get_unsubscribe_link(context, loginless=None):
    """Return absolute URI to email-unsubscribe view.

    If loginless is True or is an email or is a user object, then 'signed_data_processor' URL is used
    with data that will cause target view to turn off notifications for some email address.
    Email address is determined using next alternative methods:

    * Taken from `loginless` argument. If this is a user then value of it's "email" field is used.
    * Taken from context variable "recipient"
    * User is taken from context variable "user" and it's "email" field is used

    If loginless is False then 'users:email_unsubscribe' URL is used.

    """
    def is_user_with_email(candidate):
        return getattr(candidate, 'is_authenticated', False) and ('@' in getattr(candidate, 'email', ''))

    email = None

    if loginless:
        if isinstance(loginless, (str, bytes)) and '@' in loginless:
            email = loginless
        elif is_user_with_email(loginless):
            email = loginless.email
        elif is_user_with_email(context.get('user', None)):
            email = context['user'].email
        elif '@' in context.get('recipient', ''):
            email = context['recipient']

    if email:
        data = signing.dumps(
            {'action': 'users.email_unsubscribe', 'context': {'email': email}},
            compress=True,
            salt=get_signed_data_processor_salt(),
        )
        url = reverse('signed_data_processor', args=[data])
    else:
        url = reverse('users:email_unsubscribe')

    return with_domain(
        url=url,
        request=context.get('request')
    )


@register.filter
def round_datetime(value, round_seconds=1):
    round_seconds = int(round_seconds)
    for coeff in (1, -1):
        alt_value = value + datetime.timedelta(seconds=coeff * round_seconds)
        if alt_value.second == 0:
            return alt_value
    return value
