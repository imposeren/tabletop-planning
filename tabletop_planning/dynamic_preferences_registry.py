# -*- coding: utf-8 -*-

from django.utils.translation import ugettext_lazy as _

from dynamic_preferences.types import BooleanPreference, IntegerPreference, StringPreference
from dynamic_preferences.preferences import Section
from dynamic_preferences.registries import global_preferences_registry


main = Section('tabletop_planning')


@global_preferences_registry.register
class ProjectName(StringPreference):
    section = main
    name = 'project_name'
    default = 'PlayHard'
    help_text = _(u"Будет использовано в заголовке сайта.")


@global_preferences_registry.register
class ProjectUrl(StringPreference):
    section = main
    name = 'project_url'
    default = 'http://playhard.kiev.ua/'
    help_text = _(u"Полный URL проекта.")


@global_preferences_registry.register
class GMModeratorsGroupname(StringPreference):
    section = main
    name = 'gm_moderators_groupname'
    default = 'GM Managers'
    help_text = _(u"Имя группы модераторов для мастеров.")


@global_preferences_registry.register
class AccountingModeratorsGroupname(StringPreference):
    section = main
    name = 'accounting_moderators_groupname'
    default = 'Accounting Managers'
    help_text = _(u"Имя грыппы модераторов для опыта и финансов.")


@global_preferences_registry.register
class RequireGMConfirmation(BooleanPreference):
    section = main
    name = 'require_gm_confirmation'
    default = True
    help_text = _(u"Необходимо подтверждение статуса мастера.")


@global_preferences_registry.register
class NewGMRenotificationDelay(IntegerPreference):
    section = main
    name = 'new_gm_renotification_delay'
    default = 3
    help_text = _(u"Минимальное время между повторным уведомлением о запросе стать мастером (дни).")


@global_preferences_registry.register
class Currency(StringPreference):
    section = main
    name = 'currency'
    default = 'грн'
    help_text = _(u"Минимальное время между повторным уведомлением о запросе стать мастером (дни).")


@global_preferences_registry.register
class NonSpamHoursInterval(IntegerPreference):
    section = main
    name = 'non_spam_hours_interval'
    default = 3
    help_text = _(u'"безопасный" период между уведомлениями (в часах).')


@global_preferences_registry.register
class ShowDisqus(BooleanPreference):
    section = main
    name = 'show_disqus'
    default = False
    help_text = _(u"Показывать комментарии disqus")


@global_preferences_registry.register
class ShowVKComments(BooleanPreference):
    section = main
    name = 'show_vk_comments'
    default = True
    help_text = _(u"Показывать комментарии vk.com.")
