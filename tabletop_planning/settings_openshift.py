# -*- coding: utf-8 -*-
import sys
import socket

from .settings_default import *  # noqa
from os import environ as env
import os


EMAIL_USE_SSL = True
EMAIL_HOST = 'smtp.zoho.com'
EMAIL_PORT = 465
EMAIL_HOST_USER = env.get('EMAIL_USER')
EMAIL_HOST_PASSWORD = env.get('EMAIL_PASSWORD')
SERVER_EMAIL = DEFAULT_FROM_EMAIL = env.get('EMAIL_USER')


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': env.get('PGDATABASE'),
        'USER': env.get('OPENSHIFT_POSTGRESQL_DB_USERNAME'),
        'PASSWORD': env.get('OPENSHIFT_POSTGRESQL_DB_PASSWORD'),
        'HOST': env.get('OPENSHIFT_POSTGRESQL_DB_HOST'),
        'PORT': env.get('OPENSHIFT_POSTGRESQL_DB_PORT'),
    }
}

SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = env.get('DJANGO_SA_GOOGLE_KEY')
SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = env.get('DJANGO_SA_GOOGLE_SECRET')

SOCIAL_AUTH_VK_OAUTH2_KEY = env.get('DJANGO_SA_VK_KEY')
SOCIAL_AUTH_VK_OAUTH2_SECRET = env.get('DJANGO_SA_VK_SECRET')

SOCIAL_AUTH_FACEBOOK_KEY = env.get('DJANGO_SA_FACEBOOK_KEY')
SOCIAL_AUTH_FACEBOOK_SECRET = env.get('DJANGO_SA_FACEBOOK_SECRET')


MEDIA_ROOT = os.path.join(os.environ.get('OPENSHIFT_DATA_DIR'), 'media')
STATIC_ROOT = os.path.join(os.environ.get('OPENSHIFT_REPO_DIR'), 'wsgi', 'static')

STATIC_URL = '/static/'
MEDIA_URL = '/static/media/'


ALLOWED_HOSTS = [
    env.get('OPENSHIFT_APP_DNS'),
    socket.gethostname(),
    'www.playhard.kiev.ua',
    '*',  # some internal openchift services are getting '/' and causing lot's of mailing -> enabling '*'
]


COMPRESS_PRECOMPILERS = (
    (
        'text/less', '%s {infile} {outfile}' % (os.path.join(env.get('OPENSHIFT_DATA_DIR'), 'node_modules/less/bin/lessc'))
    ),
)
COMPRESS_OFFLINE = True


POST_OFFICE = {
    'DEFAULT_PRIORITY': 'now',
}

DEBUG = False
# INSTALLED_APPS += ('debug_toolbar',)


if 'test' in sys.argv:
    DEBUG = False
    TESTING = True
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': ':memory:',
        }
    }
    CELERY_ALWAYS_EAGER = True
    CELERY_EAGER_PROPAGATES_EXCEPTIONS = True
