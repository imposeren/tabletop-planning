# -*- coding: utf-8 -*-
"""
Django settings for tabletop_planning project.

For more information on this file, see
https://docs.djangoproject.com/en/dev/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/dev/ref/settings/
"""

from __future__ import unicode_literals

from django.contrib import messages
from django.utils.translation import ugettext_lazy as _

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
from os import environ as env
BASE_DIR = os.path.dirname(os.path.join(os.path.dirname(__file__), os.path.pardir, os.path.pardir))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/dev/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'qoifhrs1ra9ai2%&nk5=6qw!&4*_gb_^f%a_vl&n4n0=@f+ly4'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []


# Application definition

PROJECT_APPS = (  # used for locale paths
    'tabletop_planning',
    'games',
    'users',
    'tags',
    'accounting',
    'news',
    # 'disqus_sync',
    'tabletop_tools',
)

INSTALLED_APPS = ('raven.contrib.django.raven_compat', ) + PROJECT_APPS + (
    # third-parties
    'dal',
    'dal_select2',
    # 'djcelery',
    'django_extensions',
    'crispy_forms',
    'parsley',
    'social_django',
    'compressor',
    'el_pagination',
    'sorl.thumbnail',
    'sitegate',
    'post_office',
    'taggit',
    'scribbler.apps.ScribblerAppConfig',
    'messages_extends',
    'happenings',
    'tinymce',
    'grappelli.dashboard',
    'grappelli',
    'filebrowser',
    # 'disqus',
    'reversion',
    'dynamic_preferences',
    'analytical',

    # 'django_comments',
    # 'django_comments_xtd',

    # django.contrib:
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'django.contrib.flatpages',
    'django.contrib.postgres',

)

MIDDLEWARE = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'raven.contrib.django.raven_compat.middleware.SentryResponseErrorIdMiddleware',
    'social_django.middleware.SocialAuthExceptionMiddleware',
    # 'tabletop_planning.middleware.IgnoreOpenshiftExceptions',
)

ROOT_URLCONF = 'tabletop_planning.urls'

WSGI_APPLICATION = 'tabletop_planning.wsgi.application'


# Database
# https://docs.djangoproject.com/en/dev/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/dev/topics/i18n/

LANGUAGE_CODE = 'uk'
LOCALE_PATHS = tuple(
    # FIXME: after update to django 1.9 next paths should be removed
    os.path.join(BASE_DIR, app_name, 'locale') for app_name in PROJECT_APPS
)

LANGUAGES = [
    ('ru', _('Русский')),
    ('uk', _('Українська')),
]


TIME_ZONE = 'Europe/Kiev'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/dev/howto/static-files/

STATIC_URL = env.get('DJANGO_STATIC_URL', '/static/')
MEDIA_URL = env.get('DJANGO_MEDIA_URL', '/media/')

if env.get('DJANGO_RELATIVE_STATIC_ROOT'):
    STATIC_ROOT = os.path.join(BASE_DIR, env.get('DJANGO_RELATIVE_STATIC_ROOT'))
else:
    STATIC_ROOT = env.get('DJANGO_STATIC_ROOT') or os.path.join(BASE_DIR, 'varying', 'static')

if env.get('DJANGO_MEDIA_ROOT'):
    MEDIA_ROOT = env.get('DJANGO_MEDIA_ROOT')

    _deployment_suffix = env.get('DEPLOYMENT_SUFFIX', '').strip()
    if _deployment_suffix:
        MEDIA_ROOT = MEDIA_ROOT.rstrip(os.sep) + _deployment_suffix
else:
    MEDIA_ROOT = os.path.join(BASE_DIR, 'varying', 'media')

for path in (STATIC_ROOT, MEDIA_ROOT):
    if not os.path.exists(path):
        os.makedirs(path)

STATICFILES_FINDERS = (
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
    'compressor.finders.CompressorFinder',
)

AUTH_USER_MODEL = 'users.User'

AUTHENTICATION_BACKENDS = (
    # 'social_core.backends.google.GoogleOAuth2',
    'tabletop_planning.social_backends.CustomGoogleOAuth2',
    'social_core.backends.vk.VKOAuth2',
    'social_core.backends.facebook.FacebookOAuth2',
    'social_core.backends.disqus.DisqusOAuth2',

    'django.contrib.auth.backends.ModelBackend',
)


LOGIN_REDIRECT_URL = '/'  # 'games:games_index' is not supported by python-social-auth =()

LOGIN_URL = 'signin'  # 'users:login'

LOGOUT_URL = 'logout'  # 'users:logout'


# Templates configuration
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            # insert your TEMPLATE_DIRS here
        ],
        'OPTIONS': {
            'context_processors': (
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.request',
                'tabletop_planning.context_processors.default_context',
                'social_django.context_processors.backends',
                'social_django.context_processors.login_redirect',
            ),
            'loaders': [
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader',
            ],
            'debug': True,
        },
    },
]

MESSAGE_STORAGE = 'messages_extends.storages.FallbackStorage'

MESSAGE_TAGS = {
    messages.ERROR: 'danger'
}

SITE_ID = 1

EMAIL_BACKEND = 'post_office.EmailBackend'

SERVER_EMAIL = DEFAULT_FROM_EMAIL = 'robot@example.com'

SILENCED_SYSTEM_CHECKS = ['admin.E033']

# third-parties"

# START django-compressor:
COMPRESS_ENABLED = True
COMPRESS_PRECOMPILERS = (
    ('text/less', 'lessc {infile} {outfile}'),
)

COMPRESS_CSS_FILTERS = [
    'compressor.filters.css_default.CssAbsoluteFilter',
    'compressor.filters.cssmin.CSSMinFilter',
]

COMPRESS_JS_FILTERS = [
    'compressor.filters.jsmin.JSMinFilter',
]

COMPRESS_OFFLINE_CONTEXT = {
    'template': 'base.html',
    'GOOGLE_ANALYTICS_PROPERTY_ID': env.get('DJANGO_GOOGLE_ANALYTICS_PROPERTY_ID'),
}

COMPRESS_CLOSURE_COMPILER_ARGUMENTS = '--language_in ECMASCRIPT5'

# END django-compressor

# START sorl.thumbnail:

THUMBNAIL_DEBUG = False
# THUMBNAIL_ENGINE = 'sorl.thumbnail.engines.convert_engine.Engine'
# THUMBNAIL_ENGINE = 'sorl.thumbnail.engines.pil_engine.Engine'
THUMBNAIL_ENGINE = 'sorl.thumbnail.engines.wand_engine.Engine'
THUMBNAIL_FORMAT = 'PNG'
THUMBNAIL_UPSCALE = False
THUMBNAIL_PRESERVE_FORMAT = True
THUMBNAIL_PADDING = False
THUMBNAIL_ALTERNATIVE_RESOLUTIONS = [1.5, 2]

# END sorl.thumbnail:

# START crispy-forms:
CRISPY_TEMPLATE_PACK = 'bootstrap3'
# END crispy-forms


# START python-social-auth

SOCIAL_AUTH_POSTGRES_JSONFIELD = False
SOCIAL_AUTH_USERNAME_IS_FULL_EMAIL = True

SOCIAL_AUTH_LOGIN_ERROR_URL = '/'
SOCIAL_AUTH_ADMIN_USER_SEARCH_FIELDS = ['email', 'first_name', 'last_name']

SOCIAL_AUTH_GOOGLE_OAUTH2_SCOPE = [
    'openid', 'email', 'profile',
    'https://www.googleapis.com/auth/userinfo.email',
    'https://www.googleapis.com/auth/userinfo.profile',
]

SOCIAL_AUTH_GOOGLE_OAUTH2_AUTH_EXTRA_ARGUMENTS = {
    'approval_prompt': 'auto'
}

SOCIAL_AUTH_GOOGLE_OAUTH2_EXTRA_DATA = [
    'refresh_token',
]

SOCIAL_AUTH_FIELDS_STORED_IN_SESSION = [
    'drive_access',
]

SOCIAL_AUTH_VK_OAUTH2_SCOPE = [
    'email',
    # 'photos',
]

SOCIAL_AUTH_FACEBOOK_SCOPE = ['email', 'user_link']
SOCIAL_AUTH_FACEBOOK_PROFILE_EXTRA_PARAMS = {
    'locale': 'ru_RU',
    'fields': 'id, name, email, link, picture',
}
SOCIAL_AUTH_FACEBOOK_API_VERSION = '8.0'

SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = ''
SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = ''

SOCIAL_AUTH_VK_OAUTH2_KEY = ''
SOCIAL_AUTH_VK_OAUTH2_SECRET = ''

SOCIAL_AUTH_FACEBOOK_KEY = ''
SOCIAL_AUTH_FACEBOOK_SECRET = ''

SOCIAL_AUTH_DISQUS_KEY = ''
SOCIAL_AUTH_DISQUS_SECRET = ''
SOCIAL_AUTH_DISQUS_AUTH_EXTRA_ARGUMENTS = {'scope': 'read,email'}

SOCIAL_AUTH_PIPELINE = (
    'social_core.pipeline.social_auth.social_details',
    'social_core.pipeline.social_auth.social_uid',
    'social_core.pipeline.social_auth.social_user',
    'tabletop_planning.social_pipeline.redirect_if_no_refresh_token',
    'social_core.pipeline.user.get_username',
    'social_core.pipeline.social_auth.associate_by_email',
    # 'tabletop_planning.social_pipeline.require_email',
    'social_core.pipeline.user.create_user',
    'social_core.pipeline.social_auth.associate_user',
    'social_core.pipeline.social_auth.load_extra_data',
    'tabletop_planning.social_pipeline.empty_name_user_details',
    'tabletop_planning.social_pipeline.get_avatars_and_links',
)

# END python-social-auth


# START django-postoffice
POST_OFFICE = {
    'DEFAULT_PRIORITY': 'now',  # good for dev. set to medium on prod
}
# END django-postoffice

# Django-happenings:
CALENDAR_LOCALE = 'ru_RU.UTF-8'

CALENDAR_TIME_FORMAT = 'H:i'
CALENDAR_HOUR_FORMAT = 'H:i'

CALENDAR_PASS_VIEW_CONTEXT_TO_DISPLAY_METHOD = True

# START tinymce
TINYMCE_DEFAULT_CONFIG = {
    'plugins': "table,spellchecker,paste,searchreplace",
    'theme': "advanced",
    'cleanup_on_startup': True,
    'custom_undo_redo_levels': 10,
}
TINYMCE_SPELLCHECKER = True
# END tinymce

# START django-sitegate
SIGNUP_EMAIL_CHANGE_PROCESSING = True
# END django-sitegate

# START django-disqus
DISQUS_API_KEY = 'vsBq7BPMe3h0FpNqZV9krvk37kUUqQpxmK2pq0kvOFG9BDvJ7u95ciLMJ2KaRDg6'
DISQUS_WEBSITE_SHORTNAME = 'playhard-kiev'
# END django-disqus

# START django-filebrowser
FILEBROWSER_EXTENSIONS = {
    'Folder': [''],
    'Image': ['.jpg', '.jpeg', '.gif', '.png', '.tif', '.tiff'],
    'Document': ['.pdf', '.doc', '.docx', '.rtf', '.txt', '.xls', '.csv'],
    'Video': ['.mov', '.wmv', '.mpeg', '.mpg', '.avi', '.rm', '.ogv', '.mp4'],
    'Audio': ['.mp3', '.mp4', '.wav', '.aiff', '.midi', '.m4p'],
    'Archive': ['.zip', '.7z', '.rar', '.bz2', '.gz', '.tar'],
}
# END django-filebrowser


# START django-analytical
GOOGLE_ANALYTICS_PROPERTY_ID = env.get('DJANGO_GOOGLE_ANALYTICS_PROPERTY_ID')
GOOGLE_ANALYTICS_SITE_SPEED = True
# END django-postoffice

# start comments
# COMMENTS_APP = "django_comments_xtd"
# COMMENTS_XTD_CONFIRM_EMAIL = True
# COMMENTS_XTD_FORM_CLASS = 'tabletop_planning.forms.CommentForm'
# end comments


# dynamic-preferences:
DYNAMIC_PREFERENCES = {

    # a python attribute that will be added to model instances with preferences
    # override this if the default collide with one of your models attributes/fields
    'MANAGER_ATTRIBUTE': 'preferences',

    # The python module in which registered preferences will be searched within each app
    'REGISTRY_MODULE': 'dynamic_preferences_registry',

    # Allow quick editing of preferences directly in admin list view
    # WARNING: enabling this feature can cause data corruption if multiple users
    # use the same list view at the same time, see https://code.djangoproject.com/ticket/11313
    'ADMIN_ENABLE_CHANGELIST_FORM': False,

    # Customize how you can access preferences from managers. The default is to
    # separate sections and keys with two underscores. This is probably not a settings you'll
    # want to change, but it's here just in case
    'SECTION_KEY_SEPARATOR': '__',

    # Use this to disable caching of preference. This can be useful to debug things
    'ENABLE_CACHE': True,

    # Use this to disable checking preferences names. This can be useful to debug things
    'VALIDATE_NAMES': True,
}

# other:
GRAPPELLI_ADMIN_TITLE = 'Playhard administration'
GRAPPELLI_INDEX_DASHBOARD = 'tabletop_planning.dashboard.CustomIndexDashboard'

# project specific:

DEFAULT_GROUP_NAMES = [_('GM Managers'), _('Accounting Managers')]

REQUIRE_MASTER_CONFIRMATION = True

SERVICE_USER_SUFFIX = "service"
