# -*- coding: utf-8 -*-

from social_core.backends.google import GoogleOAuth2


class CustomGoogleOAuth2(GoogleOAuth2):
    name = 'google-oauth2'

    def _drive_access_required(self):
        return self.data.get('drive_access') or self.strategy.session_get('drive_access', False)

    def get_scope(self):
        scope = super(CustomGoogleOAuth2, self).get_scope()
        if self.data.get('drive_access') or self.strategy.session_get('drive_access', False):
            scope = scope + ['https://www.googleapis.com/auth/drive.file']
        return scope

    def extra_data(self, user, uid, response, details=None, *args, **kwargs):
        res = super(CustomGoogleOAuth2, self).extra_data(user, uid, response, details=details, *args, **kwargs)
        res['saved_scopes'] = self.get_scope()
        return res

    def auth_extra_arguments(self):
        res = super(CustomGoogleOAuth2, self).auth_extra_arguments()
        if self._drive_access_required():
            res['access_type'] = 'offline'
            res['approval_prompt'] = 'force'
        return res
