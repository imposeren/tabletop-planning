# -*- coding: utf-8 -*-

# python builtins:
import datetime

# django:
from django.contrib.auth.decorators import login_required, user_passes_test
from django.core import signing
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.shortcuts import redirect
from django.utils import timezone
from django.utils.translation import ugettext

try:
    from django.core.urlresolvers import reverse
except ImportError:
    from django.urls import reverse

# thirdparty:
from annoying.decorators import render_to


# project
from tabletop_planning.utils import get_statistics_for_interval, get_signed_data_processor_salt
from accounting.models import BalanceChange
from games.models import SessionVisit


def get_statistics():
    NOW = timezone.now()

    results = {}
    periods = {
        'one_month': 30,
        'ever': None,
        'one_year': 365,
        # 'three_years': 1096,
    }
    for period, num_days in periods.items():
        if num_days is None:
            start_date = end_date = None
            tag_stats_field = 'visits_count_all'
        else:
            start_date, end_date = (NOW - datetime.timedelta(days=num_days), NOW)
            if 28 <= num_days <= 31:
                tag_stats_field = 'visits_count_month'
            elif num_days in (365, 366):
                tag_stats_field = 'visits_count_year'
            else:
                tag_stats_field = None

        results['%s_stats' % period] = get_statistics_for_interval(start_date, end_date, tag_stats_field)

    return results


@render_to('tabletop_planning/statistics.html')
def statistics(request):
    return get_statistics()


def can_manage_balance(user):
    return (
        user.has_perm('accounting.add_balancechange')
        or
        user.has_perm('accounting.change_balancechange')
        or
        user.is_superuser
    )


@login_required
@user_passes_test(can_manage_balance)
@render_to('tabletop_planning/finance_statistics.html')
def finance_statistics(request):
    finance_statistics = BalanceChange.objects.filter(to_budget=True, verified=True).annotate_sum_per_date()
    return {'finance_statistics': finance_statistics}


@login_required
@user_passes_test(can_manage_balance)
@render_to('tabletop_planning/visits_statistics.html')
def visits_statistics(request):
    visits_statistics = SessionVisit.objects.filter(have_visited=True, verified=True, role=SessionVisit.ROLES.player).annotate_visits_per_date()
    return {'visits_statistics': visits_statistics}


@render_to('tabletop_planning/privacy_policy.html')
def privacy_policy(request):
    return {}


def signed_data_processor(request, base64_data):
    message = ugettext(u'Введенный путь был неправильный.')
    messanger = messages.error
    next_url = '/'
    try:
        data = signing.loads(base64_data, salt=get_signed_data_processor_salt())
    except signing.BadSignature:
        data = {}

    action, context = data.get('action'), data.get('context', {})
    if action == 'users.email_unsubscribe' and context.get('email'):
        user = get_user_model().objects.filter(email__iexact=context['email']).first()
        if user:
            message = None
            user.email_unsubscribe(request=request)
            if user == request.user:
                next_url = reverse('users:edit_profile')

    if message:
        messanger(request, message)

    return redirect(next_url)
