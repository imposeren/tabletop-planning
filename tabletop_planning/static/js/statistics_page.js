$(document).ready(function(){

  function mapInit() {
    var map = new L.Map('map');

    L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors',
      maxZoom: 18,
      detectRetina: true
    }).addTo(map);
    map.attributionControl.setPrefix(''); // Don't show the 'Powered by Leaflet' text.

    var geojsonFeatureCollection = {"type":"FeatureCollection","properties":{"name":"Playhard New path","created":"2019-06-11T16:17:43.091+02:00","modified":"2019-06-11T21:18:32.516+02:00","generated":"2019-06-11T21:35:49.361+02:00","version":-1,"metadata":""},"features":[{"type":"Feature","properties":{"id":"a_2956653685_4","path":"/Dorogozh-path","name":"Line 1","stroke":"#0066ff","stroke-opacity":1,"stroke-width":3,"popupcontent":"Шлях пішки від метро Дорогожичи"},"geometry":{"type":"LineString","coordinates":[[30.449288,50.473073],[30.448344,50.47322],[30.448215,50.473165],[30.447486,50.471239],[30.447362,50.471089],[30.447199,50.470558],[30.446657,50.470657],[30.446582,50.470489],[30.446437,50.470261],[30.443894,50.470353],[30.443835,50.470182],[30.442977,50.469008],[30.443079,50.468973]]}},{"type":"Feature","properties":{"id":"a_2956657682_146","path":"/Dorogozh-alt-path","name":"Line 3","stroke":"#330099","stroke-opacity":1,"stroke-width":2,"popupcontent":"Шлях пішки від метро Дорогожичи"},"geometry":{"type":"LineString","coordinates":[[30.44921,50.473072],[30.449023,50.473099],[30.448352,50.472037],[30.448019,50.471802],[30.447837,50.471713],[30.447472,50.470849],[30.447263,50.470781]]}},{"type":"Feature","properties":{"id":"a_2956749438_249","path":"/Shulyav-path","name":"Line 1","stroke":"#00ff66","stroke-opacity":1,"stroke-width":2,"popupcontent":"Шлях пішки від метро Шулявська"},"geometry":{"type":"LineString","coordinates":[[30.443029,50.468809],[30.442858,50.46885],[30.442418,50.468201],[30.444403,50.467696],[30.446613,50.46773],[30.446902,50.463673],[30.446929,50.463335],[30.446935,50.462423],[30.446924,50.462229],[30.446871,50.462127],[30.446967,50.461976],[30.447128,50.459036],[30.446827,50.457868],[30.446704,50.457827],[30.44665,50.457708],[30.446741,50.457609],[30.446575,50.457206],[30.446505,50.456745],[30.446275,50.456123],[30.446033,50.455802],[30.445829,50.455293],[30.445449,50.455345],[30.445202,50.455222],[30.445089,50.454983]]}},{"type":"Feature","properties":{"id":"a_2956749438_329","label":"Метро Шулявская","name":"Метро Шулявская","path":"/transport-stops","openpopup":"1","popupcontent":"Метро Шулявська."},"geometry":{"type":"Point","coordinates":[30.445172,50.45499]}},{"type":"Feature","properties":{"id":"a_2956749438_318","label":"Метро Дорогожичи","name":"Метро Дорогожичи","path":"/transport-stops","openpopup":"1","popupcontent":"Метро Дорогожичи.<br/> вихід у бік меморіалу"},"geometry":{"type":"Point","coordinates":[30.44931,50.473077]}},{"type":"Feature","properties":{"id":"a_2956693868_9","label":"Остановка ст.м.Шулявская","name":"Остановка ст.м.Шуляв","path":"/transport-stops","openpopup":"false","popupcontent":"Зупинка ст.м.Шулявська. <br/> Сісти на маршрутку 223, 421, або 242"},"geometry":{"type":"Point","coordinates":[30.446842,50.456677]}},{"type":"Feature","properties":{"id":"a_2956693868_20","label":"Остановка Бабий Яр","name":"Остановка Бабий Яр","path":"/transport-stops","openpopup":"false","popupcontent":"Зупинка Бабин Яр. <br/>Вийти з маршрутки"},"geometry":{"type":"Point","coordinates":[30.447223,50.470937]}},{"type":"Feature","properties":{"id":"a_2956657682_16","label":"Клуб PlayHard","name":"Клуб PlayHard","path":"/PH-marker","openpopup":"1","popupcontent":"Клуб PlayHard<br/>вул.Парково-Сирецька, буд. 3"},"geometry":{"type":"Point","coordinates":[30.443186,50.468936]}},{"type":"Feature","properties":{"id":"a_2956749438_280","path":"/Shulyav-alt-path","name":"Line 1","stroke":"#33cc33","stroke-opacity":1,"stroke-width":2,"popupcontent":"Шлях маршруткою від метро Шулявська"},"geometry":{"type":"LineString","coordinates":[[30.446444,50.456513],[30.446851,50.456452],[30.446916,50.456653],[30.446782,50.456674],[30.447082,50.457729],[30.447345,50.458849],[30.447382,50.459235],[30.446964,50.466492],[30.446739,50.469879],[30.44691,50.470272],[30.447031,50.470548],[30.447188,50.470949],[30.447329,50.470927]]}}]};

    geojsonLayer = L.geoJSON(
      geojsonFeatureCollection,
      {
        style: function(feature) {
            if (feature.properties.stroke) {
                return {color: feature.properties.stroke};
            }
        },
        pointToLayer: function (feature, latlng) {
          if (feature.properties.openpopup === "1") {
            return L.circleMarker(
              latlng,
              {
                title: feature.properties.label,
                radius: 4,
                opacity: 0.8,
                fillOpacity: 0.5
              }
             );
          } else {
            return L.marker(latlng, {title: feature.properties.label});
          }

        },
        onEachFeature: function onEachFeature(feature, layer) {
          if (feature.properties && feature.properties.popupcontent) {
            layer.bindPopup(
              feature.properties.popupcontent,
              {autoClose: false}
            );
            feature.properties.hasPopup = true;
          }
        }
      }
    ).addTo(map);
    map.fitBounds(geojsonLayer.getBounds());
    geojsonLayer.eachLayer(function(layer){
      if (layer.feature.properties.hasPopup && layer.feature.properties.openpopup === "1") {
        layer.openPopup();
      }
    });
  }

  mapInit();
});
