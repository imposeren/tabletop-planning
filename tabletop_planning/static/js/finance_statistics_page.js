window.graphFinanceData = function graphFinanceData(xData, yData) {
  var data = [{
    type: 'histogram',
    name: 'finance',
    histfunc: 'sum',
    histnorm: '',
    x: xData,
    y: yData,
    autobinx: false,
    autobiny: true,
    xbins: {
      start: xData[0],
      end: xData[xData.length - 1],
      size: 'M1'
    }
  }];
  var xrange = [xData[0].replace('12:00', '00:00'), xData[xData.length - 1].replace('12:00', '23:59')];

  layout = {
    bargap: 0.05,
    height: 600,
    paper_bgcolor: 'rgb(240, 240, 240)',
    plot_bgcolor: 'rgb(240, 240, 240)',
    title: '',
    xaxis: {
      tickangle: -65,
      autorange: true,
      range: xrange,
      title: 'Date',
      type: 'date',
      dtick: 'M1',
      rangeselector: {buttons: [
          {
            count: 1,
            label: '1m',
            step: 'month',
            stepmode: 'backward'
          },
          {
            count: 6,
            label: '6m',
            step: 'month',
            stepmode: 'backward'
          },
          {
            count: 12,
            label: '1y',
            step: 'month',
            stepmode: 'backward'
          },
          {step: 'all'}
        ]},
      rangeslider: {range: xrange}
    },
    yaxis: {
      title: 'Income',
      type: 'linear'
    },
    updatemenus: [{
          x: 0.85,
          y: 1.20,
          xref: 'paper',
          yref: 'paper',
          yanchor: 'top',
          active: 0,
          showactive: true,
          buttons: [{
              args: ['xbins.size', 'M1'],
              label: 'per month',
              method: 'restyle'
          },{
              args: ['xbins.size', 86400000.0],
              label: 'per day',
              method: 'restyle'
          }, {
              args: ['xbins.size', 604800000.0],
              label: 'per week',
              method: 'restyle'
          },  {
              args: ['xbins.size', 'M3'],
              label: 'per quater',
              method: 'restyle'
          }, {
              args: ['xbins.size', 'M12'],
              label: 'per year',
              method: 'restyle'
          }]
    }]
  };

  Plotly.plot('graph', data, layout);

};
