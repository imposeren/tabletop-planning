/* global ParsleyConfig */

'use strict';

window.ParsleyConfig = {
  classHandler: function (ParsleyField ) {
    // specify where parsley error-success classes are set
    return ParsleyField.$element.parents('.form-group');
  },
  errorsContainer: function(ParsleyField){
    return ParsleyField.$element.parents('.form-group');
  },
  errorsWrapper: '<span class="help-block"></span>',
  errorTemplate: '<span></span>',
  successClass: 'has-success',
  errorClass: 'has-error'
 };


 $(document).ready(function(){
  $('.taggitwidget.autocomplete').bind('selectChoice', function(e, choice, autocomplete) {
    var $this = $(this);
    $this.val($this.val()+', ');
  });

  $('form a[data-toggle=tooltip]').tooltip({container: 'body'});
  $('input:checkbox').on('click', function(e) {
    if ($(this).attr('readonly')) {
      e.preventDefault();
      return;
    }
  });

  $('[data-toggle=popover]').popover({html : true});
 });


var originalLeave = $.fn.popover.Constructor.prototype.leave;
$.fn.popover.Constructor.prototype.leave = function(obj){
  var self = obj instanceof this.constructor ?
    obj : $(obj.currentTarget)[this.type](this.getDelegateOptions()).data('bs.' + this.type)
  var container, timeout;

  originalLeave.call(this, obj);

  if(obj.currentTarget) {
    container = $(obj.relatedTarget).parent('.popover');
    timeout = self.timeout;
    container.one('mouseenter', function(){
      //We entered the actual popover – call off the dogs
      clearTimeout(timeout);
      //Let's monitor popover content instead
      container.one('mouseleave', function(){
        $.fn.popover.Constructor.prototype.leave.call(self, self);
      });
    })
  }
};
