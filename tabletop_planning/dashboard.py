# -*- coding: utf-8 -*-
"""Dashboard for django-grapelli admin.

This file was generated with the customdashboard management command and
contains the class for the main dashboard.

To activate your index dashboard add the following to your settings.py::

    GRAPPELLI_INDEX_DASHBOARD = 'tabletop-planning.dashboard.CustomIndexDashboard'

"""

from django.utils.translation import ugettext_lazy as _
from django.conf import settings

from grappelli.dashboard import modules, Dashboard


class CustomIndexDashboard(Dashboard):

    """Custom index dashboard for www."""

    def init_with_context(self, context):
        project_models = tuple('%s.*' % app for app in settings.PROJECT_APPS)
        close_to_project_models = ('post_office.*', 'happenings.*', 'dynamic_preferences.*')

        self.children.append(modules.AppList(
            _('Playhard'),
            collapsible=True,
            column=1,
            css_classes=('collapse grp-open',),
            models=project_models + close_to_project_models,
        ))

        self.children.append(modules.ModelList(
            _('System'),
            column=1,
            collapsible=True,
            css_classes=('collapse grp-closed',),
            models=('django.contrib.*',),
        ))

        self.children.append(modules.AppList(
            _('Third-parties'),
            column=1,
            collapsible=True,
            css_classes=('collapse grp-closed',),
            exclude=('django.contrib.*',) + project_models + close_to_project_models,
        ))

        # append another link list module for "support".
        self.children.append(modules.LinkList(
            _(u'Прочее'),
            column=3,
            children=[
                {
                    'title': _('FileBrowser'),
                    'url': '/admin/filebrowser/browse/',
                    'external': False,
                },
            ]
        ))

        # append a recent actions module
        self.children.append(modules.RecentActions(
            _('Recent Actions'),
            limit=5,
            collapsible=False,
            column=2,
        ))
