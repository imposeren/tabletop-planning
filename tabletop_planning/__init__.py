# -*- coding: utf-8 -*-

default_app_config = 'tabletop_planning.apps.MainAppConfig'


try:
    import oboeware.inst_django_orm

    class CursorOboeWrapper(oboeware.inst_django_orm.CursorOboeWrapper):
        def __enter__(self):
            return self

        def __exit__(self, type, value, traceback):
            self.close()

    oboeware.inst_django_orm.CursorOboeWrapper = CursorOboeWrapper
except ImportError:
    pass
