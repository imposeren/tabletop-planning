# -*- coding: utf-8 -*-
import sys

from .settings_default import *  # noqa
from os import environ as env

DEBUG = False

if env.get('DJANGO_EMAIL_USE_SSL', '0') in ['1', 'y', 'yes', 'True']:
    EMAIL_USE_SSL = True
if env.get('DJANGO_EMAIL_USE_TLS', '0') in ['1', 'y', 'yes', 'True']:
    EMAIL_USE_TLS = True
EMAIL_HOST = env.get('DJANGO_EMAIL_HOST')
EMAIL_PORT = int(env.get('DJANGO_EMAIL_PORT', 25))
EMAIL_HOST_USER = env.get('DJANGO_EMAIL_ACCESS_USR')
EMAIL_HOST_PASSWORD = env.get('DJANGO_EMAIL_ACCESS_PSW')
SERVER_EMAIL = DEFAULT_FROM_EMAIL = env.get('DJANGO_DEFAULT_FROM_EMAIL')


ALLOWED_HOSTS = ['playhard.kyiv.ua', 'playhard.kiev.ua', 'dev.playhard.kiev.ua', 'test.playhard.kiev.ua', 'staging.playhard.kiev.ua']


CACHES = {
    'default': {
        # 'BACKEND': 'django.core.cache.backends.memcached.PyLibMCCache',
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
        # 'BINARY': True,
        'TIMEOUT': 24*3600,
        'KEY_PREFIX': 'tabletop',
        'VERSION': '2014-09-28',
        'MAX_ENTRIES': 1500,
        'CULL_FREQUENCY': 5,
        # 'OPTIONS': {
        #     'tcp_nodelay': True,
        # }
    }
}

PYLIBMC_MIN_COMPRESS_LEN = 400 * 1024


COMPRESS_PRECOMPILERS = (
    (
        'text/less', '/usr/bin/lessc {infile} {outfile}'
    ),
)

COMPRESS_JS_FILTERS = [
    # 'compressor.filters.jsmin.JSMinFilter',
    'tabletop_tools.compressor_filters.SourceMappingClosureCompilerFilter'
]


COMPRESS_SOURCE_MAPS_DIR = env.get('DJANGO_COMPRESS_SOURCE_MAPS_DIR', '')
COMPRESS_CLOSURE_COMPILER_BINARY = env.get('DJANGO_COMPRESS_CLOSURE_COMPILER_BINARY', '')

COMPRESS_OFFLINE = True


new_secret_key = env.get('DJANGO_SECRET_KEY', '')
if new_secret_key:
    SECRET_KEY = new_secret_key


_db_name = env.get('PGDATABASE')
_deployment_suffix = env.get('DEPLOYMENT_SUFFIX', '').strip()

if _deployment_suffix:
    _db_name += _deployment_suffix

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': _db_name,
        'USER': env.get('POSTGRESQL_DB_ACCESS_USR'),
        'PASSWORD': env.get('POSTGRESQL_DB_ACCESS_PSW'),
        'HOST': env.get('POSTGRESQL_DB_HOST', 'localhost'),
        'PORT': env.get('POSTGRESQL_DB_PORT', '5432'),
        'OPTIONS': {
            'options': '-c statement_timeout=10000',
        }
    }
}

args_string = ' '.join(sys.argv)

if ('manage.py' in args_string) or 'celery' in args_string:
    DATABASES['default']['OPTIONS']['options'] = '-c statement_timeout=36000000'

SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = env.get('DJANGO_SA_GOOGLE_KEY')
SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = env.get('DJANGO_SA_GOOGLE_SECRET')

SOCIAL_AUTH_VK_OAUTH2_KEY = env.get('DJANGO_SA_VK_KEY')
SOCIAL_AUTH_VK_OAUTH2_SECRET = env.get('DJANGO_SA_VK_SECRET')

SOCIAL_AUTH_FACEBOOK_KEY = env.get('DJANGO_SA_FACEBOOK_KEY')
SOCIAL_AUTH_FACEBOOK_SECRET = env.get('DJANGO_SA_FACEBOOK_SECRET')

SOCIAL_AUTH_DISQUS_KEY = env.get('DJANGO_SA_DISQUS_KEY')
SOCIAL_AUTH_DISQUS_SECRET = env.get('DJANGO_SA_DISQUS_SECRET')

DJRILL_WEBHOOK_SECRET = env.get('DJANGO_DJRILL_WEBHOOK_SECRET', '')

# THUMBNAIL_ENGINE = 'sorl.thumbnail.engines.convert_engine.Engine'


TEMPLATES[0]['OPTIONS']['loaders'] = [  # noqa: F405
    (
        'django.template.loaders.cached.Loader',
        ['django.template.loaders.filesystem.Loader', 'django.template.loaders.app_directories.Loader', ]
    ),
]


LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'root': {
        'level': 'WARNING',
        'handlers': ['console'],
    },
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        }
    },
    'loggers': {
        'django.db.backends': {
            'level': 'ERROR',
            'handlers': ['console'],
            'propagate': False,
        },
        'django': {
            'level': 'ERROR',
            'handlers': ['console'],
            'propagate': False,
        },
        'social': {
            'level': 'INFO',
            'handlers': ['console'],
            'propagate': False,
        },
    },
}

if env.get('DJANGO_RAVEN_CONFIG_DSN', ''):

    RAVEN_CONFIG = {
        'dsn': env.get('DJANGO_RAVEN_CONFIG_DSN', ''),
        'include_paths': [
            'accounting',
            'games',
            'news',
            'tabletop_planning',
            'tabletop_tools',
            'tags',
            'users',
        ]
    }
    _build_number = env.get('BUILD_NUMBER', '')
    if _build_number:
        RAVEN_CONFIG['release'] = _build_number

    _env_branch = env.get('GIT_REMOTELESS_BRANCH', '') or env.get('GIT_BRANCH', '')
    if _env_branch:
        RAVEN_CONFIG['environment'] = _env_branch

    LOGGING['root']['handlers'] = ['sentry']

    LOGGING['handlers']['sentry'] = {
        'level': 'ERROR',
        'class': 'raven.contrib.django.raven_compat.handlers.SentryHandler',
    }

    LOGGING['loggers'].update({
        'raven': {
            'level': 'ERROR',
            'handlers': ['console'],
            'propagate': False,
        },
        'sentry.errors': {
            'level': 'ERROR',
            'handlers': ['console'],
            'propagate': False,
        },
    })


POST_OFFICE = {
    'DEFAULT_PRIORITY': 'medium',
}

# INSTALLED_APPS += ('debug_toolbar',)


if {'test', 'jenkins'}.intersection(set(sys.argv)):
    SITE_ID = 2
    CACHES = {
        'default': {
            'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        }
    }
