# -*- coding: utf-8 -*-
from itertools import chain

# django:
from django import forms
from django.forms.models import BaseModelFormSet
from django.utils.translation import ugettext_lazy as _

# third-parties:
# from django_comments_xtd.forms import XtdCommentForm
from crispy_forms.bootstrap import FormActions, StrictButton
from crispy_forms.helper import FormHelper
from crispy_forms.layout import HTML
# from parsley.decorators import parsleyfy


class BootstrapFormMixin(object):

    u"""Mixin to automate common bootstrap-related stylings.

    Example usage::

        class AuthenticationForm(BootstrapFormMixin, _AuthenticationForm):
            def __init__(self, *args, **kwargs):
                super(AuthenticationForm, self).__init__(*args, **kwargs)

                self._default_submit_button.content = _(u'Войти')

                self.helper.layout = Layout(
                    'username',
                    'password',
                    self._submit_action,
                )

    """

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(BootstrapFormMixin, self).__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.form_class = 'container-fluid'
        self.helper.field_class = 'input-group'
        self.helper.attrs = {'data-parsley-validate': 'data-parsley-validate'}
        self.helper.help_text_inline = True

        self._default_submit_button = StrictButton(
            _(u'Сохранить'), type='submit', css_class='btn btn-primary js-save'
        )
        self._default_cancel_button = HTML(
            u'<a href="." class="btn btn-default js-cancel">%s</a>' % _(u'Отменить')
        )

        self._default_actions = FormActions(
            self._default_submit_button,
            self._default_cancel_button,
            css_class='form-actions clearfix',
        )

        self._submit_action = FormActions(
            self._default_submit_button,
            css_class='form-actions clearfix',
        )


class BootstrapModelForm(BootstrapFormMixin, forms.ModelForm):

    """Form class to be used with crispy.

    Note: form.__init__() method also supports ``request`` argument that will be popped and saved to form.request

    """

    def __init__(self, *args, **kwargs):
        super(BootstrapModelForm, self).__init__(*args, **kwargs)


class BootstrapForm(BootstrapFormMixin, forms.Form):
    """Same as ``BootstrapModelForm`` but for forms.Form."""

    def __init__(self, *args, **kwargs):
        super(BootstrapForm, self).__init__(*args, **kwargs)


class BaseModelFormSetWithRequest(BaseModelFormSet):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(BaseModelFormSetWithRequest, self).__init__(*args, **kwargs)
        self.form_kwargs['request'] = self.request


class ExtraFirstFormSet(BaseModelFormSetWithRequest):

    """Formset that outputs extra forms before forms of instances."""

    def __iter__(self):
        forms = chain(
            filter(lambda form: not form.instance.pk, self.forms),
            filter(lambda form: form.instance.pk, self.forms),
        )
        return iter(forms)


# @parsleyfy
# class CommentForm(BootstrapFormMixin, XtdCommentForm):

#     def __init__(self, *args, **kwargs):
#         super(CommentForm, self).__init__(*args, **kwargs)

#         for field in ('name', 'email', 'url'):
#             self.fields[field].widget = forms.HiddenInput()
#             self.fields[field].required = False

#         self.helper.form_tag = False
