# -*- coding: utf-8 -*-
import logging

from django.contrib.auth.hashers import is_password_usable
from django.core.files.base import ContentFile
from django.core.files.storage import default_storage
from django.shortcuts import redirect
from django.urls import reverse

from social_core.pipeline.user import user_details
from social_core.pipeline.partial import partial
from requests import request, HTTPError


_logger = logging.getLogger(__name__)


@partial
def require_email(strategy, details, user=None, is_new=False, *args, **kwargs):
    if kwargs.get('ajax') or user and user.email:
        return
    elif is_new and not details.get('email'):
        email = strategy.request_data().get('email')
        if email and '@' in email:  # vk may return 'id143149821' as email
            details['email'] = email
        else:
            return redirect('require_email')


def get_avatars_and_links(
        backend, details, response, user=None, is_new=False,
        *args, **kwargs):

    # http://www.ikrvss.ru/2011/08/16/django-social-auth-with-images/
    # https://github.com/bilabon/myproject/edit/master/myproject/apps/myapp/avatars.py
    # see alse:
    # http://stackoverflow.com/a/19902090
    """Update user details using data from provider."""

    if user is None:
        return
    changed = False  # flag to track changes
    if not user.is_active:
        user.is_active = True
        if is_password_usable(user.password):
            # we do not know if real user registered with this email. Unset password
            user.set_unusable_password()
        changed = True

    image_url = None

    if backend.name == 'google-oauth2' and "picture" in response:
        image_url = response["picture"]

    elif backend.name == 'vk-oauth2':
        image_url = response.get('user_photo', '')

    elif backend.name == 'odnoklassniki':
        image_url = response.get('pic_2')
        if 'stub' in image_url:
            image_url = None

    elif backend.name == 'mailru-oauth2':
        if response.get('has_pic'):
            image_url = response.get('pic_big')

    elif backend.name == 'twitter':
        image_url = response.get('profile_image_url')
        if 'default_profile' not in image_url:
            image_url = image_url.replace('_normal', '_bigger')
        else:
            image_url = None

    elif backend.name == 'yandex-oauth2':
        image_url = response.get('userpic')

    elif backend.name == 'facebook':
        # save link:
        if response.get('link') and response['link'] != user.facebook_link:
            user.facebook_link = response['link']
            changed = True
        elif not response.get('link'):
            _logger.error('failed to get facebook link for response: %s', response)

        access_token = backend.data.get('access_token') or response.get('access_token')
        if not access_token:
            _logger.error('No access token available for facebook')
        else:
            image_data_url = 'https://graph.facebook.com/v{version}/{user_id}/picture'
            version = backend.setting('API_VERSION', 2.9)
            params = {
                'access_token': backend.data.get('access_token'),
                'height': '960',
                'redirect': 'false',
            }
            try:
                image_url = backend.get_json(
                    image_data_url.format(version=version, user_id=response.get('id')),
                    params=params
                ).get('data', {}).get('url', None)
            except Exception:
                _logger.exception('Failed to get facebook profile image')

            if not image_url:
                image_url = response.get('picture', {}).get('url')

    if image_url and not user.image:
        try:
            image_response = request('GET', image_url)
            image_response.raise_for_status()

            image_content = image_response.content

            image_name = default_storage.get_available_name(
                user.image.field.upload_to + '/' + str(user.id) + '.' + image_response.headers['content-type'].split('/')[1]
            )
            user.image.save(image_name, ContentFile(image_content))
            changed = True
        except HTTPError:
            pass

    if changed:
        user.save()
        backend.strategy.storage.user.changed(user)
    return


def redirect_if_no_refresh_token(backend, response, social, *args, **kwargs):
    """Reauthenticate if offlinne access is required and there is no refresh token.

    Offline access may be required if google-drive related scope is used.
    """
    reauth_required = (
        backend.name == 'google-oauth2'
        and
        social
        and
        'https://www.googleapis.com/auth/drive.file' in (social.extra_data.get('saved_scopes', []) or [])
        and
        response.get('refresh_token') is None
        and
        social.extra_data.get('refresh_token') is None
    )
    if reauth_required:
        return backend.strategy.redirect(
            '%s?approval_prompt=force&access_type=offline' % reverse('social:begin', args=['google-oauth2', ])
        )


def empty_name_user_details(strategy, details, user=None, *args, **kwargs):
    """Populate user details but only if first name is empty."""
    if (not user) or (not user.first_name):
        return user_details(strategy, details, user=user, *args, **kwargs)
