# -*- coding: utf-8 -*-
import logging

try:
    from django.core.urlresolvers import reverse, NoReverseMatch
except ImportError:
    from django.urls import reverse, NoReverseMatch

from tabletop_planning.tests import BaseTestCase

logger = logging.getLogger(__name__)


class GenericTestCase(BaseTestCase):
    def setUp(self):
        super(GenericTestCase, self).setUp()

    def check_url(self, url):
        if hasattr(url, 'name'):
            try:
                logger.info(url)
                response = self.client.get(reverse(url.name))
                self.assertLess(response.status_code, 500, msg='Failed checking %s' % url.name)
            except NoReverseMatch:
                pass
            except Exception:
                raise
        if hasattr(url, 'url_patterns'):
            self.check_url(url.url_patterns)

    def test_all_urls(self):
        from tabletop_planning import urls
        for email in (None, 'admin@example.com'):
            self.client.logout()
            if email:
                logged_in = self.client.login(email=email, password=email)
                self.assertTrue(logged_in)
            for url in urls.urlpatterns:
                with self.subTest(url):
                    self.check_url(url)
