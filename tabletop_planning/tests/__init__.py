# -*- coding: utf-8 -*-
import logging

from django.contrib.auth import get_user_model
from django.core.cache import cache
from django.core.management import call_command
from django.test import TestCase
from django.test.utils import override_settings
from users.tests.factories import UserFactory


@override_settings(DEBUG=False)
@override_settings(COMPRESS_OFFLINE=False)
@override_settings(TESTING=True)
@override_settings(CELERY_ALWAYS_EAGER=True)
@override_settings(CELERY_EAGER_PROPAGATES_EXCEPTIONS=True)
class BaseTestCase(TestCase):
    def setUp(self):
        super(BaseTestCase, self).setUp()
        cache.clear()
        call_command("createsuperuser", interactive=False, email='admin@example.com', verbosity=0)
        self.admin = get_user_model().objects.get(email='admin@example.com')
        self.admin.set_password('admin@example.com')
        self.admin.save()
        self.houses = {}
        self.club_user = get_user_model().objects.get(email=u'playhard@localhost', is_service=True)

        for logger_name in ('factory', ):
            logger = logging.getLogger(logger_name)
            logger.setLevel(logging.CRITICAL)

    def create_master(self, *args, **kwargs):
        kwargs['is_confirmed_master'] = True
        return UserFactory(*args, **kwargs)
