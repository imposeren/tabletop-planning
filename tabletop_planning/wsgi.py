#!/usr/bin/python
# -*- coding: utf-8 -*-

import os

from django.core.wsgi import get_wsgi_application  # noqa: E402

try:
    from raven.contrib.django.raven_compat.middleware.wsgi import Sentry
except ImportError:
    Sentry = None


if Sentry and os.environ.get('DJANGO_RAVEN_CONFIG_DSN', ''):
    application = Sentry(get_wsgi_application())
else:
    application = get_wsgi_application()
