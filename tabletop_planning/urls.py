# -*- coding: utf-8 -*-
from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import include, path

from sitegate.toolbox import get_sitegate_urls
from filebrowser.sites import site

import accounting.urls
import games.urls
import games.views
import news.urls
import tabletop_tools.urls
import tags.urls
import users.urls
import users.views

from tabletop_planning import views as top_views


def handler500(request):
    """500 error handler which includes ``request`` in the context.

    Templates: `500.html`
    Context: None

    """
    from django.template import loader
    from django.http import HttpResponseServerError

    t = loader.get_template('500.html')
    return HttpResponseServerError(t.render({
        'request': request,
    }))


month_view = games.views.GameEventMonthView.as_view()

urlpatterns = [
    # project includes:
    path('', games.views.game_sessions_index, name='main_games_index'),
    path('games/', include(games.urls, namespace='games')),
    path('users/', include(users.urls, namespace='users')),
    path('tags/', include(tags.urls, namespace='tags')),
    path('accounting/', include(accounting.urls, namespace='accounting')),
    path('tools/', include(tabletop_tools.urls, namespace='tabletop_tools')),
    path('news/', include(news.urls)),

    # lone project urls:
    url(r'^about/$', top_views.statistics, name='about'),
    url(r'^privacy-policy/$', top_views.privacy_policy, name='privacy_policy'),
    url(r'^statistics/$', top_views.statistics, name='statistics'),
    url(r'^statistics/finance/$', top_views.finance_statistics, name='finance_statistics'),
    url(r'^statistics/visits/$', top_views.visits_statistics, name='visits_statistics'),
    url(r'^calendar/$', month_view, name="calendar_list_override"),
    url(r'^calendar/(?P<year>\d{4})/(?P<month>\d{2}|\d{1})/$', month_view, name="calendar_list_override"),
    url(r'^calendar/(?P<year>\d{4})/(?P<month>\d{2}|\d{1})/(?P<day>\d{2}|\d{1})/$', games.views.GameEventDayView.as_view()),
    url(r'^about/$', top_views.statistics, name='about'),
    url(
        r'^process-data/(?P<base64_data>.?[A-z0-9-_=]{2,2000}:[A-z0-9-_=]{2,2000}:[A-z0-9-_=]{2,2000})/$',
        top_views.signed_data_processor,
        name='signed_data_processor'
    ),

    url(r'^signin/$', users.views.signin, name='signin'),
    url(r'^signup/$', users.views.signup, name='signup'),
    url(r'^logout/$', users.views.logout, name='logout'),
    url(r'^password_change/$', auth_views.PasswordChangeView.as_view(), name='password_change'),
    url(r'^password_change/done/$', auth_views.PasswordChangeDoneView.as_view(), name='password_change_done'),
    url(r'^password_reset/$', auth_views.PasswordResetView.as_view(), name='password_reset'),
    url(r'^password_reset/done/$', auth_views.PasswordResetDoneView.as_view(), name='password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        auth_views.PasswordResetConfirmView.as_view(),
        name='password_reset_confirm'),
    url(r'^reset/done/$', auth_views.PasswordResetDoneView.as_view(), name='password_reset_complete'),

    # third-parties
    path('admin/filebrowser/', site.urls),
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^social-auth/', include('social_django.urls', namespace='social')),
    path('admin/', admin.site.urls),
    url(r'^scribbler/', include('scribbler.urls')),
    url(r'^messages/', include('messages_extends.urls')),
    url(r'^calendar/', include('happenings.urls', namespace='calendar')),
    url(r'^tinymce/', include('tinymce.urls')),
    # url(r'^djrill/', include('djrill.urls')),
    # url(r'^comments/', include('django_comments_xtd.urls')),

] + get_sitegate_urls()

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

    try:
        import silk  # noqa
        urlpatterns += [url(r'^silk/', include('silk.urls', namespace='silk'))]
    except ImportError:
        pass
