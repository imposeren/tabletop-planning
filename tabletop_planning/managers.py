# -*- coding: utf-8 -*-


# django:
from django.db import models


class IsActiveManager(models.Manager):
    def get_queryset(self):
        return super(IsActiveManager, self).get_queryset().filter(is_active=True)
