# -*- coding: utf-8 -*-

# Python builtins:
# ...

# Django:
from django.conf import settings
from django.contrib.flatpages.views import render_flatpage
from django.contrib.sites.shortcuts import get_current_site
from django.http import Http404, HttpResponsePermanentRedirect
from django.shortcuts import get_object_or_404, redirect

# Third-parties:
from annoying.decorators import render_to
from el_pagination.decorators import page_template

# Project:
from news.models import NewsItem


@page_template('news/news_index_page.html')
@render_to('news/news_index.html')
def index(request, template=None, extra_context=None):
    context = {'do_pagination': True}
    if template:
        context['TEMPLATE'] = template
    if extra_context:
        context.update(extra_context)

    news = NewsItem.objects.get_active()
    context['news'] = news
    if not news.exists():
        return redirect('games:game_sessions_index')

    return context


@render_to('news/news_item_detail.html')
def news_item(request, url):
    if not url.startswith('/'):
        url = '/' + url

    site_id = get_current_site(request).id
    active_news = NewsItem.objects.get_active()
    try:
        f = get_object_or_404(
            active_news,
            url=url, sites=site_id
        )
    except Http404:
        if not url.endswith('/') and settings.APPEND_SLASH:
            url += '/'
            f = get_object_or_404(
                active_news,
                url=url, sites=site_id
            )
            return HttpResponsePermanentRedirect('%s/' % request.path)
        else:
            raise
    return render_flatpage(request, f)
