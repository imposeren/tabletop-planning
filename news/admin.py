# -*- coding: utf-8 -*-
try:
    from django.core.urlresolvers import reverse
except ImportError:
    from django.urls import reverse
from django.contrib import admin
from django.contrib.admin.sites import NotRegistered
from django.contrib.flatpages.forms import FlatpageForm
from django.contrib.flatpages.models import FlatPage
from django.utils.translation import ugettext_lazy as _

from tinymce.widgets import TinyMCE

from news.models import NewsItem


class CustomizedFlatpageForm(FlatpageForm):
    def __init__(self, *args, **kwargs):
        super(CustomizedFlatpageForm, self).__init__(*args, **kwargs)
        self.fields['url'].help_text = _(
            u"Пример: '/привет-мир/'. "
            u"Убедитесь, что ввели начальную и конечную косые черты. "
            u"Реальный путь будет иметь видь /news/привет-мир/."
        )
        self.fields['enable_comments'].initial = True


class TinyMCEFlatPageAdmin(admin.ModelAdmin):
    form = CustomizedFlatpageForm

    fieldsets = (
        (None, {'fields': ('is_active', 'url', 'publish_time', 'title', 'preview_text', 'content', 'sites')}),
        (_('Advanced options'), {'classes': ('collapse',), 'fields': ('enable_comments', 'registration_required', 'template_name')}),
    )
    list_display = ('url', 'title')
    list_filter = ('sites', 'enable_comments', 'registration_required')
    search_fields = ('url', 'title')

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'content':
            return db_field.formfield(widget=TinyMCE(
                attrs={'cols': 80, 'rows': 90},
                mce_attrs={'external_link_list_url': reverse('tinymce.views.flatpages_link_list'), 'height': '600px', },
            ))
        return super(TinyMCEFlatPageAdmin, self).formfield_for_dbfield(db_field, **kwargs)


try:
    admin.site.unregister(FlatPage)
except NotRegistered:
    pass

admin.site.register(NewsItem, TinyMCEFlatPageAdmin)
