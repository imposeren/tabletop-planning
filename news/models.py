# -*- coding: utf-8 -*-
from django.contrib.flatpages.models import FlatPage
try:
    from django.core.urlresolvers import reverse
except ImportError:
    from django.urls import reverse
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from tabletop_planning.utils import DisqusIDMixin


class NewsManager(models.Manager):

    use_for_related_fields = True

    def get_active(self):
        return self.get_queryset().filter(is_active=True, publish_time__lte=timezone.now())


class NewsItem(FlatPage, DisqusIDMixin):
    created_time = models.DateTimeField(_(u'Время создания'), auto_now_add=True)
    modified_time = models.DateTimeField(_(u'Время изменения'), auto_now=True)
    publish_time = models.DateTimeField(_(u'Время начала отображения'), default=timezone.now)

    is_active = models.BooleanField(_(u'Отображать новость'), default=True)
    preview_text = models.CharField(_(u'Короткое описание новости'), max_length=160)

    objects = NewsManager()

    class Meta:  # noqa: D101
        ordering = ['-publish_time']

    def get_absolute_url(self):
        return reverse('news-item-detail', kwargs={'url': self.url})

    @classmethod
    def disqus_id_prefix(cls):
        return 'news-'

    @property
    def disqus_title(self):
        return _(u'Новости: «{news_item.title}»').format(news_item=self)
