# -*- coding: utf-8 -*-
from django.apps import AppConfig


class NewsAppConfig(AppConfig):
    name = 'news'
    verbose_name = "Tabletop News"
