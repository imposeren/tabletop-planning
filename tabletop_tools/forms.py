# -*- coding: utf-8 -*-

from __future__ import unicode_literals
import time
from io import StringIO


# Python lib:
import random

# django
from django import forms
from django.core.cache import cache
from django.core.files.base import ContentFile
from django.utils.translation import ugettext_lazy as _

# Third-parties:
# import pynames
import requests
from crispy_forms.layout import Layout

# project:
from tabletop_planning.forms import BootstrapForm
# from tabletop_tools.utils import ConfigurableGenerator
from tabletop_planning.utils import get_cache_key


pynames = None
ConfigurableGenerator = None


class GeneratorForm(BootstrapForm):
    gender = forms.ChoiceField(
        label=_('Пол'),
        choices=(('m', _('мужской')), ('f', _('женский')), ('a', _('любой'))),
        initial='a'
    )

    def __init__(self, *args, **kwargs):
        super(GeneratorForm, self).__init__(*args, **kwargs)
        self._default_submit_button.content = _('Генерировать')
        self.helper.form_method = 'GET'
        self.helper.form_class = 'form-inline'
        self.helper.field_class = 'input-group'
        self.helper.layout = Layout(
            'gender',
            self._submit_action,
        )

    def clean_gender(self):
        gender = self.cleaned_data['gender']
        if gender == 'a':
            return random.choice(pynames.GENDER.ALL)
        elif gender == 'm':
            return pynames.GENDER.MALE
        elif gender == 'f':
            return pynames.GENDER.FEMALE
        else:
            raise forms.ValidationError(_('Недопустимое значение пола'), code='wrong-choice')


class CustomGeneratorForm(BootstrapForm):
    tables_file = forms.FileField(
        label=_('Файл данных'),
    )
    tables_file_url = forms.URLField(
        label=_('Ссылка к css файлу'),
    )
    help_text = _(
        "Загрузите файл в формате csv, с названием полей на первой строке."
        " Либо укажите ссылку на файл в интернете."
        " Большинство програм работающих с таблицами (MS Excel, Libreoffice/Openpffice Calc)"
        " умеют сохранять в формат csv."
        "\n"
        "Пример: http://goo.gl/ZAQCp0"
    )

    def __init__(self, *args, **kwargs):
        super(CustomGeneratorForm, self).__init__(*args, **kwargs)
        self._default_submit_button.content = _('Генерировать')
        self.fields['tables_file'].widget.attrs['accept'] = 'text/csv,.csv,.txt'
        self.helper.form_method = 'POST'
        self.helper.form_class = 'form-inline'
        self.helper.field_class = 'input-group'
        self.helper.layout = Layout(
            'tables_file',
            self._submit_action,
        )

    def generate(self):
        tables_file = self.cleaned_data['tables_file']
        try:
            class CustomGenerator(ConfigurableGenerator):
                SOURCE = [tables_file]

                def source_loader(self, source_paths, create_missing_tables=True):
                    self.load_tables(source_paths[0])

            generator = CustomGenerator()

            return generator.get_name_simple()
        except Exception:
            self.add_error(
                'tables_file',
                forms.ValidationError(
                    _('Ошибка при обработке файла %(tables_file)s'),
                    params={'tables_file': tables_file.name}
                )
            )


class UrlGeneratorForm(BootstrapForm):
    tables_file_url = forms.URLField(
        label=_('Ссылка к csv файлу'),
    )
    help_text = _(
        "Укажите ссылку на csv файл в интернете. В первой строке должны быть названия полей."
        " Большинство програм работающих с таблицами (MS Excel, Libreoffice/Openpffice Calc)"
        " умеют сохранять в формат csv."
        "\n"
        "Пример: http://goo.gl/ZAQCp0"
        "\n"
        "Также возможно использовать опубликованные файлы google spreadsheets указав csv формат. Например:"
        "\n"
        "https://docs.google.com/spreadsheets/d/1Xa9vMumxKAampuFRQSp4yqUJbQ32RZsyctihR5fUrcQ/pub?output=csv"
        "\n"
        "Внимание! Файлы скачиваются к нам на сервер, после чего не будут повторно качаться около 24 часов."
    )

    def __init__(self, *args, **kwargs):
        super(UrlGeneratorForm, self).__init__(*args, **kwargs)
        self._default_submit_button.content = _('Генерировать')
        self.helper.form_method = 'GET'
        self.helper.form_class = 'form-inline'
        self.helper.field_class = 'input-group'
        self.helper.layout = Layout(
            'tables_file_url',
            self._submit_action,
        )

    def download_file(self, file_url):
        head_response = requests.head(file_url)
        head_response.raise_for_status()
        result_file = None

        head_size = head_response.headers.get('content-length', 0)
        # using content-length in cache key to invalidate when file changes
        cache_key = get_cache_key('UrlGeneratorForm.download_file', file_url, head_size)

        errors_map = {
            'size_limit': _('Файл должен быть меньше чем 2 МБ'),
            'time_limit': _('Превышено время скачивания файла'),
        }
        result_content, error_code = cache.get(cache_key, (None, None))
        if (result_content is None) and (error_code is None):
            timeout = 5
            max_size = 2*1024*1024
            r = requests.get(file_url, stream=True, timeout=timeout)
            content_stream = StringIO()
            r.raise_for_status()

            content_length = int(r.headers.get('content-length') or 0)

            if content_length > max_size:
                error_code = 'size_limit'
            else:
                size = 0
                start = time.time()

                for chunk in r.iter_content(1024):
                    if time.time() - start > timeout:
                        error_code = 'time_limit'
                        break

                    size += len(chunk)
                    content_stream.write(chunk)
                    if size > max_size:
                        error_code = 'size_limit'
                        break
                if not error_code:
                    result_content = content_stream.getvalue()
            if content_length == 0:
                timeout = 3600
            else:
                timeout = 24*3600

            cache.set(cache_key, (result_content, error_code), timeout)

        if error_code:
            self.add_error(
                'tables_file_url',
                forms.ValidationError(errors_map[error_code])
            )
            return None
        else:
            result_file = ContentFile(result_content)

        return result_file

    def generate(self):
        try:
            tables_file_url = self.cleaned_data['tables_file_url']
            tables_file = self.download_file(tables_file_url)
            if tables_file:
                class CustomGenerator(ConfigurableGenerator):
                    SOURCE = [tables_file]

                    def source_loader(self, source_paths, create_missing_tables=True):
                        self.load_tables(source_paths[0])

                generator = CustomGenerator()

                return generator.get_name_simple()
        except Exception:
            self.add_error(
                'tables_file',
                forms.ValidationError(
                    _('Ошибка при обработке файла'),
                )
            )
