# -*- coding: utf-8 -*-
from __future__ import unicode_literals

# python lib:
import random

# django:
from django.http import Http404
from django.shortcuts import render


# thirdparties:
# import pynames

# project:
from tabletop_tools.utils import get_generator_groups
from tabletop_tools.forms import GeneratorForm, CustomGeneratorForm, UrlGeneratorForm


pynames = None


def name_generator(request, generator_path=None):

    result = None
    form = None
    generator = None

    generator_groups = get_generator_groups()

    if generator_path:
        try:
            module_name, class_name = generator_path.split('.')
            generator = generator_groups[module_name][class_name]()
            generator_groups[module_name].selected = True
            generator_groups[module_name][class_name].selected = True
        except (KeyError, ValueError):
            raise Http404

        form = GeneratorForm(request.GET or None, request.FILES or None)
        if form.is_valid() or not request.GET:
            cleaned_data = getattr(form, 'cleaned_data', {})
            result = generator.get_name_simple(cleaned_data.get('gender') or random.choice(pynames.GENDER.ALL))

    context = {
        'generator_groups': generator_groups.values(),
        'form': form,
        'result': result,
    }
    return render(request, 'tabletop_tools/name_generator.html', context)


def custom_generator(request):
    form = CustomGeneratorForm(request.POST or None, request.FILES or None)
    generator_groups = get_generator_groups()

    result = ''

    if form.is_valid():
        result = form.generate()
    context = {
        'generator_groups': generator_groups.values(),
        'form': form,
        'result': result,
        'custom_is_active': True,
    }
    return render(request, 'tabletop_tools/name_generator.html', context)


def custom_url_generator(request):
    form = UrlGeneratorForm(request.GET or None, request.FILES or None)
    generator_groups = get_generator_groups()

    result = ''

    if form.is_valid():
        result = form.generate()
    context = {
        'generator_groups': generator_groups.values(),
        'form': form,
        'result': result,
        'custom_url_is_active': True,
    }
    return render(request, 'tabletop_tools/name_generator.html', context)
