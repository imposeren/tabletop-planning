# -*- coding: utf-8 -*-

from __future__ import unicode_literals

# python lib:
# None

# django:
from django.utils.translation import ugettext_lazy as _

# third-parties:
# import pynames
# from pynames.from_tables_generator import FromCSVTablesGenerator, Template

# project:
# None


pynames = None


class GeneratorsGroup(dict):

    group_names = {
        'elven': _('Эльфы'),
        'goblin': _('Гоблины'),
        'iron_kingdoms': _('Iron Kingdoms®'),
        'korean': _('Корейские имена'),
        'mongolian': _('Монгольские имены'),
        'orc': _('Орочьи имена'),
        'russian': _('Русские имена'),
        'scandinavian': _('Скандинавские имена'),
    }

    generator_names = {
        'WarhammerNamesGenerator': _('Эльфийские имена из Warhammer'),
        'DnDNamesGenerator': _('Эльфийские имена из DnD'),
        'GoblinGenerator': _('Гоблинские имена'),
        'GobberFullnameGenerator': _('Полные имена Gobber'),
        'ThurianMorridaneFullnameGenerator': _('Полные имена Thurian'),
        'TordoranFullnameGenerator': _('Полные имена Tordoran'),
        'RynFullnameGenerator': _('Полные имена Ryn'),
        'DwarfFullnameGenerator': _('Полные имена Dwarf'),
        'IossanNyssFullnameGenerator': _('Полные имена Iossan/Nyss'),
        'CaspianMidlunderSuleseFullnameGenerator': _('Полные имена Caspian/Midlunder/Sulese'),
        'KhadoranFullnameGenerator': _('Полные имена Khadoran'),
        'OgrunFullnameGenerator': _('Полные имена Ogrun'),
        'TrollkinFullnameGenerator': _('Полные имена Trollkin'),
        'KoreanNamesGenerator': _('Корейские имена'),
        'MongolianNamesGenerator': _('Монгольские имена'),
        'OrcNamesGenerator': _('Орочьи имена'),
        'PaganNamesGenerator': _('Славянские имена'),
        'ScandinavianNamesGenerator': _('Скандинавские имена'),
    }

    def __init__(self, short_name, *args, **kwargs):
        self.short_name = short_name
        self.verbose_name = self.group_names.get(self.short_name, self.short_name)
        self.selected = False
        super(GeneratorsGroup, self).__init__(*args, **kwargs)

    def detalize_generator(self, obj):
        obj_name = obj.__name__
        obj.path = '%s.%s' % (self.short_name, obj_name)
        obj.verbose_name = self.generator_names.get(obj_name, obj_name)
        obj.group = self
        obj.selected = False

    def add(self, obj):
        self.detalize_generator(obj)
        self[obj.__name__] = obj


def get_generator_groups():
    generator_classes = pynames.get_all_generators()
    generator_groups = {}

    for generator_class in generator_classes:
        module_name = generator_class.__module__.split('.')[-1]
        generator_group = generator_groups.setdefault(module_name, GeneratorsGroup(module_name))
        generator_group.add(generator_class)

    return generator_groups


# class ConfigurableGenerator(FromCSVTablesGenerator):
#
#     def __init__(self, *args, **kwargs):
#         self.native_language = ''
#         self.languages = []
#         self.templates = []
#         self.tables = {}
#         super(ConfigurableGenerator, self).__init__(*args, **kwargs)
#
#     def load_tables(self, *args, **kwargs):
#
#         self.native_language = self.native_language or 'en'
#         self.languages = self.languages or ['en']
#
#         super(ConfigurableGenerator, self).load_tables(*args, **kwargs)
#
#         if not self.templates:
#             all_included_template = {
#                 'probability': 1,
#                 'genders': ['m', 'f'],
#                 'template': []
#             }
#             for slug in self.tables.keys():
#                 extra_template_part = [('%s: ' % slug), slug, '\n\n']
#                 all_included_template['template'].extend(extra_template_part)
#
#             self.templates.append(
#                 Template('all_inclusive', self.native_language, self.languages, all_included_template)
#             )
#             self.create_missing_tables(all_included_template['template'])
