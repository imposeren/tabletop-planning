# -*- coding: utf-8 -*-
from django.apps import AppConfig


class ToolsAppConfig(AppConfig):
    name = 'tabletop_tools'
    verbose_name = "Tabletop tools"
    models_module = None
