# -*- coding: utf-8 -*-

import io
import os
import json

from django.conf import settings
from django.utils.encoding import smart_text

from compressor.cache import get_hexdigest
from compressor.filters.base import shell_quote
from compressor.filters.closure import ClosureCompilerFilter


class SourceMappingClosureCompilerFilter(ClosureCompilerFilter):

    def get_source_maps_path(self):
        return getattr(settings, 'COMPRESS_SOURCE_MAPS_DIR', '')

    def input(self, **kwargs):
        if self.get_source_maps_path():
            return self.content
        else:
            return super(SourceMappingClosureCompilerFilter, self).input(**kwargs)

    def output(self, **kwargs):
        source_maps_path = self.get_source_maps_path()
        if source_maps_path:
            source_map_root = source_path = os.path.join(settings.STATIC_ROOT, source_maps_path)
            if not os.path.exists(source_map_root):
                os.makedirs(source_map_root)
            kwargs = dict(kwargs)
            old_command = self.command
            old_options = self.options
            old_charset = self.charset

            os.environ.pop('_JAVA_OPTIONS', '')

            # save contents to new "source" file:
            source_name = get_hexdigest(self.content) + '.js'
            source_path = os.path.join(source_map_root, source_name)
            map_path = source_path + '.map'
            with io.open(source_path, 'w', encoding=self.default_encoding) as f:
                f.write(self.content)

            source_url = settings.STATIC_URL + source_maps_path + '/' + source_name
            map_url = source_url + '.map'

            self.command = "{binary} {args} --create_source_map {map_file} --warning_level QUIET --js_output_file {outfile}"
            self.options = self.options + (('map_file', shell_quote(map_path)), )
            self.charset = self.default_encoding

            res = super(SourceMappingClosureCompilerFilter, self).input(**kwargs)
            res += smart_text('\n//# sourceMappingURL={map_url}'.format(map_url=map_url))
            with io.open(map_path, 'r', encoding=self.default_encoding) as map_file:
                map_data = json.load(map_file)
            map_data['sources'] = [source_url]
            with io.open(map_path, 'w', encoding=self.default_encoding) as map_file:
                json.dump(map_data, map_file)
            self.command = old_command
            self.options = old_options
            self.charset = old_charset
            return res
        else:
            return super(SourceMappingClosureCompilerFilter, self).output(**kwargs)
