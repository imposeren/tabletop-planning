# -*- coding: utf-8 -*-
import datetime

from django.conf import settings
from django.utils import translation, timezone

from games.models import SessionVisit
from tabletop_planning.utils import LoggedCommand
from tags.models import (
    SystemTaggedItem, SettingTaggedItem, OtherTaggedItem,
)


class Command(LoggedCommand):
    help = 'Update sessions count for tags'

    def handle(self, *args, **options):
        translation.activate(settings.LANGUAGE_CODE)

        verbosity = options.get('verbosity')

        config = (
            ('visits_count_month', 60),  # Visits for last month are usually not validated, so let's count for 2 months
            ('visits_count_year', 365),
            ('visits_count_all', None),
        )
        for tagged_item_model in (SystemTaggedItem, SettingTaggedItem, OtherTaggedItem):
            updated_tags = 0
            tag_model = tagged_item_model.tag_model()
            tags = tag_model.objects.all()
            for tag in tags.iterator():
                tagged_games = tagged_item_model.objects.filter(tag=tag).values_list('object_id').distinct(
                    'object_id')
                for count_field, days in config:
                    if days:
                        extra_filter = {
                            'cached_start_date__gte': timezone.now() - datetime.timedelta(days=days),
                        }
                    else:
                        extra_filter = {}
                    visits = SessionVisit.objects.filter(
                        game_session__game_id__in=tagged_games,
                        have_visited=True,
                        verified=True,
                        **extra_filter
                    ).distinct('pk')
                    prev_count = getattr(tag, count_field)
                    new_count = visits.count()
                    if prev_count != new_count:
                        updated_tags += 1
                        setattr(tag, count_field, new_count)
                        tag.save(update_fields=[count_field])
            if verbosity >= 2:
                print(f'Updated {updated_tags} {tag_model.__name__}')
