# -*- coding: utf-8 -*-
from django.conf import settings
from django.utils import translation

from tabletop_planning.utils import LoggedCommand
from tags.models import (
    SystemTaggedItem, SettingTaggedItem, OtherTaggedItem,
)


class Command(LoggedCommand):
    help = 'Cleanup unused tags'

    def handle(self, *args, **options):
        translation.activate(settings.LANGUAGE_CODE)

        self.verbosity = options.get('verbosity')

        for tagged_model in (SystemTaggedItem, SettingTaggedItem, OtherTaggedItem):
            tag_model = tagged_model.tag_model()
            excluded_tag_ids = tagged_model.objects.all().values_list('tag_id')
            num_deleted, __ = tag_model.objects.all().exclude(pk__in=excluded_tag_ids).delete()
            if self.verbosity >= 1:
                self.stdout.write(f'Deleted {num_deleted} {tag_model.__name__} objects.')
