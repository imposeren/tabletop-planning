# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from tabletop_planning.tests import BaseTestCase


class TagModelsTestCase(BaseTestCase):
    def test_tag_synonyms(self):
        from tags.models import SystemTag, SettingTag, OtherTag, TagSynonym
        from games.models import Game

        TagSynonym.objects.create(source_tag_name='dnd35', target_tag_name='dnd3.5')
        TagSynonym.objects.create(source_tag_name='d&d35', target_tag_name='dnd3.5')
        TagSynonym.objects.create(
            source_tag_name=None,
            source_tag_regex=r'^(d|д)(н|-н-|n|-n-|&|&amp;)(d|д).?3\.?0?$',
            target_tag_name='dnd3.0',
        )

        model_field_map = {
            SystemTag: 'system_tags',
            SettingTag: 'setting_tags',
            OtherTag: 'other_tags',
        }

        for tag_model, field_name in model_field_map.items():
            with self.subTest(field_name=field_name):
                tag0 = tag_model.objects.create(name='d-n-d3')
                tag1 = tag_model.objects.create(name='dnd3')
                tag1_ru = tag_model.objects.create(name='днд3')
                tag2, __ = tag_model.objects.get_or_create(name='dnd3.0')
                tag3 = tag_model.objects.create(name='dnd35')
                tag4 = tag_model.objects.create(name='d&d35')
                tag5, __ = tag_model.objects.get_or_create(name='dnd3.5')

                self.assertEqual(TagSynonym.get_synonym_tag(tag0), tag2)
                self.assertEqual(TagSynonym.get_synonym_tag(tag1), tag2)
                self.assertEqual(TagSynonym.get_synonym_tag(tag1_ru), tag2)
                self.assertEqual(TagSynonym.get_synonym_tag(tag2), None)

                self.assertEqual(TagSynonym.get_synonym_tag(tag3), tag5)
                self.assertEqual(TagSynonym.get_synonym_tag(tag4), tag5)
                self.assertEqual(TagSynonym.get_synonym_tag(tag5), None)

                games_tag2 = []

                for tag in [tag0, tag1, tag1_ru, tag2]:
                    game = Game.objects.create(name='h-'+tag.name)
                    getattr(game, field_name).add(tag)
                    games_tag2.append(game)

                games_tag5 = []

                for tag in [tag3, tag4, tag5]:
                    game = Game.objects.create(name='h+'+tag.name)
                    getattr(game, field_name).add(tag)
                    games_tag5.append(game)

                # TagSynonym.process_all_synonyms()

                for game in games_tag2:
                    game.refresh_from_db()
                    game_tags = list(getattr(game, field_name).all())
                    self.assertEqual(
                        game_tags,
                        [tag2],
                        msg="%s of game '%s' are '%s' instead of '%s'" % (field_name, game, game_tags, [tag2])
                    )

                for game in games_tag5:
                    game.refresh_from_db()
                    self.assertEqual(
                        list(getattr(game, field_name).all()),
                        [tag5]
                    )

                tag6 = tag_model.objects.create(name='ctulu')
                tag7 = tag_model.objects.create(name='cthulhu')

                cthulhu_games = []
                cthulhu_tags = [tag6, tag7]

                for tag in [tag6, tag7]:
                    game = Game.objects.create(name='h-'+tag.name)
                    getattr(game, field_name).add(tag)
                    cthulhu_games.append(game)

                TagSynonym.process_all_synonyms()

                # no changes!
                for i, game in enumerate(cthulhu_games):
                    game = Game.objects.get(pk=game.pk)
                    self.assertEqual(
                        list(getattr(game, field_name).all()),
                        [cthulhu_tags[i]]
                    )

                ts = TagSynonym.objects.create(source_tag_name=tag6.name, target_tag_name=tag7.name)

                # when TagSynonym is saved: tags should be replaced automatically
                for i, game in enumerate(cthulhu_games):
                    game = Game.objects.get(pk=game.pk)
                    self.assertEqual(
                        list(getattr(game, field_name).all()),
                        [tag7]
                    )

                ts.delete()
                Game.objects.all().delete()
