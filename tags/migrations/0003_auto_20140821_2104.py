# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tags', '0002_auto_20140818_1902'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='othertag',
            options={'verbose_name': '\u0442\u0435\u0433 (\u043f\u0440\u043e\u0447\u0435\u0435)', 'verbose_name_plural': '\u0442\u0435\u0433\u0438 (\u043f\u0440\u043e\u0447\u0435\u0435)'},
        ),
        migrations.AlterModelOptions(
            name='settingtag',
            options={'verbose_name': '\u0442\u0435\u0433 \u0441\u0435\u0442\u0442\u0438\u043d\u0433\u0430', 'verbose_name_plural': '\u0442\u0435\u0433\u0438 \u0441\u0435\u0442\u0442\u0438\u043d\u0433\u043e\u0432'},
        ),
        migrations.AlterModelOptions(
            name='systemtag',
            options={'verbose_name': '\u0442\u0435\u0433 \u0441\u0438\u0441\u0442\u0435\u043c\u044b', 'verbose_name_plural': '\u0442\u0435\u0433\u0438 \u0441\u0438\u0441\u0442\u0435\u043c'},
        ),
    ]
