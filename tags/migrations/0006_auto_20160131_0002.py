# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tags', '0005_tagsynonym'),
    ]

    operations = [
        migrations.AlterIndexTogether(
            name='othertaggeditem',
            index_together=set([('content_type', 'object_id')]),
        ),
        migrations.AlterIndexTogether(
            name='settingtaggeditem',
            index_together=set([('content_type', 'object_id')]),
        ),
        migrations.AlterIndexTogether(
            name='systemtaggeditem',
            index_together=set([('content_type', 'object_id')]),
        ),
    ]
