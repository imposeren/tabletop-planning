# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tags', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='othertaggeditem',
            name='tag',
            field=models.ForeignKey(to='tags.OtherTag', on_delete=models.CASCADE),
        ),
    ]
