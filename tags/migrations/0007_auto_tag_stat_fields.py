# Generated by Django 2.2.21 on 2021-05-10 14:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tags', '0006_auto_20160131_0002'),
    ]

    operations = [
        migrations.AddField(
            model_name='othertag',
            name='visits_count_all',
            field=models.PositiveIntegerField(default=0, verbose_name='Подтверждённых посещений за всё время'),
        ),
        migrations.AddField(
            model_name='othertag',
            name='visits_count_month',
            field=models.PositiveIntegerField(default=0, verbose_name='Подтверждённых посещений за месяц'),
        ),
        migrations.AddField(
            model_name='othertag',
            name='visits_count_year',
            field=models.PositiveIntegerField(default=0, verbose_name='Подтверждённых посещений за год'),
        ),
        migrations.AddField(
            model_name='settingtag',
            name='visits_count_all',
            field=models.PositiveIntegerField(default=0, verbose_name='Подтверждённых посещений за всё время'),
        ),
        migrations.AddField(
            model_name='settingtag',
            name='visits_count_month',
            field=models.PositiveIntegerField(default=0, verbose_name='Подтверждённых посещений за месяц'),
        ),
        migrations.AddField(
            model_name='settingtag',
            name='visits_count_year',
            field=models.PositiveIntegerField(default=0, verbose_name='Подтверждённых посещений за год'),
        ),
        migrations.AddField(
            model_name='systemtag',
            name='visits_count_all',
            field=models.PositiveIntegerField(default=0, verbose_name='Подтверждённых посещений за всё время'),
        ),
        migrations.AddField(
            model_name='systemtag',
            name='visits_count_month',
            field=models.PositiveIntegerField(default=0, verbose_name='Подтверждённых посещений за месяц'),
        ),
        migrations.AddField(
            model_name='systemtag',
            name='visits_count_year',
            field=models.PositiveIntegerField(default=0, verbose_name='Подтверждённых посещений за год'),
        ),
    ]
