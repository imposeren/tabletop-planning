# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tags', '0003_auto_20140821_2104'),
    ]

    operations = [
        migrations.AlterField(
            model_name='othertaggeditem',
            name='content_type',
            field=models.ForeignKey(related_name='tags_othertaggeditem_tagged_items', verbose_name='Content type', to='contenttypes.ContentType', on_delete=models.CASCADE),
        ),
        migrations.AlterField(
            model_name='othertaggeditem',
            name='tag',
            field=models.ForeignKey(related_name='tags_othertaggeditem_other_items', to='tags.OtherTag', on_delete=models.CASCADE),
        ),
        migrations.AlterField(
            model_name='settingtaggeditem',
            name='content_type',
            field=models.ForeignKey(related_name='tags_settingtaggeditem_tagged_items', verbose_name='Content type', to='contenttypes.ContentType', on_delete=models.CASCADE),
        ),
        migrations.AlterField(
            model_name='settingtaggeditem',
            name='tag',
            field=models.ForeignKey(related_name='tags_settingtaggeditem_setting_items', to='tags.SettingTag', on_delete=models.CASCADE),
        ),
        migrations.AlterField(
            model_name='systemtaggeditem',
            name='content_type',
            field=models.ForeignKey(related_name='tags_systemtaggeditem_tagged_items', verbose_name='Content type', to='contenttypes.ContentType', on_delete=models.CASCADE),
        ),
        migrations.AlterField(
            model_name='systemtaggeditem',
            name='tag',
            field=models.ForeignKey(related_name='tags_systemtaggeditem_system_items', to='tags.SystemTag', on_delete=models.CASCADE),
        ),
    ]
