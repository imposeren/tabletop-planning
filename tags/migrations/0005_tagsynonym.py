# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tags', '0004_auto_20140912_1742'),
    ]

    operations = [
        migrations.CreateModel(
            name='TagSynonym',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('source_tag_name', models.CharField(default=None, max_length=100, unique=True, null=True, blank=True)),
                ('source_tag_regex', models.CharField(default=None, max_length=100, unique=True, null=True, blank=True)),
                ('target_tag_name', models.CharField(max_length=100)),
            ],
        ),
    ]
