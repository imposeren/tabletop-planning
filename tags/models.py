# -*- coding: utf-8 -*-

# python builtin:
# ...

# django:
from django.core.exceptions import ValidationError
try:
    from django.core.urlresolvers import reverse
except ImportError:
    from django.urls import reverse
from django.core.cache import cache
from django.db import models
from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _
from django.utils.http import urlquote_plus

# third-parties:
from taggit.models import GenericTaggedItemBase, TagBase
from unidecode import unidecode
import regex


# project:
from tabletop_planning.utils import get_cache_key


def clean_tag_name(name):
    name = name.strip()
    bad_edges = ',.:'
    for bad_edge in bad_edges:
        while name.endswith(bad_edge):
            name = name[:-1]
            name = name.strip()
        while name.startswith(bad_edge):
            name = name[1:]
            name = name.strip()

    return name


class MyTagBase(TagBase):

    visits_count_month = models.PositiveIntegerField(
        _('Подтверждённых посещений за месяц'), default=0,
    )
    visits_count_year = models.PositiveIntegerField(
        _('Подтверждённых посещений за год'), default=0,
    )
    visits_count_all = models.PositiveIntegerField(
        _('Подтверждённых посещений за всё время'), default=0,
    )

    class Meta:  # noqa: D101
        abstract = True

    def clean(self):
        self.name = clean_tag_name(self.name)

    def slugify(self, tag, i=None):
        transliterated = unidecode(tag)
        slug = slugify(transliterated)
        if i is not None:
            if transliterated != tag and i == 1:
                slug += '_translit'
            else:
                slug += "_%d" % i
        return slug

    def save(self, *args, **kwargs):
        self.name = self.name.lower()
        super(MyTagBase, self).save(*args, **kwargs)

    def get_tagged_games_url(self):
        return reverse('games:players_required_tagged', kwargs={'tag': urlquote_plus(self.name)})


class SystemTag(MyTagBase):

    class Meta:  # noqa: D101
        verbose_name = _(u'тег системы')
        verbose_name_plural = _(u'теги систем')


class SettingTag(MyTagBase):

    class Meta:  # noqa: D101
        verbose_name = _(u'тег сеттинга')
        verbose_name_plural = _(u'теги сеттингов')


class OtherTag(MyTagBase):

    class Meta:  # noqa: D101
        verbose_name = _(u'тег (прочее)')
        verbose_name_plural = _(u'теги (прочее)')


class SystemTaggedItem(GenericTaggedItemBase):
    tag = models.ForeignKey(
        SystemTag,
        related_name="%(app_label)s_%(class)s_system_items",
        on_delete=models.CASCADE,
    )

    class Meta:  # noqa: D101
        index_together = (('content_type', 'object_id'), )


class SettingTaggedItem(GenericTaggedItemBase):
    tag = models.ForeignKey(
        SettingTag,
        related_name="%(app_label)s_%(class)s_setting_items",
        on_delete=models.CASCADE,
    )

    class Meta:  # noqa: D101
        index_together = (('content_type', 'object_id'), )


class OtherTaggedItem(GenericTaggedItemBase):
    tag = models.ForeignKey(
        OtherTag,
        related_name="%(app_label)s_%(class)s_other_items",
        on_delete=models.CASCADE,
    )

    class Meta:  # noqa: D101
        index_together = (('content_type', 'object_id'), )


class TagSynonym(models.Model):

    # nulls are still allowed when unique=True. So making both fields nullable
    source_tag_name = models.CharField(max_length=100, blank=True, unique=True, null=True, default=None)
    source_tag_regex = models.CharField(max_length=100, blank=True, unique=True, null=True, default=None)
    target_tag_name = models.CharField(max_length=100)

    REGEXP_CACHE_KEY = get_cache_key('tags.models.TagSynonym.get_synonym_tag:regexp')

    class Meta:  # noqa: D101
        verbose_name = _('Синоним тега')
        verbose_name_plural = _('Синонимы тегов')

    def __str__(self):
        source = self.source_tag_name or self.source_tag_regex
        return f'{self.pk}: {source} -> {self.target_tag_name}'

    def save(self, *args, **kwargs):
        super(TagSynonym, self).save(*args, **kwargs)
        self.replace_all_games_tags()
        # cache.invalidate_tags('models:%s' % self._meta.db_table)

    save.alters_data = True

    def delete(self, *args, **kwargs):
        super(TagSynonym, self).delete(*args, **kwargs)
        # cache.invalidate_tags('models:%s' % self._meta.db_table)
    delete.alters_data = True

    def clean(self):
        self.source_tag_name = (self.source_tag_name or '').lower()
        self.target_tag_name = clean_tag_name(self.target_tag_name)

        if not self.source_tag_name and not self.source_tag_regex:
            raise ValidationError(
                _('Необходимо указать source_tag_name или source_tag_regex'),
            )
        if self.source_tag_name and self.source_tag_regex:
            raise ValidationError(
                _('Нельзя указывать source_tag_name и source_tag_regex одновременно'),
            )
        if self.source_tag_name.lower() == self.target_tag_name.lower():
            raise ValidationError(
                _('source_tag_name и target_tag_name должны отличаться'),
            )

        if not self.source_tag_name:
            self.source_tag_name = None
        if not self.source_tag_regex:
            self.source_tag_regex = None
        if self.source_tag_name:
            self.source_tag_name = clean_tag_name(self.source_tag_name)

    @classmethod
    def get_synonym_tag_cache_key(cls, tag_name, tag_model):
        return get_cache_key(
            'tags.models.TagSynonym.get_synonym_tag:result',
            tag_name,
            tag_model._meta.db_table,
        )

    @classmethod
    def get_synonym_tag(cls, tag_or_tag_name, tag_model=None, create=False):
        if isinstance(tag_or_tag_name, models.Model):
            tag = tag_or_tag_name
            if tag_model and tag_model != tag.__class__:
                raise ValueError('tag_model arg specifed but does not match class of passed tag')
            tag_model = tag.__class__
            tag_name = tag.name
            del tag
        else:
            if not tag_model:
                raise ValueError("tag_model argument is required when tag_or_tag_name is not model instance ")
            tag_name = tag_or_tag_name

        if not tag_name:
            raise ValueError("tag name is required")

        # cache_version = 2
        # cache_timeout = 29*24*3600
        # cache_tags = ['models:%s' % cls._meta.db_table]
        # result_pk_cache_key = cls.get_synonym_tag_cache_key(tag_name, tag_model)
        result = None
        # result_pk = cache.get(result_pk_cache_key, False)
        result_pk = False
        # result_pk = False
        regex_target_vals = None
        if result_pk is False or (result_pk is None and create):
            if cls.objects.filter(target_tag_name__iexact=tag_name).exists():
                # tag_name is already a result of synonym search
                result = None
                result_pk = None
            else:
                synonym_objects = cls.objects.filter(source_tag_name__iexact=tag_name)
                target_name = synonym_objects.values_list('target_tag_name', flat=True).first()
                if not target_name:
                    # cache_key = cls.REGEXP_CACHE_KEY
                    # regex_target_vals = cache.get(cache_key, None, version=cache_version)

                    if regex_target_vals is None:
                        regex_target_vals = list(
                            cls.objects
                            .exclude(source_tag_regex='')
                            .exclude(source_tag_regex__isnull=True)
                            .values_list('source_tag_regex', 'target_tag_name')
                        )
                        # cache.set(cache_key, regex_target_vals, timeout=cache_timeout, tags=cache_tags, version=cache_version)

                    try:
                        for regex_val, target_name in regex_target_vals:
                            if regex.match(regex_val, tag_name, regex.IGNORECASE):
                                break
                        else:
                            target_name = None
                    except ValueError:
                        pass
                        # cache.delete(cache_key, version=cache_version)
                if target_name:
                    result = tag_model.objects.filter(name__iexact=target_name).first()
                    if create and not result:
                        result = tag_model.objects.create(name=target_name.lower())
                    result_pk = result and result.pk
                else:
                    result = None
                    result_pk = None
            # cache.set(result_pk_cache_key, result_pk, timeout=cache_timeout, tags=cache_tags, version=cache_version)
        elif result_pk:
            result = tag_model.objects.get(pk=result_pk)
        return result

    def replace_game_tags(self, game):
        system_invalidation_names = set()
        setting_invalidation_names = set()
        other_invalidation_names = set()
        field_config = (
            ('system_tags', system_invalidation_names),
            ('setting_tags', setting_invalidation_names),
            ('other_tags', other_invalidation_names),
        )
        for tag_field, invalidation_names in field_config:
            new_tags = []
            tags_changed = False
            for tag in getattr(game, tag_field).all():
                synonym_tag = self.__class__.get_synonym_tag(tag)
                tags_changed = tags_changed or bool(synonym_tag)
                new_tags.append(synonym_tag or tag)
                if synonym_tag:
                    invalidation_names.add(tag.name)
                    invalidation_names.add(synonym_tag.name)
            if tags_changed:
                getattr(game, tag_field).set(*set(new_tags))
        return system_invalidation_names, setting_invalidation_names, other_invalidation_names

    def get_matches_qs(self, model):
        if isinstance(model, TagBase):
            prefix = ''
        else:
            prefix = 'tag__'
        if self.source_tag_name:
            filter_kwargs = {
                f'{prefix}name__iexact': self.source_tag_name,
            }
            exclude_kwargs = {}
        else:
            filter_kwargs = {
                f'{prefix}name__iregex': self.source_tag_regex,
            }
            exclude_kwargs = {
                f'{prefix}name__iexact': self.target_tag_name,
            }

        qs = model.objects.filter(**filter_kwargs)
        if exclude_kwargs:
            qs = qs.exclude(**exclude_kwargs)
        return qs

    def replace_all_games_tags(self):
        invalidate_keys = []

        for tagged_model in (SystemTaggedItem, SettingTaggedItem, OtherTaggedItem):
            tag_model = tagged_model.tag_model()
            qs = self.get_matches_qs(tagged_model)
            qs = qs.prefetch_related('content_object')
            target_tag = None
            for tagged_item in qs.iterator():
                if target_tag is None:
                    target_tag, _created = tag_model.objects.get_or_create(name=self.target_tag_name)
                game = tagged_item.content_object
                tagged_model.objects.create(tag=target_tag, content_object=game)
            qs.delete()
            # invalidate_keys.append(
            #     self.get_synonym_tag_cache_key(self.source_tag_name, tag_model)
            # )
        if invalidate_keys:
            cache.delete_many(invalidate_keys)
            pass

    @classmethod
    def process_all_synonyms(cls, cleanup_tags=False):
        for obj in cls.objects.all().iterator():
            obj.replace_all_games_tags()
            if cleanup_tags:
                obj.cleanup_tags()
