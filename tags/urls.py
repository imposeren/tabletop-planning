# -*- coding: utf-8 -*-
from django.conf.urls import url

from tags import views

urlpatterns = [
    url(
        r'^systemtag-autocomplete/$',
        views.SystemTagAutocomplete.as_view(),
        name='systemtag-autocomplete',
    ),
    url(
        r'^settingtag-autocomplete/$',
        views.SettingTagAutocomplete.as_view(),
        name='settingtag-autocomplete',
    ),
    url(
        r'^othertag-autocomplete/$',
        views.OtherTagAutocomplete.as_view(),
        name='othertag-autocomplete',
    ),
]

app_name = 'tags'
