# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from tags.models import SystemTag, SettingTag, OtherTag, TagSynonym


class TagAdmin(admin.ModelAdmin):
    list_display = ["name", "slug", "visits_count_all", "visits_count_year"]
    ordering = ["name", "slug"]
    search_fields = ["name", "slug"]
    prepopulated_fields = {"slug": ["name"]}


admin.site.register(SystemTag, TagAdmin)
admin.site.register(SettingTag, TagAdmin)
admin.site.register(OtherTag, TagAdmin)


class TagSynonymdmin(admin.ModelAdmin):
    list_display = ["source_tag_name", "source_tag_regex", 'target_tag_name']


admin.site.register(TagSynonym, TagSynonymdmin)
