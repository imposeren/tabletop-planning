# -*- coding: utf-8 -*-

# Django:
from django.db.models.signals import pre_save
from django.dispatch import receiver

# thirdparties
# from happenings.models import Event

# project:
from .models import SystemTaggedItem, SettingTaggedItem, OtherTaggedItem, TagSynonym


__all__ = (
    'anything_tagged',
)


@receiver(pre_save)
def anything_tagged(sender, instance, raw, using, update_fields, **kwargs):
    if raw or sender not in (SystemTaggedItem, SettingTaggedItem, OtherTaggedItem):
        return
    if update_fields and 'tag' not in update_fields:
        return

    tag_synonym = TagSynonym.get_synonym_tag(instance.tag, create=True)
    if tag_synonym and tag_synonym.pk != instance.tag.pk:
        instance.tag = tag_synonym
    return
