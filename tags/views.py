# -*- coding: utf-8 -*-
# django
from django.db.models import Q

# third-parties:
from dal import autocomplete
from unidecode import unidecode

from .models import SystemTag, SettingTag, TagSynonym, OtherTag, clean_tag_name


class TagSearchBaseView(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = self.model.objects.all()

        if not self.request.user.is_authenticated:
            return qs.none()

        q = clean_tag_name(self.q)
        tag_filter_query = Q()
        synonym_filter_query = Q()
        candidates = set()
        if q:
            candidates.add(q)
            translit = unidecode(q)
            candidates.add(translit)

            for ignorable in ' -_':
                if ignorable in q:
                    candidates.add(q.replace(' ', ''))
                    candidates.add(translit.replace(' ', ''))

            for candidate in candidates:
                if candidate:
                    tag_filter_query |= Q(name__istartswith=candidate) | Q(name__iendswith=candidate)
                    synonym_filter_query |= Q(source_tag_name__istartswith=candidate) | Q(source_tag_name__iendswith=candidate)

            synonyms = TagSynonym.objects.filter(synonym_filter_query)
            if synonyms.exists():
                tag_filter_query |= Q(name__in=synonyms.values_list('target_tag_name'))
            qs = qs.filter(tag_filter_query)
        return qs


class SystemTagAutocomplete(TagSearchBaseView):
    model = SystemTag


class SettingTagAutocomplete(TagSearchBaseView):
    model = SettingTag


class OtherTagAutocomplete(TagSearchBaseView):
    model = OtherTag
