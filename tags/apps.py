# -*- coding: utf-8 -*-
from django.apps import AppConfig


class TagsAppConfig(AppConfig):
    name = 'tags'
    verbose_name = "Tabletop Tags"

    def ready(self):
        from tags.signal_receivers import anything_tagged  # noqa: F401
        return
