# -*- coding: utf-8 -*-
from setuptools import setup

tests_require = [
    'freezegun == 0.1.18',
    # 'hacking ~= 4.1.0',
    "pycodestyle==2.8.0",
    'flake8 ~= 3.9.2',
    'flake8-django ~= 1.1.1',
    'flake8-docstrings ~= 1.6.0',
    'flake8-comprehensions ~= 3.5.0',
    'flake8-debugger ~= 4.0.0',
    'pep8-naming ~= 0.11.1',
    # 'pylint_django == 2.4.4',
    'coverage == 5.5',
    'django-nose == 1.4.7',
    'factory-boy == 2.11.1',
    'mock == 1.0.1',
]


setup(
    name='TabletopPlanning',
    version='2.0.1',
    description='Tabletop Roleplay Club Calendar management app',
    author='Yaroslav Klyuyev',
    author_email='imposeren@gmail.com',
    url='https://bitbucket.org/imposeren/tabletop-planning/',
    install_requires=[
        # Base:
        'Django~=2.2.20',
        'django-pgjson==0.3.1',

        # Django apps with models:
        'social-auth-app-django==5.0.0',
        'social-auth-core[openidconnect]==4.1.0',
        'django-sitegate==0.13.0',
        'django-post_office==3.0.4',
        'django-taggit==1.4.0',
        'django-scribbler==1.0.0',
        'django-tinymce==2.6.1',
        'django-messages-extends==0.6.2',
        'django-happenings-update==0.4.4',
        'django-contrib-comments==2.1.0',
        'django-comments-xtd==2.9.1',
        'django-reversion==3.0.5',
        'django-dynamic-preferences==1.10.1',

        # Django apps without models(maybe)
        'django-extensions==2.0.7',
        'django-model-utils==4.1.1',
        'django-crispy-forms==1.11.2',
        'django-parsley==0.7',
        'django-autocomplete-light==3.8.2',
        'django-el-pagination==3.3.0',
        'django-annoying==0.10.6',
        'django-compressor==2.4.1',
        'Wand==0.6.6',
        'sorl-thumbnail==12.7.0',
        'django-nested-formset==0.1.4',
        'django-filebrowser~=3.14.1',
        'django-grappelli==2.15.1',
        # 'cache-dependencies==0.7.7.47',  # FIXME: non-installable in python3.6
        'django-pylibmc==0.6.1',
        'django-analytical==2.6.0',

        # Other:
        # 'pynames==0.1.0',
        'regex==2021.4.4',
        'psycopg2-binary==2.8.6',
        'Pillow==7.2.0',
        'pytz==2018.4',
        'python-dateutil==2.7.3',
        'Unidecode==1.0.22',
        'requests==2.18.4',
        'raven==6.9.0',
        'pyOpenSSL==20.0.1',
        # 'gspread==0.2.5',

        # google-drive integrations
        'google-api-python-client==1.7.12',
        'oauth2client==4.1.3',

        # Version freezes:
        'cachetools==4.2.2',
        'certifi==2020.12.5',
        'cffi==1.14.5',
        'chardet==3.0.4',
        'cryptography==3.4.7',
        'djangorestframework==3.12.4',
        'Faker==8.1.2',
        'google-auth==1.30.0',
        'google-auth-httplib2==0.1.0',
    ],
    tests_require=tests_require,
    extras_require={
        'test': tests_require,
        'memcached': ['python-memcached==1.58'],
    },
)
