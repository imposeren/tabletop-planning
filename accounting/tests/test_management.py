# -*- coding: utf-8 -*-
import datetime

from django.utils import timezone

from freezegun import freeze_time

from games.tests.factories import GameFactory
from users.tests.factories import UserFactory

from tabletop_planning.tests import BaseTestCase
from games.models import GameSession


class ManagementTestCase(BaseTestCase):

    def setUp(self, *args, **kwargs):
        super(ManagementTestCase, self).setUp(*args, **kwargs)
        self.master = UserFactory.create()
        self.game = GameFactory.create()
        self.game.masters.add(self.master)

    @freeze_time('2014-09-05')
    def test_reward_masters(self):
        """Test that masters are properly rewarded."""
        from accounting.management.commands.reward_masters import Command
        cmd = Command()
        now = timezone.now()

        master = self.master
        game = self.game
        game.masters.add(master)
        game_session = GameSession.create_with_event(game=game, start_date=now - datetime.timedelta(days=1))
        game_session.visits.create(visitor=master, verified=True)

        # visiting games should not break anything
        other_master = UserFactory.create()
        played_game = GameFactory.create()
        played_game.masters.add(other_master)
        played_game.game_players.create(player=master)
        played_session = GameSession.create_with_event(game=played_game, start_date=now - datetime.timedelta(days=1))
        played_session.visits.create(visitor=master, verified=True)

        cmd.reward_user(master)

        # experience  changed in DB. Refetch
        master = master.__class__.objects.get(pk=master.pk)

        self.assertEqual(master.cached_total_master_experience, 1)
        xp_summ = 1

        for i in range(10):
            game_session = GameSession.create_with_event(
                game=game,
                start_date=now - datetime.timedelta(days=1, hours=1+i),
            )
            game_session.visits.create(visitor=master, verified=True)

            cmd.reward_user(master)
            # extra calls should not break anything:
            cmd.reward_user(master)
            cmd.reward_user(master)

            extra = min(4, 2 + int(i/2))
            if i+2 in [2, 3]:  # second and third games give 2 XP
                self.assertEqual(extra, 2)
            elif i+2 in [4, 5]:
                self.assertEqual(extra, 3)
            elif i+2 >= 6:
                self.assertEqual(extra, 4)
            xp_summ += extra  # 3, 5, 8, 11, 15, 19, 23, 27, 31, 35, 39 ...

            # experience  changed in DB. Refetch
            master = master.__class__.objects.get(pk=master.pk)

            self.assertEqual(master.cached_total_master_experience, xp_summ)

        self.assertEqual(xp_summ, 35)

    @freeze_time('2015-05-27 14:00')
    def test_reward_masters_boundary_problems(self):
        """Solve problem when experience is reduced because of 14 days boundary when monday games is not in boundary.

        Details:

        Master has games on monday (d1), tuesday (d2), wednesday (d3).
        Management command is run in 14+2 days after monday (d1).

        When calculating experience for game on wednesday: games on monday and
        tuesday are ignored (old logic. should be fixed and tested by this text)
        so master receives less experience.

        Test details:

        * "Today" is '2015-05-27' which is wednesday

        * Games are on:

          * more than 14 days ago: '2015-05-11', '2015-05-12'
          * in 14 days period: '2015-05-14'


        """
        from accounting.management.commands.reward_masters import Command
        cmd = Command()
        now = timezone.now()

        master = self.master
        game = self.game

        # "out of bounds" games:
        game_session1 = GameSession.create_with_event(game=game, start_date=now - datetime.timedelta(days=15))
        game_session1.visits.create(visitor=master, verified=True)

        game_session2 = GameSession.create_with_event(game=game, start_date=now - datetime.timedelta(days=16))
        game_session2.visits.create(visitor=master, verified=True)

        # "in bounds" game:
        game_session3 = GameSession.create_with_event(game=game, start_date=now - datetime.timedelta(days=13))
        game_session3.visits.create(visitor=master, verified=True)

        cmd.reward_user(master, now=now-datetime.timedelta(days=14))

        # experience  changed in DB. Refetch
        master = master.__class__.objects.get(pk=master.pk)

        # all visits have at leas 1 xp. bonus xp for first 2 games is calculated:
        self.assertEqual(master.cached_total_master_experience, 1+2+1)

        # trying 2 times to check if anything brokes on recalcs
        for i in range(2):
            # recalculating XP later should still account first 2 games for xp of third game (old algorithm reduced it's xp from 2 to 1 XP):
            cmd.reward_user(master)

            # experience  changed in DB. Refetch
            master = master.__class__.objects.get(pk=master.pk)

            self.assertEqual(master.cached_total_master_experience, 1+2+2)
