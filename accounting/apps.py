# -*- coding: utf-8 -*-
from django.apps import AppConfig


class AccountingAppConfig(AppConfig):
    name = 'accounting'
    verbose_name = u"Опыт и Финансы"
