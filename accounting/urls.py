# -*- coding: utf-8 -*-
from django.conf.urls import url

from accounting import views

urlpatterns = [
    url(r'^levels/$', views.levels_info, name='levels_info'),
    url(r'^master-levels/$', views.master_levels_info, name='master_levels_info'),
    # url(r'^moderate-game/(?P<game_slug>[\w-]+)/(?P<session_number>\d+)/$', views.game_report_moderation, name='game_report_moderation'),
    # url(r'^my_xp/$', views.my_xp, name='my_xp'),
]


app_name = 'accounting'
