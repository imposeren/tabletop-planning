# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


def populate_to_budget(apps, schema_editor):
    BalanceChange = apps.get_model("accounting", "BalanceChange")
    Ticket = apps.get_model("accounting", "Ticket")
    Q = models.Q
    (
        BalanceChange
        .objects
        .filter(amount__gt=0)
        .filter(
            Q(comment='', paid_game_session_visits__isnull=True, to_master_game_session_visits__isnull=True)
            |
            Q(comment='reset user balance')
        )
        .update(to_budget=True)
    )
    for ticket in Ticket.objects.all():
        if ticket.cost:
            self = ticket
            self.budget_balance_change = self.budget_balance_change or BalanceChange()
            self.negative_balance_change = self.negative_balance_change or BalanceChange()

            self.budget_balance_change.to_budget = True
            self.negative_balance_change.to_budget = False

            self.budget_balance_change.user = self.negative_balance_change.user = self.user

            self.budget_balance_change.verified = self.negative_balance_change.verified = True

            self.budget_balance_change.amount = self.cost
            self.negative_balance_change.amount = -self.cost

            self.budget_balance_change.comment = self.negative_balance_change.comment = "ticket purchase"
            self.budget_balance_change.details = self.negative_balance_change.details = {
                'ticket': {
                    '_model': 'accounting.Ticket',
                    '_pk': self.pk,
                }
            }

            self.budget_balance_change.save()
            self.negative_balance_change.save()


class Migration(migrations.Migration):

    dependencies = [
        ('accounting', '0018_auto_20160117_1221'),
        ('users', '0014_auto_20160104_2136'),
    ]

    operations = [
        migrations.RunPython(
            code=populate_to_budget,
            atomic=True,
        ),
    ]
