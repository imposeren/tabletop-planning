# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from decimal import Decimal
import django.utils.timezone
from django.conf import settings
import django.core.validators


def create_default_level_info(apps, schema_editor):
    PlayerLevelInfo = apps.get_model("accounting", "PlayerLevelInfo")
    data = (
        (1, 0, 50, 225, 425, u'первая игра бесплатно!'),
        (2, 10, 45, 200, 400, u''),
        (3, 15, 45, 200, 400, u'Подарок'),
        (4, 20, 40, 180, 360, u''),
        (5, 25, 40, 180, 360, u'Подарок'),
        (6, 30, 35, 160, 315, u''),
        (7, 35, 35, 160, 315, u'Подарок'),
        (8, 40, 30, 140, 270, u''),
        (9, 45, 30, 140, 270, u'Подарок'),
        (10, 50, 25, 120, 225, u''),

    )
    total_xp = 0
    for (level, xp_per_level, one_ticket_cost, five_tickets_cost, ten_tickets_cost, extra_info) in data:
        total_xp += xp_per_level
        PlayerLevelInfo.objects.create(
            level=level,
            xp_per_level=xp_per_level,
            one_ticket_cost=one_ticket_cost,
            five_tickets_cost=five_tickets_cost,
            ten_tickets_cost=ten_tickets_cost,
            extra_info=extra_info,
            total_xp=total_xp,
        )


def delete_level_info(apps, schema_editor):
    PlayerLevelInfo = apps.get_model("accounting", "PlayerLevelInfo")
    PlayerLevelInfo.objects.all().delete()


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='ExperienceReward',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('amount', models.SmallIntegerField(default=1, verbose_name='\u041a\u043e\u043b\u0438\u0447\u0435\u0441\u0442\u0432\u043e', validators=[django.core.validators.MaxValueValidator(30000), django.core.validators.MinValueValidator(-30000)])),
                ('comment', models.CharField(max_length=64, verbose_name='\u041a\u043e\u043c\u0435\u043d\u0442\u0430\u0440\u0438\u0439', blank=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE)),
                ('created_datetime', models.DateTimeField(default=django.utils.timezone.now, verbose_name='\u0414\u0430\u0442\u0430 \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Payment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('amount', models.DecimalField(default=50, verbose_name='\u0421\u0443\u043c\u043c\u0430', max_digits=9, decimal_places=3, validators=[django.core.validators.MinValueValidator(Decimal('0.01'))])),
                ('from_user', models.ForeignKey(verbose_name='\u041f\u043b\u0430\u0442\u0435\u043b\u044c\u0449\u0438\u043a', to=settings.AUTH_USER_MODEL, help_text='\u0414\u043b\u044f \u043f\u043e\u043f\u043e\u043b\u043d\u0435\u043d\u0438\u044f \u0441\u0447\u0435\u0442\u0430 \u043d\u0430\u0439\u0434\u0438\u0442\u0435 \u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u044f withdraw@localhost', on_delete=models.CASCADE)),
                ('to_user', models.ForeignKey(verbose_name='\u041f\u043e\u043b\u0443\u0447\u0430\u0442\u0435\u043b\u044c', to=settings.AUTH_USER_MODEL, help_text='\u0414\u043b\u044f \u0432\u044b\u0432\u043e\u0434\u0430 \u0434\u0435\u043d\u0435\u0433 \u043d\u0430\u0439\u0434\u0438\u0442\u0435 \u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u044f refill@localhost', on_delete=models.CASCADE)),
                ('created_datetime', models.DateTimeField(default=django.utils.timezone.now, verbose_name='\u0414\u0430\u0442\u0430 \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f')),
                ('use_balance', models.BooleanField(default=False, help_text='\u041a\u0430\u043a, \u043f\u0440\u0430\u0432\u0438\u043b\u043e, \u0435\u0441\u043b\u0438 \u0438\u0433\u0440\u043e\u043a\u0438 \u043f\u043b\u0430\u0442\u044f\u0442 \u043d\u0430\u043b\u0438\u0447\u043d\u044b\u043c\u0438, \u0442\u043e \u043e\u043d\u0438 \u043f\u043b\u0430\u0442\u044f\u0442 \u043c\u0438\u043d\u0443\u044f \u0431\u0430\u043b\u0430\u043d\u0441.\n\n\u0410 \u043f\u0440\u0430\u043a\u0442\u0438\u0447\u0435\u0441\u043a\u0438 \u0432\u0441\u0435 \u043e\u043f\u0435\u0440\u0430\u0446\u0438\u0438 \u043e\u043f\u043b\u0430\u0442\u044b \u0441\u043e \u0441\u0442\u043e\u0440\u043e\u043d\u044b \u043a\u043b\u0443\u0431\u0430, \u043a\u0430\u043a \u043d\u0430\u043b\u0438\u0447\u043d\u044b\u0435 \u0442\u0430\u043a \u0438 \u0432\u043d\u0443\u0442\u0440\u0438 \u0441\u0438\u0441\u0442\u0435\u043c\u044b \u043f\u0440\u043e\u0432\u043e\u0434\u044f\u0442\u0441\u044f \u0441 \u0438\u0441\u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u043d\u0438\u0435\u043c \u0431\u0430\u043b\u0430\u043d\u0441\u0430', verbose_name='\u041e\u043f\u043b\u0430\u0442\u0430 \u0438\u0437 \u0431\u0430\u043b\u0430\u043d\u0441\u0430')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterModelOptions(
            name='experiencereward',
            options={'verbose_name': '\u043d\u0430\u0433\u0440\u0430\u0434\u0430 \u043e\u043f\u044b\u0442\u043e\u043c', 'verbose_name_plural': '\u043d\u0430\u0433\u0440\u0430\u0434\u044b \u043e\u043f\u044b\u0442\u043e\u043c'},
        ),
        migrations.AlterModelOptions(
            name='payment',
            options={'verbose_name': '\u043f\u043b\u0430\u0442\u0451\u0436', 'verbose_name_plural': '\u043f\u043b\u0430\u0442\u0435\u0436\u0438'},
        ),
        migrations.AlterField(
            model_name='payment',
            name='from_user',
            field=models.ForeignKey(verbose_name='\u041f\u043b\u0430\u0442\u0435\u043b\u044c\u0449\u0438\u043a', to=settings.AUTH_USER_MODEL, help_text='\u0414\u043b\u044f \u043f\u043e\u043f\u043e\u043b\u043d\u0435\u043d\u0438\u044f \u0441\u0447\u0435\u0442\u0430 \u043d\u0430\u0439\u0434\u0438\u0442\u0435 \u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u044f "refill@localhost" (service)', on_delete=models.CASCADE),
        ),
        migrations.AlterField(
            model_name='payment',
            name='to_user',
            field=models.ForeignKey(verbose_name='\u041f\u043e\u043b\u0443\u0447\u0430\u0442\u0435\u043b\u044c', to=settings.AUTH_USER_MODEL, help_text='\u0414\u043b\u044f \u0432\u044b\u0432\u043e\u0434\u0430 \u0434\u0435\u043d\u0435\u0433 \u043d\u0430\u0439\u0434\u0438\u0442\u0435 \u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u044f "withdraw@localhost" (service)', on_delete=models.CASCADE),
        ),
        migrations.RemoveField(
            model_name='payment',
            name='use_balance',
        ),
        migrations.AddField(
            model_name='experiencereward',
            name='verified',
            field=models.BooleanField(default=False, verbose_name='\u041c\u043e\u0434\u0435\u0440\u0430\u0446\u0438\u044f \u043f\u0440\u043e\u0439\u0434\u0435\u043d\u0430'),
            preserve_default=True,
        ),
        migrations.CreateModel(
            name='Ticket',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cost', models.DecimalField(default=50, verbose_name='\u041e\u0440\u0438\u0433\u0438\u043d\u0430\u043b\u044c\u043d\u0430\u044f \u0441\u0442\u043e\u0438\u043c\u043e\u0441\u0442\u044c', max_digits=6, decimal_places=3, validators=[django.core.validators.MinValueValidator(Decimal('0.01'))])),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='experiencereward',
            name='master_type',
            field=models.BooleanField(default=False, verbose_name='\u041e\u043f\u044b\u0442 \u0432\u0435\u0434\u0443\u0449\u0435\u0433\u043e'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='payment',
            name='finished',
            field=models.BooleanField(default=False, help_text='\u0415\u0441\u043b\u0438 \u0442\u0440\u0430\u043d\u0437\u0430\u043a\u0446\u0438\u044f \u043d\u0435 \u0437\u0430\u0432\u0435\u0440\u0448\u0435\u043d\u0430, \u0442\u043e \u043d\u0435\u043e\u0431\u0445\u043e\u0434\u0438\u043c\u044b \u0434\u043e\u043f\u043e\u043b\u043d\u0438\u0442\u0435\u043b\u044c\u043d\u044b\u0435 \u0434\u0435\u0439\u0441\u0442\u0432\u0438\u044f \u0434\u043b\u044f \u0435\u0451 \u0437\u0430\u0432\u0435\u0440\u0448\u0435\u043d\u0438\u044f. \u041a\u0430\u043a \u043f\u0440\u0430\u0432\u0438\u043b\u043e \u043d\u0435\u043e\u0431\u0445\u043e\u0434\u0438\u043c\u043e \u0444\u0438\u0437\u0438\u0447\u0435\u0441\u043a\u0438 \u043f\u0435\u0440\u0435\u0434\u0430\u0442\u044c \u0443\u043a\u0430\u0437\u0430\u043d\u043d\u0443\u044e \u0441\u0443\u043c\u043c\u0443 \u043c\u0435\u0436\u0434\u0443 \u0443\u0447\u0430\u0441\u0442\u043d\u0438\u043a\u0430\u043c\u0438 \u043f\u043b\u0430\u0442\u0435\u0436\u0430', verbose_name='\u043e\u043f\u0435\u0440\u0430\u0446\u0438\u044f \u0437\u0430\u0432\u0435\u0440\u0448\u0435\u043d\u0430'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='payment',
            name='amount',
            field=models.DecimalField(default=50, verbose_name='\u0441\u0443\u043c\u043c\u0430', max_digits=9, decimal_places=3, validators=[django.core.validators.MinValueValidator(Decimal('0.01'))]),
        ),
        migrations.AlterField(
            model_name='payment',
            name='created_datetime',
            field=models.DateTimeField(default=django.utils.timezone.now, verbose_name='\u0434\u0430\u0442\u0430 \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f'),
        ),
        migrations.AlterField(
            model_name='payment',
            name='from_user',
            field=models.ForeignKey(verbose_name='\u043f\u043b\u0430\u0442\u0435\u043b\u044c\u0449\u0438\u043a', to=settings.AUTH_USER_MODEL, help_text='\u0434\u043b\u044f \u043f\u043e\u043f\u043e\u043b\u043d\u0435\u043d\u0438\u044f \u0441\u0447\u0435\u0442\u0430 \u043d\u0430\u0439\u0434\u0438\u0442\u0435 \u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u044f "refill@localhost" (service)', on_delete=models.CASCADE),
        ),
        migrations.AlterField(
            model_name='payment',
            name='to_user',
            field=models.ForeignKey(verbose_name='\u043f\u043e\u043b\u0443\u0447\u0430\u0442\u0435\u043b\u044c', to=settings.AUTH_USER_MODEL, help_text='\u0434\u043b\u044f \u0432\u044b\u0432\u043e\u0434\u0430 \u0434\u0435\u043d\u0435\u0433 \u043d\u0430\u0439\u0434\u0438\u0442\u0435 \u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u044f "withdraw@localhost" (service)', on_delete=models.CASCADE),
        ),
        migrations.CreateModel(
            name='PlayerLevelInfo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('level', models.PositiveIntegerField(default=1, unique=True)),
                ('xp_per_level', models.PositiveIntegerField(verbose_name='\u041e\u043f\u044b\u0442\u0430 \u0434\u043b\u044f \u043f\u043e\u043b\u0443\u0447\u0435\u043d\u0438\u044f \u0443\u0440\u043e\u0432\u043d\u044f')),
                ('extra_info', models.CharField(max_length=64, verbose_name='\u0417\u0430\u043c\u0435\u0447\u0430\u043d\u0438\u044f', blank=True)),
                ('total_xp', models.PositiveIntegerField(default=0, verbose_name='\u041e\u0431\u0449\u0435\u0435 \u043a\u043e\u043b\u0438\u0447\u0435\u0441\u0442\u0432\u043e \u043e\u043f\u044b\u0442\u0430', editable=False)),
                ('one_ticket_cost', models.DecimalField(verbose_name='\u0421\u0442\u043e\u0438\u043c\u043e\u0441\u0442\u044c \u043e\u0434\u043d\u043e\u0439 \u0438\u0433\u0440\u044b', max_digits=6, decimal_places=3)),
                ('five_tickets_cost', models.DecimalField(verbose_name='\u0421\u0442\u043e\u0438\u043c\u043e\u0441\u0442\u044c \u0432\u0430\u0443\u0447\u0435\u0440\u0430 \u043d\u0430 5 \u0438\u0433\u0440', max_digits=6, decimal_places=3)),
                ('ten_tickets_cost', models.DecimalField(verbose_name='\u0421\u0442\u043e\u0438\u043c\u043e\u0441\u0442\u044c \u0432\u0430\u0443\u0447\u0435\u0440\u0430 \u043d\u0430 10 \u0438\u0433\u0440', max_digits=6, decimal_places=3)),
            ],
            options={
                'ordering': ('level',),
                'verbose_name': '\u0438\u043d\u0444\u043e\u0440\u043c\u0430\u0446\u0438\u044f \u043e \u0443\u0440\u043e\u0432\u043d\u0435',
                'verbose_name_plural': '\u0438\u043d\u0444\u043e\u0440\u043c\u0430\u0446\u0438\u044f \u043e \u0443\u0440\u043e\u0432\u043d\u044f\u0445',
            },
            bases=(models.Model,),
        ),
        migrations.RunPython(
            code=create_default_level_info,
            reverse_code=delete_level_info,
            atomic=True,
        ),
        migrations.AlterField(
            model_name='experiencereward',
            name='user',
            field=models.ForeignKey(related_name='experience_rewards', to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE),
        ),
        migrations.AlterField(
            model_name='payment',
            name='from_user',
            field=models.ForeignKey(related_name='outgoing_payments', verbose_name='\u043f\u043b\u0430\u0442\u0435\u043b\u044c\u0449\u0438\u043a', to=settings.AUTH_USER_MODEL, help_text='\u0434\u043b\u044f \u043f\u043e\u043f\u043e\u043b\u043d\u0435\u043d\u0438\u044f \u0441\u0447\u0435\u0442\u0430 \u043d\u0430\u0439\u0434\u0438\u0442\u0435 \u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u044f "refill@localhost" (service)', on_delete=models.CASCADE),
        ),
        migrations.AlterField(
            model_name='payment',
            name='to_user',
            field=models.ForeignKey(related_name='incoming_payments', verbose_name='\u043f\u043e\u043b\u0443\u0447\u0430\u0442\u0435\u043b\u044c', to=settings.AUTH_USER_MODEL, help_text='\u0434\u043b\u044f \u0432\u044b\u0432\u043e\u0434\u0430 \u0434\u0435\u043d\u0435\u0433 \u043d\u0430\u0439\u0434\u0438\u0442\u0435 \u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u044f "withdraw@localhost" (service)', on_delete=models.CASCADE),
        ),
        migrations.AlterField(
            model_name='ticket',
            name='user',
            field=models.ForeignKey(related_name='tickets', to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE),
        ),
    ]
