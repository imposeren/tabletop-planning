# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations  # noqa


def create_default_master_level_info(apps, schema_editor):
    MasterLevelInfo = apps.get_model("accounting", "MasterLevelInfo")
    data = (
        (1, 0, 40, u''),
        (2, 10, 45, u''),
        (3, 15, 45, u'Подарок'),
        (4, 25, 50, u''),
        (5, 30, 50, u'Подарок'),
        (6, 40, 55, u''),
        (7, 45, 55, u'Подарок'),
        (8, 55, 60, u''),
        (9, 60, 60, u'Подарок'),
        (10, 70, 65, u''),

    )
    total_xp = 0
    for (level, xp_per_level, one_game_salary, extra_info) in data:
        total_xp += xp_per_level
        MasterLevelInfo.objects.create(
            level=level,
            xp_per_level=xp_per_level,
            one_game_salary=one_game_salary,
            extra_info=extra_info,
            total_xp=total_xp,
        )


def delete_level_info(apps, schema_editor):
    MasterLevelInfo = apps.get_model("accounting", "MasterLevelInfo")
    MasterLevelInfo.objects.all().delete()


class Migration(migrations.Migration):

    dependencies = [
        ('accounting', '0005_masterlevelinfo'),
    ]

    operations = [
        migrations.RunPython(
            code=create_default_master_level_info,
            reverse_code=delete_level_info,
            atomic=True,
        ),
    ]
