# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('games', '0021_auto_20151226_1256'),
        ('accounting', '0010_auto_20150811_2334'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='payment',
            name='from_user',
        ),
        migrations.RemoveField(
            model_name='payment',
            name='related_payments',
        ),
        migrations.RemoveField(
            model_name='payment',
            name='to_user',
        ),
        migrations.DeleteModel(
            name='Payment',
        ),
    ]
