# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounting', '0001_squashed_0019_auto_20140929_2345'),
    ]

    operations = [
        migrations.AlterField(
            model_name='experiencereward',
            name='master_type',
            field=models.BooleanField(default=False, help_text='\u041e\u043f\u044b\u0442 \u043c\u043e\u0436\u0435\u0442 \u043f\u0435\u0440\u0435\u0441\u0447\u0438\u0442\u044b\u0432\u0430\u0442\u044c\u0441\u044f \u0432 \u0442\u0435\u0447\u0435\u043d\u0438\u0438 2\u0445 \u043d\u0435\u0434\u0435\u043b\u044c \u0438 \u0437\u0430\u0432\u0438\u0441\u0438\u0442 \u043e\u0442 \u043a\u043e\u043b\u0438\u0447\u0435\u0441\u0442\u0432\u0430 \u043f\u0440\u043e\u0432\u0435\u0434\u0435\u043d\u043d\u044b\u0445 \u0438\u0433\u0440 \u0437\u0430 \u043d\u0435\u0434\u0435\u043b\u044e', verbose_name='\u041e\u043f\u044b\u0442 \u0432\u0435\u0434\u0443\u0449\u0435\u0433\u043e'),
        ),
    ]
