# -*- coding: utf-8 -*-
# flake8: noqa
from __future__ import unicode_literals

from django.db import migrations, models


def populate_balance_change_verified(apps, schema_editor):
    return
    # BalanceChange = apps.get_model("accounting", "BalanceChange")
    # Q = models.Q
    # (
    #     BalanceChange
    #     .objects
    #     .filter(
    #         Q(cost_game_sessions__verified=True) |
    #         Q(paid_game_sessions__verified=True) |
    #         Q(to_master_game_sessions__verified=True) |
    #         Q(cost_game_sessions__isnull=True, paid_game_sessions__isnull=True, to_master_game_sessions__isnull=True)
    #     )
    #     .update(verified=True)
    # )


class Migration(migrations.Migration):

    dependencies = [
        ('accounting', '0014_auto_20160104_2136'),
        ('games', '0023_auto_20151226_1617')
    ]

    operations = [
        migrations.RunPython(
            code=populate_balance_change_verified,
            atomic=True,
        ),
    ]
