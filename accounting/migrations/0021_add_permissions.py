# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models  # noqa


def update_all_contenttypes(**kwargs):
    from django.apps import apps
    from django.contrib.contenttypes.management import create_contenttypes

    for app_config in apps.get_app_configs():
        create_contenttypes(app_config, **kwargs)


def create_all_permissions(**kwargs):
    from django.contrib.auth.management import create_permissions
    from django.apps import apps

    for app_config in apps.get_app_configs():
        create_permissions(app_config, **kwargs)


def modify_moderators_group(apps, schema_editor):
    update_all_contenttypes(verbosity=0)
    create_all_permissions()

    BalanceChange = apps.get_model("accounting", "BalanceChange")
    ContentType = apps.get_model("contenttypes", "ContentType")
    Group = apps.get_model("auth", "Group")
    Permission = apps.get_model("auth", "Permission")

    group = Group.objects.filter(name='GM Managers').first()

    if group:
        content_type = ContentType.objects.get_for_model(BalanceChange)
        for perm_codename in ('add_balancechange', 'change_balancechange'):
            permission = Permission.objects.get(
                codename=perm_codename,
                content_type=content_type,
            )
            group.permissions.add(permission)


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        ('accounting', '0020_auto_20160131_0002'),
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(
            code=modify_moderators_group,
            atomic=True,
        ),
    ]
