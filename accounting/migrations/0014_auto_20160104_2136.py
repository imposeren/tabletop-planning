# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounting', '0013_auto_20151226_1617'),
    ]

    operations = [
        migrations.AddField(
            model_name='balancechange',
            name='verified',
            field=models.BooleanField(default=False, db_index=True, verbose_name='\u041c\u043e\u0434\u0435\u0440\u0430\u0446\u0438\u044f \u043f\u0440\u043e\u0439\u0434\u0435\u043d\u0430'),
        ),
        migrations.AlterField(
            model_name='balancechange',
            name='amount',
            field=models.DecimalField(default=1, verbose_name='\u0418\u0437\u043c\u0435\u043d\u0435\u043d\u0438\u0435 \u0431\u0430\u043b\u0430\u043d\u0441\u0430', max_digits=7, decimal_places=2, db_index=True),
        ),
    ]
