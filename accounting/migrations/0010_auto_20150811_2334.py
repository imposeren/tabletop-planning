# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounting', '0009_auto_20150720_2220'),
    ]

    operations = [
        migrations.CreateModel(
            name='DriveSpreadsheetSync',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('sync_datetime', models.DateTimeField(auto_now_add=True)),
                ('user_type', models.PositiveSmallIntegerField(choices=[(1, '\u0438\u0433\u0440\u043e\u043a'), (2, '\u0432\u0435\u0434\u0443\u0449\u0438\u0439')])),
                ('drive_file_id', models.CharField(max_length=512)),
            ],
        ),
        migrations.AlterIndexTogether(
            name='drivespreadsheetsync',
            index_together=set([('sync_datetime', 'user_type')]),
        ),
    ]
