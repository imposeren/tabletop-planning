# -*- coding: utf-8 -*-
# flake8: noqa
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounting', '0016_verify_balance_change'),
    ]

    operations = [
        migrations.AddField(
            model_name='balancechange',
            name='to_budget',
            field=models.BooleanField(default=False, help_text='\u0423\u0447\u0438\u0442\u044b\u0432\u0430\u0442\u044c \u043f\u0440\u0438 \u0440\u0430\u0441\u0441\u0447\u0435\u0442\u0430\u0445 \u0431\u044e\u0434\u0436\u0435\u0442\u0430', verbose_name='\u0412 \u0431\u044e\u0434\u0436\u0435\u0442'),
        ),
    ]
