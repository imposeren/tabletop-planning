# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounting', '0012_balancechange'),
    ]

    operations = [
        migrations.AlterField(
            model_name='balancechange',
            name='comment',
            field=models.CharField(max_length=255, verbose_name='\u041a\u043e\u043c\u0435\u043d\u0442\u0430\u0440\u0438\u0439', blank=True),
        ),
    ]
