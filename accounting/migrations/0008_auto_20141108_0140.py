# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounting', '0007_auto_20141104_1455'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='payment',
            name='related_payment',
        ),
        migrations.AddField(
            model_name='payment',
            name='related_payments',
            field=models.ManyToManyField(related_name='reverse_related_payments', null=True, to='accounting.Payment', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='payment',
            name='squashed',
            field=models.BooleanField(default=False, editable=False, help_text='\u0422\u0440\u0430\u043d\u0437\u0430\u043a\u0446\u0438\u044f \u043c\u043e\u0436\u0435\u0442 \u0431\u044b\u0442\u044c \u0443\u0447\u0442\u0435\u043d\u0430 \u0432 \u0434\u0440\u0443\u0433\u043e\u0439 "\u0441\u0436\u0430\u0442\u043e\u0439" \u0442\u0440\u0430\u043d\u0437\u0430\u043a\u0446\u0438\u0438', verbose_name='\u043e\u043f\u0435\u0440\u0430\u0446\u0438\u044f \u0443\u0447\u0442\u0435\u043d\u0430 \u0432 \u0434\u0440\u0443\u0433\u043e\u0439', db_index=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='payment',
            name='tags',
            field=models.CharField(default='', max_length=64, verbose_name='\u0422\u0435\u0433\u0438', blank=True),
            preserve_default=True,
        ),
    ]
