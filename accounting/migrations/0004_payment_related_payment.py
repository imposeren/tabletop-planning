# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounting', '0003_payment_comment'),
    ]

    operations = [
        migrations.AddField(
            model_name='payment',
            name='related_payment',
            field=models.ForeignKey(blank=True, to='accounting.Payment', null=True, on_delete=models.CASCADE),
            preserve_default=True,
        ),
    ]
