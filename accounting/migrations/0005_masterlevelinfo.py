# -*- coding: utf-8 -*-
# flake8: noqa
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounting', '0004_payment_related_payment'),
    ]

    operations = [
        migrations.CreateModel(
            name='MasterLevelInfo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('level', models.PositiveIntegerField(default=1, unique=True)),
                ('xp_per_level', models.PositiveIntegerField(verbose_name='\u041e\u043f\u044b\u0442\u0430 \u0434\u043b\u044f \u043f\u043e\u043b\u0443\u0447\u0435\u043d\u0438\u044f \u0443\u0440\u043e\u0432\u043d\u044f')),
                ('extra_info', models.CharField(max_length=64, verbose_name='\u0417\u0430\u043c\u0435\u0447\u0430\u043d\u0438\u044f', blank=True)),
                ('total_xp', models.PositiveIntegerField(default=0, verbose_name='\u041e\u0431\u0449\u0435\u0435 \u043a\u043e\u043b\u0438\u0447\u0435\u0441\u0442\u0432\u043e \u043e\u043f\u044b\u0442\u0430', editable=False)),
                ('one_game_salary', models.DecimalField(verbose_name='\u041e\u043f\u043b\u0430\u0442\u0430 \u043e\u0434\u043d\u043e\u0439 \u0438\u0433\u0440\u044b', max_digits=6, decimal_places=3)),
            ],
            options={
                'ordering': ('level',),
                'verbose_name': '\u0438\u043d\u0444\u043e\u0440\u043c\u0430\u0446\u0438\u044f \u043e \u0443\u0440\u043e\u0432\u043d\u0435 \u0432\u0435\u0434\u0443\u0449\u0435\u0433\u043e',
                'verbose_name_plural': '\u0438\u043d\u0444\u043e\u0440\u043c\u0430\u0446\u0438\u044f \u043e \u0443\u0440\u043e\u0432\u043d\u044f\u0445 \u0432\u0435\u0434\u0443\u0449\u0435\u0433\u043e',
            },
            bases=(models.Model,),
        ),
    ]
