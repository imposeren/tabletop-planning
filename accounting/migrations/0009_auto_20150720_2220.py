# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounting', '0008_auto_20141108_0140'),
    ]

    operations = [
        migrations.AlterField(
            model_name='payment',
            name='related_payments',
            field=models.ManyToManyField(related_name='reverse_related_payments', to='accounting.Payment', blank=True),
        ),
    ]
