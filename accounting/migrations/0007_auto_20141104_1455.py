# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from decimal import Decimal
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('accounting', '0006_master_levels'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='ticket',
            options={'verbose_name': '\u0411\u0438\u043b\u0435\u0442 \u043d\u0430 \u0438\u0433\u0440\u0443', 'verbose_name_plural': '\u0411\u0438\u043b\u0435\u0442\u044b \u043d\u0430 \u0438\u0433\u0440\u044b'},
        ),
        migrations.AlterField(
            model_name='experiencereward',
            name='verified',
            field=models.BooleanField(default=False, db_index=True, verbose_name='\u041c\u043e\u0434\u0435\u0440\u0430\u0446\u0438\u044f \u043f\u0440\u043e\u0439\u0434\u0435\u043d\u0430'),
        ),
        migrations.AlterField(
            model_name='masterlevelinfo',
            name='total_xp',
            field=models.PositiveIntegerField(default=0, verbose_name='\u041e\u0431\u0449\u0435\u0435 \u043a\u043e\u043b\u0438\u0447\u0435\u0441\u0442\u0432\u043e \u043e\u043f\u044b\u0442\u0430', editable=False, db_index=True),
        ),
        migrations.AlterField(
            model_name='payment',
            name='finished',
            field=models.BooleanField(default=False, help_text='\u0415\u0441\u043b\u0438 \u0442\u0440\u0430\u043d\u0437\u0430\u043a\u0446\u0438\u044f \u043d\u0435 \u0437\u0430\u0432\u0435\u0440\u0448\u0435\u043d\u0430, \u0442\u043e \u043d\u0435\u043e\u0431\u0445\u043e\u0434\u0438\u043c\u044b \u0434\u043e\u043f\u043e\u043b\u043d\u0438\u0442\u0435\u043b\u044c\u043d\u044b\u0435 \u0434\u0435\u0439\u0441\u0442\u0432\u0438\u044f \u0434\u043b\u044f \u0435\u0451 \u0437\u0430\u0432\u0435\u0440\u0448\u0435\u043d\u0438\u044f. \u041a\u0430\u043a \u043f\u0440\u0430\u0432\u0438\u043b\u043e \u043d\u0435\u043e\u0431\u0445\u043e\u0434\u0438\u043c\u043e \u0444\u0438\u0437\u0438\u0447\u0435\u0441\u043a\u0438 \u043f\u0435\u0440\u0435\u0434\u0430\u0442\u044c \u0443\u043a\u0430\u0437\u0430\u043d\u043d\u0443\u044e \u0441\u0443\u043c\u043c\u0443 \u043c\u0435\u0436\u0434\u0443 \u0443\u0447\u0430\u0441\u0442\u043d\u0438\u043a\u0430\u043c\u0438 \u043f\u043b\u0430\u0442\u0435\u0436\u0430', db_index=True, verbose_name='\u043e\u043f\u0435\u0440\u0430\u0446\u0438\u044f \u0437\u0430\u0432\u0435\u0440\u0448\u0435\u043d\u0430'),
        ),
        migrations.AlterField(
            model_name='payment',
            name='related_payment',
            field=models.ForeignKey(related_name='reverse_related_payments', blank=True, to='accounting.Payment', null=True, on_delete=models.CASCADE),
        ),
        migrations.AlterField(
            model_name='playerlevelinfo',
            name='total_xp',
            field=models.PositiveIntegerField(default=0, verbose_name='\u041e\u0431\u0449\u0435\u0435 \u043a\u043e\u043b\u0438\u0447\u0435\u0441\u0442\u0432\u043e \u043e\u043f\u044b\u0442\u0430', editable=False, db_index=True),
        ),
        migrations.AlterField(
            model_name='ticket',
            name='cost',
            field=models.DecimalField(decimal_places=3, default=50, max_digits=6, validators=[django.core.validators.MinValueValidator(Decimal('0.01'))], help_text='\u0421\u0442\u043e\u0438\u043c\u043e\u0441\u0442\u044c \u043e\u0434\u043d\u043e\u0433\u043e \u043f\u043e\u0441\u0435\u0449\u0435\u043d\u0438\u044f \u043f\u043e \u0430\u0431\u043e\u043d\u0435\u043c\u0435\u043d\u0442\u0443. \u0412\u041d\u0418\u041c\u0410\u041d\u0418\u0415! \u0438\u0437\u043c\u0435\u043d\u0435\u043d\u0438\u0435 \u0446\u0435\u043d\u044b \u043d\u0435 \u0438\u0437\u043c\u0435\u043d\u0438\u0442 \u0444\u0438\u043d\u0430\u043d\u0441\u043e\u0432\u0443\u044e \u0442\u0440\u0430\u043d\u0437\u0430\u043a\u0446\u0438\u044e \u0441\u0432\u044f\u0437\u0430\u043d\u043d\u0443\u044e \u0441 \u044d\u0442\u0438\u043c \u0431\u0438\u043b\u0435\u0442\u043e\u043c', verbose_name='\u041e\u0440\u0438\u0433\u0438\u043d\u0430\u043b\u044c\u043d\u0430\u044f \u0441\u0442\u043e\u0438\u043c\u043e\u0441\u0442\u044c'),
        ),
    ]
