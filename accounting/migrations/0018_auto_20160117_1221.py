# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounting', '0017_balancechange_to_budget'),
    ]

    operations = [
        migrations.AddField(
            model_name='ticket',
            name='budget_balance_change',
            field=models.OneToOneField(related_name='budgeted_ticket', null=True, to='accounting.BalanceChange', on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name='ticket',
            name='negative_balance_change',
            field=models.OneToOneField(related_name='negatived_ticket', null=True, to='accounting.BalanceChange', on_delete=models.CASCADE),
        ),
    ]
