# -*- coding: utf-8 -*-
# flake8: noqa
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone
import django.contrib.postgres.fields
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('accounting', '0011_auto_20151226_1256'),
    ]

    operations = [
        migrations.CreateModel(
            name='BalanceChange',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_datetime', models.DateTimeField(default=django.utils.timezone.now, verbose_name='\u0414\u0430\u0442\u0430 \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f')),
                ('amount', models.DecimalField(default=1, verbose_name='\u0417\u043d\u0430\u0447\u0435\u043d\u0438\u0435', max_digits=7, decimal_places=2, db_index=True)),
                ('comment', models.CharField(max_length=64, verbose_name='\u041a\u043e\u043c\u0435\u043d\u0442\u0430\u0440\u0438\u0439', blank=True)),
                ('details', django.contrib.postgres.fields.JSONField(default=None, editable=False, blank=True)),
                ('user', models.ForeignKey(related_name='balance_changes', to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE)),
            ],
            options={
                'verbose_name': 'BalanceChange',
                'verbose_name_plural': 'BalanceChanges',
            },
        ),
    ]
