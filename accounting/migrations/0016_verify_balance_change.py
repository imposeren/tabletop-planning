# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


def populate_balance_change_verified(apps, schema_editor):
    BalanceChange = apps.get_model("accounting", "BalanceChange")
    Q = models.Q
    (
        BalanceChange
        .objects
        .filter(
            Q(cost_game_session_visits__verified=True) |
            Q(paid_game_session_visits__verified=True) |
            Q(to_master_game_session_visits__verified=True)
        )
        .update(verified=True)
    )


class Migration(migrations.Migration):

    dependencies = [
        ('accounting', '0015_verify_balance_change'),
        ('games', '0025_auto_20160114_1646')
    ]

    operations = [
        migrations.RunPython(
            code=populate_balance_change_verified,
            atomic=True,
        ),
    ]
