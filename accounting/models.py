# -*- coding: utf-8 -*-

# Python builtins:
from __future__ import unicode_literals
from decimal import Decimal

# django:
from django.conf import settings
from django.contrib.postgres.fields import JSONField
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.db.models import F, Func, Sum
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

# Third-parties:
from model_utils import FieldTracker

# project
# ...

# app:
# ...


class ExperienceReward(models.Model):
    created_datetime = models.DateTimeField(
        verbose_name=_('Дата создания'),
        default=timezone.now,
    )

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name='experience_rewards',
        on_delete=models.CASCADE,
    )
    amount = models.SmallIntegerField(
        _('Количество'), default=1,
        validators=[MaxValueValidator(30000), MinValueValidator(-30000)]
    )
    comment = models.CharField(_('Коментарий'), blank=True, max_length=64)

    verified = models.BooleanField(
        _('Модерация пройдена'), default=False, db_index=True,
    )
    master_type = models.BooleanField(
        _('Опыт ведущего'), default=False,
        help_text=_('Опыт может пересчитываться в течении 2х недель и зависит от количества проведенных игр за неделю')
    )

    # not fields:
    tracker = FieldTracker()

    class Meta:  # noqa: D101
        verbose_name = _('награда опытом')
        verbose_name_plural = _('награды опытом')

    def __str__(self):
        xp_type = 'мастера' if self.master_type else 'игрока'
        return _('%(amount)s XP (%(xp_type)s)') % {'amount': self.amount, 'xp_type': xp_type}

    def save(self, *args, **kwargs):
        created = False
        if not self.pk:
            created = True

        amount_changed = self.tracker.has_changed('amount')
        verification_changed = self.tracker.has_changed('verified')

        super(ExperienceReward, self).save(*args, **kwargs)
        if created and self.verified:
            if not self.master_type:
                self.user.cached_total_player_experience += self.amount
                update_fields = ['cached_total_player_experience']
            else:
                self.user.cached_total_master_experience += self.amount
                update_fields = ['cached_total_master_experience']
            self.user.save(update_fields=update_fields)
        elif amount_changed or verification_changed:
            self.user.recalc_experience(save_on_change=True, master_type=self.master_type)

    def delete(self, *args, **kwargs):
        super(ExperienceReward, self).delete(*args, **kwargs)
        self.user.recalc_experience(save_on_change=True, master_type=self.master_type)

    def game_session(self):
        for related in ('visit_default', 'visit_bonus', 'visit_cleaned', 'visit_xp_payed',):
            visit = getattr(self, related).all().first()
            if visit:
                return visit.game_session
        return None

    game_session.short_description = _('Игровая сессия')


class BalanceChangeQS(models.QuerySet):

    def annotate_sum_per_date(self):
        return (
            self
            .annotate(created_date=Func(F('created_datetime'), function='date'))
            .order_by('created_date')
            .values('created_date')
            .annotate(
                amount_sum=Sum('amount'),
            )
        )


class BalanceChange(models.Model):

    created_datetime = models.DateTimeField(
        verbose_name=_('Дата создания'),
        default=timezone.now,
    )

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name='balance_changes',
        on_delete=models.CASCADE,
    )

    to_budget = models.BooleanField(
        "В бюджет",
        default=False,
        help_text=_("Учитывать при рассчетах бюджета"),
    )

    amount = models.DecimalField(
        _('Изменение баланса'), default=1,
        max_digits=7, decimal_places=2,
        db_index=True,
    )
    comment = models.CharField(_('Коментарий'), blank=True, max_length=255)
    details = JSONField(default=None, blank=True, null=True, editable=False)

    verified = models.BooleanField(
        _('Модерация пройдена'), default=False, db_index=True,
    )

    # not fields:
    tracker = FieldTracker()
    objects = BalanceChangeQS.as_manager()

    class Meta:  # noqa: D101
        verbose_name = "BalanceChange"
        verbose_name_plural = "BalanceChanges"

    def __str__(self):
        return _('%(amount)s (%(user)s)') % {'amount': self.amount, 'user': self.user}

    @classmethod
    def build_details_from_session(cls, game_session):
        data = {
            'game_session': {
                '_model': 'games.GameSession',
                '_pk': game_session.pk,
                'number': game_session.number,
            },

            'game': {
                '_model': 'games.Game',
                '_pk': game_session.game.pk,
                'name': game_session.game.name,
            },
        }

        return data

    @classmethod
    def build_details_from_user(cls, user):
        data = {
            'user': {
                '_model': 'users.User',
                '_pk': user.pk,
                '__str__': str(user),
                'player_level': user.player_level.level,
                'master_level': user.master_level.level,
                'default_player_payment': float(user.default_player_payment),
                'default_master_payment': float(user.default_master_payment),
            },

        }
        return data

    def get_readable_details(self):
        text_details = []

        if not self.details:
            return ''

        have_visited = self.details.get('have_visited', False)
        if not have_visited:
            text_details.append("пропустил игру")

        field_name_templates = (
            ('is_free', 'посещение помечено бесплатным: {0}'),
            ('used_ticket', 'за абонемент: {0}'),
            ('used_experience', 'за опыт: {0}'),
            ('game__name', 'за игру: {0}'),
            ('game_session__number', 'за сессию: {0}'),
        )

        for field_name, template in field_name_templates:
            value = self.details.get(field_name, '')
            text = ''
            if value:
                text = template.format(value)
            else:
                if '__' in field_name:
                    key, subkey = field_name.split('__')
                    text = self.details.get(key, {}).get(subkey, '')
            if text:
                text_details.append(text)

        visitor_data = self.details.get('visitor', {})

        if visitor_data:
            field_name_templates = (
                ('user', 'посетитель: {0}'),
                ('user__master_level', 'уровень ведущего посетителя: {0}'),
                ('user__player_level', 'уровень игрока посетителя: {0}'),
                ('user__default_player_payment', 'обычная цена игр: {0}'),
                ('user__default_master_payment', 'обычная зарплата мастера: {0}'),
            )
            for field_name, template in field_name_templates:
                value = visitor_data.get(field_name, '')
                text = ''
                if value:
                    if hasattr(value, 'keys'):
                        value = value.get('__str__', '')
                    if value:
                        text = template.format(value)
                else:
                    if '__' in field_name:
                        key, subkey = field_name.split('__')
                        text = visitor_data.get(key, {}).get(subkey, '')

                if text:
                    text_details.append(text)

        return '; '.join(str(v) for v in text_details)

    get_readable_details.short_description = 'описание деталей'


class Ticket(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name='tickets',
        on_delete=models.CASCADE,
    )
    cost = models.DecimalField(
        _('Оригинальная стоимость'), max_digits=6, decimal_places=3,
        default=50,
        validators=[MinValueValidator(Decimal('0.01'))],
        help_text=_('Стоимость одного посещения по абонементу. ВНИМАНИЕ! изменение цены не изменит финансовую транзакцию связанную с этим билетом')
    )

    budget_balance_change = models.OneToOneField(
        BalanceChange, null=True, editable=False, related_name='budgeted_ticket',
        on_delete=models.SET_NULL,
    )
    negative_balance_change = models.OneToOneField(
        BalanceChange, null=True, editable=False, related_name='negatived_ticket',
        on_delete=models.SET_NULL,
    )

    class Meta:  # noqa: D101
        verbose_name = _('Билет на игру')
        verbose_name_plural = _('Билеты на игры')

    def __str__(self):
        return f'{self.user} ticket with cost of {self.cost}'

    def save(self, *args, **kwargs):
        super(Ticket, self).save(*args, **kwargs)
        self.update_balance()

    def update_balance(self):
        if self.cost:
            self.budget_balance_change = self.budget_balance_change or BalanceChange()
            self.negative_balance_change = self.negative_balance_change or BalanceChange()

            self.budget_balance_change.to_budget = True
            self.negative_balance_change.to_budget = False

            self.budget_balance_change.user = self.negative_balance_change.user = self.user

            self.budget_balance_change.verified = self.negative_balance_change.verified = True

            self.budget_balance_change.amount = self.cost
            self.negative_balance_change.amount = -self.cost

            self.budget_balance_change.comment = self.negative_balance_change.comment = "ticket purchase"
            self.budget_balance_change.details = self.negative_balance_change.details = {
                'ticket': {
                    '_model': 'accounting.Ticket',
                    '_pk': self.pk,
                }
            }

            if self.budget_balance_change.tracker.changed():
                self.budget_balance_change.save()

            if self.negative_balance_change.tracker.changed():
                self.negative_balance_change.save()


class LevelInfoBase(models.Model):
    level = models.PositiveIntegerField(default=1, unique=True)

    xp_per_level = models.PositiveIntegerField(
        _('Опыта для получения уровня')
    )

    extra_info = models.CharField(
        _('Замечания'),
        max_length=64,
        blank=True,
    )

    total_xp = models.PositiveIntegerField(
        _('Общее количество опыта'),
        default=0,
        editable=False,
        db_index=True,
    )

    class Meta:  # noqa: D101
        abstract = True

    def __str__(self):
        return str(self.level)

    def save(self, *args, **kwargs):
        process_next_levels = kwargs.pop('process_next_levels', True)
        self.total_xp = (
            self.__class__.objects
            .filter(level__lt=self.level)
            .aggregate(Sum('xp_per_level'))
            .get('xp_per_level__sum', 0) or 0
        ) + self.xp_per_level
        super(LevelInfoBase, self).save(*args, **kwargs)
        if process_next_levels:
            for level_info in self.__class__.objects.filter(level__gt=self.level):
                level_info.save(process_next_levels=False)


class PlayerLevelInfo(LevelInfoBase):

    one_ticket_cost = models.DecimalField(
        _('Стоимость одной игры'),
        max_digits=6, decimal_places=3,
    )

    five_tickets_cost = models.DecimalField(
        _('Стоимость ваучера на 5 игр'),
        max_digits=6, decimal_places=3,
    )

    ten_tickets_cost = models.DecimalField(
        _('Стоимость ваучера на 10 игр'),
        max_digits=6, decimal_places=3,
    )

    def five_economy(self):
        five_games_cost = self.one_ticket_cost * 5
        return (five_games_cost - self.five_tickets_cost) * 100 / five_games_cost

    five_economy.short_description = _('Экономия при покупке 5 ваучеров')

    def ten_economy(self):
        ten_games_cost = self.one_ticket_cost * 10
        return (ten_games_cost - self.ten_tickets_cost) * 100 / ten_games_cost

    ten_economy.short_description = _('Экономия при покупке 10 ваучеров')

    class Meta:  # noqa: D101
        ordering = ('level', )
        verbose_name = _('информация о уровне')
        verbose_name_plural = _('информация о уровнях')


class MasterLevelInfo(LevelInfoBase):

    one_game_salary = models.DecimalField(
        _('Оплата одной игры'),
        max_digits=6, decimal_places=3,
    )

    safe_hours = models.PositiveIntegerField(
        _('Часов до создания игры'),
        default=18,
    )

    class Meta:  # noqa: D101
        ordering = ('level', )
        verbose_name = _('информация о уровне ведущего')
        verbose_name_plural = _('информация о уровнях ведущего')
