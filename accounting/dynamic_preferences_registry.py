# -*- coding: utf-8 -*-

from django.utils.translation import ugettext_lazy as _

from dynamic_preferences.types import BooleanPreference, IntegerPreference
from dynamic_preferences.preferences import Section
from dynamic_preferences.registries import global_preferences_registry


accounting = Section('accounting')


@global_preferences_registry.register
class DefaultVisitXP(IntegerPreference):
    section = accounting
    name = 'default_visit_xp'
    default = 1
    help_text = _(u'Стандартный опыт игрока за посещенную игру')


@global_preferences_registry.register
class FreeFirstVisit(BooleanPreference):
    section = accounting
    name = 'free_first_visit'
    default = False
    help_text = _(u'Первая игра бесплатно')


@global_preferences_registry.register
class DefaultBonusXP(IntegerPreference):
    section = accounting
    name = 'default_bonus_xp'
    default = 1
    help_text = _(u'Стандартный бонусный опыт выдаваемый ведущим за игру')


@global_preferences_registry.register
class DefaultCleanedXP(IntegerPreference):
    section = accounting
    name = 'default_cleaned_xp'
    default = 1
    help_text = _(u'Стандартный опыт игрока за уборку')


@global_preferences_registry.register
class DefaultGameXPCost(IntegerPreference):
    section = accounting
    name = 'default_game_xp_cost'
    default = 10000
    help_text = _(u'Стандартная цена игры за опыт')


@global_preferences_registry.register
class MinimumPlayersForSalary(IntegerPreference):
    section = accounting
    name = 'minimum_players_for_salary'
    default = 3
    help_text = _(u'Минимум игроков для получения ЗП')


@global_preferences_registry.register
class CusomizablePlayerPaidAmount(BooleanPreference):
    section = accounting
    name = 'customizable_player_paid_amount'
    default = False
    help_text = _(u'В отчетах можно изменять оплаченную игроком сумму')
