# -*- coding: utf-8 -*-

from django.db.models import Q
from django.core.management.base import BaseCommand

from accounting.models import BalanceChange


class Command(BaseCommand):
    help = 'Mark balance changes verified'

    def handle(self, *args, **options):
        self.verbosity = int(options.get('verbosity', 1))

        for bc in BalanceChange.objects.filter(verified=False).only('pk').iterator():
            (
                BalanceChange
                .objects
                .filter(
                    Q(pk=bc.pk)
                    &
                    (
                        Q(cost_game_session_visits__verified=True) |
                        Q(paid_game_session_visits__verified=True) |
                        Q(to_master_game_session_visits__verified=True)
                    )
                )
                .update(verified=True)
            )
