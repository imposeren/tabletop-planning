# -*- coding: utf-8 -*-
import datetime

from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand, CommandError
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

import calendar
from dateutil.relativedelta import relativedelta

from games.models import GameSession, SessionVisit
from accounting.models import ExperienceReward


class Command(BaseCommand):
    args = '[<processing_days>] -- optional, 14 by default'
    help = 'Reward masters for their games'

    def reward_user(self, user, prev_date=None, now=None):
        if prev_date and now:
            raise CommandError("Only one of prev_date or now options is allowed. You supplied both")

        if not now:
            now = timezone.now()
        if not prev_date:
            prev_date = now - datetime.timedelta(days=14)

        base_qs = GameSession.objects.filter(
            visits__verified=True, visits__visitor=user,
            visits__role=SessionVisit.ROLES.master,
            game__masters=user,
            event__start_date__lte=now,
        )

        game_sessions = base_qs.filter(
            event__start_date__gte=prev_date,
        )
        for game_session in game_sessions.iterator():
            user = user.__class__.objects.get(pk=user.pk)  # experience may have changed

            if game_session.visits.filter(verified=False).exists():
                continue
            master_visit = game_session.visits.filter(role=SessionVisit.ROLES.master, visitor__pk=user.pk).first()
            if not master_visit:
                raise Exception('something wrong with logic: game_sessions are filtered with master visit but no master visit available')

            monday_on_session_week = game_session.event.start_date + relativedelta(weekday=calendar.MONDAY, days=+1) - datetime.timedelta(days=7)
            monday_on_session_week = monday_on_session_week.replace(hour=0, minute=0, second=0)

            reward_amount = 1

            prev_sessions_on_week = base_qs.filter(
                event__start_date__gte=monday_on_session_week,
                event__start_date__lt=game_session.event.start_date,
            ).exclude(pk=game_session.pk).count()

            comment = _(u'{0}-я игра за неделю').format(prev_sessions_on_week+1)

            reward_amount += min(
                3,
                int((prev_sessions_on_week+1)/2)
            )

            _created = False
            visit_xp = master_visit.visit_xp
            if not visit_xp:
                _created = True
                visit_xp = ExperienceReward(
                    user=user,
                    verified=True,
                    master_type=True,

                )

            if visit_xp.amount != reward_amount or _created:
                visit_xp.amount = reward_amount
                visit_xp.comment = comment
                visit_xp.save()

            if _created:
                master_visit.visit_xp = visit_xp
                master_visit.save()

    def handle(self, *args, **options):
        self.verbosity = int(options.get('verbosity', 1))
        now = timezone.now()
        if args:
            processing_days = int(args[0])
        else:
            processing_days = 14

        prev_date = now - datetime.timedelta(days=processing_days)
        if self.verbosity >= 1:
            self.stdout.write('Processing master rewards for %d days' % processing_days)
        if self.verbosity >= 2:
            self.stdout.write('From date %s' % prev_date)

        for user in get_user_model().objects.filter(mastering_games__isnull=False):
            if self.verbosity >= 2:
                self.stdout.write('From date %s' % prev_date)
            self.reward_user(user, prev_date, None)
