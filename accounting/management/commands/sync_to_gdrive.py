# -*- coding: utf-8 -*-
# See https://developers.google.com/drive/api/v3/quickstart/python
import datetime
import io
from csv import DictWriter

from django.core.management.base import BaseCommand
from django.utils.translation import ugettext as _

from apiclient.discovery import build
from apiclient.http import MediaIoBaseUpload
from dynamic_preferences.registries import global_preferences_registry
from httplib2 import Http
from messages_extends.models import Message
from oauth2client.client import (
    OAuth2Credentials, FlowExchangeError, AccessTokenRefreshError, HttpAccessTokenRefreshError,
    TokenRevokeError,
)
from social_django.storage import BaseDjangoStorage
from social_django.strategy import DjangoStrategy

from tabletop_planning.social_backends import CustomGoogleOAuth2
from users.models import User


oauth_exceptions = (
    FlowExchangeError, AccessTokenRefreshError, HttpAccessTokenRefreshError,
    TokenRevokeError
)


class Command(BaseCommand):
    help = 'save data to google drive'

    FOLDER_MIME = 'application/vnd.google-apps.folder'
    SPREADSHEET_MIME = 'application/vnd.google-apps.spreadsheet'

    def __init__(self, *args, **kwargs):
        super(Command, self).__init__(*args, **kwargs)
        self.social_strategy = DjangoStrategy(
            BaseDjangoStorage(),
        )
        self.social_backend = CustomGoogleOAuth2(self.social_strategy)

    def get_user_auth_credentials(self, user):
        social = user.social_auth.filter(provider='google-oauth2').first()

        data = social and social.extra_data

        all_fine = (
            social
            and
            'https://www.googleapis.com/auth/drive.file' in data.get('saved_scopes', [])
            and
            data.get('refresh_token')
            and
            data.get('access_token')
            and
            data.get('auth_time')
            and
            data.get('expires')
        )
        if not all_fine:
            self.warn_user(user, 'reenable scope')

        client_id, client_secret = self.social_backend.get_key_and_secret()

        return OAuth2Credentials(
            data['access_token'],
            client_id,
            client_secret,
            data['refresh_token'],
            token_expiry=datetime.datetime.fromtimestamp(data['auth_time']) + datetime.timedelta(seconds=data['expires']),
            token_uri=self.social_backend.access_token_url(),
            user_agent=None,
            revoke_uri=self.social_backend.revoke_token_url(data['access_token'], social.uid)
        )

    def get_drive_client(self, user):
        try:
            creds = self.get_user_auth_credentials(user)
            if creds:
                return build('drive', 'v3', http=creds.authorize(Http()))
        except oauth_exceptions:
            self.warn_user('reenable scope')
            return None

    def _q_escape(self, value):
        return value.replace("\\", "\\\\").replace("'", "\\'")

    def _drive_search_first(self, drive_client, name, mime, parent_id=None):
        q = "name = '%s' and mimeType = '%s'" % (self._q_escape(name), mime)
        if parent_id:
            q += " and '%s' in parents" % parent_id
        files_search = drive_client.files().list(
            q=q,
            fields="nextPageToken, files(id, name)",
        ).execute()

        for info in files_search.get('files', []):
            file_id = info.get('id')
            break
        else:
            file_id = None

        return file_id

    def _drive_create(self, drive_client, name, mime, **api_kwargs):
        return drive_client.files().create(
            body={'name': name, 'mimeType': mime},
            fields='id',
            **api_kwargs
        ).execute().get('id')

    def get_or_create_folder(self, drive_client, name):
        folder_id = self._drive_search_first(drive_client, name, self.FOLDER_MIME)
        return folder_id or self._drive_create(drive_client, name, self.FOLDER_MIME)

    def update_or_create_spreadsheet(self, drive_client, name, content, parent_id):
        file_id = self._drive_search_first(
            drive_client,
            name, self.SPREADSHEET_MIME,
            parent_id
        )
        file_metadata = {
            'name': name,
            'mimeType': self.SPREADSHEET_MIME,
        }
        if file_id:
            method = drive_client.files().update
            extra_kwargs = {'fileId': file_id}
        else:
            method = drive_client.files().create
            file_metadata['parents'] = [parent_id]
            extra_kwargs = {}

        if not isinstance(content, io.StringIO):
            content = io.StringIO(content)

        return method(
            body=file_metadata,
            media_body=MediaIoBaseUpload(
                content,
                mimetype='text/csv',
                resumable=False,
            ),
            fields='id',
            **extra_kwargs
        ).execute().get('id')

    def build_csv_content(self, users, xp_field, xp_field_readable=None):
        xp_field_readable = xp_field_readable or xp_field

        io_obj = io.StringIO()
        csv_writer = DictWriter(io_obj, ['user_id', 'full_name', xp_field_readable])
        csv_writer.writeheader()

        for user in users:
            csv_writer.writerow({
                'user_id': user.pk,
                'full_name': str(user).encode('utf-8'),
                xp_field_readable: getattr(user, xp_field, 0),
            })
        res = io_obj.getvalue()
        io_obj.close()
        return res

    def warn_user(self, user, message_id):
        if message_id == 'reenable scope':
            message, _created = Message.objects.get_or_create(
                user=user, extra_tags='info drive-sync-%s' % datetime.date.today(),
                level=Message.LEVEL_CHOICES[1][0],
                defaults={'message': _(
                    u'Данные для доступа к вашему гугл-диску неактуальны! '
                    u'Пожалуйста зайдите на страницу редактирования профиля и нажмите '
                    u'на ссылку "Cохранять таблицы опыта на гугл-диск"'
                )}
            )

    def handle(self, *args, **options):
        self.verbosity = int(options.get('verbosity', 1))
        global_preferences = global_preferences_registry.manager()
        project_name = global_preferences['tabletop_planning__project_name']
        finance_moderators_group_name = global_preferences['tabletop_planning__accounting_moderators_groupname']

        # build csv contents for xp tables:
        all_users = User.objects.filter(is_active=True, is_service=False).order_by('-cached_total_player_experience')
        players_csv_content = self.build_csv_content(all_users, 'cached_total_player_experience', 'player_xp')
        confirmed_balance_content = self.build_csv_content(all_users, 'balance_verified')
        non_confirmed_balance_content = self.build_csv_content(all_users, 'balance', 'balance_non_moderated')

        masters = User.objects.filter(is_active=True, is_service=False, is_confirmed_master=True).order_by('-cached_total_master_experience')
        masters_csv_content = self.build_csv_content(masters, 'cached_total_master_experience', 'master_xp')

        for user in User.objects.filter(is_active=True, is_staff=True, save_tables_to_drive=True):
            drive_client = self.get_drive_client(user)

            if not drive_client:
                continue

            folder_id = self.get_or_create_folder(drive_client, '%s Tables Backup' % project_name)
            self.update_or_create_spreadsheet(
                drive_client,
                "Players' Experience Table",
                players_csv_content,
                folder_id,
            )

            self.update_or_create_spreadsheet(
                drive_client,
                "Masters' Experience Table",
                masters_csv_content,
                folder_id,
            )

            if user.is_superuser or user.groups.filter(name=finance_moderators_group_name).exists():
                self.update_or_create_spreadsheet(
                    drive_client,
                    "Unpaid Finances Table",
                    confirmed_balance_content,
                    folder_id,
                )
                self.update_or_create_spreadsheet(
                    drive_client,
                    "Unpaid (non moderated) Finances Table",
                    non_confirmed_balance_content,
                    folder_id,
                )
