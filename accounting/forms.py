# -*- coding: utf-8 -*-

# Django:
from django import forms
from django.utils.translation import ugettext_lazy as _

# third-parties
from crispy_forms.layout import Layout

# project:
from tabletop_planning.forms import BootstrapModelForm

# app:
from .models import ExperienceReward, Ticket, BalanceChange


class ExperienceRewardAdminForm(forms.ModelForm):

    class Meta:  # noqa: D101
        model = ExperienceReward
        fields = '__all__'


class BalanceChangeAdminForm(forms.ModelForm):

    class Meta:  # noqa: D101
        model = BalanceChange
        fields = '__all__'


class TicketAddAdminForm(BootstrapModelForm):

    tickets_number = forms.IntegerField(label=_(u'Количество игр'), min_value=1, max_value=1000)
    total_cost = forms.DecimalField(
        label=_(u'Общая цена'),
        help_text=_(
            u'Цена за все игры абонемента. Установка цены 0 приведёт к отсутствию денежных транзакций. '
            u'Установка других значений создаст *подтвержденные* транзакции от игрока к клубу'
        ),
        min_value=0,
        max_value=50000,
        max_digits=6,
        decimal_places=3,
    )

    class Meta:  # noqa: D101
        model = Ticket
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(TicketAddAdminForm, self).__init__(*args, **kwargs)
        self.fields['cost'].required = False
        self.fields['cost'].widget = forms.HiddenInput()

        self.helper.layout = Layout(
            'player', 'tickets_number', 'total_cost'
        )

    def save(self, commit=True):
        cd = self.cleaned_data

        one_ticket_cost = cd['total_cost'] / cd['tickets_number']
        results = []

        for i in range(cd['tickets_number']):
            ticket = Ticket(user=cd['user'], cost=one_ticket_cost)

            if commit:
                ticket.save()

            results.append(ticket)
        return results

    def save_m2m(self):
        pass


class TicketChangeAdminForm(forms.ModelForm):
    class Meta:  # noqa: D101
        model = Ticket
        fields = '__all__'


class ExperienceRewardModerateForm(BootstrapModelForm):
    class Meta:  # noqa: D101
        model = ExperienceReward
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(ExperienceRewardModerateForm, self).__init__(*args, **kwargs)
        self.fields['user'].queryset = self.instance.user.__class__.objects.filter(pk=self.instance.user.pk)
        self.fields['user'].empty_label = None
        self.fields['created_datetime'].widget.attrs['readonly'] = ''
        self.fields['master_type'].widget.attrs['readonly'] = ''
        self.helper.form_tag = False
        self.helper.label_class = 'col-lg-2'
        self.helper.field_class = 'col-lg-8'
