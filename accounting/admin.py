# -*- coding: utf-8 -*-
from django.contrib import admin
from django.contrib import messages
from django.contrib.admin.models import LogEntry, DELETION
try:
    from django.core.urlresolvers import reverse
except ImportError:
    from django.urls import reverse
from django.utils.html import escape
from django.utils.translation import ugettext_lazy as _

from reversion.admin import VersionAdmin

from .models import ExperienceReward, PlayerLevelInfo, Ticket, MasterLevelInfo, BalanceChange
from .forms import ExperienceRewardAdminForm, TicketAddAdminForm, TicketChangeAdminForm, BalanceChangeAdminForm


class ExperienceRewardAdmin(VersionAdmin):

    list_display = ('__str__', 'user', 'amount', 'game_session', 'comment', 'created_datetime', 'verified', )
    search_fields = ('user__email', 'user__first_name', 'user__last_name')
    readonly_fields = ('game_session',)
    raw_id_fields = ('user',)

    form = ExperienceRewardAdminForm


class PlayerLevelInfoAdmin(admin.ModelAdmin):

    list_display = (
        'level', 'xp_per_level', 'total_xp', 'one_ticket_cost',
        'five_tickets_cost', 'ten_tickets_cost', 'extra_info',
        'five_economy', 'ten_economy',
    )

    list_editable = (
        'xp_per_level', 'one_ticket_cost',
        'five_tickets_cost', 'ten_tickets_cost', 'extra_info',
    )

    readonly_fields = (
        'five_economy', 'ten_economy', 'total_xp',
    )


class MasterLevelInfoAdmin(admin.ModelAdmin):

    list_display = (
        'level', 'xp_per_level', 'total_xp', 'one_game_salary', 'safe_hours', 'extra_info',
    )

    list_editable = (
        'xp_per_level', 'one_game_salary', 'safe_hours', 'extra_info',
    )

    readonly_fields = (
        'total_xp',
    )


class TicketAdmin(VersionAdmin):
    list_display = ('pk', 'user', 'cost', 'visit_used', 'event__start_date',)
    list_filter = ('cost', 'user')
    ordering = ('-visit_used__game_session__event__start_date', 'pk', 'user')

    readonly_fields = ('visit_used', 'budget_balance_change')
    raw_id_fields = ('user',)

    form = TicketChangeAdminForm
    add_form = TicketAddAdminForm

    def event__start_date(self, obj):
        visit = getattr(obj, 'visit_used', None)
        if not visit:
            return None
        game_session = getattr(visit, 'game_session', None)
        if not game_session:
            return None
        return getattr(game_session.event, 'start_date', None)
    event__start_date.admin_order_field = 'visit_used__game_session__event__start_date'

    def get_queryset(self, request):
        qs = super(TicketAdmin, self).get_queryset(request)
        qs = qs.select_related('visit_used__game_session__event', 'user')
        return qs

    def get_form(self, request, obj=None, **kwargs):
        """Use special form during user creation."""
        defaults = {}
        if obj is None:
            defaults['form'] = self.add_form
        defaults.update(kwargs)
        return super(TicketAdmin, self).get_form(request, obj, **defaults)

    def response_add(self, request, obj, post_url_continue=None):
        if isinstance(obj, list):
            obj = obj[0]
            self.message_user(
                request,
                _(
                    u'Было добавлено несколько билетов на игры. Если вы выбрали "сохранить и редактировать", '
                    u'то будет показан только один из них'
                ),
                messages.INFO
            )
        return super(TicketAdmin, self).response_add(request, obj, post_url_continue)

    def _create_formsets(self, request, obj, change):
        if isinstance(obj, list):
            return [], []
        else:
            return super(TicketAdmin, self)._create_formsets(request, obj, change)

    def save_model(self, request, obj, form, change):
        """Given a model instance save it to the database."""
        if isinstance(obj, list):
            for subobj in obj:
                super(TicketAdmin, self).save_model(request, subobj, form, change)
        else:
            return super(TicketAdmin, self).save_model(request, obj, form, change)

    def log_addition(self, request, object, message):
        """Log that an object has been successfully added.

        The default implementation creates an admin LogEntry object.
        """
        if isinstance(object, list):
            for subobj in object:
                super(TicketAdmin, self).log_addition(request, subobj, message)
        else:
            return super(TicketAdmin, self).log_addition(request, object, message)


class LogEntryAdmin(admin.ModelAdmin):

    date_hierarchy = 'action_time'

    readonly_fields = [f.name for f in LogEntry._meta.get_fields()]

    list_filter = [
        'user',
        'content_type',
        'action_flag'
    ]

    search_fields = [
        'object_repr',
        'change_message'
    ]

    list_display = [
        'action_time',
        'user',
        'content_type',
        'object_link',
        'action_flag',
        'change_message',
    ]

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return request.user.is_superuser and request.method != 'POST'

    def has_delete_permission(self, request, obj=None):
        return False

    def object_link(self, obj):
        if obj.action_flag == DELETION:
            link = escape(obj.object_repr)
        else:
            ct = obj.content_type
            link = u'<a href="%s">%s</a>' % (
                reverse('admin:%s_%s_change' % (ct.app_label, ct.model), args=[obj.object_id]),
                escape(obj.object_repr),
            )
        return link
    object_link.allow_tags = True
    object_link.admin_order_field = 'object_repr'
    object_link.short_description = u'object'

    def queryset(self, request):
        return super(LogEntryAdmin, self).queryset(request) \
            .prefetch_related('content_type')


class BalanceChangeAdmin(VersionAdmin):

    list_display = ('created_datetime', 'user', 'to_budget', 'verified', 'amount', 'comment', 'get_readable_details', )
    list_filter = ('user', 'created_datetime', 'verified', 'to_budget')

    readonly_fields = ('details', 'get_readable_details', )
    raw_id_fields = ('user',)

    form = BalanceChangeAdminForm


admin.site.register(ExperienceReward, ExperienceRewardAdmin)
admin.site.register(PlayerLevelInfo, PlayerLevelInfoAdmin)
admin.site.register(MasterLevelInfo, MasterLevelInfoAdmin)
admin.site.register(Ticket, TicketAdmin)
admin.site.register(LogEntry, LogEntryAdmin)
admin.site.register(BalanceChange, BalanceChangeAdmin)
