# -*- coding: utf-8 -*-

# python builtins:
# ...

# django:
# ...

# thirdparty:
from annoying.decorators import render_to

# project:
# None

# current app:
from .models import PlayerLevelInfo, MasterLevelInfo


@render_to('accounting/levels_info.html')
def levels_info(request):
    level_infos = PlayerLevelInfo.objects.all()

    return {
        'level_infos': level_infos,
        'masters': False,
    }


@render_to('accounting/levels_info.html')
def master_levels_info(request):
    level_infos = MasterLevelInfo.objects.all()

    return {
        'level_infos': level_infos,
        'masters': True,
    }
