# -*- coding: utf-8 -*-
from django.apps import AppConfig


class GamesAppConfig(AppConfig):
    name = 'games'
    verbose_name = "Tabletop Games"

    def ready(self):
        from games.signal_receivers import session_change  # noqa: F401
        return
