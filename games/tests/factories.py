# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import factory
from factory.django import DjangoModelFactory
from games import models


class GameFactory(DjangoModelFactory):
    class Meta:  # noqa: D101
        model = models.Game

    name = factory.Sequence(lambda n: 'game-%d' % n)
