# -*- coding: utf-8 -*-
import datetime

from django.core.exceptions import ValidationError
from django.utils import timezone

from freezegun import freeze_time

from games.tests.factories import GameFactory
from users.tests.factories import UserFactory

from tabletop_planning.tests import BaseTestCase
from games.models import GameSession, Game, SessionVisit


class GameModelsTestCase(BaseTestCase):
    def test_game_session_number(self):
        game = GameFactory.create(max_players=3)
        game.masters.add(self.admin)

        session0 = GameSession.create_with_event(
            game=game,
            start_date=timezone.now()
        )

        self.assertEqual(session0.number, 1)

        # manual number setting
        session0.number = 14
        session0.save()
        self.assertEqual(session0.number, 14)

        # previous sessions should change nothing if number is lower:
        session1 = GameSession.create_with_event(
            game=game,
            start_date=timezone.now()-datetime.timedelta(hours=5),
            number=10,
        )
        # refetch sessions:
        session0 = GameSession.objects.get(pk=session0.pk)

        # check
        self.assertEqual(session1.number, 10)
        self.assertEqual(session0.number, 14)

        # next conflict (newer session has lower number) is not allowed. just raise validationError
        with self.assertRaises(ValidationError):
            GameSession.create_with_event(
                game=game,
                start_date=timezone.now()-datetime.timedelta(hours=4),
                number=10,
            )

        # autoincremente future session numbers on older creation
        session3 = GameSession.create_with_event(
            game=game,
            start_date=timezone.now()-datetime.timedelta(hours=6),
            number=12,
        )

        # refetch sessions:
        session0 = GameSession.objects.get(pk=session0.pk)
        session1 = GameSession.objects.get(pk=session1.pk)

        # check
        self.assertEqual(session3.number, 12)
        self.assertEqual(session1.number, 13)
        self.assertEqual(session0.number, 17)

        # test players addition/removal:
        pl1 = UserFactory.create()
        pl2 = UserFactory.create()
        pl3 = UserFactory.create()
        pl4 = UserFactory.create()

        self.assertEqual(game.current_players, 0)

        game.add_player(pl1)
        self.assertEqual(game.current_players, 1)

        game.add_player(pl2)
        self.assertEqual(game.current_players, 2)

        game.add_player(pl3)
        self.assertEqual(game.current_players, 3)

        # maximum reached! player should not be added
        game.add_player(pl4)
        self.assertEqual(game.current_players, 3)

        self.assertCountEqual([pl1, pl2, pl3], list(game.players.all()))

        game.remove_player(pl2)
        self.assertEqual(game.current_players, 2)
        self.assertCountEqual([pl1, pl3], list(game.players.all()))

    # freeze on monday
    @freeze_time('2014-09-01')
    def test_game_get_parallel_games(self):
        new_user = UserFactory.create()

        # one user with game
        new_user.is_confirmed_master = True
        new_user.save()
        new_user_game = GameFactory.create(is_active=True)
        new_user_game.masters.add(new_user)

        # Create other user with games
        one_more_user = UserFactory.create()
        one_more_user.is_confirmed_master = True
        one_more_user.save()
        one_more_user_game = GameFactory.create(is_active=True)
        one_more_user_game.masters.add(one_more_user)

        # and more:
        other_user = UserFactory.create()
        other_user.is_confirmed_master = True
        other_user.save()
        other_user_game = GameFactory.create(is_active=True)
        other_user_game.masters.add(other_user)

        # tuesday on next week
        test_start_date = timezone.make_aware(datetime.datetime(2014, 9, 9, 14, 0), timezone.get_fixed_timezone(3*60))
        test_end_date = timezone.make_aware(datetime.datetime(2014, 9, 9, 17, 0), timezone.get_fixed_timezone(3*60))

        self.assertEqual(
            Game.get_parallel_games_without_parallel_sessions(Game.objects.all(), test_start_date, test_end_date).count(),
            0
        )

        # tuesday on current week:
        conflict_start_date = test_start_date - datetime.timedelta(days=7)
        conflict_end_date = test_end_date - datetime.timedelta(days=7)

        # create sessions that will increase number of parallel games
        GameSession.create_with_event(
            game=new_user_game,
            start_date=conflict_start_date + datetime.timedelta(minutes=5),
            end_date=conflict_end_date - datetime.timedelta(minutes=5),
        )

        # refetch from Database
        new_user_game = Game.objects.get(pk=new_user_game.pk)

        db_range = (
            new_user_game.last_session_time_range.lower,
            new_user_game.last_session_time_range.upper,
        )

        self.assertEqual(db_range, (21406, 21655))

        self.assertEqual(
            Game.get_parallel_games_without_parallel_sessions(Game.objects.all(), test_start_date, test_end_date).count(),
            1
        )

        GameSession.create_with_event(
            game=other_user_game,
            start_date=conflict_start_date + datetime.timedelta(minutes=5),
            end_date=conflict_end_date - datetime.timedelta(minutes=5),
        )

        # refetch from Database
        other_user_game = Game.objects.get(pk=other_user_game.pk)
        db_range = (
            other_user_game.last_session_time_range.lower,
            other_user_game.last_session_time_range.upper,
        )
        self.assertEqual(db_range, (21406, 21655))

        self.assertEqual(
            Game.get_parallel_games_without_parallel_sessions(Game.objects.all(), test_start_date, test_end_date).count(),
            2
        )

        # Create nonconflicting session:
        GameSession.create_with_event(
            game=one_more_user_game,
            start_date=conflict_start_date + datetime.timedelta(hours=5, minutes=5),
            end_date=conflict_end_date - datetime.timedelta(minutes=5) + datetime.timedelta(hours=5),
        )

        # no changes
        self.assertEqual(
            Game.get_parallel_games_without_parallel_sessions(Game.objects.all(), test_start_date, test_end_date).count(),
            2
        )

        # create conflicting session:
        GameSession.create_with_event(
            game=other_user_game,
            start_date=test_start_date + datetime.timedelta(minutes=6),
            end_date=test_end_date - datetime.timedelta(minutes=6),
        )

        # refetch from Database
        other_user_game = Game.objects.get(pk=other_user_game.pk)
        db_range = (
            other_user_game.last_session_time_range.lower,
            other_user_game.last_session_time_range.upper,
        )
        self.assertEqual(db_range, (21407, 21654))

        # we now have "real" parallel session. so numbe of games should be reduced by 1
        self.assertEqual(
            Game.get_parallel_games_without_parallel_sessions(Game.objects.all(), test_start_date, test_end_date).count(),
            1
        )

    def test_participated_players(self):
        game = GameFactory.create(max_players=3)
        game.masters.add(self.admin)

        session0 = GameSession.create_with_event(
            game=game,
            start_date=timezone.now()
        )
        other_user = UserFactory.create()
        game.game_players.create(player=other_user)
        self.assertFalse(game.participated_players.exists())

        SessionVisit.objects.create(
            visitor=other_user,
            game_session=session0,
            role=SessionVisit.ROLES.player,
            have_visited=True,
            verified=True,
        )

        self.assertTrue(
            game.participated_players.filter(pk=other_user.pk).exists()
        )
        game.game_players.all().delete()
        self.assertTrue(
            game.participated_players.filter(pk=other_user.pk).exists()
        )
        self.assertEqual(other_user.participated_games.count(), 1)
        self.assertEqual(other_user.participated_games.first().pk, game.pk)
