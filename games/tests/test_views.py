# -*- coding: utf-8 -*-

try:
    from django.core.urlresolvers import reverse
except ImportError:
    from django.urls import reverse

from freezegun import freeze_time

from games.tests.factories import GameFactory
from users.tests.factories import UserFactory

from tabletop_planning.tests import BaseTestCase
from games.forms import GameEditForm


class GameViewsTestCase(BaseTestCase):
    # freeze on friday
    @freeze_time('2014-09-05')
    def test_game_edit_view(self):
        """Test game editing views.

        1. test that new user is not allowed to edit/create games
        2. test that confirmed user can open game creation view
        3. test that confirmed user can't edit games of other users

        """
        root_url = reverse('games:game_sessions_index')
        create_url = reverse('games:create_game')
        new_user = UserFactory.create()

        self.assertTrue(self.client.login(email=new_user.email, password=new_user.email))

        response = self.client.get(create_url, follow=True)
        self.assertRedirects(response, root_url)

        # test that some message was shown
        self.assertTrue(response.context['messages']._loaded_messages)

        # test confirmed user
        new_user.is_confirmed_master = True
        new_user.save()

        response = self.client.get(create_url, follow=True)

        self.assertTrue(response.context['form'])
        self.assertTrue(response.context['sessions_formset'])

        self.assertIsInstance(response.context['form'], GameEditForm)
        # form is tested in test_forms module

        # let's create game for new_user
        new_user_game = GameFactory.create()
        new_user_game.masters.add(new_user)

        # test that he can edit it:
        response = self.client.get(new_user_game.get_edit_url())
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, new_user_game.name)

        # test other user editing this game
        other_user = UserFactory.create()
        other_user.is_confirmed_master = True
        other_user.save()
        self.client.logout()

        self.assertTrue(self.client.login(email=other_user.email, password=other_user.email))

        response = self.client.get(new_user_game.get_edit_url())

        self.assertEqual(response.status_code, 404)
