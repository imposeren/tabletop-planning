# -*- coding: utf-8 -*-
import datetime

from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.utils import timezone

from freezegun import freeze_time

from games.tests.factories import GameFactory
from users.tests.factories import UserFactory

from tabletop_planning.tests import BaseTestCase
from tabletop_planning.utils import make_formset_data, config_value
from games.models import GameSession, SessionVisit
from games.forms import GameReportFormset
from accounting.models import ExperienceReward, PlayerLevelInfo, MasterLevelInfo

User = get_user_model()


class GameViewsTestCase(BaseTestCase):

    @freeze_time('2014-09-05')
    def setUp(self, *args, **kwargs):
        super(GameViewsTestCase, self).setUp(*args, **kwargs)

        # 2 masters and 2 players
        self.master = self.create_master()
        self.player1 = UserFactory.create()
        self.player2 = UserFactory.create()
        self.other_master = self.create_master()

        # game of first master:
        self.game = GameFactory()
        self.game.masters.add(self.master)

        # add 2 players to that game
        self.game.game_players.create(player=self.player1)
        self.game.game_players.create(player=self.player2)

        # create "future" session (test time is freezed to 2014-09-05 00:00:
        with freeze_time("2014-09-05 12:00:03"):
            now = timezone.now()
            self.game_session = GameSession.create_with_event(game=self.game, start_date=now+datetime.timedelta(minutes=1))

            self.visit = SessionVisit(
                game_session=self.game_session,
                visitor=self.master,
            )

    @freeze_time('2014-09-05')
    def test_game_reports(self):
        """Test gamesession reports logic.

        1. game session not started yet --- report is unavailable.
        2. session started -- master can report it, others can't.
        3. after report master do not see "report" button for game but can still see for session.
        4. after report moderation master do not see repot buttons.

        Not tesing checking errors that should be produces by values of several SessionVisit
        instances (such as cleaned_xp). They should be checked in form.

        """
        from dynamic_preferences.registries import global_preferences_registry
        global_preferences = global_preferences_registry.manager()

        global_preferences['accounting__default_game_xp_cost'] = 10

        master = self.master
        player1 = self.player1
        other_master = self.other_master

        game = self.game
        game_session = self.game_session

        visit = self.visit

        # game time is in future: no reporting allowed:
        for obj in (game_session, game):
            self.assertFalse(obj.can_be_reported_by(master))
            self.assertFalse(obj.can_be_reported_by(other_master))

        with freeze_time("2014-09-05 12:00:03"):
            with self.assertRaises(ValidationError):
                visit.clean()

        with freeze_time("2014-09-05 12:01:04"):
            # game and session can be reported after being started
            for obj in (game_session, game):
                # delete cached properties:
                for _ in obj, getattr(obj, 'game', None):
                    if hasattr(_, 'reportables'):
                        del _.reportables
                self.assertTrue(obj.can_be_reported_by(master))
                self.assertFalse(obj.can_be_reported_by(other_master))

            # "reporting tests"

            # master visit tests
            visit.clean()
            self.assertEqual(visit.role, visit.ROLES.master)

            # test that player only fields are not allowed for master visit:
            for field in ('bonus', 'used_experience', 'used_ticket'):
                setattr(visit, field, True)
                with self.assertRaises(ValidationError):
                    visit.clean()
                setattr(visit, field, False)

                # not raising exception anymore
                visit.clean()

            # test XP for cleaning
            visit.cleaned = True
            visit.clean()
            visit.save()
            self.assertEqual(visit.cleaned_xp.amount, config_value('accounting', 'DEFAULT_CLEANED_XP'))

            # test XP for cleaning floor
            visit.cleaned_floor = True
            visit.clean()
            visit.save()
            self.assertEqual(visit.cleaned_xp.amount, 2*config_value('accounting', 'DEFAULT_CLEANED_XP'))

            # test that changing some clean fields recalculates XP properly
            visit.cleaned = False
            visit.clean()
            visit.save()
            self.assertEqual(visit.cleaned_xp.amount, config_value('accounting', 'DEFAULT_CLEANED_XP'))

            # no master XP until validation
            self.assertFalse(visit.visit_xp.verified)

            visit.verified = True
            visit.save()
            self.assertEqual(visit.visit_xp.amount, 1)
            self.assertTrue(visit.visit_xp.master_type)

            # player visit tests:
            visit = SessionVisit(
                game_session=game_session,
                visitor=player1,
                have_visited=False,
            )

            visit.save()
            # all XP fields are None if player have not visited
            for field in ('visit_xp', 'bonus_xp', 'cleaned_xp', 'payment_xp', 'ticket'):
                self.assertFalse(getattr(visit, field))

            # test XP creation withoud model cleaning
            visit.have_visited = True
            visit.save()
            self.assertEqual(visit.visit_xp.amount, config_value('accounting', 'DEFAULT_VISIT_XP'))
            self.assertFalse(visit.visit_xp.verified)

            visit.cleaned = True
            visit.save()
            self.assertEqual(visit.cleaned_xp.amount, config_value('accounting', 'DEFAULT_CLEANED_XP'))
            self.assertFalse(visit.cleaned_xp.verified)

            visit.used_experience = True
            visit.save()
            self.assertEqual(visit.payment_xp.amount, -config_value('accounting', 'DEFAULT_GAME_XP_COST'))
            self.assertFalse(visit.payment_xp.verified)

            visit.bonus = True
            visit.save()
            self.assertEqual(visit.bonus_xp.amount, config_value('accounting', 'DEFAULT_BONUS_XP'))
            self.assertFalse(visit.cleaned_xp.verified)

            # player has 4 xp and master has 2 xp objects:
            self.assertEqual(ExperienceReward.objects.count(), 4 + 2)

            # test that visit can't be both "free" and "for xp"
            visit.is_free = True
            with self.assertRaises(ValidationError):
                visit.clean()

            # but when used_experience is False then 'free' is fine (but may still be not moderated)
            visit.used_experience = False
            visit.clean()

            # revert back to XP payment
            visit.used_experience = True
            visit.is_free = False

            with self.assertRaises(ValidationError):
                # user have no XP to pay with xp
                visit.clean()

            xp_reward = visit.visitor.experience_rewards.create(amount=20, verified=False)

            # not verified XP is still bad"
            with self.assertRaises(ValidationError):
                visit.clean()

            xp_reward.verified = True
            xp_reward.save()

            # player has 4 xp objects for this visit, 1 for something else, and master has 2:
            self.assertEqual(ExperienceReward.objects.count(), 4 + 1 + 2)

            # now user have enough verified XP and visit can be cleaned:
            visit.clean()
            visit.save()

            visit.used_ticket = True

            with self.assertRaises(ValidationError):
                # user have no tickets!
                visit.clean()

            self.assertFalse(visit.ticket)

            visit.visitor.tickets.create()

            # delete has_tickets cache
            del visit.visitor.has_tickets

            # user have tickets but pays with XP so he can't use ticket when paying xp
            with self.assertRaises(ValidationError):
                visit.clean()

            visit.used_experience = False

            # delete has_tickets cache
            del visit.visitor.has_tickets

            # now user do not pay XP so ticket can be used"
            visit.clean()
            visit.save()
            self.assertTrue(visit.ticket)

            # xp objects are not yet verified because visit is not verified
            self.assertFalse(visit.visit_xp.verified)
            self.assertFalse(visit.bonus_xp.verified)
            self.assertFalse(visit.cleaned_xp.verified)

            visit.verified = True

            visit.clean()
            visit.save()

            # after verifying visit all xp objects become verified too:
            self.assertTrue(visit.visit_xp.verified)
            self.assertTrue(visit.bonus_xp.verified)
            self.assertTrue(visit.cleaned_xp.verified)
            self.assertFalse(visit.payment_xp)

            # 3 xp for this session, 1 xp for something else and 2 xp for master
            self.assertEqual(ExperienceReward.objects.count(), 3 + 1 + 2)

            visit.used_ticket = False
            visit.used_experience = True
            visit.clean()
            visit.save()

            self.assertFalse(visit.ticket)
            self.assertTrue(visit.payment_xp.verified)

            # new XP object should be added for payment:
            self.assertEqual(ExperienceReward.objects.count(), 4 + 1 + 2)

            # changing back to not using xp_payement: should delete corresponding XP object
            visit.used_ticket = True
            visit.used_experience = False
            visit.clean()
            visit.save()
            self.assertEqual(ExperienceReward.objects.count(), 3 + 1 + 2)

            self.assertTrue(visit.ticket)
            self.assertFalse(visit.payment_xp)

            # unverify visti and xp:
            visit.verified = False
            visit.save()
            self.assertFalse(visit.visit_xp.verified)
            self.assertFalse(visit.bonus_xp.verified)
            self.assertFalse(visit.cleaned_xp.verified)

    @freeze_time('2014-09-06')
    def test_game_reports_form(self):
        expenses_amount = 5
        self.player3 = UserFactory.create()
        self.player4 = UserFactory.create()
        self.game.game_players.create(player=self.player3)
        self.game.game_players.create(player=self.player4)

        players = {
            self.player1: self.player1,
            self.player2: self.player2,
            self.player3: self.player3,
            self.player4: self.player4,
        }

        self.assertEqual(self.master.balance, 0)
        del self.master.balance

        for player in players.values():
            self.assertEqual(player.balance, 0)
            del player.balance

        # modify player 1 to not have free game
        level1 = PlayerLevelInfo.objects.get(level=1)

        ExperienceReward.objects.create(
            user=self.player1, amount=1, verified=True
        )
        self.player1 = User.objects.get(pk=self.player1.pk)

        self.assertEqual(self.player1.player_level, level1)
        self.assertEqual(self.player1.default_player_payment, level1.one_ticket_cost)

        # modify player 2 to have level 2
        level2 = PlayerLevelInfo.objects.get(level=2)

        ExperienceReward.objects.create(
            user=self.player2, amount=level2.total_xp, verified=True
        )
        self.player2 = User.objects.get(pk=self.player2.pk)

        self.assertEqual(self.player2.player_level, level2)
        self.assertEqual(self.player2.default_player_payment, level2.one_ticket_cost)

        # modify player 3 to have level 3
        level3 = PlayerLevelInfo.objects.get(level=3)

        ExperienceReward.objects.create(
            user=self.player3, amount=level3.total_xp, verified=True
        )
        self.player3 = User.objects.get(pk=self.player3.pk)

        self.assertEqual(self.player3.player_level, level3)
        self.assertEqual(self.player3.default_player_payment, level3.one_ticket_cost)

        # modify player 4 to have level 1"
        ExperienceReward.objects.create(
            user=self.player4, amount=1, verified=True
        )
        self.player4 = User.objects.get(pk=self.player4.pk)

        # modify master to have level 5
        master_level5 = MasterLevelInfo.objects.get(level=5)

        ExperienceReward.objects.create(
            user=self.master, amount=master_level5.total_xp, verified=True,
            master_type=True,
        )
        self.master = User.objects.get(pk=self.master.pk)

        self.assertEqual(self.master.master_level, master_level5)
        self.assertEqual(self.master.default_master_payment, master_level5.one_game_salary)

        formset = GameReportFormset(game_session=self.game_session)
        data = [subform.initial for subform in formset.forms]

        for index, subdata in enumerate(data):
            player = players.get(subdata['visitor'], None)
            expense_user = subdata['visitor'].is_service
            if player == self.player4:
                # player4 have not visited game
                subdata['have_visited'] = False
            else:
                subdata['have_visited'] = True
            subdata['id'] = None
            subdata['visitor_level'] = 1

            if player:
                self.assertEqual(subdata['role'], SessionVisit.ROLES.player)
                self.assertEqual(subdata['game_cost'], player.default_player_payment)
                self.assertEqual(subdata['paid_amount'], player.default_player_payment)
            elif expense_user:
                subdata['visitor_level'] = 0
                subdata['paid_amount'] = expenses_amount
                self.assertEqual(subdata['role'], SessionVisit.ROLES.expenses)
                self.assertEqual(subdata['game_cost'], 0)
                # self.assertEqual(subdata['paid_amount'], 0)
            else:
                self.assertEqual(subdata['role'], SessionVisit.ROLES.master)
                self.assertEqual(subdata['game_cost'], -self.master.default_master_payment)
                self.assertEqual(subdata['paid_amount'], -self.master.default_master_payment)

        # now let's process this data.
        post_data = make_formset_data(
            {'TOTAL_FORMS': len(data), 'INITIAL_FORMS': 0, 'MAX_NUM_FORMS': len(data)},
            data,
        )
        formset = GameReportFormset(post_data, game_session=self.game_session)
        formset.clean()
        self.assertTrue(formset.is_valid(), msg="form is not valid. Form errors: %s" % formset.errors)
        formset.save()

        target_amount = (
            0
            + self.player1.default_player_payment
            + self.player2.default_player_payment
            + self.player3.default_player_payment
            # player4 have not visited game
            - self.master.default_master_payment
            - expenses_amount
        )

        self.assertEqual(self.master.balance, -target_amount)

        for player in players.values():
            self.assertEqual(player.balance, 0)
