# -*- coding: utf-8 -*-
from __future__ import unicode_literals

# python builtins:
import datetime
from urllib.parse import unquote_plus

# django:
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.db import transaction
from django.db.models import F, Q, Prefetch
from django.forms.formsets import DELETION_FIELD_NAME
from django.forms.models import modelformset_factory
from django.http import Http404
from django.shortcuts import get_object_or_404, redirect
from django.utils import timezone
from django.utils.encoding import force_str
from django.utils.translation import ugettext_lazy as _

try:
    from django.core.urlresolvers import reverse
except ImportError:
    from django.urls import reverse


# thirdparty:
from annoying.decorators import render_to
from el_pagination.decorators import page_template
from happenings.views import EventMonthView, EventDayView

# project:
from tabletop_planning.forms import ExtraFirstFormSet
from tabletop_planning.context_processors import default_context
from tabletop_planning.utils import config_value

# current app:
from .models import Game, GameSession
from .forms import GameEditForm, GameSessionForm, GameReportFormset


User = get_user_model()


@page_template('games/game_sessions_index_page.html')  # template for single page when ajax paging is used
@render_to('games/game_sessions_index.html')
def game_sessions_index(
        request, tag=None, template=None, extra_context=None):
    from news.models import NewsItem
    context = {'do_pagination': True, 'image_geometry': default_context(None)['BIGGER_GAME_IMAGE_GEOMETRY']}
    if template:
        context['TEMPLATE'] = template
    if extra_context:
        context.update(extra_context)

    base_qs = (
        GameSession.objects.filter(game__is_active=True, game__is_finished=False)
        .select_related('event', 'game')
        .prefetch_related('game__masters', 'game__players', 'game__system_tags', 'game__setting_tags', 'game__other_tags')
        .order_by('event__start_date')
    )
    if tag:
        tag = unquote_plus(force_str(tag)).decode('utf-8')
        base_qs = base_qs.filter(
            Q(game__system_tags__name__iexact=tag) |
            Q(game__setting_tags__name__iexact=tag) |
            Q(game__other_tags__name__iexact=tag)
        )

    time_limit = timezone.now() + datetime.timedelta(hours=36)
    context['nearest_game_sessions'] = (
        base_qs.filter(event__start_date__gte=timezone.now(), event__start_date__lte=time_limit).distinct()
    )

    context['other_game_sessions'] = (
        base_qs.filter(event__start_date__gt=time_limit).distinct()
    )
    context['sessions'] = context['other_game_sessions']
    context['latest_news'] = NewsItem.objects.get_active()[:5]

    return context


@page_template('games/players_required_page.html')  # template for single page when ajax paging is used
@render_to('games/players_required_index.html')
def players_required(request, tag=None, template=None, extra_context=None):
    from news.models import NewsItem
    context = {'do_pagination': True}
    if template:
        context['TEMPLATE'] = template
    if extra_context:
        context.update(extra_context)

    games = (
        Game.actives.all()
        .filter(is_finished=False)
        .prefetch_related('system_tags', 'setting_tags', 'other_tags', 'masters', 'players')
        .prefetch_related(Prefetch('sessions', queryset=GameSession.objects.select_related('event')))
        .order_by('-last_modified')
    )
    if tag:
        tag = unquote_plus(force_str(tag))
        games = games.filter(
            Q(system_tags__name__iexact=tag) |
            Q(setting_tags__name__iexact=tag) |
            Q(other_tags__name__iexact=tag)
        )
    games = games.filter(current_players__lt=F('max_players')).distinct()

    context['games'] = games

    context['latest_news'] = NewsItem.objects.get_active()[:5]

    return context


@render_to('games/game_details.html')
def game_details(request, slug):
    game = get_object_or_404(
        (
            Game.objects.all()
            .prefetch_related('system_tags', 'setting_tags', 'other_tags', 'masters', 'players')
            .prefetch_related(Prefetch('sessions', queryset=GameSession.objects.select_related('event')))
        ),
        slug=slug
    )
    if not game.is_active and not game.masters.filter(pk=request.user.pk).exists():
        raise Http404
    return {
        'game': game
    }


@login_required
def join_game(request, slug):
    with transaction.atomic():
        game = get_object_or_404(Game.actives.all(), slug=slug)

        if game.add_player(request.user):
            messages.info(request, _('Вы успешно присоединились к игре "%s"') % game)
        elif game.players.filter(pk=request.user.pk).exists():
            messages.error(request, _('Вы уже участвуете в этой игре'))
        elif game.masters.filter(pk=request.user.pk).exists():
            messages.error(request, _('Ведущий не может присоединиться к своей игре как игрок'))
        elif game.is_finished:
            messages.error(request, _('К сожалению вы не можете присоединиться к этой игре, потому-что она уже завершилась'))
        else:
            messages.error(request, _('К сожалению вы не можете присоединиться к этой игре'))
        return redirect(game.get_absolute_url())


@login_required
def leave_game(request, slug):
    game = get_object_or_404(Game.actives.all(), slug=slug)

    if game.remove_player(request.user):
        messages.info(request, _('Вы успешно покинули игру "%s"') % game)
    else:
        messages.error(request, _('Вы не участвуете в этой игре!'))
    return redirect(game.get_absolute_url())


@login_required
def remove_player(request, slug, user_pk):
    with transaction.atomic():
        game = get_object_or_404(Game, slug=slug, masters__pk=request.user.pk)
        user = get_object_or_404(User, pk=user_pk)

        if game.can_be_leaved_by(user):
            messages.info(request, _('Игрок "%(user)s" успешно удалён из игры "%(game)s"') % {'user': user, 'game': game})
            game.remove_player(user)
        else:
            messages.error(request, _('Выбранный игрок не участвует в этой игре!'))
        return redirect(game.get_absolute_url())


@login_required
def confirm_player(request, slug, user_pk):
    game = get_object_or_404(Game, slug=slug, masters__pk=request.user.pk)
    user = get_object_or_404(User, pk=user_pk)

    if game.can_be_confirmed(user):
        messages.info(request, _('Участие "%(user)s" в игре "%(game)s" успешно подтверждёно.') % {'user': user, 'game': game})
        game.confirm_player(user)
    else:
        messages.error(request, _('Выбранный игрок не участвует в этой игре или уже подтверждён!'))
    return redirect(game.get_absolute_url())


@login_required
@render_to('games/game_edit.html')
def game_edit(request, slug=None):

    confirmation_needed = (
        config_value('tabletop_planning', 'REQUIRE_GM_CONFIRMATION')
        and not request.user.is_confirmed_master
    )
    if confirmation_needed and not request.user.is_superuser:
        messages.error(request, _('Необходимо подтвердить что вы ведущий. Запрос выслан администраторам.'))
        request.user.requested_to_be_master_datetime = timezone.now()
        request.user.save(request=request)
        return redirect("games:game_sessions_index")

    if slug:
        game = get_object_or_404(Game, slug=slug, masters__pk=request.user.pk)
    else:
        game = None
    form = GameEditForm(
        request.POST or None,
        request.FILES or None,
        instance=game,
        request=request,
    )

    form.helper.form_tag = False

    initial_dict = {'game': game}
    if game:
        game_sessions = (
            game.sessions
            .filter(event__start_date__gte=timezone.now()-datetime.timedelta(hours=12))
            .select_related('event').order_by('-event__start_date')
        )
        num_forms = len(game_sessions) + 1
    else:
        game_sessions = GameSession.objects.none()
        initial_dict['create'] = True
        num_forms = 1
    GameSessionFormset = modelformset_factory(
        GameSession, GameSessionForm,
        formset=ExtraFirstFormSet,
        can_delete=True,
        extra=1,
    )
    sessions_formset = GameSessionFormset(
        request.POST or None,
        request.FILES or None,
        initial=[dict(initial_dict) for i in range(num_forms)],
        queryset=game_sessions,
        request=request,
    )

    response = {
        'form': form,
        'sessions_formset': sessions_formset,
    }

    if request.POST:
        with transaction.atomic():

            sid = transaction.savepoint()
            if game:
                GameSession.objects.select_for_update().filter(game_id=game.id)
                Game.objects.select_for_update().filter(pk=game.id)
            if form.is_valid():
                game = form.save()

            if game:
                for _form in sessions_formset:
                    if not (_form.instance and _form.instance.pk):
                        del _form.fields[DELETION_FIELD_NAME]
                    _form.data = _form.data.copy()
                    _form.data[_form.add_prefix('game')] = game.pk

                if sessions_formset.is_valid():
                    try:
                        sessions_formset.save()
                        transaction.savepoint_commit(sid)
                        response = redirect(game.get_absolute_url())
                    except Exception as e:
                        transaction.savepoint_rollback(sid)
                        form.add_error(ValidationError("Неожиданная ошибка: %s" % e, code='unexpected'))
                else:
                    transaction.savepoint_rollback(sid)
            else:
                transaction.savepoint_rollback(sid)

    else:
        response['sessions_formset'] = GameSessionFormset(
            request.POST or None,
            request.FILES or None,
            initial=[dict(initial_dict) for i in range(num_forms)],
            queryset=game_sessions,
        )

    return response


@login_required
@render_to('games/game_report.html')
def game_report(request, game_slug, session_number):
    game_sessions = (
        GameSession.objects
        .filter(
            game__slug=game_slug, number=session_number,
        )
        .order_by('-event__start_date')
    )
    if not (request.user.is_staff or request.user.is_superuser):
        game_sessions = game_sessions.filter(game__masters__pk=request.user.pk)

    game_session = game_sessions.first()
    if not game_session:
        raise Http404

    form = GameReportFormset(request.POST or None, game_session=game_session, request=request)
    if form.is_valid():
        form.save()
        return redirect(game_session.game.get_absolute_url())
    return {
        'form': form,
        'game_session': game_session,
    }


def filter_events(old_events):
    as_items = False

    filtered_events = {}

    if not isinstance(old_events, dict):
        old_events = dict(old_events)
        as_items = True

    for key, events_list in old_events.items():
        filtered_events[key] = []
        for event in events_list:
            if not event.game_session or event.game_session.game.is_active:
                filtered_events[key].append(event)

    if as_items:
        filtered_events = sorted(filtered_events.items())
    return filtered_events


class GameEventMonthView(EventMonthView):
    def get_calendar_back_url(self, year, month_num):
        return reverse('calendar_list_override', args=(year, month_num))

    def get_context_data(self, **kwargs):
        res = super(GameEventMonthView, self).get_context_data(**kwargs)
        res['ignore_participation'] = (
            (not self.request.user.is_authenticated)
            or
            not any(
                # It's not too bad to iterate here even if participation is rechecked in template,
                # because :py:meth:`User.participates_in_game` caches results per instance
                self.request.user.participates_in_game(event.game_session)
                for event in res['raw_all_month_events']
            )
        )
        return res

    def get_month_events(self, *args, **kwargs):
        qs = super(GameEventMonthView, self).get_month_events(*args, **kwargs)
        return (
            qs
            .filter(
                Q(game_session__isnull=True) | Q(game_session__game__is_active=True) |
                (Q(game_session__game__is_active=False) & Q(game_session__visits__have_visited=True))
            )
            .select_related('game_session__game')
            .prefetch_related('game_session__game__masters', 'game_session__game__players')
            .distinct()
        )


class GameEventDayView(EventDayView):
    def get_month_events(self, *args, **kwargs):
        qs = super(GameEventDayView, self).get_month_events(*args, **kwargs)
        return (
            qs
            .filter(
                Q(game_session__isnull=True)
                |
                Q(game_session__game__is_active=True)
                |
                (Q(game_session__game__is_active=False) & Q(game_session__visits__have_visited=True))
            ).distinct()
        )
