# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.db.models import Case, When, Value, IntegerField, F, Func, Q, CharField
from django.db.models.functions import Concat
from django.utils.html import format_html_join
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _, ugettext

try:
    from django.core.urlresolvers import reverse
except ImportError:
    from django.urls import reverse

from reversion.admin import VersionAdmin
from sorl.thumbnail.admin import AdminImageMixin

from tabletop_planning.utils import NullFilterSpec
from .models import Game, GameSession, SessionVisit, GamePlayer


class GamePlayerInline(admin.TabularInline):
    model = GamePlayer
    extra = 2


class GameAdmin(AdminImageMixin, admin.ModelAdmin):

    list_display = ('name', 'slug', 'master', 'current_players', 'max_players',)
    search_fields = ('name', 'slug', 'description',)
    ordering = ('slug',)
    inlines = (GamePlayerInline, )
    readonly_fields = ('current_players', 'master')


class CarefulSessionVisitMixin(object):
    def get_queryset(self, request):
        qs = super(CarefulSessionVisitMixin, self).get_queryset(request)
        qs = qs.select_related(
            'cost_balance_change', 'paid_balance_change', 'visit_xp', 'bonus_xp', 'cleaned_xp', 'payment_xp', 'ticket',
        )

        qs = (
            qs
            .annotate(
                requires_careful_moderation=Case(
                    When(
                        Q(
                            have_visited=True,
                            paid_balance_change__amount__lt=Func(F('cost_balance_change__amount'), function='abs')
                        ),
                        then=Value(2)
                    ),
                    When(
                        Q(
                            have_visited=True,
                            is_free=True
                        ),
                        then=Value(1),
                    ),
                    default=0,
                    output_field=IntegerField()
                ),
            )
        )

        if not (set(qs.query.order_by) & {'pk', '-pk', 'id', '-id'}):
            new_ordering = list(qs.query.order_by)
            new_ordering.append('pk')
            qs = qs.order_by(*new_ordering)

        return qs.distinct()

    def get_readonly_fields(self, request, obj=None):
        return [
            'visitor', 'visit_xp', 'bonus_xp', 'cleaned_xp', 'payment_xp', 'ticket',
            'game_cost',
            'paid_amount',
            'to_master_balance_change',
            'get_warn_msgs',
        ]

    def has_add_permission(self, request):
        return False

    def get_warn_msgs(self, instance):
        msgs_args = []
        if instance.requires_careful_moderation:
            if instance.requires_careful_moderation == 2 or (instance.cost_balance_change and instance.cost_balance_change.amount < 0):
                if instance.requires_careful_moderation == 2 or (-instance.cost_balance_change.amount > instance.paid_balance_change.amount):
                    underpaiment = -instance.cost_balance_change.amount - instance.paid_balance_change.amount
                    if (not underpaiment) and instance.paid_balance_change.amount < 0:
                        # this is actually an indication of "club-related expenses"
                        underpaiment = -instance.paid_balance_change.amount

                    msgs_args.append(
                        (ugettext('Не оплачено {underpaiment}').format(underpaiment=underpaiment),)
                    )
            if instance.is_free:
                msgs_args.append(
                    (ugettext('Бесплатное посещение!'),)
                )
        if msgs_args:
            return format_html_join(
                '; ',
                '<b style="background: #ff8888;">{}</b>',
                msgs_args,
            )
        return ""
    get_warn_msgs.short_description = _("Важно!")
    get_warn_msgs.admin_order_field = 'requires_careful_moderation'

    def get_requires_careful_moderation(self, instance):
        return instance.requires_careful_moderation
    get_requires_careful_moderation.short_description = _('1 — бесплатно, 2 — недоплата')
    get_requires_careful_moderation.admin_order_field = 'requires_careful_moderation'

    def game_cost(self, obj):
        if obj.cost_balance_change:
            return -obj.cost_balance_change.amount
        elif obj.role == SessionVisit.ROLES.player:
            return obj.visitor.default_player_payment
        else:
            return obj.visitor.default_master_payment
    game_cost.short_description = _('Цена')

    def paid_amount(self, obj):
        if obj.paid_balance_change:
            return obj.paid_balance_change.amount
        else:
            return 0
    paid_amount.short_description = _('Оплачено')


class SessionVisitInline(CarefulSessionVisitMixin, admin.TabularInline):
    model = SessionVisit
    can_delete = False
    extra = 0
    editable_fields = []
    fields = [
        'visitor', 'verified', 'role', 'have_visited', 'get_warn_msgs', 'comment', 'game_cost', 'paid_amount',
        'used_ticket', 'used_experience', 'is_free', 'bonus', 'cleaned', 'cleaned_floor',
        'visit_xp', 'bonus_xp', 'cleaned_xp', 'payment_xp', 'ticket', 'to_master_balance_change',
    ]


class HaveReportFilterSpec(NullFilterSpec):
    title = GameSession.have_report.short_description
    parameter_name = 'reported'
    query_parameter = 'visits'


class NeedsModerationFilterSpec(NullFilterSpec):
    title = GameSession.needs_moderation.short_description
    parameter_name = 'needs_moderation'
    query_parameter = 'visits'

    def queryset(self, request, queryset):
        queryset = super(NeedsModerationFilterSpec, self).queryset(request, queryset)
        if self.value() == '1':
            queryset = queryset.filter(visits__verified=False)
        return queryset


class GameSessionAdmin(VersionAdmin):

    list_display = (
        '__str__',
        'game',
        'master',
        'event__start_date',
        'have_report',
        'get_have_visits_unverified',
        'get_requires_careful_moderation',
    )
    list_filter = (HaveReportFilterSpec, NeedsModerationFilterSpec, 'game__masters')
    readonly_fields = ('event__start_date', 'master')
    # list_filter = ('is_active', )
    search_fields = ('event__description', 'game__name',)
    inlines = [SessionVisitInline]

    ordering = (
        '-have_visits_unverified',
        'event__start_date',
        'game__name',
        '-requires_careful_moderation',
        'id',
    )

    def get_ordering(self, request):
        if '/games/gamesession/' in request.path_info:
            return super(GameSessionAdmin, self).get_ordering(request)
        return ()

    def view_on_site(self, obj):
        return reverse(
            'games:report_game_session',
            args=[obj.game.slug, obj.number],
        )

    def event__start_date(self, obj):
        return getattr(obj.event, 'start_date', None)
    event__start_date.admin_order_field = 'event__start_date'
    event__start_date.short_description = _('Дата')

    def get_queryset(self, request):
        qs = super(GameSessionAdmin, self).get_queryset(request)
        qs = qs.select_related('event')

        if len(request.path_info.strip('/').split('/')) > 3:
            return qs

        if '/games/gamesession/' not in request.path_info:
            return qs

        qs = (
            qs
            .annotate(
                have_visits_unverified=Case(
                    When(visits__verified=False, then=Value(1)),
                    default=0,
                    output_field=IntegerField()
                ),
                requires_careful_moderation=Case(
                    When(
                        Q(
                            visits__have_visited=True,
                            visits__paid_balance_change__amount__lt=Func(F('visits__cost_balance_change__amount'), function='abs')
                        ),
                        then=Value(2)
                    ),
                    When(
                        Q(
                            visits__have_visited=True,
                            visits__is_free=True
                        ),
                        then=Value(1),
                    ),
                    default=0,
                    output_field=IntegerField()
                ),
            )
        )

        if not (set(qs.query.order_by) & {'pk', '-pk', 'id', '-id'}):
            new_ordering = list(qs.query.order_by)
            new_ordering.append('pk')
            qs = qs.order_by(*new_ordering)

        return qs.distinct()

    def get_have_visits_unverified(self, instance):
        return instance.have_visits_unverified

    get_have_visits_unverified.short_description = _('Требует модерации')
    get_have_visits_unverified.admin_order_field = 'have_visits_unverified'

    def get_requires_careful_moderation(self, instance):
        return instance.requires_careful_moderation
    get_requires_careful_moderation.short_description = _('1 — бесплатно, 2 — недоплата')
    get_requires_careful_moderation.admin_order_field = 'requires_careful_moderation'


class SessionVisitAdmin(CarefulSessionVisitMixin, VersionAdmin):

    actions = None
    view_on_site = True

    list_display = (
        'pk', 'get_session_name', 'verified', 'event__start_date', 'visitor', 'role',
        'have_visited', 'get_warn_msgs', 'comment', 'game_cost', 'paid_amount',
        'used_ticket', 'used_experience', 'is_free',
        'visitor_level', 'edit_report',
    )
    list_editable = ('verified',)
    raw_id_fields = (
        'game_session', 'visitor', 'ticket', 'visit_xp', 'bonus_xp', 'cleaned_xp', 'payment_xp',
        'cost_balance_change', 'paid_balance_change', 'to_master_balance_change',
    )
    autocomplete_lookup_fields = {
        'fk': ['game_session', 'visitor'],
    }

    ordering = (
        'verified',
        'game_session__event__start_date',
        'session_name',
        '-have_visited',
        '-requires_careful_moderation',
        'id',
    )

    def get_ordering(self, request):
        if '/games/sessionvisit/' in request.path_info:
            return super(SessionVisitAdmin, self).get_ordering(request)
        return ()

    def get_queryset(self, request):
        qs = super(SessionVisitAdmin, self).get_queryset(request)
        qs = qs.select_related('game_session__event', 'game_session__game')
        qs = qs.annotate(
            session_name=Concat(
                F('game_session__game__name'), Value(': #'), F('game_session__number'),
                output_field=CharField(),
            ),
        )
        return qs

    def get_readonly_fields(self, request, obj=None):
        res = super(SessionVisitAdmin, self).get_readonly_fields(request, obj=obj)
        res.extend([
            'event__start_date',
            'get_session_name',
        ])
        if obj:
            res.extend(self.raw_id_fields)
        return res

    def event__start_date(self, obj):
        game_session = getattr(obj, 'game_session', None)
        if game_session:
            return getattr(game_session.event, 'start_date', None)
        return None
    event__start_date.admin_order_field = 'game_session__event__start_date'
    event__start_date.short_description = _('Дата')

    def get_session_name(self, instance):
        return instance.session_name
    get_session_name.admin_order_field = 'session_name'
    get_session_name.short_description = _('Сессия')

    def edit_report(self, obj):
        return mark_safe(
            '<a href="{0}">{1}</a>'.format(
                obj.get_absolute_url(),
                _("Изменить")
            )
        )
    edit_report.allow_tags = True
    edit_report.short_description = _("Отчёт")


admin.site.register(Game, GameAdmin)
admin.site.register(GameSession, GameSessionAdmin)
admin.site.register(SessionVisit, SessionVisitAdmin)
