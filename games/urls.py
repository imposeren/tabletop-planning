# -*- coding: utf-8 -*-
from django.conf.urls import url

from games import views

urlpatterns = [
    url(r'^$', views.game_sessions_index, name='game_sessions_index'),
    url(r'^future/(?P<tag>.*)/$', views.game_sessions_index, name='game_sessions_index_tagged'),

    url(r'^free/$', views.players_required, name='players_required'),
    url(r'^free/(?P<tag>.*)/$', views.players_required, name='players_required_tagged'),

    url(r'^new/$', views.game_edit, name='create_game'),
    url(r'^details/(?P<slug>[\w-]+)/$', views.game_details, name='game_details'),
    url(r'^join/(?P<slug>[\w-]+)/$', views.join_game, name='join_game'),
    url(r'^leave/(?P<slug>[\w-]+)/$', views.leave_game, name='leave_game'),
    url(r'^remove_player/(?P<slug>[\w-]+)/(?P<user_pk>\d+)/$', views.remove_player, name='remove_player'),
    url(r'^confirm_player/(?P<slug>[\w-]+)/(?P<user_pk>\d+)/$', views.confirm_player, name='confirm_player'),
    url(r'^edit/(?P<slug>[\w-]+)/$', views.game_edit, name='edit_game'),
    url(r'^report/(?P<game_slug>[\w-]+)/(?P<session_number>\d+)/$', views.game_report, name='report_game_session'),
]

app_name = 'games'
