# -*- coding: utf-8 -*-

from django.utils.translation import ugettext_lazy as _

from dynamic_preferences.types import IntegerPreference
from dynamic_preferences.preferences import Section
from dynamic_preferences.registries import global_preferences_registry


games = Section('games')


@global_preferences_registry.register
class NotificationDays(IntegerPreference):
    section = games
    name = 'notification_days'
    default = 1
    help_text = _(u'За сколько дней предупреждать о предстоящих сессиях')


@global_preferences_registry.register
class GameDefaultDuration(IntegerPreference):
    section = games
    name = 'game_default_duration'
    default = 4
    help_text = _(u'Предполагаемая длительность игровых сессий в часах. Учитывается при расчете занятости помещений')


@global_preferences_registry.register
class GamesCapacity(IntegerPreference):
    section = games
    name = 'games_capacity'
    default = 3
    help_text = _(u'Максимум параллельных игр')
