# -*- coding: utf-8 -*-

# Django:
from django.core.exceptions import ObjectDoesNotExist
from django.db.models.signals import post_save
from django.dispatch import receiver

# thirdparties
from happenings.models import Event

# project:
# from disqus_sync.models import DisqusPost
# from games.models import Game, GameSession


__all__ = (
    'session_change',
)


@receiver(post_save, sender=Event)
def session_change(sender, instance, created, raw, **kwargs):
    try:
        if not raw and instance.game_session:
            instance.game_session.invalidate_render_game_or_session()
            instance.game_session.game.update_last_session_time_range()
    except ObjectDoesNotExist:
        pass


# @receiver(post_save, sender=DisqusPost)
# def disqus_post_created(sender, instance, created, raw, **kwargs):
#     if created and not raw:
#         for identifier in instance.thread_identifiers:
#             try:
#                 if identifier.startswith(GameSession.disqus_id_prefix()):
#                     object_id = int(identifier.replace(GameSession.disqus_id_prefix(), ''))
#                     game_session = GameSession.objects.get(id=object_id)
#                     game_session.disqus_posts.add(instance)
#                     break
#                 elif identifier.startswith(Game.disqus_id_prefix()):
#                     object_id = int(identifier.replace(Game.disqus_id_prefix(), ''))
#                     game = Game.objects.get(id=object_id)
#                     game.disqus_posts.add(instance)
#                     break
#             except (ObjectDoesNotExist, ) as e:
#                 continue
#             except ValueError as e:
#                 if e.message.find('int()') >= 0:
#                     continue
#                 else:
#                     raise
