/* jshint jquery: true */
/* jshint browser: true */
/* global ParsleyConfig,Raven */

Raven.context(function () {$(document).ready(function(){
  'use strict';

  var zeroCostSelector = 'input[name$=is_free], input[name$=used_ticket], input[name$=used_experience]';

  var zeroGameCost = function zeroGameCost(element, event, onChecked, zeroCostSelector){
    var $elem = $(element);
    var $parentTr = $elem.closest('tr');
    var elemIsChecked = $elem.is(':checked');
    if (elemIsChecked === onChecked) {
      // uncheck other checkboxes in row that grant free game, and set paid_amount to zero
      $parentTr.find('input[name$=paid_amount]').val(0);

      $parentTr.find(zeroCostSelector).not('[name=' + $elem[0].name + ']').prop('checked', false);
    } else {
      // set paid_amount to game_cost
      $parentTr.find('input[name$=paid_amount]').val($elem.closest('tr').find('input[name$=game_cost]').val());
    }
  };

  $(zeroCostSelector).not('[readonly]').change(function(event){
    zeroGameCost(this, event, true, zeroCostSelector);
  });

  $('input[name$=have_visited]').not('[readonly]').change(function(event){
    zeroGameCost(this, event, false, zeroCostSelector);
  });

  $.each(['cleaned', 'cleaned_floor', 'bonus'], function(index, fieldName){
    $('input[name$=' + fieldName +']').change(function(event){
      var $this = $(this);
      if ($this.is(':checked')) {
        $this.closest('table').find('input[name$='+ fieldName +']').not($this).prop('checked', false);
      }
    });
  });
});});
