/* jshint jquery: true */
/* jshint browser: true */
/* global ParsleyConfig,moment,Raven */

Raven.context(function () {$(document).ready(function(){
  'use strict';

  $('.js-new-session input').on('keyup change', function(){
    $(this).parents('.js-new-session').find('[name$=\'create\']').prop('checked', true);
  });

  // START: configure datetimepicker.
  $('.js-new-session .datetimeinput').datetimepicker({
    locale: 'ru', sideBySide: true, minDate: moment().add(1, 'days').startOf('day'),
    keepOpen: true
  });
  $('.js-new-session .datetimeinput').data("DateTimePicker").date(moment().add(1, 'days').startOf('hour'));

  $('.js-old-session .datetimeinput').datetimepicker({
    locale: 'ru', sideBySide: true, keepOpen: true
  });

  $('.datetimeinput').on('dp.change', function(){
    var $this = $(this);
    $this.change();
    $this.children('input').change();
  });
  // END: configure datetimepicker.

  if ($('.js-old-session').length === 0 && $('.js-new-session input[name$=number]').val() == '0') {
    $('.js-new-session input[name$=number]').val('1');
  }
  $('.js-main-form input[name=name]').change(function(){
    $('.js-new-session input[name$=title]').val($(this).val() +': №' + $('.js-new-session input[name$=number]').val());
  });

  $('input[type$=\'number\']').on('keyup change', function(){
    var $this = $(this);
    var cur_val = $this.val().toString();
    var clean_val = cur_val.replace(/\..*/g, '');
    if (cur_val !== clean_val) {
      $this.val(clean_val);
    }
  });

  var formset = $('.js-game-edit-formset');
  formset.submit(function(){
    var submitButton = formset.find('input[type=submit]');
    if (submitButton.prop('disabled')) {
      return false;
    }
    submitButton.prop("disabled", true);
  });
});});
