# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('games', '0008_auto_20141012_2226'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sessionvisit',
            name='ticket',
            field=models.OneToOneField(related_name='visit_used', null=True, on_delete=django.db.models.deletion.SET_NULL, blank=True, to='accounting.Ticket'),
        ),
    ]
