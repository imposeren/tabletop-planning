# -*- coding: utf-8 -*-
# flake8: noqa
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('games', '0015_game_headline'),
    ]

    operations = [
        migrations.AddField(
            model_name='gameplayer',
            name='master_notified',
            field=models.DateTimeField(default=django.utils.timezone.now, null=True, verbose_name='\u0412\u0440\u0435\u043c\u044f \u0443\u0432\u0435\u0434\u043e\u043c\u043b\u0435\u043d\u0438\u044f \u0432\u0435\u0434\u0443\u0449\u0435\u0433\u043e', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='gameplayer',
            name='master_notified',
            field=models.DateTimeField(default=None, null=True, verbose_name='\u0412\u0440\u0435\u043c\u044f \u0443\u0432\u0435\u0434\u043e\u043c\u043b\u0435\u043d\u0438\u044f \u0432\u0435\u0434\u0443\u0449\u0435\u0433\u043e', blank=True),
            preserve_default=True,
        ),
    ]
