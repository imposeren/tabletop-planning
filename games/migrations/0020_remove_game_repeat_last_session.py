# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations  # noqa


class Migration(migrations.Migration):

    dependencies = [
        ('games', '0019_auto_20150806_2341'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='game',
            name='repeat_last_session',
        ),
    ]
