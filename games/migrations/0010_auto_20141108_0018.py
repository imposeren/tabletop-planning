# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('games', '0009_auto_20141013_2349'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='gamesession',
            options={'ordering': ('-event__start_date', '-pk'), 'verbose_name': '\u0438\u0433\u0440\u043e\u0432\u0430\u044f \u0441\u0435\u0441\u0441\u0438\u044f', 'verbose_name_plural': '\u0438\u0433\u0440\u043e\u0432\u044b\u0435 \u0441\u0435\u0441\u0441\u0438\u0438'},
        ),
    ]
