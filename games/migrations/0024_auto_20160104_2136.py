# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('games', '0023_auto_20151226_1617'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sessionvisit',
            name='comment',
            field=models.CharField(default='', help_text='\u0415\u0441\u043b\u0438 \u0438\u0433\u0440\u043e\u043a \u043e\u0442\u0434\u0430\u043b \u043d\u0435 \u0432\u0441\u044e \u0441\u0443\u043c\u043c\u0443, \u0442\u043e \u0443\u043a\u0430\u0436\u0438\u0442\u0435 \u043f\u0440\u0438\u0447\u0438\u043d\u0443', max_length=64, verbose_name='\u041a\u043e\u043c\u043c\u0435\u043d\u0442\u0430\u0440\u0438\u0439', blank=True),
        ),
    ]
