# -*- coding: utf-8 -*-
# flake8: noqa
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.postgres.fields.ranges


class Migration(migrations.Migration):

    dependencies = [
        ('games', '0016_gameplayer_master_notified'),
    ]

    operations = [
        migrations.AddField(
            model_name='game',
            name='last_session_time_range',
            field=django.contrib.postgres.fields.ranges.IntegerRangeField(default=None, null=True, blank=True),
        ),
    ]
