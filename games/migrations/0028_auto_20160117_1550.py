# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('games', '0027_auto_20160117_1437'),
    ]

    operations = [
        migrations.AddField(
            model_name='gamesession',
            name='cached_end_date',
            field=models.DateTimeField(null=True, editable=False),
        ),
        migrations.AddField(
            model_name='gamesession',
            name='cached_start_date',
            field=models.DateTimeField(null=True, editable=False),
        ),
        migrations.AddField(
            model_name='sessionvisit',
            name='cached_end_date',
            field=models.DateTimeField(null=True, editable=False),
        ),
        migrations.AddField(
            model_name='sessionvisit',
            name='cached_start_date',
            field=models.DateTimeField(null=True, editable=False),
        ),
    ]
