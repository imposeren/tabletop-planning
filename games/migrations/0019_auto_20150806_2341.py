# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('games', '0018_game_is_finished'),
    ]

    operations = [
        migrations.AlterField(
            model_name='game',
            name='is_active',
            field=models.BooleanField(default=True, help_text='\u041d\u0435\u0430\u043a\u0442\u0438\u0432\u043d\u044b\u0435 \u0438\u0433\u0440\u044b \u043d\u0435 \u043e\u0442\u043e\u0431\u0440\u0430\u0436\u0430\u044e\u0442\u0441\u044f \u0432 \u043e\u0431\u0449\u0435\u043c \u0441\u043f\u0438\u0441\u043a\u0435 \u0438 \u0432 \u0432\u0430\u0448\u0435\u043c \u043f\u0440\u043e\u0444\u0438\u043b\u0435', db_index=True, verbose_name='\u0418\u0433\u0440\u0430 \u0430\u043a\u0442\u0438\u0432\u043d\u0430'),
        ),
        migrations.AlterField(
            model_name='game',
            name='is_finished',
            field=models.BooleanField(default=False, help_text='\u0417\u0430\u0432\u0435\u0440\u0448\u0435\u043d\u043d\u044b\u0435 \u0438\u0433\u0440\u044b \u043e\u0441\u0442\u0430\u044e\u0442\u0441\u044f \u0432 \u0441\u043f\u0438\u0441\u043a\u0430\u0445 \u0438 \u043f\u0440\u043e\u0444\u0438\u043b\u0435, \u043d\u043e \u043d\u0435 \u0432\u0438\u0434\u043d\u044b \u0432 \u0432 \u0441\u043f\u0438\u0441\u043a\u0435 "\u0412\u0435\u0434\u0451\u0442\u0435"', db_index=True, verbose_name='\u0418\u0433\u0440\u0430 \u0437\u0430\u0432\u0435\u0440\u0448\u0435\u043d\u0430'),
        ),
        migrations.AlterField(
            model_name='sessionvisit',
            name='game_session',
            field=models.ForeignKey(related_name='visits', on_delete=django.db.models.deletion.SET_NULL, to='games.GameSession', null=True),
        ),
    ]
