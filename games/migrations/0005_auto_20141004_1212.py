# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('games', '0004_sessionvisit_debt_payment'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sessionvisit',
            name='visit_xp',
            field=models.ForeignKey(related_name='visit_default', blank=True, to='accounting.ExperienceReward', help_text='\u041e\u043f\u044b\u0442 \u0437\u0430 \u043f\u043e\u0441\u0435\u0449\u0435\u043d\u0438\u0435. \u041e\u043f\u044b\u0442 \u043c\u0430\u0441\u0442\u0435\u0440\u0430 \u043f\u0435\u0440\u0435\u0441\u0447\u0438\u0442\u044b\u0432\u0430\u0435\u0442\u0441\u044f \u0440\u0430\u0437 \u0432 \u0441\u0443\u0442\u043a\u0438 \u0438 \u0437\u0430\u0432\u0438\u0441\u0438\u0442 \u043e\u0442 \u043a\u043e\u043b\u0438\u0447\u0435\u0441\u0442\u0432\u0430 \u043f\u0440\u043e\u043c\u043e\u0434\u0435\u0440\u0438\u0440\u043e\u0432\u0430\u043d\u043d\u044b\u0445 \u0438\u0433\u0440 \u043d\u0430 \u044d\u0442\u043e\u0439 \u043d\u0435\u0434\u0435\u043b\u0435', null=True, on_delete=models.CASCADE),
        ),
        migrations.AlterField(
            model_name='sessionvisit',
            name='visitor',
            field=models.ForeignKey(related_name='session_visits', to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE),
        ),
    ]
