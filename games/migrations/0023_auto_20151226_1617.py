# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounting', '0013_auto_20151226_1617'),
        ('games', '0022_sessionvisit_balance_change'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='sessionvisit',
            name='balance_change',
        ),
        migrations.AddField(
            model_name='sessionvisit',
            name='cost_balance_change',
            field=models.ForeignKey(related_name='cost_game_sessions', default=None, blank=True, to='accounting.BalanceChange', null=True, on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name='sessionvisit',
            name='paid_balance_change',
            field=models.ForeignKey(related_name='paid_game_sessions', default=None, blank=True, to='accounting.BalanceChange', null=True, on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name='sessionvisit',
            name='to_master_balance_change',
            field=models.ForeignKey(related_name='to_master_game_sessions', default=None, blank=True, to='accounting.BalanceChange', null=True, on_delete=models.CASCADE),
        ),
    ]
