# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('games', '0010_auto_20141108_0018'),
    ]

    operations = [
        migrations.AddField(
            model_name='sessionvisit',
            name='cleaned_floor',
            field=models.BooleanField(default=False, help_text='\u0423\u0431\u0438\u0440\u0430\u043b \u043f\u043e\u0441\u043b\u0435 \u0438\u0433\u0440\u044b. \u041e\u0434\u0438\u043d \u0438\u0433\u0440\u043e\u043a', verbose_name='\u0423\u0431\u0438\u0440\u0430\u043b \u043f\u043e\u043b'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='sessionvisit',
            name='cleaned',
            field=models.BooleanField(default=False, help_text='\u0423\u0431\u0438\u0440\u0430\u043b \u043f\u043e\u0441\u043b\u0435 \u0438\u0433\u0440\u044b. \u041e\u0434\u0438\u043d \u0438\u0433\u0440\u043e\u043a', verbose_name='\u0423\u0431\u0438\u0440\u0430\u043b \u0441\u0442\u043e\u043b'),
        ),
    ]
