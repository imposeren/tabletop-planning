# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models  # noqa


class Migration(migrations.Migration):

    dependencies = [
        ('games', '0025_auto_20160114_1646'),
    ]

    operations = [
        migrations.RunSQL('CREATE INDEX "happenings_event_start_date_idx" ON "happenings_event" (start_date);'),
        migrations.RunSQL('CREATE INDEX "happenings_event_end_date_idx" ON "happenings_event" (end_date);'),
        migrations.RunSQL('CREATE INDEX "happenings_event_dates_idx" ON "happenings_event" (start_date, end_date);'),
    ]
