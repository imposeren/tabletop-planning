# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('games', '0001_squashed_0023_auto_20140929_2349'),
    ]

    operations = [
        migrations.AddField(
            model_name='sessionvisit',
            name='have_visited',
            field=models.BooleanField(default=True, verbose_name='\u0411\u044b\u043b \u043d\u0430 \u0438\u0433\u0440\u0435'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='gamesession',
            name='number',
            field=models.PositiveIntegerField(default=0, help_text='\u0411\u0443\u0434\u0435\u0442 \u0440\u0430\u0441\u0441\u0447\u0438\u0442\u0430\u043d \u0430\u0432\u0442\u043e\u043c\u0430\u0442\u0438\u0447\u0435\u0441\u043a\u0438 \u0435\u0441\u043b\u0438 \u043e\u0441\u0442\u0430\u0432\u0438\u0442\u044c \u0437\u043d\u0430\u0447\u0435\u043d\u0438\u0435 0 (\u043c\u0430\u043a\u0441\u0438\u043c\u0430\u043b\u044c\u043d\u044b\u0439 \u043d\u043e\u043c\u0435\u0440 \u0441\u0435\u0441\u0441\u0438\u0439 \u044d\u0442\u043e\u0439 \u0438\u0433\u0440\u044b \u043f\u043b\u044e\u0441 1)', db_index=True, verbose_name='\u043d\u043e\u043c\u0435\u0440 \u0441\u0435\u0441\u0441\u0438\u0438', blank=True),
        ),
        migrations.AlterField(
            model_name='sessionvisit',
            name='used_experience',
            field=models.BooleanField(default=False, verbose_name='\u0418\u0433\u0440\u0430 \u0437\u0430 \u043e\u043f\u044b\u0442'),
        ),
    ]
