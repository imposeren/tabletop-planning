# -*- coding: utf-8 -*-
# flake8: noqa
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('games', '0017_game_last_session_time_range'),
    ]

    operations = [
        migrations.AddField(
            model_name='game',
            name='is_finished',
            field=models.BooleanField(default=False, help_text='\u0410\u043a\u0442\u0438\u0432\u043d\u044b\u0435 \u0438\u0433\u0440\u044b \u043c\u043e\u0436\u043d\u043e \u043f\u0440\u043e\u0441\u043c\u0430\u0442\u0440\u0438\u0432\u0430\u0442\u044c, \u043d\u043e \u043a \u043d\u0438\u043c \u043d\u0435\u043b\u044c\u0437\u044f \u043f\u0440\u0438\u0441\u043e\u0435\u0434\u0438\u043d\u0438\u0442\u044c\u0441\u044f', db_index=True, verbose_name='\u0418\u0433\u0440\u0430 \u043e\u043a\u043e\u043d\u0447\u0435\u043d\u0430'),
        ),
    ]
