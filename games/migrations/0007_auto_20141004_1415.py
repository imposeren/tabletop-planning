# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('games', '0006_auto_20141004_1353'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sessionvisit',
            name='comment',
            field=models.CharField(default='', max_length=64, verbose_name='\u041a\u043e\u043c\u043c\u0435\u043d\u0442\u0430\u0440\u0438\u0439', blank=True),
        ),
    ]
