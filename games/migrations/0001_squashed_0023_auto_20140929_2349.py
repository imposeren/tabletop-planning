# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import sorl.thumbnail.fields
import games.models
from django.conf import settings
import taggit.managers


UNSUB_HTML_CONTENT = '<br/><br/>Если вы больше не хотите получать такие уведомления, то можете <a href="{% get_unsubscribe_link recipient %}">отписаться от уведомлений</a>'

UNSUB_TEXT_CONTENT = '\n\nЕсли вы больше не хотите получать такие уведомления, то откройте в браузере следующую ссылку:\n{% get_unsubscribe_link recipient %}'


def create_upcoming_session_reminder_template(apps, schema_editor):
    EmailTemplate = apps.get_model("post_office", "EmailTemplate")
    name = 'upcoming-session-reminder'
    if not EmailTemplate.objects.filter(name=name).exists():
        EmailTemplate.objects.create(
            name=name,
            subject='Напоминание о предстоящей игре {{ game_session.game }} {{ game_session.start_datetime }}',
            html_content='{% load common_tags %}\n<b>{{ game_session.event.start_date }}</b> вас ждёт {{ game_session.number }}-я сессия в игре <b><a href="{{ game_session.game.get_absolute_url|with_domain }}">{{ game_session.game }}</a></b>.' + UNSUB_HTML_CONTENT,
            content='{% load common_tags %}\n{{ game_session.event.start_date }} вас ждёт {{ game_session.number }}-я сессия в игре {{ game_session.game }} ({{ game_session.game.get_absolute_url|with_domain }}).' + UNSUB_TEXT_CONTENT,
        )


def create_request_for_info_mail_template(apps, schema_editor):
    EmailTemplate = apps.get_model("post_office", "EmailTemplate")
    name = 'request-for-report'
    if not EmailTemplate.objects.filter(name=name).exists():
        EmailTemplate.objects.create(
            name=name,
            subject='Заполнить отчет о игре {{ game_session.game.name }} {{ game_session.start_datetime.date }}',
            html_content='{% load common_tags %}\nПожалуйста, заполните <a href="{{ game_session.get_report_url|with_domain }}">отчет</a> о проведенной или начавшейся игре.' + UNSUB_HTML_CONTENT,
            content='{% load common_tags %}\nПожалуйста, заполните отчет ({{ game_session.get_report_url|with_domain }}) о проведенной или начавшейся игре.' + UNSUB_TEXT_CONTENT,
        )

    name = 'request-for-report-moderation'
    if not EmailTemplate.objects.filter(name=name).exists():
        EmailTemplate.objects.create(
            name=name,
            subject='Заполнен или обновлен отчет о игре {{ game_session.game.name }} {{ game_session.start_datetime.date }} (ожидает модерации)',
            html_content='{% load common_tags %}\nПожалуйста, проверьте <a href="{{ game_session.get_report_moderation_url|with_domain }}">отчет</a> о проведенной игре.' + UNSUB_HTML_CONTENT,
            content='{% load common_tags %}\nПожалуйста, проверьте отчет ({{ game_session.get_report_moderation_url|with_domain }}) о проведенной игре.' + UNSUB_TEXT_CONTENT,
        )


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('happenings', '0001_initial'),
        ('accounting', '0001_squashed_0019_auto_20140929_2345'),
        ('tags', '0002_auto_20140818_1902'),
    ]

    operations = [
        migrations.CreateModel(
            name='Game',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('slug', models.SlugField(max_length=255, blank=True, help_text='\u0411\u0443\u0434\u0435\u0442 \u0437\u0430\u043f\u043e\u043b\u043d\u0435\u043d\u043e \u0430\u0432\u0442\u043e\u043c\u0430\u0442\u0438\u0447\u0435\u0441\u043a\u0438', unique=True, verbose_name='\u0422\u0435\u043a\u0441\u0442 \u0432 \u0441\u0441\u044b\u043b\u043a\u0430\u0445')),
                ('description', models.TextField(default='', verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True)),
                ('masters', models.ManyToManyField(to=settings.AUTH_USER_MODEL, verbose_name='\u0432\u0435\u0434\u0443\u0449\u0438\u0435')),
            ],
            options={
                'verbose_name': '\u0438\u0433\u0440\u0430',
                'verbose_name_plural': '\u0438\u0433\u0440\u044b',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='GameSession',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('start_datetime', models.DateTimeField(default=games.models.default_session_datetime, verbose_name='\u0432\u0440\u0435\u043c\u044f \u043f\u0440\u043e\u0432\u0435\u0434\u0435\u043d\u0438\u044f')),
                ('game', models.ForeignKey(to='games.Game', on_delete=models.CASCADE)),
                ('description', models.CharField(default='', max_length=255, verbose_name='\u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True)),
            ],
            options={
                'verbose_name': '\u0438\u0433\u0440\u043e\u0432\u0430\u044f \u0441\u0435\u0441\u0441\u0438\u044f',
                'verbose_name_plural': '\u0438\u0433\u0440\u043e\u0432\u044b\u0435 \u0441\u0435\u0441\u0441\u0438\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='game',
            name='max_players',
            field=models.PositiveIntegerField(default=3, help_text='\u041d\u043e\u0432\u044b\u0435 \u0438\u0433\u0440\u043e\u043a\u0438 \u043d\u0435 \u0441\u043c\u043e\u0433\u0443\u0442 \u043f\u0440\u0438\u0441\u043e\u0435\u0434\u0438\u043d\u0438\u0442\u044c\u0441\u044f \u043a \u0438\u0433\u0440\u0435, \u0435\u0441\u043b\u0438 \u0432 \u043d\u0435\u0439 \u0443\u0436\u0435 \u0443\u0447\u0430\u0441\u0442\u0432\u0443\u0435\u0442 \u043c\u0430\u043a\u0441\u0438\u043c\u0430\u043b\u044c\u043d\u043e\u0435 \u043a\u043e\u043b\u0438\u0447\u0435\u0441\u0442\u0432\u043e \u0438\u0433\u0440\u043e\u043a\u043e\u0432', verbose_name='\u043c\u0430\u043a\u0441\u0438\u043c\u0443\u043c \u0438\u0433\u0440\u043e\u043a\u043e\u0432'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='game',
            name='is_active',
            field=models.BooleanField(default=True, help_text='\u0415\u0441\u043b\u0438 \u0438\u0433\u0440\u0430 \u0430\u043a\u0442\u0438\u0432\u043d\u0430, \u0442\u043e \u043e\u043d\u0430 \u043e\u0442\u043e\u0431\u0440\u0430\u0436\u0430\u0435\u0442\u0441\u044f \u0432 \u043e\u0431\u0449\u0435\u043c \u0441\u043f\u0438\u0441\u043a\u0435 \u0438\u0433\u0440, \u0430 \u0442\u0430\u043a\u0436\u0435 \u0443\u0447\u0430\u0441\u0442\u043d\u0438\u043a\u0438 \u0438\u0433\u0440\u044b \u0431\u0443\u0434\u0443\u0442 \u043f\u043e\u043b\u0443\u0447\u0430\u0442\u044c \u0443\u0432\u0435\u0434\u043e\u043c\u043b\u0435\u043d\u0438\u044f \u043e \u043f\u0440\u0435\u0434\u0441\u0442\u043e\u044f\u0449\u0438\u0445 \u0441\u0435\u0441\u0441\u0438\u044f\u0445', db_index=True, verbose_name='\u0418\u0433\u0440\u0430 \u0430\u043a\u0442\u0438\u0432\u043d\u0430'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='game',
            name='image',
            field=sorl.thumbnail.fields.ImageField(default='', height_field='image_height', width_field='image_width', upload_to=games.models.games_image_path, blank=True, verbose_name='\u043f\u043e\u0441\u0442\u0435\u0440'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='game',
            name='image_height',
            field=models.PositiveIntegerField(default=0, null=True, editable=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='game',
            name='image_width',
            field=models.PositiveIntegerField(default=0, null=True, editable=False),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='game',
            name='description',
            field=models.TextField(default='', verbose_name='\u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True),
        ),
        migrations.AlterField(
            model_name='game',
            name='name',
            field=models.CharField(unique=True, max_length=255, verbose_name='\u043d\u0430\u0437\u0432\u0430\u043d\u0438\u0435'),
        ),
        migrations.AlterField(
            model_name='game',
            name='slug',
            field=models.SlugField(max_length=255, blank=True, help_text='\u0411\u0443\u0434\u0435\u0442 \u0437\u0430\u043f\u043e\u043b\u043d\u0435\u043d\u043e \u0430\u0432\u0442\u043e\u043c\u0430\u0442\u0438\u0447\u0435\u0441\u043a\u0438', unique=True, verbose_name='\u0442\u0435\u043a\u0441\u0442 \u0432 \u0441\u0441\u044b\u043b\u043a\u0430\u0445'),
        ),
        migrations.AddField(
            model_name='game',
            name='repeat_last_session',
            field=models.BooleanField(default=False, help_text='\u0415\u0441\u043b\u0438 \u0434\u0430\u043d\u043d\u0430\u044f \u043e\u043f\u0446\u0438\u044f \u0432\u043a\u043b\u044e\u0447\u0435\u043d\u0430, \u0442\u043e \u043f\u0440\u0438\u0431\u043b\u0438\u0437\u0438\u0442\u0435\u043b\u044c\u043d\u043e \u0447\u0435\u0440\u0435\u0437 \u0441\u0443\u0442\u043a\u0438 \u043f\u043e\u0441\u043b\u0435 \u0434\u0430\u0442\u044b \u043d\u0430\u0447\u0430\u043b\u0430 \u043f\u043e\u0441\u043b\u0435\u0434\u043d\u0435\u0439 \u0441\u0435\u0441\u0441\u0438\u0438 \u0431\u0443\u0434\u0435\u0442 \u0441\u043e\u0437\u0434\u0430\u043d\u0430 \u0435\u0449\u0435 \u043e\u0434\u043d\u0430 \u0441\u0435\u0441\u0441\u0438\u044f \u043d\u0430 \u0432\u0440\u0435\u043c\u044f \u0447\u0435\u0440\u0435\u0437 7 \u0434\u043d\u0435\u0439 \u043f\u043e\u0441\u043b\u0435 \u043f\u0440\u0435\u0434\u044b\u0434\u0443\u0449\u0435\u0439. \u0415\u0441\u043b\u0438 \u0432\u043e \u0432\u0440\u0435\u043c\u044f \u0430\u0432\u0442\u043e\u043c\u0430\u0442\u0438\u0447\u0435\u0441\u043a\u043e\u0433\u043e \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f \u0431\u0443\u0434\u0443\u0442 \u0443\u0436\u0435 \u0441\u043e\u0437\u0434\u0430\u043d\u044b \u0434\u0440\u0443\u0433\u0438\u0435 \u0441\u0435\u0441\u0441\u0438\u0438 \u043d\u0430 \u0431\u0443\u0434\u0443\u0449\u0435\u0435, \u0442\u043e \u043f\u0440\u043e\u0446\u0435\u0441\u0441 \u0430\u0432\u0442\u043e\u043c\u0430\u0442\u0438\u0447\u0435\u0441\u043a\u043e\u0433\u043e \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f \u0431\u0443\u0434\u0435\u0442 \u043e\u0441\u0442\u0430\u043d\u043e\u0432\u043b\u0435\u043d.', db_index=True, verbose_name='\u041f\u043e\u0432\u0442\u043e\u0440\u044f\u0442\u044c \u043f\u043e\u0441\u043b\u0435\u0434\u043d\u044e\u044e \u0441\u0435\u0441\u0441\u0438\u044e \u0447\u0435\u0440\u0435\u0437 \u043d\u0435\u0434\u0435\u043b\u044e'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='game',
            name='other_tags',
            field=taggit.managers.TaggableManager(to='tags.OtherTag', through='tags.OtherTaggedItem', blank=True, help_text='A comma-separated list of tags.', verbose_name='\u0422\u0435\u0433\u0438'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='game',
            name='setting_tags',
            field=taggit.managers.TaggableManager(to='tags.SettingTag', through='tags.SettingTaggedItem', help_text='A comma-separated list of tags.', verbose_name='\u0421\u0435\u0442\u0442\u0438\u043d\u0433'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='game',
            name='system_tags',
            field=taggit.managers.TaggableManager(to='tags.SystemTag', through='tags.SystemTaggedItem', help_text='A comma-separated list of tags.', verbose_name='\u0421\u0438\u0441\u0442\u0435\u043c\u0430'),
            preserve_default=True,
        ),
        migrations.CreateModel(
            name='SessionVisit',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('game_session', models.ForeignKey(to='games.GameSession', on_delete=models.CASCADE)),
                ('visitor', models.ForeignKey(to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='sessionvisit',
            unique_together=set([('game_session', 'visitor')]),
        ),
        migrations.AddField(
            model_name='gamesession',
            name='number',
            field=models.PositiveIntegerField(default=0, editable=False, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='sessionvisit',
            name='verified',
            field=models.BooleanField(default=False, verbose_name='\u041c\u043e\u0434\u0435\u0440\u0430\u0446\u0438\u044f \u043f\u0440\u043e\u0439\u0434\u0435\u043d\u0430'),
            preserve_default=True,
        ),
        migrations.RunPython(
            code=create_upcoming_session_reminder_template,
            reverse_code=None,
            atomic=True,
        ),
        migrations.AddField(
            model_name='gamesession',
            name='notified',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='gamesession',
            unique_together=set([('game', 'number')]),
        ),
        migrations.AlterField(
            model_name='game',
            name='masters',
            field=models.ManyToManyField(related_name='mastering_games', verbose_name='\u0432\u0435\u0434\u0443\u0449\u0438\u0435', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='gamesession',
            name='game',
            field=models.ForeignKey(related_name='sessions', to='games.Game', on_delete=models.CASCADE),
        ),
        migrations.AlterField(
            model_name='sessionvisit',
            name='game_session',
            field=models.ForeignKey(related_name='visits', to='games.GameSession', on_delete=models.CASCADE),
        ),
        migrations.RunPython(
            code=create_request_for_info_mail_template,
            reverse_code=None,
            atomic=True,
        ),
        migrations.AlterModelOptions(
            name='sessionvisit',
            options={'verbose_name': '\u043f\u043e\u0441\u0435\u0449\u0435\u043d\u0438\u0435 \u0438\u0433\u0440\u044b', 'verbose_name_plural': '\u043f\u043e\u0441\u0435\u0449\u0435\u043d\u0438\u044f \u0438\u0433\u0440'},
        ),
        migrations.RemoveField(
            model_name='gamesession',
            name='notified',
        ),
        migrations.AddField(
            model_name='gamesession',
            name='report_notified',
            field=models.BooleanField(default=False, verbose_name='\u0423\u0432\u0435\u0434\u043e\u043c\u043b\u0435\u043d\u0438\u0435 \u043e \u043d\u0435\u043e\u0431\u0445\u043e\u0434\u0438\u043c\u043e\u0441\u0442\u0438 \u043e\u0442\u0447\u0435\u0442\u0430 \u0432\u044b\u0441\u043b\u0430\u043d\u043e'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='gamesession',
            name='upcoming_notified',
            field=models.BooleanField(default=False, verbose_name='\u0423\u0432\u0435\u0434\u043e\u043c\u043b\u0435\u043d\u0438\u0435 \u043e \u043f\u0440\u0435\u0434\u0441\u0442\u043e\u044f\u0449\u0435\u0439 \u0438\u0433\u0440\u0435 \u0432\u044b\u0441\u043b\u0430\u043d\u043e'),
            preserve_default=True,
        ),
        migrations.RemoveField(
            model_name='gamesession',
            name='description',
        ),
        migrations.RemoveField(
            model_name='gamesession',
            name='start_datetime',
        ),
        migrations.AddField(
            model_name='gamesession',
            name='event',
            field=models.OneToOneField(related_name='game_session', null=True, default=None, editable=False, to='happenings.Event', blank=True, on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AlterModelOptions(
            name='gamesession',
            options={'ordering': ('-event__start_date',), 'verbose_name': '\u0438\u0433\u0440\u043e\u0432\u0430\u044f \u0441\u0435\u0441\u0441\u0438\u044f', 'verbose_name_plural': '\u0438\u0433\u0440\u043e\u0432\u044b\u0435 \u0441\u0435\u0441\u0441\u0438\u0438'},
        ),
        migrations.CreateModel(
            name='GamePlayer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('game', models.ForeignKey(related_name='game_players', to='games.Game', on_delete=models.CASCADE)),
                ('player', models.ForeignKey(related_name='player_games', to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='game',
            name='current_players',
            field=models.PositiveIntegerField(default=0, verbose_name='\u0442\u0435\u043a\u0443\u0449\u0435\u0435 \u043a\u043e\u043b\u0438\u0447\u0435\u0441\u0442\u0432\u043e \u0438\u0433\u0440\u043e\u043a\u043e\u0432', editable=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='game',
            name='players',
            field=models.ManyToManyField(related_name='playing_games', verbose_name='\u0438\u0433\u0440\u043e\u043a\u0438', to=settings.AUTH_USER_MODEL, through='games.GamePlayer', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='gamesession',
            name='image',
            field=sorl.thumbnail.fields.ImageField(default='', height_field='image_height', width_field='image_width', upload_to=games.models.games_image_path, blank=True, help_text='\u0435\u0441\u043b\u0438 \u043d\u0435 \u0437\u0430\u0434\u0430\u0442\u044c, \u0442\u043e \u0431\u0443\u0434\u0435\u0442 \u0438\u0441\u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u044c\u0441\u044f \u043f\u043e\u0441\u0442\u0435\u0440 \u0441\u0430\u043c\u043e\u0439 \u0438\u0433\u0440\u044b', verbose_name='\u043f\u043e\u0441\u0442\u0435\u0440'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='gamesession',
            name='image_height',
            field=models.PositiveIntegerField(default=0, null=True, editable=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='gamesession',
            name='image_width',
            field=models.PositiveIntegerField(default=0, null=True, editable=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='sessionvisit',
            name='bonus',
            field=models.BooleanField(default=False, help_text='\u0411\u043e\u043d\u0443\u0441\u043d\u044b\u0439 \u043e\u043f\u044b\u0442 \u043e\u0442 \u043c\u0430\u0441\u0442\u0435\u0440\u0430. \u041e\u0434\u043d\u043e\u043c\u0443 \u0438\u0433\u0440\u043e\u043a\u0443', verbose_name='\u0411\u043e\u043d\u0443\u0441'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='sessionvisit',
            name='bonus_xp',
            field=models.ForeignKey(related_name='visit_bonus', blank=True, to='accounting.ExperienceReward', null=True, on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='sessionvisit',
            name='cleaned',
            field=models.BooleanField(default=False, help_text='\u0423\u0431\u0438\u0440\u0430\u043b \u043f\u043e\u0441\u043b\u0435 \u0438\u0433\u0440\u044b. \u041e\u0434\u0438\u043d \u0438\u0433\u0440\u043e\u043a', verbose_name='\u0423\u0431\u0438\u0440\u0430\u043b'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='sessionvisit',
            name='cleaned_xp',
            field=models.ForeignKey(related_name='visit_cleaned', blank=True, to='accounting.ExperienceReward', null=True, on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='sessionvisit',
            name='incoming_payment',
            field=models.ForeignKey(related_name='income_visit', blank=True, to='accounting.Payment', null=True, on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='sessionvisit',
            name='is_free',
            field=models.BooleanField(default=False, verbose_name='\u0411\u0435\u0441\u043f\u043b\u0430\u0442\u043d\u0430\u044f \u0438\u0433\u0440\u0430'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='sessionvisit',
            name='outgoing_payment',
            field=models.ForeignKey(related_name='outgo_visit', blank=True, to='accounting.Payment', null=True, on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='sessionvisit',
            name='payment_xp',
            field=models.ForeignKey(related_name='visit_xp_payed', blank=True, to='accounting.ExperienceReward', null=True, on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='sessionvisit',
            name='role',
            field=models.PositiveSmallIntegerField(default=1, verbose_name='\u0440\u043e\u043b\u044c', choices=[(1, '\u0432\u0435\u0434\u0443\u0449\u0438\u0439'), (1, '\u0438\u0433\u0440\u043e\u043a')]),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='sessionvisit',
            name='ticket',
            field=models.ForeignKey(related_name='visit_used', blank=True, to='accounting.Ticket', null=True, on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='sessionvisit',
            name='used_experience',
            field=models.BooleanField(default=False, verbose_name='\u0417\u0430 \u043e\u043f\u044b\u0442'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='sessionvisit',
            name='used_ticket',
            field=models.BooleanField(default=False, verbose_name='\u0410\u0431\u043e\u043d\u0435\u043c\u0435\u043d\u0442'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='sessionvisit',
            name='visit_xp',
            field=models.ForeignKey(related_name='visit_default', blank=True, to='accounting.ExperienceReward', null=True, on_delete=models.CASCADE),
            preserve_default=True,
        ),
    ]
