# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        # ('disqus_sync', '0001_initial'),
        ('games', '0013_auto_20141123_1519'),
    ]

    operations = [
        # migrations.AddField(
        #     model_name='game',
        #     name='disqus_posts',
        #     field=models.ManyToManyField(to='disqus_sync.DisqusPost'),
        #     preserve_default=True,
        # ),
        # migrations.AddField(
        #     model_name='gamesession',
        #     name='disqus_posts',
        #     field=models.ManyToManyField(to='disqus_sync.DisqusPost'),
        #     preserve_default=True,
        # ),
    ]
