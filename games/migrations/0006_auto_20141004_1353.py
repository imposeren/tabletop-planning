# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('games', '0005_auto_20141004_1212'),
    ]

    operations = [
        migrations.AddField(
            model_name='sessionvisit',
            name='comment',
            field=models.CharField(default='', max_length=64, verbose_name='\u041a\u043e\u043c\u0435\u043d\u0442\u0430\u0440\u0438\u0439', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='sessionvisit',
            name='bonus_xp',
            field=models.ForeignKey(related_name='visit_bonus', on_delete=django.db.models.deletion.SET_NULL, blank=True, to='accounting.ExperienceReward', null=True),
        ),
        migrations.AlterField(
            model_name='sessionvisit',
            name='cleaned_xp',
            field=models.ForeignKey(related_name='visit_cleaned', on_delete=django.db.models.deletion.SET_NULL, blank=True, to='accounting.ExperienceReward', null=True),
        ),
        migrations.AlterField(
            model_name='sessionvisit',
            name='debt_payment',
            field=models.ForeignKey(related_name='debt_visit', on_delete=django.db.models.deletion.SET_NULL, blank=True, to='accounting.Payment', null=True),
        ),
        migrations.AlterField(
            model_name='sessionvisit',
            name='incoming_payment',
            field=models.ForeignKey(related_name='income_visit', on_delete=django.db.models.deletion.SET_NULL, blank=True, to='accounting.Payment', null=True),
        ),
        migrations.AlterField(
            model_name='sessionvisit',
            name='is_free',
            field=models.BooleanField(default=False, help_text='\u041f\u0435\u0440\u0432\u044b\u0435 \u0438\u0433\u0440\u044b \u043c\u043e\u0433\u0443\u0442 \u0431\u044b\u0442\u044c \u0431\u0435\u0441\u043f\u043b\u0430\u0442\u043d\u044b. \u0414\u0440\u0443\u0433\u0438\u0435 \u0442\u043e\u0436\u0435 \u043c\u043e\u0433\u0443\u0442 \u0431\u044b\u0442\u044c, \u043d\u043e \u043c\u043e\u0434\u0435\u0440\u0430\u0442\u043e\u0440 \u043f\u0440\u043e\u0432\u0435\u0440\u0438\u0442 \u044d\u0442\u043e', verbose_name='\u0411\u0435\u0441\u043f\u043b\u0430\u0442\u043d\u0430\u044f \u0438\u0433\u0440\u0430'),
        ),
        migrations.AlterField(
            model_name='sessionvisit',
            name='outgoing_payment',
            field=models.ForeignKey(related_name='outgo_visit', on_delete=django.db.models.deletion.SET_NULL, blank=True, to='accounting.Payment', null=True),
        ),
        migrations.AlterField(
            model_name='sessionvisit',
            name='payment_xp',
            field=models.ForeignKey(related_name='visit_xp_payed', on_delete=django.db.models.deletion.SET_NULL, blank=True, to='accounting.ExperienceReward', null=True),
        ),
        migrations.AlterField(
            model_name='sessionvisit',
            name='ticket',
            field=models.ForeignKey(related_name='visit_used', on_delete=django.db.models.deletion.SET_NULL, blank=True, to='accounting.Ticket', null=True),
        ),
        migrations.AlterField(
            model_name='sessionvisit',
            name='visit_xp',
            field=models.ForeignKey(related_name='visit_default', on_delete=django.db.models.deletion.SET_NULL, blank=True, to='accounting.ExperienceReward', help_text='\u041e\u043f\u044b\u0442 \u0437\u0430 \u043f\u043e\u0441\u0435\u0449\u0435\u043d\u0438\u0435. \u041e\u043f\u044b\u0442 \u043c\u0430\u0441\u0442\u0435\u0440\u0430 \u043f\u0435\u0440\u0435\u0441\u0447\u0438\u0442\u044b\u0432\u0430\u0435\u0442\u0441\u044f \u0440\u0430\u0437 \u0432 \u0441\u0443\u0442\u043a\u0438 \u0438 \u0437\u0430\u0432\u0438\u0441\u0438\u0442 \u043e\u0442 \u043a\u043e\u043b\u0438\u0447\u0435\u0441\u0442\u0432\u0430 \u043f\u0440\u043e\u043c\u043e\u0434\u0435\u0440\u0438\u0440\u043e\u0432\u0430\u043d\u043d\u044b\u0445 \u0438\u0433\u0440 \u043d\u0430 \u044d\u0442\u043e\u0439 \u043d\u0435\u0434\u0435\u043b\u0435', null=True),
        ),
    ]
