# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models  # noqa


def cache_dates(apps, schema_editor):
    GameSession = apps.get_model("games", "GameSession")
    SessionVisit = apps.get_model("games", "SessionVisit")

    for game_session in GameSession.objects.all():
        if not game_session.event:
            continue
        GameSession.objects.filter(pk=game_session.pk).update(
            cached_start_date=game_session.event.start_date,
            cached_end_date=game_session.event.end_date,
        )

    for session_visit in SessionVisit.objects.all():
        if not (session_visit.game_session and session_visit.game_session.event):
            continue
        SessionVisit.objects.filter(pk=session_visit.pk).update(
            cached_start_date=session_visit.game_session.event.start_date,
            cached_end_date=session_visit.game_session.event.end_date,
        )


class Migration(migrations.Migration):

    dependencies = [
        ('games', '0028_auto_20160117_1550'),
    ]

    operations = [
        migrations.RunPython(
            code=cache_dates,
            atomic=True,
        ),
    ]
