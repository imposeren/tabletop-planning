# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('games', '0020_remove_game_repeat_last_session'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='sessionvisit',
            name='debt_payment',
        ),
        migrations.RemoveField(
            model_name='sessionvisit',
            name='incoming_payment',
        ),
        migrations.RemoveField(
            model_name='sessionvisit',
            name='outgoing_payment',
        ),
        migrations.AddField(
            model_name='sessionvisit',
            name='visitor_level',
            field=models.PositiveSmallIntegerField(default=0, verbose_name='\u0423\u0440\u043e\u0432\u0435\u043d\u044c \u0434\u043e \u043e\u0442\u0447\u0435\u0442\u0430'),
        ),
    ]
