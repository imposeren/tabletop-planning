# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import taggit.managers


class Migration(migrations.Migration):

    dependencies = [
        ('games', '0007_auto_20141004_1415'),
    ]

    operations = [
        migrations.AlterField(
            model_name='game',
            name='other_tags',
            field=taggit.managers.TaggableManager(to='tags.OtherTag', through='tags.OtherTaggedItem', blank=True, help_text='\u041f\u0440\u043e\u0447\u0438\u0435 \u0442\u0435\u0433\u0438. \u0422\u0435\u0433\u0438 \u0441\u0435\u0442\u0442\u0438\u043d\u0433\u0430 \u0438 \u0441\u0438\u0441\u0442\u0435\u043c\u044b \u0436\u0435\u043b\u0430\u0442\u0435\u043b\u044c\u043d\u043e \u043d\u0435 \u0443\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c', verbose_name='\u041f\u0440\u043e\u0447\u0438\u0435 \u0442\u0435\u0433\u0438'),
        ),
        migrations.AlterField(
            model_name='gamesession',
            name='number',
            field=models.PositiveIntegerField(default=0, help_text='\u0411\u0443\u0434\u0435\u0442 \u0438\u0441\u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u044c\u0441\u044f \u0434\u043b\u044f \u0430\u0432\u0442\u043e\u043c\u0430\u0442\u0438\u0447\u0435\u0441\u043a\u043e\u0433\u043e \u043e\u043f\u0440\u0435\u0434\u0435\u043b\u0435\u043d\u0438\u044f \u043d\u0430\u0437\u0432\u0430\u043d\u0438\u0439 \u0441\u043b\u0435\u0434\u0443\u044e\u0449\u0438\u0445 \u0441\u0435\u0441\u0441\u0438\u0439', db_index=True, verbose_name='\u043d\u043e\u043c\u0435\u0440 \u0441\u0435\u0441\u0441\u0438\u0438', blank=True),
        ),
    ]
