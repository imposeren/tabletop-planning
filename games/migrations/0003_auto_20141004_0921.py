# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('games', '0002_auto_20140930_0351'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sessionvisit',
            name='role',
            field=models.PositiveSmallIntegerField(default=0, verbose_name='\u0440\u043e\u043b\u044c', choices=[(0, '\u043d\u0435\u0438\u0437\u0432\u0435\u0441\u0442\u043d\u043e'), (1, '\u0438\u0433\u0440\u043e\u043a'), (2, '\u0432\u0435\u0434\u0443\u0449\u0438\u0439')]),
        ),
    ]
