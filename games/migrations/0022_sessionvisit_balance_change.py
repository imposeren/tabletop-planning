# -*- coding: utf-8 -*-
# flake8: noqa
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounting', '0012_balancechange'),
        ('games', '0021_auto_20151226_1256'),
    ]

    operations = [
        migrations.AddField(
            model_name='sessionvisit',
            name='balance_change',
            field=models.ForeignKey(related_name='game_sessions', default=None, blank=True, to='accounting.BalanceChange', null=True, on_delete=models.CASCADE),
        ),
    ]
