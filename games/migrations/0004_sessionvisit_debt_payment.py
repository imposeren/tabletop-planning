# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounting', '0003_payment_comment'),
        ('games', '0003_auto_20141004_0921'),
    ]

    operations = [
        migrations.AddField(
            model_name='sessionvisit',
            name='debt_payment',
            field=models.ForeignKey(related_name='debt_visit', blank=True, to='accounting.Payment', null=True, on_delete=models.CASCADE),
            preserve_default=True,
        ),
    ]
