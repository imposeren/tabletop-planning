# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import taggit.managers


class Migration(migrations.Migration):

    dependencies = [
        ('games', '0012_auto_20141108_0129'),
    ]

    operations = [
        migrations.AlterField(
            model_name='game',
            name='other_tags',
            field=taggit.managers.TaggableManager(to='tags.OtherTag', through='tags.OtherTaggedItem', blank=True, help_text='\u041f\u0440\u043e\u0447\u0438\u0435 \u0442\u0435\u0433\u0438. \u0422\u0435\u0433\u0438 \u0441\u0435\u0442\u0442\u0438\u043d\u0433\u0430 \u0438 \u0441\u0438\u0441\u0442\u0435\u043c\u044b \u0436\u0435\u043b\u0430\u0442\u0435\u043b\u044c\u043d\u043e \u043d\u0435 \u0434\u0443\u0431\u043b\u0438\u0440\u043e\u0432\u0430\u0442\u044c \u0432 \u044d\u0442\u043e \u043f\u043e\u043b\u0435', verbose_name='\u041f\u0440\u043e\u0447\u0438\u0435 \u0442\u0435\u0433\u0438'),
            preserve_default=True,
        ),
    ]
