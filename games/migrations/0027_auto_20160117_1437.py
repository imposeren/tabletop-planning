# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('games', '0026_events_indexing'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sessionvisit',
            name='have_visited',
            field=models.BooleanField(default=True, db_index=True, verbose_name='\u0411\u044b\u043b \u043d\u0430 \u0438\u0433\u0440\u0435'),
        ),
        migrations.AlterField(
            model_name='sessionvisit',
            name='role',
            field=models.PositiveSmallIntegerField(default=0, db_index=True, verbose_name='\u0440\u043e\u043b\u044c', choices=[(0, '\u043d\u0435\u0438\u0437\u0432\u0435\u0441\u0442\u043d\u043e'), (1, '\u0438\u0433\u0440\u043e\u043a'), (2, '\u0432\u0435\u0434\u0443\u0449\u0438\u0439')]),
        ),
        migrations.AlterField(
            model_name='sessionvisit',
            name='verified',
            field=models.BooleanField(default=False, db_index=True, verbose_name='\u041c\u043e\u0434\u0435\u0440\u0430\u0446\u0438\u044f \u043f\u0440\u043e\u0439\u0434\u0435\u043d\u0430'),
        ),
        migrations.AlterIndexTogether(
            name='sessionvisit',
            index_together=set([('have_visited', 'verified', 'role'), ('have_visited', 'verified', 'role', 'game_session'), ('have_visited', 'verified')]),
        ),
    ]
