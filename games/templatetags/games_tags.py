# -*- coding: utf-8 -*-
from django import template
from django.core.cache import cache
from django.template.loader import render_to_string

from games.models import GameSession, Game

register = template.Library()


@register.filter
def can_join(user, game):
    return game and game.can_be_joined_by(user)


@register.filter
def can_leave(user, game):
    return game and game.can_be_leaved_by(user)


@register.filter
def can_edit(user, game):
    return game and game.can_be_edited_by(user)


@register.filter
def can_report(user, game_session):
    return game_session and game_session.can_be_reported_by(user)


@register.filter
def can_view_report(user, game_session):
    return game_session and game_session.report_can_be_viewed_by(user)


@register.filter
def can_be_confirmed_in(user, game):
    return game and game.can_be_confirmed(user)


@register.filter
def confirmed_in(user, game):
    return game and game.confirmed_player(user)


@register.filter
def can_edit_report(user, game_session):
    return game_session and game_session.report_can_be_edited_by(user)


@register.filter
def remove_from_game_url(user, game):
    return game and game.get_player_remove_url(user)


@register.filter
def confirm_for_game_url(user, game):
    return game and game.get_player_confirm_url(user)


@register.filter
def participates(game_session, user):
    return user.participates_in_game(game_session)


@register.filter
def plays_with_me(one_user, other_user):
    return one_user.pk and other_user.pk and one_user.plays_with_me(other_user)


@register.simple_tag()
def render_game_or_session(game_or_session, user, image_geometry=None):
    if isinstance(game_or_session, Game):
        game = game_or_session
        session = None
    elif isinstance(game_or_session, GameSession):
        session = game_or_session
        game = session.game
    else:
        raise ValueError('game_or_session should be instance of Game or Session. Got "%s" instead' % type(game_or_session))

    is_master = False

    if user.is_authenticated:
        is_master = (user in game.masters.all())  # using .all as it is most likely prefetched

    description_cache_key = game_or_session.get_render_game_or_session_description_cache_key(is_master)

    version = 2

    context = {
        'game': game,
        'session': session,
        'user': user,
    }

    # description_html = cache.get(description_cache_key, version=version)
    description_html = None
    if not description_html:
        description_html = render_to_string(
            'games/blocks/game_session_item_description.html',
            context
        )
        if is_master:
            cache_time = 60*15  # 15 min
        else:
            cache_time = 60*60*24 * 30
        cache.set(description_cache_key, description_html, cache_time, version=version)

    context['image_geometry'] = image_geometry

    image_html = render_to_string('games/blocks/game_session_item_image.html', context)

    context.update({'description_html': description_html, 'image_html': image_html})
    return render_to_string(
        'games/blocks/game_session_item.html',
        context
    )
