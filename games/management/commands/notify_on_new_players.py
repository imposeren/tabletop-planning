# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime

from django.conf import settings
from django.utils import timezone, translation

from games.models import Game
from tabletop_planning.utils import LoggedCommand, mail_send, config_value
from users.models import User


class Command(LoggedCommand):
    help = 'Notify everyone about upcoming game sessions'

    def handle(self, *args, **options):
        translation.activate(settings.LANGUAGE_CODE)

        self.verbosity = options.get('verbosity')

        notification_interval = config_value('tabletop_planning', 'NON_SPAM_HOURS_INTERVAL')

        exclude_after = timezone.now() - datetime.timedelta(hours=notification_interval)

        target_games = list(
            Game.objects
            .filter(game_players__isnull=False, game_players__master_notified__isnull=True)
            .exclude(game_players__master_notified__gte=exclude_after)
            .distinct()
            .values_list('id', flat=True)
        )

        candidate_masters = (
            User.objects
            .filter(mastering_games__in=target_games)
            .filter(is_active=True, email__contains='@', receive_notifications=True)
            .distinct()
        )

        for master in candidate_masters:
            target_games = (
                master.mastering_games
                .filter(
                    game_players__master_notified__isnull=True,
                )
                .distinct()
            )
            mail_send(
                [master.email],
                template='new-players-notification',
                context={'user': master, 'target_games': target_games},
                tags='notification',
            )
            for game in target_games:
                game.game_players.all().update(master_notified=timezone.now())
