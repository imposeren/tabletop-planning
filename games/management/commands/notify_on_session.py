# -*- coding: utf-8 -*-
import datetime

from django.conf import settings
from django.utils import timezone
from django.utils import translation

from tabletop_planning.utils import LoggedCommand, config_value
from games.models import GameSession


class Command(LoggedCommand):
    help = 'Notify everyone about upcoming game sessions'

    def handle(self, *args, **options):
        translation.activate(settings.LANGUAGE_CODE)

        self.verbosity = options.get('verbosity')

        notification_days = config_value('games', 'NOTIFICATION_DAYS')

        # Сессия между (сейчас+X; сейчас+X+1) дней
        after_time = timezone.now().date() + datetime.timedelta(days=notification_days)
        before_time = after_time + datetime.timedelta(days=1)

        game_sessions = GameSession.objects.filter(
            event__start_date__gte=after_time, event__start_date__lt=before_time,
            upcoming_notified=False,
        ).distinct()
        for session in game_sessions:
            session.notify()
