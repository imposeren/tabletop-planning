# -*- coding: utf-8 -*-

import datetime

# Django:
from django.utils import timezone
from django.template.loader import render_to_string

# Third-parties:
from messages_extends.models import Message

# ptoject
from games.models import GameSession
from tabletop_planning.utils import LoggedCommand, mail_send, config_value


class Command(LoggedCommand):
    help = 'Request masters to fill reports'

    def __init__(self, *args, **kwargs):
        super(Command, self).__init__(*args, **kwargs)
        self.__now = timezone.now()

    def get_sessions_qs(self):
        return GameSession.objects.reportables().filter(
            report_notified=False,
            event__start_date__gte=timezone.now() - datetime.timedelta(days=7)
        )

    def handle(self, *args, **options):
        for game_session in self.get_sessions_qs():

            user = game_session.game.masters.all()[0]

            # send emails:
            if user.receive_notifications and '@' in user.email:
                recipients = [user.email]
                mail_send(
                    recipients, template='request-for-report',
                    context={'game_session': game_session, 'user': user},
                    tags='notification',
                )

            # site message:
            extra_tags = u'info %s' % (game_session.get_complex_id(),)

            message_text = render_to_string(
                'games/notification_game_report.html',
                {
                    'CURRENCY': config_value('tabletop_planning', 'CURRENCY'),
                    'game_session': game_session
                }
            )

            message, _created = Message.objects.get_or_create(
                user=user, extra_tags=extra_tags,
                level=Message.LEVEL_CHOICES[1][0],
                defaults={'message': message_text}
            )

            game_session.report_notified = True
            game_session.save()
