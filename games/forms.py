# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime

# Django:
from django import forms
from django.contrib.auth import get_user_model
from django.core.mail import mail_admins
from django.db import transaction
from django.forms.models import modelformset_factory
from django.forms.formsets import DELETION_FIELD_NAME
from django.utils import timezone, formats
from django.utils.translation import ugettext_lazy as _

# third-parties
from happenings.models import Event
from messages_extends.models import Message
from parsley.decorators import parsleyfy
from dal_select2_taggit.widgets import TaggitSelect2
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field

# project:
from tabletop_planning.forms import BootstrapModelForm, BaseModelFormSetWithRequest
from tabletop_planning.utils import mail_send, config_value
from tabletop_planning.widgets import ImageWidget

# app:
from .models import Game, GameSession, SessionVisit, default_session_datetime


@parsleyfy
class GameEditForm(BootstrapModelForm):

    class Meta:  # noqa: D101
        model = Game
        fields = [
            'name', 'max_players', 'image',
            'system_tags', 'setting_tags', 'other_tags',
            'headline',
            'description',
            'secret_link',
            'secret_text',
            'light_secret',
            'is_active', 'is_finished',
        ]
        widgets = {
            'image': ImageWidget(attrs={'accept': "image/*"}),
            'system_tags': TaggitSelect2(
                url='tags:systemtag-autocomplete',
                attrs={
                    'data-minimum-input-length': 2,
                },
            ),
            'setting_tags': TaggitSelect2(
                url='tags:settingtag-autocomplete',
                attrs={
                    'data-minimum-input-length': 3,
                },
            ),
            'other_tags': TaggitSelect2(
                url='tags:othertag-autocomplete',
                attrs={
                    'data-minimum-input-length': 2,
                },
            ),
        }

    def __init__(self, *args, **kwargs):
        super(GameEditForm, self).__init__(*args, **kwargs)

        self.helper.form_tag = False

        self.helper.layout = Layout(
            Field('name', autofocus='autofocus'), 'max_players', 'image',
            'system_tags', 'setting_tags', 'other_tags',
            'headline',
            'description',
            'secret_link',
            'secret_text',
            'light_secret',
            'is_active',
            'is_finished',
        )

    def save(self, *args, **kwargs):
        super(GameEditForm, self).save(*args, **kwargs)
        if not self.instance.masters.filter(pk=self.request.user.pk).exists():
            self.instance.masters.add(self.request.user)
            # self.instance.follow_comments_by_master(self.request.user)
        return self.instance


def get_dt_format():
    for fmt in formats.get_format('DATETIME_INPUT_FORMATS'):
        if '%S' not in fmt:
            return fmt


@parsleyfy
class GameSessionForm(BootstrapModelForm):

    start_date = forms.DateTimeField(
        label=_('Время проведения'),
    )
    end_date = forms.DateTimeField(
        label=_('Время окончания'),
        required=False,
    )

    title = forms.CharField(
        label=_('Название'),
        required=True,
        help_text=_('Название отображаемое в календаре'),
    )

    description = forms.CharField(
        label=_('Описание'),
        required=False,
    )

    duration = forms.IntegerField(
        label=_('Продолжительность, часов'),
        help_text=_('Приблизительная продолжительность сессии в часах'),
        min_value=1, max_value=48,
    )

    class Meta:  # noqa: D101
        model = GameSession
        fields = ['game', 'image', 'number']
        widgets = {
            'image': ImageWidget(attrs={'accept': "image/*"}),
        }

    def __init__(self, *args, **kwargs):
        super(GameSessionForm, self).__init__(*args, **kwargs)
        self.fields['game'].widget = forms.HiddenInput()

        # new users are too lazy to change this. So no default for them!
        self.fields['start_date'].initial = (
            default_session_datetime().date().strftime(formats.get_format('DATE_INPUT_FORMATS')[0])
        ) + ' HH:MM'

        self.fields['end_date'].widget = forms.HiddenInput()

        self.fields['duration'].initial = config_value('games', 'game_default_duration')

        self.helper.layout = Layout(
            'game', 'title', 'number', 'start_date', 'duration', 'description', 'end_date', 'image',
        )
        self.helper.form_tag = False
        self.helper.render_unmentioned_fields = True

        self.fields['game'].required = False  # it's really reuired but not editable and not visible.

        if not self.instance.pk:
            self.fields['create'] = forms.BooleanField(label=_('Создать'), initial=False, required=False)
            self.helper.layout.fields.insert(0, 'create')
            self.helper.layout.fields.append(Field(DELETION_FIELD_NAME, readonly='readonly'))
            initial_game = kwargs.get('initial', {}).get('game', None)
            if isinstance(initial_game, int):
                initial_game = Game.objects.get(pk=initial_game)
            if initial_game:
                last_session = initial_game.last_session
                if last_session:
                    session_number = (last_session.number + 1)
                else:
                    session_number = 1
                self.fields['title'].initial = '%s: №%s' % (initial_game.name, session_number)
                self.fields['number'].initial = session_number
        elif self.instance.event and self.instance.event.start_date <= timezone.now():
            self.helper[:].wrap(Field, readonly='readonly')
            self.readonly_class = 'readonly'

        if self.instance.pk and self.instance.event:
            self.helper.layout.fields.insert(0, DELETION_FIELD_NAME)
            for field_name in ('start_date', 'end_date', 'title', 'description'):
                self.fields[field_name].initial = getattr(self.instance.event, field_name)
                duration_initial = int(
                    ((self.instance.event.end_date - self.instance.event.start_date).total_seconds() + 1)/3600
                )
                duration_initial = max(1, duration_initial)
            self.fields['duration'].initial = duration_initial

    def clean_start_date(self):
        master = self.instance.pk and self.instance.game.master or self.request.user

        value = self.cleaned_data.get('start_date', timezone.now())
        is_old_session = (self.instance and getattr(self.instance.event, 'start_date', None) == value)
        if not is_old_session and value <= timezone.now() - datetime.timedelta(hours=master.default_safe_hours):
            raise forms.ValidationError(_('Слишком поздно создавать уже прошедшую сессию'))
        return value

    def has_changed(self):
        cd = getattr(self, 'cleaned_data', {})
        return cd.get('create', True) or super(GameSessionForm, self).has_changed()

    def clean(self):
        cd = self.cleaned_data
        if cd.get(DELETION_FIELD_NAME, False) and self.instance and self.instance.pk:
            # django formsets perform self.instance.clean() even for deleted forms and this causes form to show error
            self.instance.delete()

        if not cd.get('create', True):
            # this object should not be created if it has 'create' field and it's not set
            self._errors = {}
            return

        if not cd.get('start_date'):
            self.add_error(
                'start_date',
                forms.ValidationError(
                    _('Дата начала не заполнена или указана неверно.'),
                    code='start_date_wrong',
                )
            )
        else:
            cd['end_date'] = (
                cd['start_date']
                +
                datetime.timedelta(
                    hours=(cd.get('duration', config_value('games', 'game_default_duration'))),
                    seconds=-1,
                )
            )
            if cd['end_date'] >= timezone.now():
                parallel_sessions = (
                    GameSession
                    .objects
                    .filter(
                        event__start_date__lt=cd['end_date'], event__end_date__gt=cd['start_date'],
                        game__is_active=True, game__is_finished=False,
                    )
                    .distinct()
                )
                if self.instance and self.instance.pk:
                    parallel_sessions = parallel_sessions.exclude(pk=self.instance.pk)

                parallel_sessions = parallel_sessions.distinct()

                # games_with_not_created_parallel_sessions = Game.get_parallel_games_without_parallel_sessions(
                #     Game.actives.exclude(sessions__in=parallel_sessions),
                #     cd['start_date'], cd['end_date']
                # )

                game_id = self.instance and self.instance.game_id
                if not game_id and self.initial.get('game', None):
                    if isinstance(self.initial['game'], Game):
                        game_id = self.initial['game'].id
                    else:
                        game_id = self.initial['game']

                # if game_id:
                #     games_with_not_created_parallel_sessions = games_with_not_created_parallel_sessions.exclude(pk=game_id)

                parallel_count = parallel_sessions.count()  # + games_with_not_created_parallel_sessions.count()
                max_capacity = config_value('games', 'games_capacity')
                if parallel_count >= max_capacity:
                    # parallel_games = list(games_with_not_created_parallel_sessions)
                    parallel_games = []
                    parallel_games.extend(Game.objects.filter(sessions__in=parallel_sessions))
                    game_names = ', '.join([game.name for game in set(parallel_games)])
                    self.add_error(
                        'start_date',
                        forms.ValidationError(
                            _(
                                'В выбранном промежутке времени уже запланировано %(count)s или больше игр: %(game_names)s. '
                                'Пожалуйста выберите другое время и/или продолжительность'
                            ),
                            params={'count': max_capacity, 'game_names': game_names},
                            code='games_limit_reached',
                        )
                    )
                same_game_conflict = parallel_sessions.filter(game_id=game_id).first()
                if same_game_conflict:
                    self.add_error(
                        'start_date',
                        forms.ValidationError(
                            _(
                                'В выбранном промежутке времени уже запланировано сессия этой игры "%(session_name)s". '
                                'Пожалуйста выберите другое время'
                            ),
                            params={'session_name': same_game_conflict.event.title},
                            code='games_limit_reached',
                        )
                    )

    def save(self, *args, **kwargs):
        with transaction.atomic():
            initial_commit = kwargs.pop('commit', True)
            cd = self.cleaned_data
            if not cd.get('create', True):
                # this object should not be created
                return

            master = self.instance.game.master or self.request.user

            if not self.instance.event:
                event = Event()
            else:
                event = self.instance.event
            event.created_by = master
            event.title = cd['title']
            event.description = cd['description']
            event.start_date = cd['start_date']
            event.end_date = cd['end_date']
            event.save()
            if not self.instance.event:
                self.instance.event = event

            instance = super(GameSessionForm, self).save(*args, **kwargs)

            if not instance.event:
                instance.event = event
            if initial_commit:
                instance.save()
            return instance


class GameReportFormLine(forms.ModelForm):

    paid_amount = forms.DecimalField(
        label=_('Оплата'), max_digits=6, decimal_places=3,
        initial=0,
        help_text=_('Сколько игрок заплатил, либо сколько было потрачено на нужды клуба.')
    )
    game_cost = forms.DecimalField(
        label=_('Цена'), max_digits=6, decimal_places=3, required=False,
        help_text=_('Стоимость игры'),
    )
    master_readonly_fields = ('bonus', 'paid_amount', 'used_ticket', 'used_experience')

    class Meta:  # noqa: D101
        model = SessionVisit
        fields = (
            'verified',
            'game_session',
            'visitor',
            'visitor_level',
            'have_visited',
            'bonus',
            'cleaned',
            'cleaned_floor',
            'used_experience',
            'used_ticket',
            # 'is_free',  # TODO: add dynamic preference to enable/disable free games
            'comment',
        )

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        self.user_is_staff = (
            getattr(self.request, 'user', None)
            and
            (
                self.request.user.is_staff
                or
                self.request.user.is_superuser
            )
        )
        super(GameReportFormLine, self).__init__(*args, **kwargs)

        visitor = self._get_field_value('visitor')

        if visitor:
            self.fields['visitor'].queryset = get_user_model().objects.filter(pk=visitor.pk)
        self.fields['visitor'].empty_label = None
        self.fields['visitor'].widget.attrs['readonly'] = 'readonly'
        self.fields['game_cost'].widget.attrs['readonly'] = 'readonly'
        for field in self.fields.keys():
            self.fields[field].widget.attrs['class'] = 'report-field-' + field

        if (not getattr(visitor, 'is_service', False)) and not (self.user_is_staff or config_value('accounting', 'customizable_player_paid_amount')):
            self.can_edit_paid_amount = False
            self.fields['paid_amount'].widget.attrs['readonly'] = 'readonly'
        else:
            self.can_edit_paid_amount = True

        is_master = self._get_field_value('game_session').game.masters.filter(pk=visitor.pk).exists()
        if is_master:
            for field_name in self.master_readonly_fields:
                self.fields[field_name].widget.attrs['readonly'] = 'readonly'

        if visitor:
            if not visitor.has_tickets and not self._get_field_value('ticket'):
                self.fields['used_ticket'].widget.attrs['readonly'] = 'readonly'
            if visitor.cached_total_player_experience < config_value('accounting', 'default_game_xp_cost'):
                self.fields['used_experience'].widget.attrs['readonly'] = 'readonly'
            if is_master:
                self.fields['visitor_level'].initial = visitor.master_level
            elif not visitor.is_service:
                self.fields['visitor_level'].initial = visitor.player_level
            else:
                for field in self.fields.keys():
                    if field in ('paid_amount', 'comment', 'ok'):
                        continue
                    self.fields[field].widget.attrs['readonly'] = True

        self.fields['visitor_level'].widget.attrs['readonly'] = True

        game_session = self._get_field_value('game_session')
        self.fields['game_session'].queryset = GameSession.objects.filter(pk=game_session.pk)
        self.fields['game_session'].widget = forms.HiddenInput()

        if not self.user_is_staff:
            self.fields['verified'].widget.attrs['readonly'] = 'readonly'

        if self.instance.pk:
            if self.instance.cost_balance_change:
                self.fields['game_cost'].initial = -self.instance.cost_balance_change.amount
                if self.instance.visitor.is_service:
                    self.fields['game_cost'].initial = 0
            else:
                if self.instance.role == SessionVisit.ROLES.player:
                    self.fields['game_cost'].initial = self.instance.visitor.default_player_payment
                elif self.instance.role == SessionVisit.ROLES.master:
                    self.fields['game_cost'].initial = -self.instance.visitor.default_master_payment
                    self.fields['game_cost'].initial = 0

            if self.instance.paid_balance_change and not is_master:
                self.fields['paid_amount'].initial = self.instance.paid_balance_change.amount
                if self.instance.visitor.is_service and self.fields['paid_amount'] != 0:
                    self.fields['paid_amount'].initial *= -1

            if self.instance.verified:
                for field in self.fields.keys():
                    self.fields[field].widget.attrs['readonly'] = True

    def _get_field_value(self, field_name):
        if self.instance.pk:
            return getattr(self.instance, field_name)
        else:
            return self.initial.get(field_name, None)

    def clean(self, *args, **kwargs):
        super(GameReportFormLine, self).clean(*args, **kwargs)
        cd = self.cleaned_data
        user = self._get_field_value('visitor')
        is_master = cd['game_session'].game.masters.filter(pk=user.pk).exists()
        if self.fields['game_cost'].initial is not None:
            cd['game_cost'] = self.fields['game_cost'].initial

        if not self.user_is_staff:
            cd['verified'] = self.fields['verified'].initial

        if cd['have_visited']:
            if (not is_master) and (not user.is_service):

                if cd.get('used_ticket', False) or cd.get('is_free', False) or cd.get('used_experience', False):
                    cd['paid_amount'] = 0
                    cd['game_cost'] = 0
                elif cd['paid_amount'] != cd['game_cost'] and not self.can_edit_paid_amount:
                    self.add_error(
                        'paid_amount',
                        _('Оплаченная сумма не может отличаться от стоимости игры')
                    )

                if cd.get('used_ticket', False):
                    if not user.has_tickets and not self.instance.ticket:
                        self.add_error('used_ticket', _('У игрока нет ваучеров'))

                if cd.get('used_experience') and cd.get('used_ticket'):
                    raise self.add_error('used_experience', _('Нельзя оплатить и опытом и ваучером'))

                if cd.get('paid_amount', 0) < 0:
                    self.add_error('paid_amount', _('Игрок не может получать деньги за игру'))
            elif is_master:
                if cd.get('paid_amount', 0) > 0:
                    self.add_error(
                        'paid_amount',
                        _('Ведущий не может платить деньги за игру, используйте отрицательные значения')
                    )
                cd['bonus'] = False
                cd['used_ticket'] = False
            elif user.is_service:
                paid_amount = cd.get('paid_amount', 0) or 0
                if paid_amount < 0:
                    self.add_error(
                        'paid_amount',
                        _('Траты должны либо отсутствовать, либо быть положительными')
                    )
                if paid_amount != 0:
                    paid_amount = -paid_amount
                cd['paid_amount'] = paid_amount
                cd['game_cost'] = paid_amount
                cd['bonus'] = False
                cd['used_ticket'] = False
                cd['used_experience'] = False
                cd['is_free'] = False

            if cd.get('is_free'):
                cd['game_cost'] = 0
        else:
            cd['cleaned'] = False
            cd['bonus'] = False
            cd['paid_amount'] = 0
            cd['game_cost'] = 0
            cd['used_ticket'] = False
        if self.instance.pk and self.instance.verified:
            raise forms.ValidationError(
                _('Отчет по посещению {instance.visitor} уже проверен модератором').format(instance=self.instance)
            )
        return cd

    def save(self, *args, **kwargs):
        from accounting.models import BalanceChange
        with transaction.atomic():
            initial_commit = kwargs.pop('commit', True)
            instance = super(GameReportFormLine, self).save(*args, **kwargs)

            cd = self.cleaned_data
            game_session = cd['game_session']
            master = game_session.game.masters.all()[0]

            if self.errors:
                raise forms.ValidationError('Ошибки в форме')

            is_master = cd['game_session'].game.masters.filter(pk=cd['visitor'].pk).exists()

            game_details_data = BalanceChange.build_details_from_session(game_session)
            game_details_data['visitor'] = BalanceChange.build_details_from_user(cd['visitor'])
            game_details_data['master'] = BalanceChange.build_details_from_user(master)
            for key in ('used_ticket', 'used_experience', 'have_visited'):  # TODO: 'is_free',
                game_details_data[key] = cd[key]

            if cd['visitor']:
                # change balance by negative cost
                visitor = cd['visitor']
                cost_balance_change = instance.cost_balance_change or BalanceChange(user=cd['visitor'])
                cost_balance_change.user = visitor
                cost_balance_change.user_id = visitor.pk
                cost_balance_change.amount = -cd['game_cost']
                if is_master:
                    if 'master_salary' in cd['comment']:
                        cost_balance_change.comment = 'master_salary'
                    else:
                        cost_balance_change.comment = 'master_salary'
                        if cd['comment']:
                            cost_balance_change.comment += '. Extra: ' + cd['comment']

                    cost_balance_change.comment = (
                        cost_balance_change.comment[:BalanceChange._meta.get_field('comment').max_length]
                    )
                else:
                    cost_balance_change.comment = 'should_have_paid'
                cost_balance_change.details = game_details_data
                cost_balance_change.save()
                instance.cost_balance_change = cost_balance_change

                if not is_master:
                    # refill balance by paid amount
                    paid_balance_change = instance.paid_balance_change or BalanceChange()
                    paid_balance_change.user = visitor
                    paid_balance_change.amount = cd['paid_amount']
                    paid_balance_change.comment = 'actually_paid'
                    paid_balance_change.details = game_details_data
                    paid_balance_change.save()
                    instance.paid_balance_change = paid_balance_change

                    debt = cd['game_cost'] - cd['paid_amount']
                    if debt != 0:
                        # still refill balance -- master should be responsible for
                        # handling this problems
                        # TODO: implement (do not forget to change code after if)

                        pass

                    # decrease balance of master because he received this money
                    to_master_balance_change = instance.to_master_balance_change or BalanceChange()
                    if visitor.is_service:
                        comment = 'money used for club expenses'
                    else:
                        comment = 'money from player'
                    if cd['comment']:
                        comment += '. Extra: ' + cd['comment']
                    comment = comment[:BalanceChange._meta.get_field('comment').max_length]

                    to_master_balance_change.user = master
                    to_master_balance_change.amount = -cd['paid_amount']
                    to_master_balance_change.comment = comment
                    to_master_balance_change.details = game_details_data
                    to_master_balance_change.save()
                    instance.to_master_balance_change = to_master_balance_change

            if initial_commit:
                instance.save()
        return instance

    def has_changed(self):
        return True


BaseGameReportFormset = modelformset_factory(
    SessionVisit,
    GameReportFormLine,
    extra=0,
    formset=BaseModelFormSetWithRequest,
)


class GameReportFormset(BaseGameReportFormset):

    def __init__(self, *args, **kwargs):
        game_session = kwargs.pop('game_session')
        self.request = kwargs.pop('request', None)
        self.game_session = game_session

        initial = []
        num_players = 0
        for player in game_session.game.players.all():
            num_players += 1
            player_initial = {
                'visitor': player,
                'role': SessionVisit.ROLES.player,
                'game_session': self.game_session,
            }
            player_initial['game_cost'] = player.default_player_payment
            if player.default_player_payment == 0:
                player_initial['is_free'] = True

            if player.has_tickets and player.default_player_payment > 0:
                player_initial['used_ticket'] = True
                player_initial['paid_amount'] = 0
            else:
                player_initial['used_ticket'] = False
                player_initial['paid_amount'] = player.default_player_payment
            initial.append(player_initial)

        if num_players < config_value('accounting', 'minimum_players_for_salary'):
            salary_override = 0
        else:
            salary_override = None

        for master in game_session.game.masters.all():
            if salary_override is None:
                salary = master.default_master_payment
            else:
                salary = salary_override
            master_initial = {
                'visitor': master,
                'role': SessionVisit.ROLES.master,
                'game_session': self.game_session,
            }
            master_initial['paid_amount'] = master_initial['game_cost'] = -salary
            initial.append(master_initial)

        initial.append({
            'visitor': get_user_model().objects.get(email='expenses@localhost', is_service=True),
            'role': SessionVisit.ROLES.expenses,
            'game_session': self.game_session,
            'game_cost': 0,
            'paid_amount': 0,
        })

        super(BaseGameReportFormset, self).__init__(initial=initial, *args, **kwargs)
        self.form_kwargs['request'] = self.request
        self.queryset = SessionVisit.objects.filter(game_session=game_session)

        objs_count = self.queryset.count()
        self.extra = objs_count or len(initial)
        if objs_count:
            self.initial_extra = None
            self.initial = None
            self.extra = 0
        self.helper = FormHelper()
        self.helper.template = 'bootstrap3/table_formset.html'
        self.helper.help_text_inline = True
        self.helper.render_unmentioned_fields = True

    def clean(self):
        if any(self.errors):
            return
        bonus_count = 0
        cleaned_count = 0
        cleaned_floor_count = 0
        for form in self.forms:
            cd = form.cleaned_data

            if cd.get('bonus'):
                bonus_count += 1
                if bonus_count >= 2:
                    raise forms.ValidationError(_('Можно выдать бонус лишь одному игроку.'))
            if cd.get('cleaned'):
                cleaned_count += 1
                if cleaned_count >= 2:
                    raise forms.ValidationError(_('Опыт за уборку стола можно выдать лишь одному человеку.'))
            if cd.get('cleaned_floor'):
                cleaned_floor_count += 1
                if cleaned_floor_count >= 2:
                    raise forms.ValidationError(_('Опыт за уборку пола можно выдать лишь одному человеку.'))

    def save(self):
        from accounting.models import BalanceChange
        with transaction.atomic():
            results = []

            player_visits = []
            master_visits = []

            for form in self.forms:
                visit = form.save()
                if visit.role == visit.ROLES.master:
                    master_visits.append(visit)
                elif visit.role == visit.ROLES.player:
                    player_visits.append(visit)
                results.append(visit)

            min_players = config_value('accounting', 'minimum_players_for_salary')
            if len([None for visit in player_visits if visit.have_visited]) < min_players:
                for visit in master_visits:
                    balance_change = visit.cost_balance_change
                    old_amount = balance_change.amount
                    old_comment = balance_change.comment

                    balance_change.amount = 0
                    comment_extra = '. Too few players'
                    if comment_extra not in balance_change.comment:
                        balance_change.comment += comment_extra
                    balance_change.comment = balance_change.comment[:BalanceChange._meta.get_field('comment').max_length]

                    if (old_amount != balance_change.amount) or (old_comment != balance_change.comment):
                        (
                            BalanceChange
                            .objects
                            .filter(pk=balance_change.pk)
                            .update(
                                amount=balance_change.amount,
                                comment=balance_change.comment
                            )
                        )

            game_session = self.game_session

            (
                Message.objects
                .filter(read=False, extra_tags__endswith=' %s' % (game_session.get_complex_id(),))
                .update(read=True)
            )

            # send notification to moderators
            moderators_group = config_value('tabletop_planning', 'accounting_moderators_groupname')
            moderators = list(
                get_user_model().objects.filter(
                    groups__name=moderators_group,
                    is_active=True,
                    receive_notifications=True,
                    email__contains='@',
                )
            )
            recipients = [m.email for m in moderators]

            if recipients:
                mail_send(
                    recipients, template='request-for-report-moderation',
                    context={'game_session': game_session},
                    tags=['notification', 'moderation'],
                )
            else:
                mail_admins(
                    'No accouning moderators available',
                    'No active accounting moderators found. Please add some to "%s" group' % moderators_group
                )
            return results
