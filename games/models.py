# -*- coding: utf-8 -*-
from __future__ import unicode_literals

# Python builtins:
import datetime

# django:
from django.conf import settings
from django.contrib.postgres.fields import IntegerRangeField
from django.core.cache import cache
from django.core.exceptions import ValidationError, ObjectDoesNotExist
try:
    from django.core.urlresolvers import reverse
except ImportError:
    from django.urls import reverse
from django.db import models
from django.db import transaction
from django.db.models import Q, Prefetch, F, Func, Count
from django.utils import timezone
from django.utils.functional import cached_property
from django.utils.text import slugify
# from django.utils.translation import ugettext
from django.utils.translation import ugettext_lazy as _
from psycopg2.extras import NumericRange

# Third-parties:
# from django_comments_xtd.models import XtdComment
from happenings.models import Event
from model_utils import FieldTracker, Choices
from sorl.thumbnail import ImageField
from taggit.managers import TaggableManager
from unidecode import unidecode

# project:
from accounting.models import ExperienceReward, BalanceChange
from tabletop_planning.managers import IsActiveManager
from tabletop_planning.utils import get_cache_key, mail_send, config_value
from tags.models import SystemTaggedItem, SettingTaggedItem, OtherTaggedItem


def games_image_path(instance, filename):
    if len(filename) > 80:
        if filename.find('.') > 0:
            name, extension = filename.rsplit('.', 1)
            name = name[:(79-len(extension))]
            filename = '%s.%s' % (name, extension)
        else:
            filename = filename[:69] + '_' + filename[-10:]
    return 'games/{instance.pk}/{filename}'.format(instance=instance, filename=filename)


class GameQuerySet(models.QuerySet):
    use_for_related_fields = True

    def optimize_for_output(self):
        return self.prefetch_related(
            'masters',
            'players',
            'system_tags',
            'setting_tags',
            'other_tags',
            Prefetch('sessions', queryset=GameSession.objects.all().select_related('event')),
        ).order_by('-pk', 'name')


class Game(models.Model):

    # not fields
    tracker = FieldTracker()

    DAYS_OF_WEEK = Choices(
        # values compatible with "week_day" query in django 1.8: https://docs.djangoproject.com/en/1.8/ref/models/querysets/#week-day
        (0, 'unknown', _('неизвестно')),
        (1, 'sunday', _('воскресенье')),
        (2, 'monday', _('понедельник')),
        (3, 'tuesday', _('вторник')),
        (4, 'wednesday', _('среда')),
        (5, 'thursday', _('четверг')),
        (6, 'friday', _('пятница')),
        (7, 'saturday', _('суббота')),
    )

    RESERVED_SLUGS = ('new', 'create', 'delete',)

    # Fields:

    last_modified = models.DateTimeField(auto_now=True, db_index=True)

    image = ImageField(
        _('постер'),
        upload_to=games_image_path,
        height_field='image_height',
        width_field='image_width',
        default='',
        blank=True,
    )
    image_height = models.PositiveIntegerField(editable=False, default=0, null=True)
    image_width = models.PositiveIntegerField(editable=False, default=0, null=True)

    name = models.CharField(
        _('название'),
        max_length=255,
        unique=True,
    )
    slug = models.SlugField(
        _('текст в ссылках'),
        max_length=255,
        blank=True,
        unique=True,
        help_text=_('Будет заполнено автоматически'),
    )
    description = models.TextField(
        _('описание'),
        blank=True,
        default='',
    )

    headline = models.CharField(
        _('Короткое описание'),
        max_length=70,
        blank=True,
        default='',
    )

    # WARNING: use ``game.game_players.create(player=user)`` instead of ``game.players.add(user)``
    # 'through' used to explicitly process player addition/removal to/from games (without m2m_changed signals)
    players = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        through='GamePlayer',
        verbose_name=_('игроки'),
        related_name='playing_games',
        blank=True,

    )
    masters = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        verbose_name=_('ведущие'),
        related_name='mastering_games',
    )

    participated_players = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        verbose_name=_('все игравшие игроки'),
        related_name='participated_games',
        blank=True,
    )

    max_players = models.PositiveIntegerField(
        _('максимум игроков'),
        default=3,
        help_text=_(
            'Новые игроки не смогут присоединиться к игре, если в ней уже участвует максимальное количество игроков')
    )

    current_players = models.PositiveIntegerField(
        _('текущее количество игроков'),
        default=0,
        editable=False,
    )

    is_active = models.BooleanField(
        _('Игра активна'),
        default=True,
        db_index=True,
        help_text=_(
            'Неактивные игры не отображаются в общем списке и в вашем профиле')
    )

    is_finished = models.BooleanField(
        _('Игра завершена'),
        default=False,
        db_index=True,
        help_text=_(
            'Завершенные игры остаются в списках и профиле, но не видны в в списке "Ведёте"')
    )

    last_session_time_range = IntegerRangeField(
        blank=True,
        null=True,
        default=None,
    )

    system_tags = TaggableManager(
        verbose_name=_('Система'), through=SystemTaggedItem, blank=False,
    )

    setting_tags = TaggableManager(
        verbose_name=_('Сеттинг'), through=SettingTaggedItem, blank=False
    )

    other_tags = TaggableManager(
        verbose_name=_('Прочие теги'), through=OtherTaggedItem, blank=True,
        help_text=_('Прочие теги. Теги сеттинга и системы желательно не дублировать в это поле')
    )

    secret_link = models.URLField(
        _("Секретная ссылка"),
        max_length=4096,
        blank=True,
        null=True,
        default=None,
        help_text=_('Если заполнить, то секретный текст станет гипер-ссылкой')
    )
    secret_text = models.TextField(
        _("Секретный текст"),
        blank=True,
        null=True,
        default=None,
        help_text=_(
            'Текст, который отображается только (подтвержденным) игрокам. '
            'Пример использования: в поле секретный текст "Чат игры", '
            'в поле секретная ссылка: https://t.me/joinchat/SOME_CHAT_LINK"'
        )
    )

    light_secret = models.BooleanField(
        _("Секрет видно без подтверждения игрока"),
        default=False,
        help_text=_(
            'Секретный текст будет виден как подтвержденным так и не подтвержденным игрокам. '
            'В противном случае он будет отображаться только игрокам которые подтверждены ведущим.'
        )
    )

    # Managers:
    objects = GameQuerySet.as_manager()
    actives = IsActiveManager()

    # start models API

    class Meta:  # noqa: D101
        verbose_name = _('игра')
        verbose_name_plural = _('игры')

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.clean()
        super(Game, self).save(*args, **kwargs)
        self.invalidate_render_game_or_session()

    def clean(self):
        if self.tracker.has_changed('name'):
            self.slug = slugify(unidecode(self.name))
            conflict = Game.objects.filter(slug=self.slug).first()
            while (conflict and conflict != self):
                self.slug += '-x'
                conflict = Game.objects.filter(slug=self.slug).first()
        if self.slug in self.RESERVED_SLUGS:
            raise ValidationError(_('Выбранное вами название зарезервировано'))

    def delete(self, *args, **kwargs):
        super(Game, self).delete(*args, **kwargs)
        self.invalidate_render_game_or_session()

    # end models API

    # start urls:

    def get_absolute_url(self):
        return reverse('games:game_details', args=[self.slug])

    def get_join_url(self):
        return reverse('games:join_game', args=[self.slug])

    def get_leave_url(self):
        return reverse('games:leave_game', args=[self.slug])

    def get_edit_url(self):
        return reverse('games:edit_game', args=[self.slug])

    def get_player_remove_url(self, player):
        return reverse('games:remove_player', args=[self.slug, player.pk])

    def get_player_confirm_url(self, player):
        return reverse('games:confirm_player', args=[self.slug, player.pk])

    def get_report_url(self):
        """Return report url` of first unreported session.

        Method is used for compatibility with GameSession.

        """
        session = self.reportables and self.reportables[0] or None
        if session:
            return reverse('games:report_game_session', kwargs={'game_slug': self.slug, 'session_number': session.number})
    # end urls

    # start properties and info

    @property
    def vacant_players(self):
        return self.max_players - self.current_players

    @property
    def short_description(self):
        if self.headline:
            return self.headline
        else:
            if self.last_session:
                return self.last_session.event.description
        return ''

    def have_vacant_players(self):
        return self.vacant_players > 0

    def report_can_be_viewed_by(self, user):
        """Retrun true if can be viewed by user."""
        return False

    def report_can_be_edited_by(self, user):
        """Retrun true if can be edited by user."""
        return False

    @cached_property
    def someone_has_key(self):
        """Return 1 if master has key, 0.5 if some of players have key or 0 if noone has key."""
        if any(user.has_key for user in self.masters.all()):
            return 1
        elif any(user.has_key for user in self.players.all()):
            return 0.5
        else:
            return 0

    @cached_property
    def last_session_start_time(self):
        return getattr(self.last_session, 'cached_start_date', None)

    # end properties and info

    # start related objects processing

    @property
    def master(self):
        # using masters.all() as it most likely to be prefetched
        masters = self.masters.all()
        return masters and masters[0] or None

    @cached_property
    def next_session(self):
        """Nearest future session."""
        return self.sessions.filter(event__start_date__gte=timezone.now()).select_related('event').first()

    @cached_property
    def last_session(self):
        return self.sessions.all().order_by('-event__start_date').select_related('event').first()

    @property
    def new_players(self):
        """QS of players that joined game without master being notified."""
        return self.players.filter(player_games__master_notified__isnull=True).distinct()

    def can_be_joined_by(self, user):
        return (
            # using masters/players.all() as it most likely to be prefetched
            user.is_authenticated
            and
            (not self.is_finished)
            and
            (self.have_vacant_players())
            and
            (not (user in self.players.all()))
            and
            (not (user in self.masters.all()))
        )

    def can_be_leaved_by(self, user):
        return (
            # using players.all() as it most likely to be prefetched
            user.is_authenticated
            and user in self.players.all()
        )

    def can_be_confirmed(self, user):
        return (
            user.pk
            and
            self.game_players.filter(player__pk=user.pk, confirmed=False).exists()
        )

    def confirmed_player(self, user):
        return (
            user.pk
            and
            self.game_players.filter(player__pk=user.pk, confirmed=True).exists()
        )

    def can_be_edited_by(self, user):
        return (
            # using masters.all() as it most likely to be prefetched
            user.is_authenticated
            and user in self.masters.all()
        )

    @cached_property
    def reportables(self):
        return self.sessions.reportables()

    def can_be_reported_by(self, user):
        """Return ``True`` if game have unreported old sessions and user have rights to report them.

        Method is used for compatibility with GameSession.

        """
        return (
            user.is_authenticated

            # using masters.all() as it most likely to be prefetched
            and user in self.masters.all()

            and bool(self.reportables)
        )

    # end related objects processing

    # start actions

    def add_player(self, player):
        if self.can_be_joined_by(player):
            self.game_players.create(player=player)
            return True
        else:
            return False

    add_player.alters_data = True

    def remove_player(self, player):
        if self.can_be_leaved_by(player):
            for game_player in self.game_players.filter(player__pk=player.pk):
                game_player.delete()
            return True
        else:
            return False

    remove_player.alters_data = True

    def confirm_player(self, player):
        with transaction.atomic():
            if self.can_be_confirmed(player):
                self.game_players.filter(player__pk=player.pk).update(confirmed=True)
                return True
            else:
                return False

    confirm_player.alters_data = True

    def update_last_session_time_range(self):
        dates = (
            self.sessions.all()
            .order_by('-event__start_date')
            .values_list('event__start_date', 'event__end_date').first()
        )
        if dates and dates[0] and dates[1] and dates[0] != dates[1]:
            (
                Game.objects
                .filter(pk=self.pk)
                .update(last_session_time_range=NumericRange(*Game.datetimes_to_time_range(*dates), bounds='()'))
            )

    update_last_session_time_range.alters_data = True

    # def follow_comments_by_master(self, master=None):
    #     if master is None:
    #         masters = self.masters.all()
    #     else:
    #         masters = [master]
    #     for master in masters:
    #         XtdComment.objects.create(
    #             content_object=self,
    #             user=master,
    #             followup=True,
    #             site_id=settings.SITE_ID,
    #             submit_date=timezone.now(),
    #             comment=ugettext('Автоматический коментарий, для подписки мастера на следующие комментарии'),
    #         )

    # end actions

    # start other

    def get_render_game_or_session_description_cache_key(self, user_or_is_master):
        is_master = False
        if user_or_is_master in (True, False):
            is_master = user_or_is_master
        elif user_or_is_master.is_authenticated:
            is_master = (user_or_is_master in self.masters.all())  # using .all as it is most likely prefetched
        return get_cache_key(
            'games.models.Game.get_render_game_or_session_description_cache_key',
            self.pk,
            0,
            is_master,
        )

    def invalidate_render_game_or_session(self, invalidate_sessions=True):
        for is_master in (True, False):
            cache_key = self.get_render_game_or_session_description_cache_key(is_master)
            cache.delete(cache_key)

        if invalidate_sessions:
            for session in self.sessions.all():
                session.invalidate_render_game_or_session(invalidate_game=False)

    @classmethod
    def datetimes_to_time_range(cls, start_date, end_date):
        """Return tuple with integers in form (WHHMM, WHHMM).

        W -- ISO day of week (1 - Mon, 7- Sun).
        HH -- hour,
        MM -- minute.

        when dates range overlap sunday then monday is represented as 8, tuesday as 9, etc

        Examples:
            (21400, 30130) is range from tuesday 14:00 to wednesday 01:30
            (31400, 90130) is range from wednesday 14:00 to tuesday 01:30

        """
        start_date = timezone.make_naive(start_date, timezone.get_default_timezone())
        end_date = timezone.make_naive(end_date, timezone.get_default_timezone())
        numeric_start_time = start_date.isoweekday() * 10000 + start_date.hour*100 + start_date.minute
        numeric_end_time = end_date.isoweekday() * 10000 + end_date.hour*100 + end_date.minute

        if numeric_end_time < numeric_start_time:
            numeric_end_time += 70000

        time_range = (
            numeric_start_time,
            numeric_end_time,
        )

        return time_range

    @classmethod
    def get_parallel_games_without_parallel_sessions(cls, qs, start_date, end_date):
        """Return number of parallel game that do not yet have parallel session."""
        time_range = cls.datetimes_to_time_range(start_date, end_date)

        return (
            qs
            .filter(
                is_active=True,
                is_finished=False,
                last_session_time_range__overlap=NumericRange(*time_range, bounds='()'),
            )
            .exclude(
                sessions__event__start_date__lte=end_date,
                sessions__event__end_date__gte=start_date,
            )
            .distinct()
        )

    # end other


class GamePlayer(models.Model):
    game = models.ForeignKey(
        Game, related_name='game_players',
        on_delete=models.CASCADE,
    )
    player = models.ForeignKey(
        settings.AUTH_USER_MODEL, related_name='player_games',
        on_delete=models.CASCADE,
    )

    master_notified = models.DateTimeField(
        _('Время уведомления ведущего'), blank=True, null=True, default=None,
    )
    confirmed = models.BooleanField(
        _('Подтверждён ведущим'), blank=True, default=False,
    )

    class Meta:  # noqa: D101
        verbose_name = _('игрок игры')
        verbose_name_plural = _('игроки игр')

    def __str__(self):
        return f'{self.player} in {self.game}'

    def save(self, *args, **kwargs):
        super(GamePlayer, self).save(*args, **kwargs)
        self.game.current_players = self.game.players.count()
        if self.game.tracker.has_changed('current_players'):
            self.game.save()

    def delete(self, *args, **kwargs):
        super(GamePlayer, self).delete(*args, **kwargs)
        self.game.current_players = self.game.players.count()
        if self.game.tracker.has_changed('current_players'):
            self.game.save()


def default_session_datetime():
    start_datetime = timezone.now()
    start_datetime = start_datetime.replace(hour=11, minute=0, second=0)
    start_datetime += datetime.timedelta(days=1)
    return start_datetime


def sessions_image_path(instance, filename):
    return 'games/{game.pk}/{instance.pk}/{filename}'.format(instance=instance, filename=filename, game=instance.game)


class GameSessionQuerySet(models.QuerySet):
    use_for_related_fields = True

    def reportables(self):
        return self.filter(
            event__start_date__lte=timezone.now(), visits__isnull=True,
            game__masters__isnull=False,
        )


class GameSession(models.Model):
    event = models.OneToOneField(
        Event, null=True, blank=True, editable=False, default=None, related_name='game_session',
        on_delete=models.CASCADE,
    )

    game = models.ForeignKey(
        Game,
        related_name='sessions',
        blank=False,
        on_delete=models.CASCADE,
    )

    number = models.PositiveIntegerField(
        _('номер сессии'),
        blank=True,
        default=0,
        db_index=True,
        help_text=_('Будет использоваться для автоматического определения названий следующих сессий')
    )
    upcoming_notified = models.BooleanField(
        _('Уведомление о предстоящей игре выслано'), default=False,
    )

    report_notified = models.BooleanField(
        _('Уведомление о необходимости отчета выслано'), default=False,
    )

    image = ImageField(
        _('постер'),
        upload_to=games_image_path,
        height_field='image_height',
        width_field='image_width',
        default='',
        blank=True,
        help_text=_('если не задать, то будет использоваться постер самой игры')
    )
    image_height = models.PositiveIntegerField(editable=False, default=0, null=True)
    image_width = models.PositiveIntegerField(editable=False, default=0, null=True)

    cached_start_date = models.DateTimeField(editable=False, null=True)
    cached_end_date = models.DateTimeField(editable=False, null=True)

    # not fields:
    tracker = FieldTracker()
    objects = GameSessionQuerySet.as_manager()

    # start models API
    class Meta:  # noqa: D101
        verbose_name = _('игровая сессия')
        verbose_name_plural = _('игровые сессии')
        unique_together = (('game', 'number'), )
        ordering = ('-event__start_date', '-pk')

    def __str__(self):
        if self.event:
            return self.event.title
        else:
            return '{session.game.name} - {session.number}'.format(session=self)

    def save(self, *args, **kwargs):
        with transaction.atomic():
            check_conflicts = kwargs.pop('check_conflicts', True)
            created = not bool(self.pk)

            self.populate_number()

            if check_conflicts:
                # check if future session nubmers should be changed:
                future_sessions = (
                    self
                    .siblings
                    .filter(Q(event__start_date__gt=self.event.start_date) | Q(number=self.number))
                    .order_by('event__start_date')
                )
                if getattr(future_sessions.first(), 'number', self.number+1) <= self.number:
                    future_sessions = list(future_sessions)
                    diff = self.number - future_sessions[0].number + 1
                    for conflict in future_sessions:
                        conflict.number += diff
                    for conflict in reversed(future_sessions):
                        # save in reverse order to not fail unique constraint
                        conflict.save(check_conflicts=False)

            self.cached_start_date = self.event.start_date
            self.cached_end_date = self.event.end_date

            super(GameSession, self).save(*args, **kwargs)

            self.game.update_last_session_time_range()
            self.invalidate_render_game_or_session()
            if created:
                # invalidate self.siblings cache
                self.siblings
                del self.siblings

    def delete(self, *args, **kwargs):
        super(GameSession, self).delete(*args, **kwargs)
        if self.event:
            self.event.delete()
        self.invalidate_render_game_or_session()

    def clean(self):
        self.populate_number()
        if self.event and self.siblings.filter(number__gte=self.number, event__start_date__lte=self.event.start_date).exists():
            raise ValidationError('Уже существует более старая сессия с большим номером')

    # end models API

    # start urls
    def get_absolute_url(self):
        if self.event:
            return self.event.get_absolute_url()
        else:
            return self.game.get_absolute_url()

    def get_report_url(self):
        return reverse(
            'games:report_game_session',
            kwargs={'game_slug': self.game.slug, 'session_number': self.number}
        )

    def get_report_moderation_url(self):
        return reverse('admin:games_gamesession_change', args=(self.pk,))
    # end urls

    # start properties and info
    def get_complex_id(self):
        return '%s-%s' % (self.game.slug, self.number)
    # end properties and info

    # start related objects processing
    @cached_property
    def siblings(self):
        """Return QuerySet for other sessions of ``self.game``."""
        siblings = self.game.sessions.all()
        if self.pk:
            siblings = siblings.exclude(pk=self.pk)
        return siblings

    def can_be_reported_by(self, user):
        return (
            user.is_authenticated
            and
            self.cached_start_date <= timezone.now()
            and
            # using masters.all() as it most likely to be prefetched
            user in self.game.masters.all()
            and
            self in self.game.reportables
        )

    def report_can_be_edited_by(self, user):
        return (
            user.is_authenticated
            and
            self.cached_start_date <= timezone.now()
            and
            # using masters.all() as it most likely to be prefetched
            user in self.game.masters.all()
            and
            self.visits.filter(verified=False).exists()
        )

    def report_can_be_viewed_by(self, user):
        """Return True if report can be viewed by user."""
        return (
            user.is_authenticated
            and
            self.cached_start_date <= timezone.now()
            and
            # using masters.all() as it most likely to be prefetched
            user in self.game.masters.all()
            and
            self.visits.all().exists()
        )

    @cached_property
    def system_tags(self):
        return self.game.system_tags.all()

    @cached_property
    def setting_tags(self):
        return self.game.setting_tags.all()

    @cached_property
    def other_tags(self):
        return self.game.other_tags.all()

    @property
    def master(self):
        return self.game.master

    def needs_moderation(self):
        return self.visits.filter(verified=False).exists()
    needs_moderation.short_description = _('Требует модерации')

    def have_report(self):
        return self.visits.exists()
    have_report.short_description = _('Отчет отправлен')
    # end related objects processing

    # start actions
    def populate_event(self, event=None, *args, **kwargs):
        event = event or self.event or Event(*args, **kwargs)
        if not event.title:
            event.title = '{session.game.name} №{session.number}'.format(session=self)
        if self.game:
            event.created_by = self.game.master
        if not event.start_date:
            event.start_date = timezone.now()
        if not event.end_date:
            event.end_date = timezone.now()
        return event

    populate_event.alters_data = True

    def populate_number(self):
        """Populate ``self.number`` if it's not set. By default it's set to ``session_before_self.number + 1``."""
        if not self.number and self.event:
            latest_session = (
                self.siblings.filter(event__start_date__lt=self.event.start_date).order_by('-number').first()
            )
            if not latest_session:
                self.number = 1
            else:
                self.number = latest_session.number + 1

    populate_number.alters_data = True

    def notify(self):
        """Notify everyone about session."""
        recipients = [
            user.email
            for user in self.game.players.filter(is_active=True, receive_notifications=True, email__contains='@')
        ]
        recipients.extend([
            user.email
            for user in self.game.masters.filter(is_active=True, receive_notifications=True, email__contains='@')
        ])
        if recipients:
            mail_send(
                recipients,
                template='upcoming-session-reminder',
                context={'game_session': self},
                tags='notification',
            )
        self.upcoming_notified = True
        self.save()
    notify.alters_data = True

    @classmethod
    def create_with_event(cls, *args, **kwargs):
        """Create GameSession and associated calendar event.

        Example::

            GameSession.create_with_event(game=game, start_date=tomorrow)

        """
        commit = kwargs.pop('commit', True)
        event_kwargs = {}
        all_field_names = [f.name for f in cls._meta.get_fields()]
        for key in list(kwargs.keys()):
            if key not in all_field_names:
                event_kwargs[key] = kwargs.pop(key)
        instance = cls(*args, **kwargs)
        event = instance.populate_event()
        for key in event_kwargs:
            setattr(event, key, event_kwargs[key])
        event.save()
        instance.event = event
        instance.populate_number()
        if not kwargs.get('title', ''):
            instance.event.title = ''
            instance.populate_event()
        if commit:
            instance.event.save()
            instance.clean()
            instance.save()
        return instance

    # end actions

    # start other

    def get_render_game_or_session_description_cache_key(self, user_or_is_master):
        is_master = False
        if user_or_is_master in (True, False):
            is_master = user_or_is_master
        elif user_or_is_master.is_authenticated:
            is_master = (user_or_is_master in self.game.masters.all())  # using .all as it is most likely prefetched
        return get_cache_key(
            'games.models.GameSession.get_render_game_or_session_description_cache_key',
            self.game.pk,
            self.pk,
            is_master,
        )

    def invalidate_render_game_or_session(self, invalidate_game=True):
        for is_master in (True, False):
            cache_key = self.get_render_game_or_session_description_cache_key(is_master)
            cache.delete(cache_key)
        if invalidate_game:
            self.game.invalidate_render_game_or_session(invalidate_sessions=False)

    # end other


class SessionVisitQS(models.QuerySet):

    def annotate_visits_per_date(self):
        return (
            self
            .annotate(visit_date=Func(F('game_session__event__start_date'), function='date'))
            .order_by('visit_date')
            .values('visit_date')
            .annotate(
                total_visits=Count('id'),
            )
        )


class SessionVisit(models.Model):
    ROLES = Choices(
        (0, 'unknown', _('неизвестно')),
        (1, 'player', _('игрок')),
        (2, 'master', _('ведущий')),
        (3, 'expenses', _('траты'))
    )

    verified = models.BooleanField(
        _('Ок?'),
        default=False, db_index=True,
        help_text=_('Проверено модератором'),
    )

    game_session = models.ForeignKey(
        GameSession, related_name='visits',
        null=True,
        on_delete=models.SET_NULL,
    )
    visitor = models.ForeignKey(
        settings.AUTH_USER_MODEL, blank=False, related_name='session_visits',
        verbose_name=_('Посетитель'),
        on_delete=models.CASCADE,
    )

    visitor_level = models.PositiveSmallIntegerField(
        _('Lvl'),
        default=0,
        help_text=_('Уровень до подтверждения отчёта')
    )

    have_visited = models.BooleanField(
        _('Был'), default=True, db_index=True,
        help_text=_("Был ли игрок/ведущий на игре?"),
    )

    role = models.PositiveSmallIntegerField(_('роль'), choices=ROLES, default=ROLES.unknown, db_index=True)

    visit_xp = models.ForeignKey(
        'accounting.ExperienceReward', null=True, blank=True, related_name='visit_default',
        on_delete=models.SET_NULL,
        help_text=_('Опыт за посещение. Опыт мастера пересчитывается раз в сутки и зависит от количества промодерированных игр на этой неделе')
    )

    bonus = models.BooleanField(
        _('+XP'), default=False,
        help_text=_('Бонусный опыт от мастера. Одному игроку')
    )
    bonus_xp = models.ForeignKey(
        'accounting.ExperienceReward', null=True, blank=True, related_name='visit_bonus',
        on_delete=models.SET_NULL,
    )

    cleaned = models.BooleanField(
        _('Стол 🚮'), default=False, help_text=_('Убрал стол после игры. Один игрок')
    )
    cleaned_floor = models.BooleanField(
        _('Пол 🚮'), default=False, help_text=_('Убрал пол после игры. Один игрок')
    )
    cleaned_xp = models.ForeignKey(
        'accounting.ExperienceReward', null=True, blank=True, related_name='visit_cleaned',
        on_delete=models.SET_NULL,
    )

    used_experience = models.BooleanField(
        _('$XP'), default=False,
        help_text=_('Игра за опыт')
    )
    payment_xp = models.ForeignKey(
        'accounting.ExperienceReward', null=True, blank=True, related_name='visit_xp_payed',
        on_delete=models.SET_NULL,
    )

    used_ticket = models.BooleanField(
        _('Абон.'), default=False,
        help_text=_('Абонемент'),
    )
    ticket = models.OneToOneField(
        'accounting.Ticket', null=True, blank=True, related_name='visit_used',
        on_delete=models.SET_NULL,
    )

    is_free = models.BooleanField(
        _('Бесплатно'), default=False,
        help_text=_('Первые игры могут быть бесплатны. Другие тоже могут быть, но модератор проверит это'),
    )

    cost_balance_change = models.ForeignKey(
        'accounting.BalanceChange',
        related_name='cost_game_session_visits', null=True, blank=True, default=None,
        verbose_name=_('-цена'),
        on_delete=models.SET_NULL,
    )
    paid_balance_change = models.ForeignKey(
        'accounting.BalanceChange',
        related_name='paid_game_session_visits', null=True, blank=True, default=None,
        verbose_name=_('оплата'),
        on_delete=models.SET_NULL,
    )
    to_master_balance_change = models.ForeignKey(
        'accounting.BalanceChange', related_name='to_master_game_session_visits', null=True, blank=True, default=None,
        on_delete=models.SET_NULL,
    )

    comment = models.CharField(
        _('Комментарий'), blank=True, default='', max_length=64,
        help_text=_('Если игрок отдал не всю сумму, то укажите причину')
    )

    cached_start_date = models.DateTimeField(editable=False, null=True)
    cached_end_date = models.DateTimeField(editable=False, null=True)

    # not fields
    tracker = FieldTracker()
    objects = SessionVisitQS.as_manager()

    # start models API
    class Meta:  # noqa: D101
        verbose_name = _('посещение игры')
        verbose_name_plural = _('посещения игр')
        unique_together = (('game_session', 'visitor'),)
        index_together = (
            ('have_visited', 'verified'),
            ('have_visited', 'verified', 'role'),
            ('have_visited', 'verified', 'role', 'game_session'),
        )

    def __str__(self):
        if self.role == self.ROLES.player:
            return 'Посещение "{visit.game_session}" игроком {visit.visitor}'.format(visit=self)
        elif self.role == self.ROLES.master:
            return 'Посещение "{visit.game_session}" ведущим {visit.visitor}'.format(visit=self)
        else:
            return 'Посещение "{visit.game_session}" пользователем {visit.visitor} по непонятно й причине'.format(visit=self)

    def save(self, *args, **kwargs):
        self.update_role()
        self.populate_xp_rewards()
        with transaction.atomic():
            self.cached_start_date = self.game_session.event.start_date
            self.cached_end_date = self.game_session.event.end_date
            super(SessionVisit, self).save(*args, **kwargs)

            if self.verified and self.have_visited:
                self.verify_related()
                self.remember_players()
            else:
                self.unverify_related()
            self.game_session.invalidate_render_game_or_session()

    def delete(self, *args, **kwargs):
        with transaction.atomic():
            fk_fields = (
                'visit_xp', 'bonus_xp', 'cleaned_xp', 'payment_xp', 'ticket',
            )

            for field in fk_fields:
                obj = None
                try:
                    obj = getattr(self, field, None)
                except ObjectDoesNotExist:
                    pass
                if obj:
                    obj.delete()
            super(SessionVisit, self).delete(*args, **kwargs)
            self.game_session and self.game_session.invalidate_render_game_or_session()

    def clean(self):  # noqa: D101
        self.update_role()
        if self.role == self.ROLES.unknown:
            raise ValidationError(
                _('Посетитель %s не является ни игроком ни ведущим игры') % (self.visitor, )
            )

        if not self.pk and (not self.game_session.event or self.game_session.event.start_date > timezone.now()):
            raise ValidationError(
                'Невозможно создавать посещения для не стартовавших сессий и для сессий без Event'
            )

        if self.role == self.ROLES.master:
            player_field_is_set = any([
                self.bonus, self.bonus_xp, self.used_experience, self.payment_xp, self.used_ticket, self.ticket,
            ])
            if player_field_is_set:
                raise ValidationError(
                    _('Из полей опыта для мастера можно заполнять только поле уборки. Также нельзя выбирать абонемент')
                )

        if self.used_experience and self.is_free:
            raise ValidationError(
                _('Игра посетителя %s не может быть и бесплатной и "за опыт"') % self.visitor
            )

        if self.used_ticket and not self.visitor.has_tickets and not self.ticket:
            raise ValidationError(
                _('У посетителя %s нет неиспользованных абонементов') % self.visitor
            )
        elif self.used_ticket and self.used_experience:
            raise ValidationError(
                _('Посетитель %s не может и платить опытом и абонементом одновременно') % self.visitor
            )
        elif self.used_ticket and self.is_free:
            raise ValidationError(
                _('Посетитель %s не может одновременно играть бесплатно и за абонемент') % self.visitor
            )

        if self.used_experience:
            self.visitor.recalc_experience()
            if self.visitor.cached_total_player_experience < config_value('accounting', 'DEFAULT_GAME_XP_COST'):
                raise ValidationError(
                    _('У посетителя %s недостаточно опыта для оплаты игры (опытом)') % self.visitor
                )

        return True

    def get_absolute_url(self):
        if self.game_session and self.game_session.game:
            return reverse(
                'games:report_game_session',
                args=[self.game_session.game.slug, self.game_session.number],
            )
        else:
            return None

    # end models api

    # start actions

    def update_role(self, force_check=False):
        if (self.role == self.ROLES.unknown or force_check) and self.visitor:
            if self.visitor.is_service:
                self.role = self.ROLES.expenses
            if self.game_session.game.masters.filter(pk=self.visitor.pk).exists():
                self.role = self.ROLES.master
            elif self.game_session.game.players.filter(pk=self.visitor.pk).exists():
                self.role = self.ROLES.player

    update_role.alters_data = True

    def populate_xp_rewards(self):
        bool_xp_fields = (
            # boolean_field, xp_field, xp_setting, multiplier, stackable
            ('have_visited', 'visit_xp', 'DEFAULT_VISIT_XP', 1, False),
            ('bonus', 'bonus_xp', 'DEFAULT_BONUS_XP', 1, False),
            ('cleaned', 'cleaned_xp', 'DEFAULT_CLEANED_XP', 1, True),
            ('cleaned_floor', 'cleaned_xp', 'DEFAULT_CLEANED_XP', 1, True),
            ('used_experience', 'payment_xp', 'DEFAULT_GAME_XP_COST', -1, False),
        )
        firsts = {}
        for boolean_field, xp_field, xp_setting, multiplier, stackable in bool_xp_fields:
            bool_value = getattr(self, boolean_field)
            xp_value = getattr(self, xp_field)

            first = firsts.get(xp_field, True)
            firsts[xp_field] = False

            if self.have_visited and bool_value:
                amount = config_value('accounting', xp_setting) * multiplier
                if not xp_value:
                    xp_value = ExperienceReward(
                        user=self.visitor,
                        amount=amount,
                        comment=boolean_field,
                    )
                    if boolean_field == 'have_visited' and self.role == self.ROLES.master:
                        xp_value.master_type = True
                    xp_value.save()
                    setattr(self, xp_field, xp_value)
                elif stackable:
                    if first:
                        xp_value.amount = amount
                    else:
                        xp_value.amount += amount
                    xp_value.save()

            if xp_value and first and (not bool_value or not self.have_visited):
                # cleanup xp objects for unset boolean fields
                setattr(self, xp_field, None)
                xp_value.delete()

        if self.have_visited and self.used_ticket and not self.ticket:
            self.ticket = self.visitor.unused_tickets.first()
        elif self.ticket and (not self.used_ticket or not self.have_visited):
            self.ticket = None

    populate_xp_rewards.alters_data = True

    def verify_related(self):

        for field in ('visit_xp', 'bonus_xp', 'cleaned_xp', 'payment_xp'):
            xp_object = getattr(self, field, None)
            if xp_object and not xp_object.verified:
                xp_object.verified = True
                xp_object.save()

        (
            BalanceChange
            .objects
            .filter(
                Q(cost_game_session_visits__pk=self.pk) |
                Q(paid_game_session_visits__pk=self.pk) |
                Q(to_master_game_session_visits__pk=self.pk)
            )
            .update(verified=True)
        )

    verify_related.alters_data = True

    def remember_players(self):
        may_remember = (
            self.role == self.ROLES.player
            and
            not (
                self.game_session.game
                .participated_players
                .filter(pk=self.visitor.pk)
                .exists()
            )
        )
        if may_remember:
            self.game_session.game.participated_players.add(
                self.visitor
            )

    def unverify_related(self):
        for field in ('visit_xp', 'bonus_xp', 'cleaned_xp', 'payment_xp'):
            xp_object = getattr(self, field, None)
            if xp_object and xp_object.verified:
                xp_object.verified = False
                xp_object.save()

        (
            BalanceChange
            .objects
            .filter(
                Q(cost_game_session_visits__pk=self.pk) |
                Q(paid_game_session_visits__pk=self.pk) |
                Q(to_master_game_session_visits__pk=self.pk)
            )
            .update(verified=False)
        )

    unverify_related.alters_data = True

    # end actions
